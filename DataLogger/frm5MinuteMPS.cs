﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLogger.Entities;
using System.Resources;
using System.Reflection;
using System.Globalization;
using DataLogger.Utils;
using DataLogger.Data;

namespace DataLogger
{
    public partial class frm5MinuteMPS : Form
    {
        //ResourceManager res_man;    // declare Resource manager to access to specific cultureinfo
        //CultureInfo cul;            //declare culture info
        LanguageService lang;
        public data_value obj_data_value { get; set; }
        public frm5MinuteMPS()
        {
            InitializeComponent();
        }
        public frm5MinuteMPS(data_value obj, LanguageService _lang)
        {
            InitializeComponent();
            obj_data_value = obj;
            lang = _lang;
            switch_language();
        }
        private void switch_language()
        {
            this.lblHeaderTitle.Text = lang.getText("form_5_minute_title");
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            GlobalVar.stationSettings = new station_repository().get_info();
            GlobalVar.moduleSettings = new module_repository().get_all();
            //for (int i = 1; i <= GlobalVar.moduleSettings.Count(); i++)
            //{
            //    foreach (var item in GlobalVar.moduleSettings)
            //    {
            //        string currentvar = "var" + i.ToString();
            //        string currentlabelname = "txt" + currentvar;
            //        string currentlabelunit = "txt" + currentvar + "Unit";
            //        string currentlabelvalue = "txt" + currentvar + "Value";
            //        if (item.item_name.Equals(currentvar))
            //        {

            //            frmNewMain.ClearLabel(this, item.display_name, currentlabelname);
            //            frmNewMain.ClearLabel(this, item.unit, currentlabelunit);
            //            try
            //            {
            //                //frmNewMain.ClearTextbox(this, "---", currentlabelvalue);
            //            }
            //            catch (Exception ex)
            //            {
            //                Console.WriteLine(ex.StackTrace);
            //            }
            //        }
            //    }
            //}

            foreach (var item in GlobalVar.moduleSettings)
            {
                string currentvar = item.item_name;
                string currentlabelvalue = "txt" + currentvar + "Value";
                string currentlabelname = "txt" + currentvar;
                string currentlabelunit = "txt" + currentvar + "Unit";
                string param = currentvar;  //VD : var1
                string param_status = currentvar + "_status";
                Type paramType = typeof(data_value);
                PropertyInfo prop = paramType.GetProperty(param);
                PropertyInfo prop_status = paramType.GetProperty(param_status);
                var status = prop_status.GetValue(obj_data_value);
                string value = null;

                if ((int)status != 0)
                {
                    value = "---";
                }
                else
                {
                    value = ((double)prop.GetValue(obj_data_value)).ToString("##0.00"); //VD : lay display_var = data.var1
                }
                if (item.item_name.Equals(currentvar))
                {
                    frmNewMain.ClearLabel(this, item.display_name, currentlabelname);
                    frmNewMain.ClearLabel(this, item.unit, currentlabelunit);
                    SetValueTextbox(this, value, currentlabelvalue);
                }
            }
        }
        public static void SetValueTextbox(System.Windows.Forms.Control control, string text, string label)
        {
            if (control is TextBox)
            {
                TextBox tb = (TextBox)control;
                if (tb.Name.StartsWith(label))
                {
                    if (tb.InvokeRequired)
                    {
                        tb.Invoke(new MethodInvoker(delegate { tb.Text = text; }));
                    }
                    else
                    {
                        tb.Text = text;
                    }
                }

            }
            else
            {
                foreach (System.Windows.Forms.Control child in control.Controls)
                {
                    SetValueTextbox(child, text, label);
                }
            }

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void txtvar_Paint(object sender, PaintEventArgs e)
        {
            while (txtvar1.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar1.Text,
     new Font(txtvar1.Font.FontFamily, txtvar1.Font.Size, txtvar1.Font.Style)).Width)
            {
                txtvar1.Font = new Font(txtvar1.Font.FontFamily, txtvar1.Font.Size - 0.5f, txtvar1.Font.Style);
            }

            while (txtvar1Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar1Unit.Text,
new Font(txtvar1Unit.Font.FontFamily, txtvar1Unit.Font.Size, txtvar1Unit.Font.Style)).Width)
            {
                txtvar1Unit.Font = new Font(txtvar1Unit.Font.FontFamily, txtvar1Unit.Font.Size - 0.5f, txtvar1Unit.Font.Style);
            }
            while (txtvar2.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar2.Text,
     new Font(txtvar2.Font.FontFamily, txtvar2.Font.Size, txtvar2.Font.Style)).Width)
            {
                txtvar2.Font = new Font(txtvar2.Font.FontFamily, txtvar2.Font.Size - 0.5f, txtvar2.Font.Style);
            }
            while (txtvar2Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar2Unit.Text,
new Font(txtvar2Unit.Font.FontFamily, txtvar2Unit.Font.Size, txtvar2Unit.Font.Style)).Width)
            {
                txtvar2Unit.Font = new Font(txtvar2Unit.Font.FontFamily, txtvar2Unit.Font.Size - 0.5f, txtvar2Unit.Font.Style);
            }
            while (txtvar3.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar3.Text,
new Font(txtvar3.Font.FontFamily, txtvar3.Font.Size, txtvar3.Font.Style)).Width)
            {
                txtvar3.Font = new Font(txtvar3.Font.FontFamily, txtvar3.Font.Size - 0.5f, txtvar3.Font.Style);
            }
            while (txtvar3Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar3Unit.Text,
new Font(txtvar3Unit.Font.FontFamily, txtvar3Unit.Font.Size, txtvar3Unit.Font.Style)).Width)
            {
                txtvar3Unit.Font = new Font(txtvar3Unit.Font.FontFamily, txtvar3Unit.Font.Size - 0.5f, txtvar3Unit.Font.Style);
            }
            while (txtvar4.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar4.Text,
new Font(txtvar4.Font.FontFamily, txtvar4.Font.Size, txtvar4.Font.Style)).Width)
            {
                txtvar4.Font = new Font(txtvar4.Font.FontFamily, txtvar4.Font.Size - 0.5f, txtvar4.Font.Style);
            }
            while (txtvar4Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar4Unit.Text,
new Font(txtvar4Unit.Font.FontFamily, txtvar4Unit.Font.Size, txtvar4Unit.Font.Style)).Width)
            {
                txtvar4Unit.Font = new Font(txtvar4Unit.Font.FontFamily, txtvar4Unit.Font.Size - 0.5f, txtvar4Unit.Font.Style);
            }
            while (txtvar5.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar5.Text,
new Font(txtvar5.Font.FontFamily, txtvar5.Font.Size, txtvar5.Font.Style)).Width)
            {
                txtvar5.Font = new Font(txtvar5.Font.FontFamily, txtvar5.Font.Size - 0.5f, txtvar5.Font.Style);
            }
            while (txtvar5Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar5Unit.Text,
new Font(txtvar5Unit.Font.FontFamily, txtvar5Unit.Font.Size, txtvar5Unit.Font.Style)).Width)
            {
                txtvar5Unit.Font = new Font(txtvar5Unit.Font.FontFamily, txtvar5Unit.Font.Size - 0.5f, txtvar5Unit.Font.Style);
            }
            while (txtvar6.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar6.Text,
new Font(txtvar6.Font.FontFamily, txtvar6.Font.Size, txtvar6.Font.Style)).Width)
            {
                txtvar6.Font = new Font(txtvar6.Font.FontFamily, txtvar6.Font.Size - 0.5f, txtvar6.Font.Style);
            }
            while (txtvar6Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar6Unit.Text,
new Font(txtvar6Unit.Font.FontFamily, txtvar6Unit.Font.Size, txtvar6Unit.Font.Style)).Width)
            {
                txtvar6Unit.Font = new Font(txtvar6Unit.Font.FontFamily, txtvar6Unit.Font.Size - 0.5f, txtvar6Unit.Font.Style);
            }
            while (txtvar7.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar7.Text,
new Font(txtvar7.Font.FontFamily, txtvar7.Font.Size, txtvar7.Font.Style)).Width)
            {
                txtvar7.Font = new Font(txtvar7.Font.FontFamily, txtvar7.Font.Size - 0.5f, txtvar7.Font.Style);
            }
            while (txtvar7Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar7Unit.Text,
new Font(txtvar7Unit.Font.FontFamily, txtvar7Unit.Font.Size, txtvar7Unit.Font.Style)).Width)
            {
                txtvar7Unit.Font = new Font(txtvar7Unit.Font.FontFamily, txtvar7Unit.Font.Size - 0.5f, txtvar7Unit.Font.Style);
            }
            while (txtvar8.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar8.Text,
new Font(txtvar8.Font.FontFamily, txtvar8.Font.Size, txtvar8.Font.Style)).Width)
            {
                txtvar8.Font = new Font(txtvar8.Font.FontFamily, txtvar8.Font.Size - 0.5f, txtvar8.Font.Style);
            }
            while (txtvar8Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar8Unit.Text,
new Font(txtvar8Unit.Font.FontFamily, txtvar8Unit.Font.Size, txtvar8Unit.Font.Style)).Width)
            {
                txtvar8Unit.Font = new Font(txtvar8Unit.Font.FontFamily, txtvar8Unit.Font.Size - 0.5f, txtvar8Unit.Font.Style);
            }
            while (txtvar9.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar9.Text,
new Font(txtvar9.Font.FontFamily, txtvar9.Font.Size, txtvar9.Font.Style)).Width)
            {
                txtvar9.Font = new Font(txtvar9.Font.FontFamily, txtvar9.Font.Size - 0.5f, txtvar9.Font.Style);
            }
            while (txtvar9Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar9Unit.Text,
new Font(txtvar9Unit.Font.FontFamily, txtvar9Unit.Font.Size, txtvar9Unit.Font.Style)).Width)
            {
                txtvar9Unit.Font = new Font(txtvar9Unit.Font.FontFamily, txtvar9Unit.Font.Size - 0.5f, txtvar9Unit.Font.Style);
            }
            while (txtvar10.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar10.Text,
new Font(txtvar10.Font.FontFamily, txtvar10.Font.Size, txtvar10.Font.Style)).Width)
            {
                txtvar10.Font = new Font(txtvar10.Font.FontFamily, txtvar10.Font.Size - 0.5f, txtvar10.Font.Style);
            }
            while (txtvar10Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar10Unit.Text,
new Font(txtvar10Unit.Font.FontFamily, txtvar10Unit.Font.Size, txtvar10Unit.Font.Style)).Width)
            {
                txtvar10Unit.Font = new Font(txtvar10Unit.Font.FontFamily, txtvar10Unit.Font.Size - 0.5f, txtvar10Unit.Font.Style);
            }
            while (txtvar11.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar11.Text,
new Font(txtvar11.Font.FontFamily, txtvar11.Font.Size, txtvar11.Font.Style)).Width)
            {
                txtvar11.Font = new Font(txtvar11.Font.FontFamily, txtvar11.Font.Size - 0.5f, txtvar11.Font.Style);
            }
            while (txtvar11Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar11Unit.Text,
new Font(txtvar11Unit.Font.FontFamily, txtvar11Unit.Font.Size, txtvar11Unit.Font.Style)).Width)
            {
                txtvar11Unit.Font = new Font(txtvar11Unit.Font.FontFamily, txtvar11Unit.Font.Size - 0.5f, txtvar11Unit.Font.Style);
            }
            while (txtvar12.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar12.Text,
new Font(txtvar12.Font.FontFamily, txtvar12.Font.Size, txtvar12.Font.Style)).Width)
            {
                txtvar12.Font = new Font(txtvar12.Font.FontFamily, txtvar12.Font.Size - 0.5f, txtvar12.Font.Style);
            }
            while (txtvar12Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar12Unit.Text,
new Font(txtvar12Unit.Font.FontFamily, txtvar12Unit.Font.Size, txtvar12Unit.Font.Style)).Width)
            {
                txtvar12Unit.Font = new Font(txtvar12Unit.Font.FontFamily, txtvar12Unit.Font.Size - 0.5f, txtvar12Unit.Font.Style);
            }
            while (txtvar13.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar13.Text,
new Font(txtvar13.Font.FontFamily, txtvar13.Font.Size, txtvar13.Font.Style)).Width)
            {
                txtvar13.Font = new Font(txtvar13.Font.FontFamily, txtvar13.Font.Size - 0.5f, txtvar13.Font.Style);
            }
            while (txtvar13Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar13Unit.Text,
new Font(txtvar13Unit.Font.FontFamily, txtvar13Unit.Font.Size, txtvar13Unit.Font.Style)).Width)
            {
                txtvar13Unit.Font = new Font(txtvar13Unit.Font.FontFamily, txtvar13Unit.Font.Size - 0.5f, txtvar13Unit.Font.Style);
            }
            while (txtvar14.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar14.Text,
new Font(txtvar14.Font.FontFamily, txtvar14.Font.Size, txtvar14.Font.Style)).Width)
            {
                txtvar14.Font = new Font(txtvar14.Font.FontFamily, txtvar14.Font.Size - 0.5f, txtvar14.Font.Style);
            }
            while (txtvar14Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar14Unit.Text,
new Font(txtvar14Unit.Font.FontFamily, txtvar14Unit.Font.Size, txtvar14Unit.Font.Style)).Width)
            {
                txtvar14Unit.Font = new Font(txtvar14Unit.Font.FontFamily, txtvar14Unit.Font.Size - 0.5f, txtvar14Unit.Font.Style);
            }
            while (txtvar15.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar15.Text,
new Font(txtvar15.Font.FontFamily, txtvar15.Font.Size, txtvar15.Font.Style)).Width)
            {
                txtvar15.Font = new Font(txtvar15.Font.FontFamily, txtvar15.Font.Size - 0.5f, txtvar15.Font.Style);
            }
            while (txtvar15Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar15Unit.Text,
new Font(txtvar15Unit.Font.FontFamily, txtvar15Unit.Font.Size, txtvar15Unit.Font.Style)).Width)
            {
                txtvar15Unit.Font = new Font(txtvar15Unit.Font.FontFamily, txtvar15Unit.Font.Size - 0.5f, txtvar15Unit.Font.Style);
            }
            while (txtvar16.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar16.Text,
new Font(txtvar16.Font.FontFamily, txtvar16.Font.Size, txtvar16.Font.Style)).Width)
            {
                txtvar16.Font = new Font(txtvar16.Font.FontFamily, txtvar16.Font.Size - 0.5f, txtvar16.Font.Style);
            }
            while (txtvar16Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar16Unit.Text,
new Font(txtvar16Unit.Font.FontFamily, txtvar16Unit.Font.Size, txtvar16Unit.Font.Style)).Width)
            {
                txtvar16Unit.Font = new Font(txtvar16Unit.Font.FontFamily, txtvar16Unit.Font.Size - 0.5f, txtvar16Unit.Font.Style);
            }
            while (txtvar17.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar17.Text,
new Font(txtvar17.Font.FontFamily, txtvar17.Font.Size, txtvar17.Font.Style)).Width)
            {
                txtvar17.Font = new Font(txtvar17.Font.FontFamily, txtvar17.Font.Size - 0.5f, txtvar17.Font.Style);
            }
            while (txtvar17Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar17Unit.Text,
new Font(txtvar17Unit.Font.FontFamily, txtvar17Unit.Font.Size, txtvar17Unit.Font.Style)).Width)
            {
                txtvar17Unit.Font = new Font(txtvar17Unit.Font.FontFamily, txtvar17Unit.Font.Size - 0.5f, txtvar17Unit.Font.Style);
            }
            while (txtvar18.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar18.Text,
new Font(txtvar18.Font.FontFamily, txtvar18.Font.Size, txtvar18.Font.Style)).Width)
            {
                txtvar18.Font = new Font(txtvar18.Font.FontFamily, txtvar18.Font.Size - 0.5f, txtvar18.Font.Style);
            }
            while (txtvar18Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar18Unit.Text,
new Font(txtvar18Unit.Font.FontFamily, txtvar18Unit.Font.Size, txtvar18Unit.Font.Style)).Width)
            {
                txtvar18Unit.Font = new Font(txtvar18Unit.Font.FontFamily, txtvar18Unit.Font.Size - 0.5f, txtvar18Unit.Font.Style);
            }
            while (txtvar19.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar19.Text,
new Font(txtvar19.Font.FontFamily, txtvar19.Font.Size, txtvar19.Font.Style)).Width)
            {
                txtvar19.Font = new Font(txtvar19.Font.FontFamily, txtvar19.Font.Size - 0.5f, txtvar19.Font.Style);
            }
            while (txtvar19Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar19Unit.Text,
new Font(txtvar19Unit.Font.FontFamily, txtvar19Unit.Font.Size, txtvar19Unit.Font.Style)).Width)
            {
                txtvar19Unit.Font = new Font(txtvar19Unit.Font.FontFamily, txtvar19Unit.Font.Size - 0.5f, txtvar19Unit.Font.Style);
            }
            while (txtvar20.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar20.Text,
new Font(txtvar20.Font.FontFamily, txtvar20.Font.Size, txtvar20.Font.Style)).Width)
            {
                txtvar20.Font = new Font(txtvar20.Font.FontFamily, txtvar20.Font.Size - 0.5f, txtvar20.Font.Style);
            }
            while (txtvar20Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar20Unit.Text,
new Font(txtvar20Unit.Font.FontFamily, txtvar20Unit.Font.Size, txtvar20Unit.Font.Style)).Width)
            {
                txtvar20Unit.Font = new Font(txtvar20Unit.Font.FontFamily, txtvar20Unit.Font.Size - 0.5f, txtvar20Unit.Font.Style);
            }
            while (txtvar21.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar21.Text,
new Font(txtvar21.Font.FontFamily, txtvar21.Font.Size, txtvar21.Font.Style)).Width)
            {
                txtvar21.Font = new Font(txtvar21.Font.FontFamily, txtvar21.Font.Size - 0.5f, txtvar21.Font.Style);
            }
            while (txtvar21Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar21Unit.Text,
new Font(txtvar21Unit.Font.FontFamily, txtvar21Unit.Font.Size, txtvar21Unit.Font.Style)).Width)
            {
                txtvar21Unit.Font = new Font(txtvar21Unit.Font.FontFamily, txtvar21Unit.Font.Size - 0.5f, txtvar21Unit.Font.Style);
            }
            while (txtvar22.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar22.Text,
new Font(txtvar22.Font.FontFamily, txtvar22.Font.Size, txtvar22.Font.Style)).Width)
            {
                txtvar22.Font = new Font(txtvar22.Font.FontFamily, txtvar22.Font.Size - 0.5f, txtvar22.Font.Style);
            }
            while (txtvar22Unit.Width < System.Windows.Forms.TextRenderer.MeasureText(txtvar22Unit.Text,
new Font(txtvar22Unit.Font.FontFamily, txtvar22Unit.Font.Size, txtvar22Unit.Font.Style)).Width)
            {
                txtvar22Unit.Font = new Font(txtvar22Unit.Font.FontFamily, txtvar22Unit.Font.Size - 0.5f, txtvar22Unit.Font.Style);
            }
        }

    }
}
