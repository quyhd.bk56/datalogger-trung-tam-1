﻿namespace DataLogger
{
    partial class frmNewMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewMain));
            this.bgwMonthlyReport = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.panel20 = new System.Windows.Forms.Panel();
            this.lblMainMenuTitle = new System.Windows.Forms.Label();
            this.pnSoftwareInfo = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSurfaceWaterQuality = new System.Windows.Forms.Label();
            this.lblAutomaticMonitoring = new System.Windows.Forms.Label();
            this.lblThaiNguyenStation = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblWaterLevel = new System.Windows.Forms.Label();
            this.lblHeaderNationName = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.RichTextBox();
            this.picSamplerTank = new System.Windows.Forms.PictureBox();
            this.btnLanguage = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel45 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtvar16 = new System.Windows.Forms.Label();
            this.txtvar16Value = new System.Windows.Forms.TextBox();
            this.txtvar16Unit = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.txtvar17 = new System.Windows.Forms.Label();
            this.txtvar17Value = new System.Windows.Forms.TextBox();
            this.txtvar17Unit = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.txtvar18 = new System.Windows.Forms.Label();
            this.txtvar18Value = new System.Windows.Forms.TextBox();
            this.txtvar18Unit = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txtvar13 = new System.Windows.Forms.Label();
            this.txtvar13Value = new System.Windows.Forms.TextBox();
            this.txtvar13Unit = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.txtvar14 = new System.Windows.Forms.Label();
            this.txtvar14Value = new System.Windows.Forms.TextBox();
            this.txtvar14Unit = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.txtvar15 = new System.Windows.Forms.Label();
            this.txtvar15Value = new System.Windows.Forms.TextBox();
            this.txtvar15Unit = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtvar10 = new System.Windows.Forms.Label();
            this.txtvar10Value = new System.Windows.Forms.TextBox();
            this.txtvar10Unit = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtvar11 = new System.Windows.Forms.Label();
            this.txtvar11Value = new System.Windows.Forms.TextBox();
            this.txtvar11Unit = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtvar12 = new System.Windows.Forms.Label();
            this.txtvar12Value = new System.Windows.Forms.TextBox();
            this.txtvar12Unit = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtvar1 = new System.Windows.Forms.Label();
            this.txtvar1Value = new System.Windows.Forms.TextBox();
            this.txtvar1Unit = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtvar2 = new System.Windows.Forms.Label();
            this.txtvar2Value = new System.Windows.Forms.TextBox();
            this.txtvar2Unit = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtvar3 = new System.Windows.Forms.Label();
            this.txtvar3Value = new System.Windows.Forms.TextBox();
            this.txtvar3Unit = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtvar4 = new System.Windows.Forms.Label();
            this.txtvar4Value = new System.Windows.Forms.TextBox();
            this.txtvar4Unit = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtvar6 = new System.Windows.Forms.Label();
            this.txtvar6Value = new System.Windows.Forms.TextBox();
            this.txtvar6Unit = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtvar5 = new System.Windows.Forms.Label();
            this.txtvar5Value = new System.Windows.Forms.TextBox();
            this.txtvar5Unit = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtvar7 = new System.Windows.Forms.Label();
            this.txtvar7Value = new System.Windows.Forms.TextBox();
            this.txtvar7Unit = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtvar8 = new System.Windows.Forms.Label();
            this.txtvar8Value = new System.Windows.Forms.TextBox();
            this.txtvar8Unit = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.txtvar9 = new System.Windows.Forms.Label();
            this.txtvar9Value = new System.Windows.Forms.TextBox();
            this.txtvar9Unit = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel46 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.txtvar23 = new System.Windows.Forms.Label();
            this.txtvar23Value = new System.Windows.Forms.TextBox();
            this.txtvar23Unit = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.txtvar27 = new System.Windows.Forms.Label();
            this.txtvar27Value = new System.Windows.Forms.TextBox();
            this.txtvar27Unit = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.txtvar26 = new System.Windows.Forms.Label();
            this.txtvar26Value = new System.Windows.Forms.TextBox();
            this.txtvar26Unit = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.txtvar25 = new System.Windows.Forms.Label();
            this.txtvar25Value = new System.Windows.Forms.TextBox();
            this.txtvar25Unit = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.txtvar24 = new System.Windows.Forms.Label();
            this.txtvar24Value = new System.Windows.Forms.TextBox();
            this.txtvar24Unit = new System.Windows.Forms.Label();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.txtvar34 = new System.Windows.Forms.Label();
            this.txtvar34Value = new System.Windows.Forms.TextBox();
            this.txtvar34Unit = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.txtvar35 = new System.Windows.Forms.Label();
            this.txtvar35Value = new System.Windows.Forms.TextBox();
            this.txtvar35Unit = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.txtvar28 = new System.Windows.Forms.Label();
            this.txtvar28Value = new System.Windows.Forms.TextBox();
            this.txtvar28Unit = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.txtvar29 = new System.Windows.Forms.Label();
            this.txtvar29Value = new System.Windows.Forms.TextBox();
            this.txtvar29Unit = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.txtvar30 = new System.Windows.Forms.Label();
            this.txtvar30Value = new System.Windows.Forms.TextBox();
            this.txtvar30Unit = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.panel37 = new System.Windows.Forms.Panel();
            this.txtvar31 = new System.Windows.Forms.Label();
            this.txtvar31Value = new System.Windows.Forms.TextBox();
            this.txtvar31Unit = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.txtvar32 = new System.Windows.Forms.Label();
            this.txtvar32Value = new System.Windows.Forms.TextBox();
            this.txtvar32Unit = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.txtvar33 = new System.Windows.Forms.Label();
            this.txtvar33Value = new System.Windows.Forms.TextBox();
            this.txtvar33Unit = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.txtvar19 = new System.Windows.Forms.Label();
            this.txtvar19Value = new System.Windows.Forms.TextBox();
            this.txtvar19Unit = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.txtvar20 = new System.Windows.Forms.Label();
            this.txtvar20Value = new System.Windows.Forms.TextBox();
            this.txtvar20Unit = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.txtvar21 = new System.Windows.Forms.Label();
            this.txtvar21Value = new System.Windows.Forms.TextBox();
            this.txtvar21Unit = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.txtvar22 = new System.Windows.Forms.Label();
            this.txtvar22Value = new System.Windows.Forms.TextBox();
            this.txtvar22Unit = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnMPSHistoryData = new System.Windows.Forms.Button();
            this.btnMPS1Hour = new System.Windows.Forms.Button();
            this.btnMPS5Minute = new System.Windows.Forms.Button();
            this.picMPSStatus = new System.Windows.Forms.PictureBox();
            this.pnLeftSide = new System.Windows.Forms.Panel();
            this.vprgMonthlyReport = new VerticalProgressBar.VerticalProgressBar();
            this.btnMaintenance = new System.Windows.Forms.Button();
            this.btnMonthlyReport = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnAllHistory = new System.Windows.Forms.Button();
            this.pnHeader = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnLoginLogout = new System.Windows.Forms.Button();
            this.lblLoginDisplayName = new System.Windows.Forms.Label();
            this.lblHeadingTime = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel20.SuspendLayout();
            this.pnSoftwareInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSamplerTank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel30.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMPSStatus)).BeginInit();
            this.pnLeftSide.SuspendLayout();
            this.pnHeader.SuspendLayout();
            this.panel18.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bgwMonthlyReport
            // 
            this.bgwMonthlyReport.WorkerReportsProgress = true;
            this.bgwMonthlyReport.WorkerSupportsCancellation = true;
            this.bgwMonthlyReport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerMonthlyReport_DoWork_2);
            this.bgwMonthlyReport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerMonthlyReport_ProgressChanged);
            this.bgwMonthlyReport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerMonthlyReport_RunWorkerCompleted);
            // 
            // backgroundWorkerMain
            // 
            this.backgroundWorkerMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerMain_DoWork);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.lblMainMenuTitle);
            this.panel20.Controls.Add(this.pnSoftwareInfo);
            this.panel20.Controls.Add(this.pictureBox52);
            this.panel20.Controls.Add(this.btnExit);
            this.panel20.Controls.Add(this.lblWaterLevel);
            this.panel20.Controls.Add(this.lblHeaderNationName);
            this.panel20.Controls.Add(this.txtData);
            this.panel20.Controls.Add(this.picSamplerTank);
            this.panel20.Controls.Add(this.btnLanguage);
            this.panel20.Controls.Add(this.pictureBox5);
            this.panel20.Controls.Add(this.button5);
            this.panel20.Controls.Add(this.button4);
            this.panel20.Location = new System.Drawing.Point(604, 457);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(28, 15);
            this.panel20.TabIndex = 70;
            // 
            // lblMainMenuTitle
            // 
            this.lblMainMenuTitle.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainMenuTitle.ForeColor = System.Drawing.Color.White;
            this.lblMainMenuTitle.Location = new System.Drawing.Point(-51, 33);
            this.lblMainMenuTitle.Name = "lblMainMenuTitle";
            this.lblMainMenuTitle.Size = new System.Drawing.Size(150, 22);
            this.lblMainMenuTitle.TabIndex = 3;
            this.lblMainMenuTitle.Text = "MAIN MENU";
            this.lblMainMenuTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblMainMenuTitle.Visible = false;
            // 
            // pnSoftwareInfo
            // 
            this.pnSoftwareInfo.BackColor = System.Drawing.Color.White;
            this.pnSoftwareInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnSoftwareInfo.Controls.Add(this.label6);
            this.pnSoftwareInfo.Controls.Add(this.lblSurfaceWaterQuality);
            this.pnSoftwareInfo.Controls.Add(this.lblAutomaticMonitoring);
            this.pnSoftwareInfo.Controls.Add(this.lblThaiNguyenStation);
            this.pnSoftwareInfo.Controls.Add(this.pictureBox3);
            this.pnSoftwareInfo.Controls.Add(this.pictureBox2);
            this.pnSoftwareInfo.Location = new System.Drawing.Point(-416, 23);
            this.pnSoftwareInfo.Name = "pnSoftwareInfo";
            this.pnSoftwareInfo.Size = new System.Drawing.Size(34, 13);
            this.pnSoftwareInfo.TabIndex = 4;
            this.pnSoftwareInfo.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(145, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "2015";
            // 
            // lblSurfaceWaterQuality
            // 
            this.lblSurfaceWaterQuality.AutoSize = true;
            this.lblSurfaceWaterQuality.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurfaceWaterQuality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblSurfaceWaterQuality.Location = new System.Drawing.Point(43, 117);
            this.lblSurfaceWaterQuality.Name = "lblSurfaceWaterQuality";
            this.lblSurfaceWaterQuality.Size = new System.Drawing.Size(214, 19);
            this.lblSurfaceWaterQuality.TabIndex = 5;
            this.lblSurfaceWaterQuality.Text = "SURFACE WATER QUALITY";
            // 
            // lblAutomaticMonitoring
            // 
            this.lblAutomaticMonitoring.AutoSize = true;
            this.lblAutomaticMonitoring.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutomaticMonitoring.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblAutomaticMonitoring.Location = new System.Drawing.Point(43, 97);
            this.lblAutomaticMonitoring.Name = "lblAutomaticMonitoring";
            this.lblAutomaticMonitoring.Size = new System.Drawing.Size(220, 19);
            this.lblAutomaticMonitoring.TabIndex = 4;
            this.lblAutomaticMonitoring.Text = "AUTOMATIC MONITORING";
            // 
            // lblThaiNguyenStation
            // 
            this.lblThaiNguyenStation.AutoSize = true;
            this.lblThaiNguyenStation.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThaiNguyenStation.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblThaiNguyenStation.Location = new System.Drawing.Point(53, 66);
            this.lblThaiNguyenStation.Name = "lblThaiNguyenStation";
            this.lblThaiNguyenStation.Size = new System.Drawing.Size(126, 19);
            this.lblThaiNguyenStation.TabIndex = 3;
            this.lblThaiNguyenStation.Text = "DMM STATION";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::DataLogger.Properties.Resources.Flag_of_South_Korea_48x32;
            this.pictureBox3.Location = new System.Drawing.Point(202, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 32);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::DataLogger.Properties.Resources.Flag_of_Vietnam_43x32;
            this.pictureBox2.Location = new System.Drawing.Point(76, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(43, 32);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox52.BackgroundImage = global::DataLogger.Properties.Resources.SamplerTank_Ruler;
            this.pictureBox52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox52.Location = new System.Drawing.Point(138, 32);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(10, 25);
            this.pictureBox52.TabIndex = 63;
            this.pictureBox52.TabStop = false;
            this.pictureBox52.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.BackgroundImage = global::DataLogger.Properties.Resources.Shutdown_Box_Red;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Enabled = false;
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(154, 32);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(40, 10);
            this.btnExit.TabIndex = 7;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblWaterLevel
            // 
            this.lblWaterLevel.AutoSize = true;
            this.lblWaterLevel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWaterLevel.Location = new System.Drawing.Point(-47, 36);
            this.lblWaterLevel.Name = "lblWaterLevel";
            this.lblWaterLevel.Size = new System.Drawing.Size(67, 15);
            this.lblWaterLevel.TabIndex = 31;
            this.lblWaterLevel.Text = "Water level";
            this.lblWaterLevel.Visible = false;
            // 
            // lblHeaderNationName
            // 
            this.lblHeaderNationName.AutoSize = true;
            this.lblHeaderNationName.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderNationName.ForeColor = System.Drawing.Color.White;
            this.lblHeaderNationName.Location = new System.Drawing.Point(-316, 34);
            this.lblHeaderNationName.Name = "lblHeaderNationName";
            this.lblHeaderNationName.Size = new System.Drawing.Size(84, 17);
            this.lblHeaderNationName.TabIndex = 1;
            this.lblHeaderNationName.Text = "Vietnamese";
            this.lblHeaderNationName.Visible = false;
            // 
            // txtData
            // 
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.ForeColor = System.Drawing.Color.Maroon;
            this.txtData.Location = new System.Drawing.Point(94, 41);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(48, 10);
            this.txtData.TabIndex = 62;
            this.txtData.Text = "";
            this.txtData.Visible = false;
            // 
            // picSamplerTank
            // 
            this.picSamplerTank.BackColor = System.Drawing.Color.White;
            this.picSamplerTank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSamplerTank.Image = global::DataLogger.Properties.Resources.SamplerTankerWater;
            this.picSamplerTank.Location = new System.Drawing.Point(110, 31);
            this.picSamplerTank.Name = "picSamplerTank";
            this.picSamplerTank.Size = new System.Drawing.Size(12, 26);
            this.picSamplerTank.TabIndex = 31;
            this.picSamplerTank.TabStop = false;
            this.picSamplerTank.Visible = false;
            // 
            // btnLanguage
            // 
            this.btnLanguage.BackColor = System.Drawing.Color.Transparent;
            this.btnLanguage.BackgroundImage = global::DataLogger.Properties.Resources.Flag_of_Vietnam_43x32;
            this.btnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLanguage.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnLanguage.FlatAppearance.BorderSize = 0;
            this.btnLanguage.Location = new System.Drawing.Point(-365, 29);
            this.btnLanguage.Name = "btnLanguage";
            this.btnLanguage.Size = new System.Drawing.Size(43, 16);
            this.btnLanguage.TabIndex = 50;
            this.btnLanguage.UseVisualStyleBackColor = false;
            this.btnLanguage.Visible = false;
            this.btnLanguage.Click += new System.EventHandler(this.btnLanguage_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::DataLogger.Properties.Resources.WaterLevel;
            this.pictureBox5.Location = new System.Drawing.Point(-107, 34);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(64, 21);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.BackgroundImage = global::DataLogger.Properties.Resources.logo;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(141)))), ((int)(((byte)(196)))));
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(-226, 31);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(47, 22);
            this.button5.TabIndex = 67;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.BackgroundImage = global::DataLogger.Properties.Resources.clock;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(141)))), ((int)(((byte)(196)))));
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(-173, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(37, 24);
            this.button4.TabIndex = 66;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            // 
            // panel30
            // 
            this.panel30.AutoSize = true;
            this.panel30.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel30.BackColor = System.Drawing.Color.Transparent;
            this.panel30.BackgroundImage = global::DataLogger.Properties.Resources.main;
            this.panel30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel30.Controls.Add(this.tabControl1);
            this.panel30.Controls.Add(this.panel20);
            this.panel30.Controls.Add(this.panel19);
            this.panel30.Controls.Add(this.panel17);
            this.panel30.Controls.Add(this.panel15);
            this.panel30.Controls.Add(this.btnMPSHistoryData);
            this.panel30.Controls.Add(this.btnMPS1Hour);
            this.panel30.Controls.Add(this.btnMPS5Minute);
            this.panel30.Controls.Add(this.picMPSStatus);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(88, 63);
            this.panel30.Margin = new System.Windows.Forms.Padding(10);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(1186, 588);
            this.panel30.TabIndex = 65;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(32, 19);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1115, 471);
            this.tabControl1.TabIndex = 111;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel45);
            this.tabPage1.Controls.Add(this.panel44);
            this.tabPage1.Controls.Add(this.panel43);
            this.tabPage1.Controls.Add(this.panel42);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1107, 445);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tủ 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel45
            // 
            this.panel45.BackgroundImage = global::DataLogger.Properties.Resources.Station1;
            this.panel45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel45.Controls.Add(this.label14);
            this.panel45.Controls.Add(this.panel9);
            this.panel45.Controls.Add(this.panel22);
            this.panel45.Controls.Add(this.panel23);
            this.panel45.Location = new System.Drawing.Point(584, 307);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(497, 118);
            this.panel45.TabIndex = 132;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(491, 23);
            this.label14.TabIndex = 126;
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Controls.Add(this.txtvar16);
            this.panel9.Controls.Add(this.txtvar16Value);
            this.panel9.Controls.Add(this.txtvar16Unit);
            this.panel9.Location = new System.Drawing.Point(30, 31);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(135, 78);
            this.panel9.TabIndex = 119;
            // 
            // txtvar16
            // 
            this.txtvar16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16.ForeColor = System.Drawing.Color.Black;
            this.txtvar16.Location = new System.Drawing.Point(20, 14);
            this.txtvar16.Name = "txtvar16";
            this.txtvar16.Size = new System.Drawing.Size(94, 19);
            this.txtvar16.TabIndex = 96;
            this.txtvar16.Text = "DO:";
            this.txtvar16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar16.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar16_Paint);
            // 
            // txtvar16Value
            // 
            this.txtvar16Value.BackColor = System.Drawing.Color.White;
            this.txtvar16Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar16Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar16Value.Location = new System.Drawing.Point(7, 45);
            this.txtvar16Value.Name = "txtvar16Value";
            this.txtvar16Value.ReadOnly = true;
            this.txtvar16Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar16Value.TabIndex = 102;
            this.txtvar16Value.Text = "6.88";
            this.txtvar16Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar16Unit
            // 
            this.txtvar16Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar16Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar16Unit.Location = new System.Drawing.Point(77, 45);
            this.txtvar16Unit.Name = "txtvar16Unit";
            this.txtvar16Unit.Size = new System.Drawing.Size(54, 19);
            this.txtvar16Unit.TabIndex = 105;
            this.txtvar16Unit.Text = "mg/Nm3";
            this.txtvar16Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar16Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar16Unit_Paint);
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.White;
            this.panel22.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel22.Controls.Add(this.txtvar17);
            this.panel22.Controls.Add(this.txtvar17Value);
            this.panel22.Controls.Add(this.txtvar17Unit);
            this.panel22.Location = new System.Drawing.Point(184, 31);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(135, 78);
            this.panel22.TabIndex = 127;
            // 
            // txtvar17
            // 
            this.txtvar17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17.ForeColor = System.Drawing.Color.Black;
            this.txtvar17.Location = new System.Drawing.Point(24, 15);
            this.txtvar17.Name = "txtvar17";
            this.txtvar17.Size = new System.Drawing.Size(88, 19);
            this.txtvar17.TabIndex = 97;
            this.txtvar17.Text = "TSS:";
            this.txtvar17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar17.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar17_Paint);
            // 
            // txtvar17Value
            // 
            this.txtvar17Value.BackColor = System.Drawing.Color.White;
            this.txtvar17Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar17Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar17Value.Location = new System.Drawing.Point(12, 44);
            this.txtvar17Value.Name = "txtvar17Value";
            this.txtvar17Value.ReadOnly = true;
            this.txtvar17Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar17Value.TabIndex = 103;
            this.txtvar17Value.Text = "9.29";
            this.txtvar17Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar17Unit
            // 
            this.txtvar17Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar17Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar17Unit.Location = new System.Drawing.Point(81, 42);
            this.txtvar17Unit.Name = "txtvar17Unit";
            this.txtvar17Unit.Size = new System.Drawing.Size(52, 19);
            this.txtvar17Unit.TabIndex = 106;
            this.txtvar17Unit.Text = "mg/Nm3";
            this.txtvar17Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar17Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar17Unit_Paint);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.White;
            this.panel23.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel23.Controls.Add(this.txtvar18);
            this.panel23.Controls.Add(this.txtvar18Value);
            this.panel23.Controls.Add(this.txtvar18Unit);
            this.panel23.Enabled = false;
            this.panel23.Location = new System.Drawing.Point(340, 31);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(135, 78);
            this.panel23.TabIndex = 128;
            // 
            // txtvar18
            // 
            this.txtvar18.Enabled = false;
            this.txtvar18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18.ForeColor = System.Drawing.Color.Black;
            this.txtvar18.Location = new System.Drawing.Point(20, 13);
            this.txtvar18.Name = "txtvar18";
            this.txtvar18.Size = new System.Drawing.Size(95, 19);
            this.txtvar18.TabIndex = 98;
            this.txtvar18.Text = "---";
            this.txtvar18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar18.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar18_Paint);
            // 
            // txtvar18Value
            // 
            this.txtvar18Value.BackColor = System.Drawing.Color.White;
            this.txtvar18Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar18Value.Enabled = false;
            this.txtvar18Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar18Value.Location = new System.Drawing.Point(12, 42);
            this.txtvar18Value.Multiline = true;
            this.txtvar18Value.Name = "txtvar18Value";
            this.txtvar18Value.ReadOnly = true;
            this.txtvar18Value.Size = new System.Drawing.Size(64, 21);
            this.txtvar18Value.TabIndex = 104;
            this.txtvar18Value.Text = "---";
            this.txtvar18Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar18Unit
            // 
            this.txtvar18Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar18Unit.Enabled = false;
            this.txtvar18Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar18Unit.Location = new System.Drawing.Point(82, 42);
            this.txtvar18Unit.Name = "txtvar18Unit";
            this.txtvar18Unit.Size = new System.Drawing.Size(50, 20);
            this.txtvar18Unit.TabIndex = 107;
            this.txtvar18Unit.Text = "---";
            this.txtvar18Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar18Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar18Unit_Paint);
            // 
            // panel44
            // 
            this.panel44.BackgroundImage = global::DataLogger.Properties.Resources.Station1;
            this.panel44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel44.Controls.Add(this.label10);
            this.panel44.Controls.Add(this.panel14);
            this.panel44.Controls.Add(this.panel16);
            this.panel44.Controls.Add(this.panel21);
            this.panel44.Location = new System.Drawing.Point(584, 165);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(497, 123);
            this.panel44.TabIndex = 131;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(491, 23);
            this.label10.TabIndex = 129;
            this.label10.Text = "Ống Khói Làm Nguội Clinker";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.White;
            this.panel14.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel14.Controls.Add(this.txtvar13);
            this.panel14.Controls.Add(this.txtvar13Value);
            this.panel14.Controls.Add(this.txtvar13Unit);
            this.panel14.Location = new System.Drawing.Point(32, 34);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(135, 78);
            this.panel14.TabIndex = 124;
            this.panel14.Paint += new System.Windows.Forms.PaintEventHandler(this.panel14_Paint);
            // 
            // txtvar13
            // 
            this.txtvar13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar13.ForeColor = System.Drawing.Color.Black;
            this.txtvar13.Location = new System.Drawing.Point(20, 13);
            this.txtvar13.Name = "txtvar13";
            this.txtvar13.Size = new System.Drawing.Size(90, 19);
            this.txtvar13.TabIndex = 93;
            this.txtvar13.Text = "pH:";
            this.txtvar13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar13.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar13_Paint);
            // 
            // txtvar13Value
            // 
            this.txtvar13Value.BackColor = System.Drawing.Color.White;
            this.txtvar13Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar13Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar13Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar13Value.Location = new System.Drawing.Point(10, 44);
            this.txtvar13Value.Name = "txtvar13Value";
            this.txtvar13Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar13Value.TabIndex = 99;
            this.txtvar13Value.Text = "7.20";
            this.txtvar13Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar13Unit
            // 
            this.txtvar13Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar13Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar13Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar13Unit.Location = new System.Drawing.Point(78, 44);
            this.txtvar13Unit.Name = "txtvar13Unit";
            this.txtvar13Unit.Size = new System.Drawing.Size(52, 19);
            this.txtvar13Unit.TabIndex = 110;
            this.txtvar13Unit.Text = "mg/Nm3";
            this.txtvar13Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar13Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar13Unit_Paint);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.White;
            this.panel16.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel16.Controls.Add(this.txtvar14);
            this.panel16.Controls.Add(this.txtvar14Value);
            this.panel16.Controls.Add(this.txtvar14Unit);
            this.panel16.Location = new System.Drawing.Point(186, 34);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(135, 78);
            this.panel16.TabIndex = 125;
            // 
            // txtvar14
            // 
            this.txtvar14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14.ForeColor = System.Drawing.Color.Black;
            this.txtvar14.Location = new System.Drawing.Point(19, 13);
            this.txtvar14.Name = "txtvar14";
            this.txtvar14.Size = new System.Drawing.Size(101, 19);
            this.txtvar14.TabIndex = 94;
            this.txtvar14.Text = "ORP:";
            this.txtvar14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar14.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar14_Paint);
            // 
            // txtvar14Value
            // 
            this.txtvar14Value.BackColor = System.Drawing.Color.White;
            this.txtvar14Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar14Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar14Value.Location = new System.Drawing.Point(10, 44);
            this.txtvar14Value.Name = "txtvar14Value";
            this.txtvar14Value.ReadOnly = true;
            this.txtvar14Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar14Value.TabIndex = 100;
            this.txtvar14Value.Text = "426.17";
            this.txtvar14Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar14Unit
            // 
            this.txtvar14Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar14Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar14Unit.Location = new System.Drawing.Point(78, 43);
            this.txtvar14Unit.Name = "txtvar14Unit";
            this.txtvar14Unit.Size = new System.Drawing.Size(55, 19);
            this.txtvar14Unit.TabIndex = 108;
            this.txtvar14Unit.Text = "mg/Nm3";
            this.txtvar14Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar14Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar14Unit_Paint);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.White;
            this.panel21.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel21.Controls.Add(this.txtvar15);
            this.panel21.Controls.Add(this.txtvar15Value);
            this.panel21.Controls.Add(this.txtvar15Unit);
            this.panel21.Enabled = false;
            this.panel21.Location = new System.Drawing.Point(342, 34);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(135, 78);
            this.panel21.TabIndex = 126;
            // 
            // txtvar15
            // 
            this.txtvar15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15.ForeColor = System.Drawing.Color.Black;
            this.txtvar15.Location = new System.Drawing.Point(19, 14);
            this.txtvar15.Name = "txtvar15";
            this.txtvar15.Size = new System.Drawing.Size(97, 19);
            this.txtvar15.TabIndex = 95;
            this.txtvar15.Text = "---";
            this.txtvar15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar15.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar15_Paint);
            // 
            // txtvar15Value
            // 
            this.txtvar15Value.BackColor = System.Drawing.Color.White;
            this.txtvar15Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar15Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar15Value.Location = new System.Drawing.Point(10, 44);
            this.txtvar15Value.Name = "txtvar15Value";
            this.txtvar15Value.ReadOnly = true;
            this.txtvar15Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar15Value.TabIndex = 101;
            this.txtvar15Value.Text = "---";
            this.txtvar15Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar15Unit
            // 
            this.txtvar15Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar15Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar15Unit.Location = new System.Drawing.Point(80, 42);
            this.txtvar15Unit.Name = "txtvar15Unit";
            this.txtvar15Unit.Size = new System.Drawing.Size(53, 19);
            this.txtvar15Unit.TabIndex = 109;
            this.txtvar15Unit.Text = "---";
            this.txtvar15Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar15Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar15Unit_Paint);
            // 
            // panel43
            // 
            this.panel43.BackgroundImage = global::DataLogger.Properties.Resources.Station1;
            this.panel43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel43.Controls.Add(this.label13);
            this.panel43.Controls.Add(this.panel11);
            this.panel43.Controls.Add(this.panel12);
            this.panel43.Controls.Add(this.panel13);
            this.panel43.Location = new System.Drawing.Point(584, 19);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(497, 130);
            this.panel43.TabIndex = 130;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(3, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(491, 23);
            this.label13.TabIndex = 125;
            this.label13.Text = "Ống Khói Nghiền Than";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.txtvar10);
            this.panel11.Controls.Add(this.txtvar10Value);
            this.panel11.Controls.Add(this.txtvar10Unit);
            this.panel11.Location = new System.Drawing.Point(32, 36);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(135, 78);
            this.panel11.TabIndex = 121;
            // 
            // txtvar10
            // 
            this.txtvar10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar10.ForeColor = System.Drawing.Color.Black;
            this.txtvar10.Location = new System.Drawing.Point(14, 13);
            this.txtvar10.Name = "txtvar10";
            this.txtvar10.Size = new System.Drawing.Size(103, 19);
            this.txtvar10.TabIndex = 78;
            this.txtvar10.Text = "DO:";
            this.txtvar10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar10.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar10_Paint);
            // 
            // txtvar10Value
            // 
            this.txtvar10Value.BackColor = System.Drawing.Color.White;
            this.txtvar10Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar10Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar10Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar10Value.Location = new System.Drawing.Point(8, 46);
            this.txtvar10Value.Name = "txtvar10Value";
            this.txtvar10Value.ReadOnly = true;
            this.txtvar10Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar10Value.TabIndex = 84;
            this.txtvar10Value.Text = "6.88";
            this.txtvar10Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar10Unit
            // 
            this.txtvar10Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar10Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar10Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar10Unit.Location = new System.Drawing.Point(78, 44);
            this.txtvar10Unit.Name = "txtvar10Unit";
            this.txtvar10Unit.Size = new System.Drawing.Size(51, 19);
            this.txtvar10Unit.TabIndex = 87;
            this.txtvar10Unit.Text = "mg/Nm3";
            this.txtvar10Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar10Unit.Click += new System.EventHandler(this.txtvar10Unit_Click);
            this.txtvar10Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar10Unit_Paint);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.txtvar11);
            this.panel12.Controls.Add(this.txtvar11Value);
            this.panel12.Controls.Add(this.txtvar11Unit);
            this.panel12.Location = new System.Drawing.Point(186, 35);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(135, 78);
            this.panel12.TabIndex = 122;
            // 
            // txtvar11
            // 
            this.txtvar11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar11.ForeColor = System.Drawing.Color.Black;
            this.txtvar11.Location = new System.Drawing.Point(15, 12);
            this.txtvar11.Name = "txtvar11";
            this.txtvar11.Size = new System.Drawing.Size(101, 19);
            this.txtvar11.TabIndex = 79;
            this.txtvar11.Text = "TSS:";
            this.txtvar11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar11.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar11_Paint);
            // 
            // txtvar11Value
            // 
            this.txtvar11Value.BackColor = System.Drawing.Color.White;
            this.txtvar11Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar11Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar11Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar11Value.Location = new System.Drawing.Point(7, 44);
            this.txtvar11Value.Name = "txtvar11Value";
            this.txtvar11Value.ReadOnly = true;
            this.txtvar11Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar11Value.TabIndex = 85;
            this.txtvar11Value.Text = "9.29";
            this.txtvar11Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar11Unit
            // 
            this.txtvar11Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar11Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar11Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar11Unit.Location = new System.Drawing.Point(78, 43);
            this.txtvar11Unit.Name = "txtvar11Unit";
            this.txtvar11Unit.Size = new System.Drawing.Size(53, 19);
            this.txtvar11Unit.TabIndex = 88;
            this.txtvar11Unit.Text = "mg/Nm3";
            this.txtvar11Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar11Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar11Unit_Paint);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel13.Controls.Add(this.txtvar12);
            this.panel13.Controls.Add(this.txtvar12Value);
            this.panel13.Controls.Add(this.txtvar12Unit);
            this.panel13.Enabled = false;
            this.panel13.Location = new System.Drawing.Point(342, 36);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(135, 78);
            this.panel13.TabIndex = 123;
            // 
            // txtvar12
            // 
            this.txtvar12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar12.ForeColor = System.Drawing.Color.Black;
            this.txtvar12.Location = new System.Drawing.Point(20, 12);
            this.txtvar12.Name = "txtvar12";
            this.txtvar12.Size = new System.Drawing.Size(95, 19);
            this.txtvar12.TabIndex = 80;
            this.txtvar12.Text = "----";
            this.txtvar12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar12.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar12_Paint);
            // 
            // txtvar12Value
            // 
            this.txtvar12Value.BackColor = System.Drawing.Color.White;
            this.txtvar12Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar12Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar12Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar12Value.Location = new System.Drawing.Point(12, 43);
            this.txtvar12Value.Multiline = true;
            this.txtvar12Value.Name = "txtvar12Value";
            this.txtvar12Value.ReadOnly = true;
            this.txtvar12Value.Size = new System.Drawing.Size(64, 21);
            this.txtvar12Value.TabIndex = 86;
            this.txtvar12Value.Text = "---";
            this.txtvar12Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar12Unit
            // 
            this.txtvar12Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar12Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar12Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar12Unit.Location = new System.Drawing.Point(78, 41);
            this.txtvar12Unit.Name = "txtvar12Unit";
            this.txtvar12Unit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtvar12Unit.Size = new System.Drawing.Size(54, 20);
            this.txtvar12Unit.TabIndex = 89;
            this.txtvar12Unit.Text = "---";
            this.txtvar12Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar12Unit.Click += new System.EventHandler(this.txtvar12Unit_Click);
            this.txtvar12Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar12Unit_Paint);
            // 
            // panel42
            // 
            this.panel42.BackgroundImage = global::DataLogger.Properties.Resources.Control;
            this.panel42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel42.Controls.Add(this.label11);
            this.panel42.Controls.Add(this.panel1);
            this.panel42.Controls.Add(this.panel6);
            this.panel42.Controls.Add(this.panel3);
            this.panel42.Controls.Add(this.panel4);
            this.panel42.Controls.Add(this.panel7);
            this.panel42.Controls.Add(this.panel5);
            this.panel42.Controls.Add(this.panel8);
            this.panel42.Controls.Add(this.panel2);
            this.panel42.Controls.Add(this.panel10);
            this.panel42.Location = new System.Drawing.Point(26, 15);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(529, 413);
            this.panel42.TabIndex = 129;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(523, 30);
            this.label11.TabIndex = 122;
            this.label11.Text = "Ống Khói Chính";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.txtvar1);
            this.panel1.Controls.Add(this.txtvar1Value);
            this.panel1.Controls.Add(this.txtvar1Unit);
            this.panel1.Location = new System.Drawing.Point(24, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(137, 78);
            this.panel1.TabIndex = 112;
            // 
            // txtvar1
            // 
            this.txtvar1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1.ForeColor = System.Drawing.Color.Black;
            this.txtvar1.Location = new System.Drawing.Point(10, 12);
            this.txtvar1.Name = "txtvar1";
            this.txtvar1.Size = new System.Drawing.Size(113, 19);
            this.txtvar1.TabIndex = 45;
            this.txtvar1.Text = "pH:";
            this.txtvar1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar1.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar1_Paint);
            // 
            // txtvar1Value
            // 
            this.txtvar1Value.BackColor = System.Drawing.Color.White;
            this.txtvar1Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar1Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar1Value.Location = new System.Drawing.Point(9, 44);
            this.txtvar1Value.Name = "txtvar1Value";
            this.txtvar1Value.Size = new System.Drawing.Size(62, 19);
            this.txtvar1Value.TabIndex = 51;
            this.txtvar1Value.Text = "-";
            this.txtvar1Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar1Unit
            // 
            this.txtvar1Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar1Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar1Unit.Location = new System.Drawing.Point(76, 43);
            this.txtvar1Unit.Name = "txtvar1Unit";
            this.txtvar1Unit.Size = new System.Drawing.Size(57, 19);
            this.txtvar1Unit.TabIndex = 68;
            this.txtvar1Unit.Text = "mg/Nm3";
            this.txtvar1Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar1Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar1Unit_Paint);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Controls.Add(this.txtvar2);
            this.panel6.Controls.Add(this.txtvar2Value);
            this.panel6.Controls.Add(this.txtvar2Unit);
            this.panel6.Location = new System.Drawing.Point(200, 56);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(135, 78);
            this.panel6.TabIndex = 111;
            // 
            // txtvar2
            // 
            this.txtvar2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2.ForeColor = System.Drawing.Color.Black;
            this.txtvar2.Location = new System.Drawing.Point(17, 12);
            this.txtvar2.Name = "txtvar2";
            this.txtvar2.Size = new System.Drawing.Size(95, 19);
            this.txtvar2.TabIndex = 46;
            this.txtvar2.Text = "ORP:";
            this.txtvar2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar2.Click += new System.EventHandler(this.txtvar2_Click);
            this.txtvar2.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar2_Paint);
            // 
            // txtvar2Value
            // 
            this.txtvar2Value.BackColor = System.Drawing.Color.White;
            this.txtvar2Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar2Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar2Value.Location = new System.Drawing.Point(9, 44);
            this.txtvar2Value.Name = "txtvar2Value";
            this.txtvar2Value.ReadOnly = true;
            this.txtvar2Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar2Value.TabIndex = 52;
            this.txtvar2Value.Text = "-";
            this.txtvar2Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar2Unit
            // 
            this.txtvar2Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar2Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar2Unit.Location = new System.Drawing.Point(74, 44);
            this.txtvar2Unit.Name = "txtvar2Unit";
            this.txtvar2Unit.Size = new System.Drawing.Size(56, 19);
            this.txtvar2Unit.TabIndex = 66;
            this.txtvar2Unit.Text = "mg/Nm3";
            this.txtvar2Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar2Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar2Unit_Paint);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.txtvar3);
            this.panel3.Controls.Add(this.txtvar3Value);
            this.panel3.Controls.Add(this.txtvar3Unit);
            this.panel3.Location = new System.Drawing.Point(374, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(135, 78);
            this.panel3.TabIndex = 114;
            // 
            // txtvar3
            // 
            this.txtvar3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3.ForeColor = System.Drawing.Color.Black;
            this.txtvar3.Location = new System.Drawing.Point(19, 12);
            this.txtvar3.Name = "txtvar3";
            this.txtvar3.Size = new System.Drawing.Size(91, 19);
            this.txtvar3.TabIndex = 47;
            this.txtvar3.Text = "Temp:";
            this.txtvar3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar3.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar3_Paint);
            // 
            // txtvar3Value
            // 
            this.txtvar3Value.BackColor = System.Drawing.Color.White;
            this.txtvar3Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar3Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar3Value.Location = new System.Drawing.Point(11, 44);
            this.txtvar3Value.Name = "txtvar3Value";
            this.txtvar3Value.ReadOnly = true;
            this.txtvar3Value.Size = new System.Drawing.Size(60, 19);
            this.txtvar3Value.TabIndex = 53;
            this.txtvar3Value.Text = "-";
            this.txtvar3Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar3Unit
            // 
            this.txtvar3Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar3Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar3Unit.Location = new System.Drawing.Point(75, 44);
            this.txtvar3Unit.Name = "txtvar3Unit";
            this.txtvar3Unit.Size = new System.Drawing.Size(54, 19);
            this.txtvar3Unit.TabIndex = 67;
            this.txtvar3Unit.Text = "mg/Nm3";
            this.txtvar3Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar3Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar3Unit_Paint);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.txtvar4);
            this.panel4.Controls.Add(this.txtvar4Value);
            this.panel4.Controls.Add(this.txtvar4Unit);
            this.panel4.Location = new System.Drawing.Point(24, 178);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(135, 78);
            this.panel4.TabIndex = 115;
            // 
            // txtvar4
            // 
            this.txtvar4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4.ForeColor = System.Drawing.Color.Black;
            this.txtvar4.Location = new System.Drawing.Point(18, 12);
            this.txtvar4.Name = "txtvar4";
            this.txtvar4.Size = new System.Drawing.Size(91, 19);
            this.txtvar4.TabIndex = 48;
            this.txtvar4.Text = "DO:";
            this.txtvar4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar4.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar4_Paint);
            // 
            // txtvar4Value
            // 
            this.txtvar4Value.BackColor = System.Drawing.Color.White;
            this.txtvar4Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar4Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar4Value.Location = new System.Drawing.Point(9, 45);
            this.txtvar4Value.Name = "txtvar4Value";
            this.txtvar4Value.ReadOnly = true;
            this.txtvar4Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar4Value.TabIndex = 54;
            this.txtvar4Value.Text = "6.88";
            this.txtvar4Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar4Unit
            // 
            this.txtvar4Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar4Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar4Unit.Location = new System.Drawing.Point(78, 45);
            this.txtvar4Unit.Name = "txtvar4Unit";
            this.txtvar4Unit.Size = new System.Drawing.Size(53, 19);
            this.txtvar4Unit.TabIndex = 63;
            this.txtvar4Unit.Text = "mg/Nm3";
            this.txtvar4Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar4Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar4Unit_Paint);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Controls.Add(this.txtvar6);
            this.panel7.Controls.Add(this.txtvar6Value);
            this.panel7.Controls.Add(this.txtvar6Unit);
            this.panel7.Location = new System.Drawing.Point(200, 177);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(135, 78);
            this.panel7.TabIndex = 117;
            // 
            // txtvar6
            // 
            this.txtvar6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar6.ForeColor = System.Drawing.Color.Black;
            this.txtvar6.Location = new System.Drawing.Point(17, 13);
            this.txtvar6.Name = "txtvar6";
            this.txtvar6.Size = new System.Drawing.Size(101, 19);
            this.txtvar6.TabIndex = 50;
            this.txtvar6.Text = "EC:";
            this.txtvar6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar6.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar6_Paint);
            // 
            // txtvar6Value
            // 
            this.txtvar6Value.BackColor = System.Drawing.Color.White;
            this.txtvar6Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar6Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar6Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar6Value.Location = new System.Drawing.Point(7, 45);
            this.txtvar6Value.Multiline = true;
            this.txtvar6Value.Name = "txtvar6Value";
            this.txtvar6Value.ReadOnly = true;
            this.txtvar6Value.Size = new System.Drawing.Size(64, 21);
            this.txtvar6Value.TabIndex = 56;
            this.txtvar6Value.Text = "117.242";
            this.txtvar6Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar6Unit
            // 
            this.txtvar6Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar6Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar6Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar6Unit.Location = new System.Drawing.Point(77, 45);
            this.txtvar6Unit.Name = "txtvar6Unit";
            this.txtvar6Unit.Size = new System.Drawing.Size(54, 20);
            this.txtvar6Unit.TabIndex = 65;
            this.txtvar6Unit.Text = "mg/Nm3";
            this.txtvar6Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar6Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar6Unit_Paint);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.txtvar5);
            this.panel5.Controls.Add(this.txtvar5Value);
            this.panel5.Controls.Add(this.txtvar5Unit);
            this.panel5.Location = new System.Drawing.Point(374, 179);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(135, 78);
            this.panel5.TabIndex = 116;
            // 
            // txtvar5
            // 
            this.txtvar5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar5.ForeColor = System.Drawing.Color.Black;
            this.txtvar5.Location = new System.Drawing.Point(16, 14);
            this.txtvar5.Name = "txtvar5";
            this.txtvar5.Size = new System.Drawing.Size(94, 19);
            this.txtvar5.TabIndex = 49;
            this.txtvar5.Text = "TSS:";
            this.txtvar5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar5.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar5_Paint);
            // 
            // txtvar5Value
            // 
            this.txtvar5Value.BackColor = System.Drawing.Color.White;
            this.txtvar5Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar5Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar5Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar5Value.Location = new System.Drawing.Point(10, 46);
            this.txtvar5Value.Name = "txtvar5Value";
            this.txtvar5Value.ReadOnly = true;
            this.txtvar5Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar5Value.TabIndex = 55;
            this.txtvar5Value.Text = "9.29";
            this.txtvar5Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar5Unit
            // 
            this.txtvar5Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar5Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar5Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar5Unit.Location = new System.Drawing.Point(78, 45);
            this.txtvar5Unit.Name = "txtvar5Unit";
            this.txtvar5Unit.Size = new System.Drawing.Size(54, 19);
            this.txtvar5Unit.TabIndex = 64;
            this.txtvar5Unit.Text = "mg/Nm3";
            this.txtvar5Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar5Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar5Unit_Paint);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.Controls.Add(this.txtvar7);
            this.panel8.Controls.Add(this.txtvar7Value);
            this.panel8.Controls.Add(this.txtvar7Unit);
            this.panel8.Location = new System.Drawing.Point(24, 297);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(135, 78);
            this.panel8.TabIndex = 118;
            // 
            // txtvar7
            // 
            this.txtvar7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar7.ForeColor = System.Drawing.Color.Black;
            this.txtvar7.Location = new System.Drawing.Point(17, 14);
            this.txtvar7.Name = "txtvar7";
            this.txtvar7.Size = new System.Drawing.Size(101, 19);
            this.txtvar7.TabIndex = 75;
            this.txtvar7.Text = "pH:";
            this.txtvar7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar7.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar7_Paint);
            // 
            // txtvar7Value
            // 
            this.txtvar7Value.BackColor = System.Drawing.Color.White;
            this.txtvar7Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar7Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar7Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar7Value.Location = new System.Drawing.Point(8, 44);
            this.txtvar7Value.Name = "txtvar7Value";
            this.txtvar7Value.Size = new System.Drawing.Size(67, 19);
            this.txtvar7Value.TabIndex = 81;
            this.txtvar7Value.Text = "7.20";
            this.txtvar7Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar7Unit
            // 
            this.txtvar7Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar7Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar7Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar7Unit.Location = new System.Drawing.Point(80, 43);
            this.txtvar7Unit.Name = "txtvar7Unit";
            this.txtvar7Unit.Size = new System.Drawing.Size(52, 19);
            this.txtvar7Unit.TabIndex = 92;
            this.txtvar7Unit.Text = "mg/Nm3";
            this.txtvar7Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar7Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar7Unit_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.txtvar8);
            this.panel2.Controls.Add(this.txtvar8Value);
            this.panel2.Controls.Add(this.txtvar8Unit);
            this.panel2.Location = new System.Drawing.Point(200, 297);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(135, 78);
            this.panel2.TabIndex = 113;
            // 
            // txtvar8
            // 
            this.txtvar8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar8.ForeColor = System.Drawing.Color.Black;
            this.txtvar8.Location = new System.Drawing.Point(14, 13);
            this.txtvar8.Name = "txtvar8";
            this.txtvar8.Size = new System.Drawing.Size(106, 19);
            this.txtvar8.TabIndex = 76;
            this.txtvar8.Text = "---";
            this.txtvar8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar8.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar8_Paint);
            // 
            // txtvar8Value
            // 
            this.txtvar8Value.BackColor = System.Drawing.Color.White;
            this.txtvar8Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar8Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar8Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar8Value.Location = new System.Drawing.Point(8, 45);
            this.txtvar8Value.Multiline = true;
            this.txtvar8Value.Name = "txtvar8Value";
            this.txtvar8Value.ReadOnly = true;
            this.txtvar8Value.Size = new System.Drawing.Size(64, 20);
            this.txtvar8Value.TabIndex = 82;
            this.txtvar8Value.Text = "---";
            this.txtvar8Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar8Unit
            // 
            this.txtvar8Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar8Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar8Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar8Unit.Location = new System.Drawing.Point(75, 46);
            this.txtvar8Unit.Name = "txtvar8Unit";
            this.txtvar8Unit.Size = new System.Drawing.Size(57, 19);
            this.txtvar8Unit.TabIndex = 90;
            this.txtvar8Unit.Text = "---";
            this.txtvar8Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar8Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar8Unit_Paint);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.txtvar9);
            this.panel10.Controls.Add(this.txtvar9Value);
            this.panel10.Controls.Add(this.txtvar9Unit);
            this.panel10.Enabled = false;
            this.panel10.Location = new System.Drawing.Point(374, 302);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(135, 78);
            this.panel10.TabIndex = 120;
            // 
            // txtvar9
            // 
            this.txtvar9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar9.ForeColor = System.Drawing.Color.Black;
            this.txtvar9.Location = new System.Drawing.Point(16, 12);
            this.txtvar9.Name = "txtvar9";
            this.txtvar9.Size = new System.Drawing.Size(105, 19);
            this.txtvar9.TabIndex = 77;
            this.txtvar9.Text = "---";
            this.txtvar9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar9.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar9_Paint);
            // 
            // txtvar9Value
            // 
            this.txtvar9Value.BackColor = System.Drawing.Color.White;
            this.txtvar9Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar9Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar9Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar9Value.Location = new System.Drawing.Point(11, 44);
            this.txtvar9Value.Name = "txtvar9Value";
            this.txtvar9Value.ReadOnly = true;
            this.txtvar9Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar9Value.TabIndex = 83;
            this.txtvar9Value.Text = "---";
            this.txtvar9Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar9Unit
            // 
            this.txtvar9Unit.BackColor = System.Drawing.Color.Transparent;
            this.txtvar9Unit.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar9Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar9Unit.Location = new System.Drawing.Point(80, 44);
            this.txtvar9Unit.Name = "txtvar9Unit";
            this.txtvar9Unit.Size = new System.Drawing.Size(54, 19);
            this.txtvar9Unit.TabIndex = 91;
            this.txtvar9Unit.Text = "---";
            this.txtvar9Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar9Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar9Unit_Paint);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel46);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1107, 445);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tủ 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel46
            // 
            this.panel46.BackgroundImage = global::DataLogger.Properties.Resources.Control;
            this.panel46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel46.Controls.Add(this.label1);
            this.panel46.Controls.Add(this.label7);
            this.panel46.Controls.Add(this.panel51);
            this.panel46.Controls.Add(this.panel50);
            this.panel46.Controls.Add(this.panel25);
            this.panel46.Controls.Add(this.panel26);
            this.panel46.Controls.Add(this.panel27);
            this.panel46.Controls.Add(this.panel28);
            this.panel46.Location = new System.Drawing.Point(20, 28);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(1075, 401);
            this.panel46.TabIndex = 182;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(4, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1065, 23);
            this.label1.TabIndex = 189;
            this.label1.Text = "Ống Khói Nghiền Xi (Nghiền Đứng)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(3, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(1067, 23);
            this.label7.TabIndex = 188;
            this.label7.Text = "Ống Khói Nghiền Xi (Nghiền Bi)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.panel29);
            this.panel51.Controls.Add(this.panel34);
            this.panel51.Controls.Add(this.panel33);
            this.panel51.Controls.Add(this.panel32);
            this.panel51.Controls.Add(this.panel31);
            this.panel51.Location = new System.Drawing.Point(925, 101);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(116, 45);
            this.panel51.TabIndex = 187;
            this.panel51.Visible = false;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.White;
            this.panel29.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel29.Controls.Add(this.txtvar23);
            this.panel29.Controls.Add(this.txtvar23Value);
            this.panel29.Controls.Add(this.txtvar23Unit);
            this.panel29.Location = new System.Drawing.Point(33, 36);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(135, 78);
            this.panel29.TabIndex = 170;
            // 
            // txtvar23
            // 
            this.txtvar23.AutoSize = true;
            this.txtvar23.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar23.ForeColor = System.Drawing.Color.Black;
            this.txtvar23.Location = new System.Drawing.Point(47, 12);
            this.txtvar23.Name = "txtvar23";
            this.txtvar23.Size = new System.Drawing.Size(42, 19);
            this.txtvar23.TabIndex = 115;
            this.txtvar23.Text = "TSS:";
            // 
            // txtvar23Value
            // 
            this.txtvar23Value.BackColor = System.Drawing.Color.White;
            this.txtvar23Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar23Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar23Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar23Value.Location = new System.Drawing.Point(11, 45);
            this.txtvar23Value.Name = "txtvar23Value";
            this.txtvar23Value.ReadOnly = true;
            this.txtvar23Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar23Value.TabIndex = 121;
            this.txtvar23Value.Text = "9.29";
            this.txtvar23Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar23Unit
            // 
            this.txtvar23Unit.AutoSize = true;
            this.txtvar23Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar23Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar23Unit.Location = new System.Drawing.Point(82, 44);
            this.txtvar23Unit.Name = "txtvar23Unit";
            this.txtvar23Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar23Unit.TabIndex = 124;
            this.txtvar23Unit.Text = "mg/L";
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.White;
            this.panel34.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel34.Controls.Add(this.txtvar27);
            this.panel34.Controls.Add(this.txtvar27Value);
            this.panel34.Controls.Add(this.txtvar27Unit);
            this.panel34.Location = new System.Drawing.Point(200, 155);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(135, 78);
            this.panel34.TabIndex = 174;
            // 
            // txtvar27
            // 
            this.txtvar27.AutoSize = true;
            this.txtvar27.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar27.ForeColor = System.Drawing.Color.Black;
            this.txtvar27.Location = new System.Drawing.Point(46, 10);
            this.txtvar27.Name = "txtvar27";
            this.txtvar27.Size = new System.Drawing.Size(51, 19);
            this.txtvar27.TabIndex = 131;
            this.txtvar27.Text = "Temp:";
            // 
            // txtvar27Value
            // 
            this.txtvar27Value.BackColor = System.Drawing.Color.White;
            this.txtvar27Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar27Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar27Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar27Value.Location = new System.Drawing.Point(11, 44);
            this.txtvar27Value.Name = "txtvar27Value";
            this.txtvar27Value.ReadOnly = true;
            this.txtvar27Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar27Value.TabIndex = 137;
            this.txtvar27Value.Text = "27.65";
            this.txtvar27Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar27Unit
            // 
            this.txtvar27Unit.AutoSize = true;
            this.txtvar27Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar27Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar27Unit.Location = new System.Drawing.Point(88, 42);
            this.txtvar27Unit.Name = "txtvar27Unit";
            this.txtvar27Unit.Size = new System.Drawing.Size(28, 19);
            this.txtvar27Unit.TabIndex = 145;
            this.txtvar27Unit.Text = "oC";
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.White;
            this.panel33.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel33.Controls.Add(this.txtvar26);
            this.panel33.Controls.Add(this.txtvar26Value);
            this.panel33.Controls.Add(this.txtvar26Unit);
            this.panel33.Location = new System.Drawing.Point(200, 36);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(135, 78);
            this.panel33.TabIndex = 173;
            // 
            // txtvar26
            // 
            this.txtvar26.AutoSize = true;
            this.txtvar26.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar26.ForeColor = System.Drawing.Color.Black;
            this.txtvar26.Location = new System.Drawing.Point(39, 10);
            this.txtvar26.Name = "txtvar26";
            this.txtvar26.Size = new System.Drawing.Size(47, 19);
            this.txtvar26.TabIndex = 130;
            this.txtvar26.Text = "ORP:";
            // 
            // txtvar26Value
            // 
            this.txtvar26Value.BackColor = System.Drawing.Color.White;
            this.txtvar26Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar26Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar26Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar26Value.Location = new System.Drawing.Point(8, 42);
            this.txtvar26Value.Name = "txtvar26Value";
            this.txtvar26Value.ReadOnly = true;
            this.txtvar26Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar26Value.TabIndex = 136;
            this.txtvar26Value.Text = "426.17";
            this.txtvar26Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar26Unit
            // 
            this.txtvar26Unit.AutoSize = true;
            this.txtvar26Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar26Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar26Unit.Location = new System.Drawing.Point(86, 42);
            this.txtvar26Unit.Name = "txtvar26Unit";
            this.txtvar26Unit.Size = new System.Drawing.Size(32, 19);
            this.txtvar26Unit.TabIndex = 144;
            this.txtvar26Unit.Text = "mV";
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.White;
            this.panel32.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel32.Controls.Add(this.txtvar25);
            this.panel32.Controls.Add(this.txtvar25Value);
            this.panel32.Controls.Add(this.txtvar25Unit);
            this.panel32.Location = new System.Drawing.Point(200, -79);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(135, 78);
            this.panel32.TabIndex = 172;
            // 
            // txtvar25
            // 
            this.txtvar25.AutoSize = true;
            this.txtvar25.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar25.ForeColor = System.Drawing.Color.Black;
            this.txtvar25.Location = new System.Drawing.Point(49, 12);
            this.txtvar25.Name = "txtvar25";
            this.txtvar25.Size = new System.Drawing.Size(34, 19);
            this.txtvar25.TabIndex = 129;
            this.txtvar25.Text = "pH:";
            // 
            // txtvar25Value
            // 
            this.txtvar25Value.BackColor = System.Drawing.Color.White;
            this.txtvar25Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar25Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar25Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar25Value.Location = new System.Drawing.Point(8, 42);
            this.txtvar25Value.Name = "txtvar25Value";
            this.txtvar25Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar25Value.TabIndex = 135;
            this.txtvar25Value.Text = "7.20";
            this.txtvar25Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar25Unit
            // 
            this.txtvar25Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar25Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar25Unit.Location = new System.Drawing.Point(86, 42);
            this.txtvar25Unit.Name = "txtvar25Unit";
            this.txtvar25Unit.Size = new System.Drawing.Size(31, 19);
            this.txtvar25Unit.TabIndex = 146;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.White;
            this.panel31.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel31.Controls.Add(this.txtvar24);
            this.panel31.Controls.Add(this.txtvar24Value);
            this.panel31.Controls.Add(this.txtvar24Unit);
            this.panel31.Location = new System.Drawing.Point(33, 155);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(135, 78);
            this.panel31.TabIndex = 171;
            // 
            // txtvar24
            // 
            this.txtvar24.AutoSize = true;
            this.txtvar24.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar24.ForeColor = System.Drawing.Color.Black;
            this.txtvar24.Location = new System.Drawing.Point(50, 11);
            this.txtvar24.Name = "txtvar24";
            this.txtvar24.Size = new System.Drawing.Size(35, 19);
            this.txtvar24.TabIndex = 116;
            this.txtvar24.Text = "EC:";
            // 
            // txtvar24Value
            // 
            this.txtvar24Value.BackColor = System.Drawing.Color.White;
            this.txtvar24Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar24Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar24Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar24Value.Location = new System.Drawing.Point(8, 43);
            this.txtvar24Value.Multiline = true;
            this.txtvar24Value.Name = "txtvar24Value";
            this.txtvar24Value.ReadOnly = true;
            this.txtvar24Value.Size = new System.Drawing.Size(64, 21);
            this.txtvar24Value.TabIndex = 122;
            this.txtvar24Value.Text = "117.242";
            this.txtvar24Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar24Unit
            // 
            this.txtvar24Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar24Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar24Unit.Location = new System.Drawing.Point(78, 43);
            this.txtvar24Unit.Name = "txtvar24Unit";
            this.txtvar24Unit.Size = new System.Drawing.Size(48, 20);
            this.txtvar24Unit.TabIndex = 125;
            this.txtvar24Unit.Text = "uS/cm";
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.panel49);
            this.panel50.Controls.Add(this.panel47);
            this.panel50.Controls.Add(this.panel48);
            this.panel50.Location = new System.Drawing.Point(925, 28);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(116, 53);
            this.panel50.TabIndex = 186;
            this.panel50.Visible = false;
            // 
            // panel49
            // 
            this.panel49.BackgroundImage = global::DataLogger.Properties.Resources.Station1;
            this.panel49.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel49.Controls.Add(this.panel40);
            this.panel49.Controls.Add(this.panel41);
            this.panel49.Location = new System.Drawing.Point(26, 299);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(482, 115);
            this.panel49.TabIndex = 185;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.White;
            this.panel40.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel40.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel40.Controls.Add(this.txtvar34);
            this.panel40.Controls.Add(this.txtvar34Value);
            this.panel40.Controls.Add(this.txtvar34Unit);
            this.panel40.Location = new System.Drawing.Point(21, 19);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(135, 78);
            this.panel40.TabIndex = 180;
            // 
            // txtvar34
            // 
            this.txtvar34.AutoSize = true;
            this.txtvar34.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar34.ForeColor = System.Drawing.Color.Black;
            this.txtvar34.Location = new System.Drawing.Point(44, 12);
            this.txtvar34.Name = "txtvar34";
            this.txtvar34.Size = new System.Drawing.Size(38, 19);
            this.txtvar34.TabIndex = 150;
            this.txtvar34.Text = "DO:";
            // 
            // txtvar34Value
            // 
            this.txtvar34Value.BackColor = System.Drawing.Color.White;
            this.txtvar34Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar34Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar34Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar34Value.Location = new System.Drawing.Point(14, 44);
            this.txtvar34Value.Name = "txtvar34Value";
            this.txtvar34Value.ReadOnly = true;
            this.txtvar34Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar34Value.TabIndex = 156;
            this.txtvar34Value.Text = "6.88";
            this.txtvar34Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar34Unit
            // 
            this.txtvar34Unit.AutoSize = true;
            this.txtvar34Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar34Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar34Unit.Location = new System.Drawing.Point(83, 43);
            this.txtvar34Unit.Name = "txtvar34Unit";
            this.txtvar34Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar34Unit.TabIndex = 159;
            this.txtvar34Unit.Text = "mg/L";
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.White;
            this.panel41.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel41.Controls.Add(this.txtvar35);
            this.panel41.Controls.Add(this.txtvar35Value);
            this.panel41.Controls.Add(this.txtvar35Unit);
            this.panel41.Location = new System.Drawing.Point(172, 20);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(135, 78);
            this.panel41.TabIndex = 181;
            // 
            // txtvar35
            // 
            this.txtvar35.AutoSize = true;
            this.txtvar35.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar35.ForeColor = System.Drawing.Color.Black;
            this.txtvar35.Location = new System.Drawing.Point(48, 9);
            this.txtvar35.Name = "txtvar35";
            this.txtvar35.Size = new System.Drawing.Size(42, 19);
            this.txtvar35.TabIndex = 151;
            this.txtvar35.Text = "TSS:";
            // 
            // txtvar35Value
            // 
            this.txtvar35Value.BackColor = System.Drawing.Color.White;
            this.txtvar35Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar35Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar35Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar35Value.Location = new System.Drawing.Point(13, 43);
            this.txtvar35Value.Name = "txtvar35Value";
            this.txtvar35Value.ReadOnly = true;
            this.txtvar35Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar35Value.TabIndex = 157;
            this.txtvar35Value.Text = "9.29";
            this.txtvar35Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar35Unit
            // 
            this.txtvar35Unit.AutoSize = true;
            this.txtvar35Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar35Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar35Unit.Location = new System.Drawing.Point(83, 44);
            this.txtvar35Unit.Name = "txtvar35Unit";
            this.txtvar35Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar35Unit.TabIndex = 160;
            this.txtvar35Unit.Text = "mg/L";
            // 
            // panel47
            // 
            this.panel47.BackgroundImage = global::DataLogger.Properties.Resources.Station1;
            this.panel47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel47.Controls.Add(this.panel35);
            this.panel47.Controls.Add(this.panel36);
            this.panel47.Controls.Add(this.panel24);
            this.panel47.Location = new System.Drawing.Point(26, 16);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(482, 129);
            this.panel47.TabIndex = 183;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.White;
            this.panel35.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel35.Controls.Add(this.txtvar28);
            this.panel35.Controls.Add(this.txtvar28Value);
            this.panel35.Controls.Add(this.txtvar28Unit);
            this.panel35.Location = new System.Drawing.Point(21, 24);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(135, 78);
            this.panel35.TabIndex = 175;
            // 
            // txtvar28
            // 
            this.txtvar28.AutoSize = true;
            this.txtvar28.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar28.ForeColor = System.Drawing.Color.Black;
            this.txtvar28.Location = new System.Drawing.Point(46, 11);
            this.txtvar28.Name = "txtvar28";
            this.txtvar28.Size = new System.Drawing.Size(38, 19);
            this.txtvar28.TabIndex = 132;
            this.txtvar28.Text = "DO:";
            // 
            // txtvar28Value
            // 
            this.txtvar28Value.BackColor = System.Drawing.Color.White;
            this.txtvar28Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar28Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar28Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar28Value.Location = new System.Drawing.Point(11, 45);
            this.txtvar28Value.Name = "txtvar28Value";
            this.txtvar28Value.ReadOnly = true;
            this.txtvar28Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar28Value.TabIndex = 138;
            this.txtvar28Value.Text = "6.88";
            this.txtvar28Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar28Unit
            // 
            this.txtvar28Unit.AutoSize = true;
            this.txtvar28Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar28Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar28Unit.Location = new System.Drawing.Point(81, 45);
            this.txtvar28Unit.Name = "txtvar28Unit";
            this.txtvar28Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar28Unit.TabIndex = 141;
            this.txtvar28Unit.Text = "mg/L";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.White;
            this.panel36.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel36.Controls.Add(this.txtvar29);
            this.panel36.Controls.Add(this.txtvar29Value);
            this.panel36.Controls.Add(this.txtvar29Unit);
            this.panel36.Location = new System.Drawing.Point(172, 24);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(135, 78);
            this.panel36.TabIndex = 176;
            // 
            // txtvar29
            // 
            this.txtvar29.AutoSize = true;
            this.txtvar29.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar29.ForeColor = System.Drawing.Color.Black;
            this.txtvar29.Location = new System.Drawing.Point(44, 9);
            this.txtvar29.Name = "txtvar29";
            this.txtvar29.Size = new System.Drawing.Size(42, 19);
            this.txtvar29.TabIndex = 133;
            this.txtvar29.Text = "TSS:";
            // 
            // txtvar29Value
            // 
            this.txtvar29Value.BackColor = System.Drawing.Color.White;
            this.txtvar29Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar29Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar29Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar29Value.Location = new System.Drawing.Point(9, 40);
            this.txtvar29Value.Name = "txtvar29Value";
            this.txtvar29Value.ReadOnly = true;
            this.txtvar29Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar29Value.TabIndex = 139;
            this.txtvar29Value.Text = "9.29";
            this.txtvar29Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar29Unit
            // 
            this.txtvar29Unit.AutoSize = true;
            this.txtvar29Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar29Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar29Unit.Location = new System.Drawing.Point(80, 40);
            this.txtvar29Unit.Name = "txtvar29Unit";
            this.txtvar29Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar29Unit.TabIndex = 142;
            this.txtvar29Unit.Text = "mg/L";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.White;
            this.panel24.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel24.Controls.Add(this.txtvar30);
            this.panel24.Controls.Add(this.txtvar30Value);
            this.panel24.Controls.Add(this.txtvar30Unit);
            this.panel24.Location = new System.Drawing.Point(324, 24);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(135, 78);
            this.panel24.TabIndex = 165;
            // 
            // txtvar30
            // 
            this.txtvar30.AutoSize = true;
            this.txtvar30.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar30.ForeColor = System.Drawing.Color.Black;
            this.txtvar30.Location = new System.Drawing.Point(50, 10);
            this.txtvar30.Name = "txtvar30";
            this.txtvar30.Size = new System.Drawing.Size(35, 19);
            this.txtvar30.TabIndex = 134;
            this.txtvar30.Text = "EC:";
            // 
            // txtvar30Value
            // 
            this.txtvar30Value.BackColor = System.Drawing.Color.White;
            this.txtvar30Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar30Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar30Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar30Value.Location = new System.Drawing.Point(11, 41);
            this.txtvar30Value.Multiline = true;
            this.txtvar30Value.Name = "txtvar30Value";
            this.txtvar30Value.ReadOnly = true;
            this.txtvar30Value.Size = new System.Drawing.Size(64, 21);
            this.txtvar30Value.TabIndex = 140;
            this.txtvar30Value.Text = "117.242";
            this.txtvar30Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar30Unit
            // 
            this.txtvar30Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar30Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar30Unit.Location = new System.Drawing.Point(78, 42);
            this.txtvar30Unit.Name = "txtvar30Unit";
            this.txtvar30Unit.Size = new System.Drawing.Size(48, 20);
            this.txtvar30Unit.TabIndex = 143;
            this.txtvar30Unit.Text = "uS/cm";
            // 
            // panel48
            // 
            this.panel48.BackgroundImage = global::DataLogger.Properties.Resources.Station1;
            this.panel48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel48.Controls.Add(this.panel37);
            this.panel48.Controls.Add(this.panel38);
            this.panel48.Controls.Add(this.panel39);
            this.panel48.Location = new System.Drawing.Point(26, 168);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(482, 125);
            this.panel48.TabIndex = 184;
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.White;
            this.panel37.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel37.Controls.Add(this.txtvar31);
            this.panel37.Controls.Add(this.txtvar31Value);
            this.panel37.Controls.Add(this.txtvar31Unit);
            this.panel37.Location = new System.Drawing.Point(21, 23);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(135, 78);
            this.panel37.TabIndex = 177;
            // 
            // txtvar31
            // 
            this.txtvar31.AutoSize = true;
            this.txtvar31.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar31.ForeColor = System.Drawing.Color.Black;
            this.txtvar31.Location = new System.Drawing.Point(45, 9);
            this.txtvar31.Name = "txtvar31";
            this.txtvar31.Size = new System.Drawing.Size(34, 19);
            this.txtvar31.TabIndex = 147;
            this.txtvar31.Text = "pH:";
            // 
            // txtvar31Value
            // 
            this.txtvar31Value.BackColor = System.Drawing.Color.White;
            this.txtvar31Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar31Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar31Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar31Value.Location = new System.Drawing.Point(13, 40);
            this.txtvar31Value.Name = "txtvar31Value";
            this.txtvar31Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar31Value.TabIndex = 153;
            this.txtvar31Value.Text = "7.20";
            this.txtvar31Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar31Unit
            // 
            this.txtvar31Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar31Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar31Unit.Location = new System.Drawing.Point(98, 41);
            this.txtvar31Unit.Name = "txtvar31Unit";
            this.txtvar31Unit.Size = new System.Drawing.Size(22, 19);
            this.txtvar31Unit.TabIndex = 164;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.Color.White;
            this.panel38.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel38.Controls.Add(this.txtvar32);
            this.panel38.Controls.Add(this.txtvar32Value);
            this.panel38.Controls.Add(this.txtvar32Unit);
            this.panel38.Location = new System.Drawing.Point(172, 23);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(135, 78);
            this.panel38.TabIndex = 178;
            // 
            // txtvar32
            // 
            this.txtvar32.AutoSize = true;
            this.txtvar32.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar32.ForeColor = System.Drawing.Color.Black;
            this.txtvar32.Location = new System.Drawing.Point(44, 12);
            this.txtvar32.Name = "txtvar32";
            this.txtvar32.Size = new System.Drawing.Size(47, 19);
            this.txtvar32.TabIndex = 148;
            this.txtvar32.Text = "ORP:";
            // 
            // txtvar32Value
            // 
            this.txtvar32Value.BackColor = System.Drawing.Color.White;
            this.txtvar32Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar32Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar32Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar32Value.Location = new System.Drawing.Point(12, 43);
            this.txtvar32Value.Name = "txtvar32Value";
            this.txtvar32Value.ReadOnly = true;
            this.txtvar32Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar32Value.TabIndex = 154;
            this.txtvar32Value.Text = "426.17";
            this.txtvar32Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar32Unit
            // 
            this.txtvar32Unit.AutoSize = true;
            this.txtvar32Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar32Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar32Unit.Location = new System.Drawing.Point(90, 43);
            this.txtvar32Unit.Name = "txtvar32Unit";
            this.txtvar32Unit.Size = new System.Drawing.Size(32, 19);
            this.txtvar32Unit.TabIndex = 162;
            this.txtvar32Unit.Text = "mV";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.White;
            this.panel39.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel39.Controls.Add(this.txtvar33);
            this.panel39.Controls.Add(this.txtvar33Value);
            this.panel39.Controls.Add(this.txtvar33Unit);
            this.panel39.Location = new System.Drawing.Point(324, 23);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(135, 78);
            this.panel39.TabIndex = 179;
            // 
            // txtvar33
            // 
            this.txtvar33.AutoSize = true;
            this.txtvar33.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar33.ForeColor = System.Drawing.Color.Black;
            this.txtvar33.Location = new System.Drawing.Point(41, 11);
            this.txtvar33.Name = "txtvar33";
            this.txtvar33.Size = new System.Drawing.Size(51, 19);
            this.txtvar33.TabIndex = 149;
            this.txtvar33.Text = "Temp:";
            // 
            // txtvar33Value
            // 
            this.txtvar33Value.BackColor = System.Drawing.Color.White;
            this.txtvar33Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar33Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar33Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar33Value.Location = new System.Drawing.Point(11, 45);
            this.txtvar33Value.Name = "txtvar33Value";
            this.txtvar33Value.ReadOnly = true;
            this.txtvar33Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar33Value.TabIndex = 155;
            this.txtvar33Value.Text = "27.65";
            this.txtvar33Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtvar33Unit
            // 
            this.txtvar33Unit.AutoSize = true;
            this.txtvar33Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar33Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar33Unit.Location = new System.Drawing.Point(95, 47);
            this.txtvar33Unit.Name = "txtvar33Unit";
            this.txtvar33Unit.Size = new System.Drawing.Size(28, 19);
            this.txtvar33Unit.TabIndex = 163;
            this.txtvar33Unit.Text = "oC";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.White;
            this.panel25.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel25.Controls.Add(this.txtvar19);
            this.panel25.Controls.Add(this.txtvar19Value);
            this.panel25.Controls.Add(this.txtvar19Unit);
            this.panel25.Location = new System.Drawing.Point(136, 38);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(352, 154);
            this.panel25.TabIndex = 166;
            // 
            // txtvar19
            // 
            this.txtvar19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar19.ForeColor = System.Drawing.Color.Black;
            this.txtvar19.Location = new System.Drawing.Point(37, 28);
            this.txtvar19.Name = "txtvar19";
            this.txtvar19.Size = new System.Drawing.Size(278, 19);
            this.txtvar19.TabIndex = 111;
            this.txtvar19.Text = "pH:";
            this.txtvar19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar19.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar19_Paint);
            // 
            // txtvar19Value
            // 
            this.txtvar19Value.BackColor = System.Drawing.Color.White;
            this.txtvar19Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar19Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar19Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar19Value.Location = new System.Drawing.Point(41, 67);
            this.txtvar19Value.Name = "txtvar19Value";
            this.txtvar19Value.Size = new System.Drawing.Size(274, 19);
            this.txtvar19Value.TabIndex = 117;
            this.txtvar19Value.Text = "7.20";
            this.txtvar19Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtvar19Unit
            // 
            this.txtvar19Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar19Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar19Unit.Location = new System.Drawing.Point(37, 103);
            this.txtvar19Unit.Name = "txtvar19Unit";
            this.txtvar19Unit.Size = new System.Drawing.Size(278, 19);
            this.txtvar19Unit.TabIndex = 128;
            this.txtvar19Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar19Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar19Unit_Paint);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.White;
            this.panel26.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel26.Controls.Add(this.txtvar20);
            this.panel26.Controls.Add(this.txtvar20Value);
            this.panel26.Controls.Add(this.txtvar20Unit);
            this.panel26.Location = new System.Drawing.Point(572, 38);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(352, 154);
            this.panel26.TabIndex = 167;
            // 
            // txtvar20
            // 
            this.txtvar20.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar20.ForeColor = System.Drawing.Color.Black;
            this.txtvar20.Location = new System.Drawing.Point(32, 28);
            this.txtvar20.Name = "txtvar20";
            this.txtvar20.Size = new System.Drawing.Size(285, 19);
            this.txtvar20.TabIndex = 112;
            this.txtvar20.Text = "ORP:";
            this.txtvar20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar20.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar20_Paint);
            // 
            // txtvar20Value
            // 
            this.txtvar20Value.BackColor = System.Drawing.Color.White;
            this.txtvar20Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar20Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar20Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar20Value.Location = new System.Drawing.Point(31, 67);
            this.txtvar20Value.Name = "txtvar20Value";
            this.txtvar20Value.ReadOnly = true;
            this.txtvar20Value.Size = new System.Drawing.Size(286, 19);
            this.txtvar20Value.TabIndex = 118;
            this.txtvar20Value.Text = "426.17";
            this.txtvar20Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtvar20Unit
            // 
            this.txtvar20Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar20Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar20Unit.Location = new System.Drawing.Point(32, 103);
            this.txtvar20Unit.Name = "txtvar20Unit";
            this.txtvar20Unit.Size = new System.Drawing.Size(285, 19);
            this.txtvar20Unit.TabIndex = 126;
            this.txtvar20Unit.Text = "mV";
            this.txtvar20Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar20Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar20Unit_Paint);
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.White;
            this.panel27.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel27.Controls.Add(this.txtvar21);
            this.panel27.Controls.Add(this.txtvar21Value);
            this.panel27.Controls.Add(this.txtvar21Unit);
            this.panel27.Location = new System.Drawing.Point(136, 236);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(352, 154);
            this.panel27.TabIndex = 168;
            // 
            // txtvar21
            // 
            this.txtvar21.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar21.ForeColor = System.Drawing.Color.Black;
            this.txtvar21.Location = new System.Drawing.Point(37, 22);
            this.txtvar21.Name = "txtvar21";
            this.txtvar21.Size = new System.Drawing.Size(278, 19);
            this.txtvar21.TabIndex = 113;
            this.txtvar21.Text = "Temp:";
            this.txtvar21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar21.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar21_Paint);
            // 
            // txtvar21Value
            // 
            this.txtvar21Value.BackColor = System.Drawing.Color.White;
            this.txtvar21Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar21Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar21Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar21Value.Location = new System.Drawing.Point(41, 59);
            this.txtvar21Value.Name = "txtvar21Value";
            this.txtvar21Value.ReadOnly = true;
            this.txtvar21Value.Size = new System.Drawing.Size(274, 19);
            this.txtvar21Value.TabIndex = 119;
            this.txtvar21Value.Text = "27.65";
            this.txtvar21Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtvar21Unit
            // 
            this.txtvar21Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar21Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar21Unit.Location = new System.Drawing.Point(37, 99);
            this.txtvar21Unit.Name = "txtvar21Unit";
            this.txtvar21Unit.Size = new System.Drawing.Size(278, 19);
            this.txtvar21Unit.TabIndex = 127;
            this.txtvar21Unit.Text = "oC";
            this.txtvar21Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar21Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar21Unit_Paint);
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.White;
            this.panel28.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel28.Controls.Add(this.txtvar22);
            this.panel28.Controls.Add(this.txtvar22Value);
            this.panel28.Controls.Add(this.txtvar22Unit);
            this.panel28.Location = new System.Drawing.Point(572, 236);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(352, 154);
            this.panel28.TabIndex = 169;
            // 
            // txtvar22
            // 
            this.txtvar22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar22.ForeColor = System.Drawing.Color.Black;
            this.txtvar22.Location = new System.Drawing.Point(32, 22);
            this.txtvar22.Name = "txtvar22";
            this.txtvar22.Size = new System.Drawing.Size(285, 19);
            this.txtvar22.TabIndex = 114;
            this.txtvar22.Text = "DO:";
            this.txtvar22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar22.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar22_Paint);
            // 
            // txtvar22Value
            // 
            this.txtvar22Value.BackColor = System.Drawing.Color.White;
            this.txtvar22Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar22Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar22Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar22Value.Location = new System.Drawing.Point(31, 59);
            this.txtvar22Value.Name = "txtvar22Value";
            this.txtvar22Value.ReadOnly = true;
            this.txtvar22Value.Size = new System.Drawing.Size(286, 19);
            this.txtvar22Value.TabIndex = 120;
            this.txtvar22Value.Text = "6.88";
            this.txtvar22Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtvar22Unit
            // 
            this.txtvar22Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar22Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar22Unit.Location = new System.Drawing.Point(27, 99);
            this.txtvar22Unit.Name = "txtvar22Unit";
            this.txtvar22Unit.Size = new System.Drawing.Size(290, 19);
            this.txtvar22Unit.TabIndex = 123;
            this.txtvar22Unit.Text = "mg/L";
            this.txtvar22Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar22Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar22Unit_Paint);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Location = new System.Drawing.Point(205, 3);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(40, 10);
            this.panel19.TabIndex = 74;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Location = new System.Drawing.Point(604, 480);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(40, 10);
            this.panel17.TabIndex = 73;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Location = new System.Drawing.Point(744, 10);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(40, 10);
            this.panel15.TabIndex = 72;
            // 
            // btnMPSHistoryData
            // 
            this.btnMPSHistoryData.BackColor = System.Drawing.Color.Transparent;
            this.btnMPSHistoryData.BackgroundImage = global::DataLogger.Properties.Resources._8;
            this.btnMPSHistoryData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMPSHistoryData.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnMPSHistoryData.FlatAppearance.BorderSize = 0;
            this.btnMPSHistoryData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMPSHistoryData.Location = new System.Drawing.Point(449, 507);
            this.btnMPSHistoryData.Name = "btnMPSHistoryData";
            this.btnMPSHistoryData.Size = new System.Drawing.Size(58, 58);
            this.btnMPSHistoryData.TabIndex = 50;
            this.btnMPSHistoryData.UseVisualStyleBackColor = false;
            this.btnMPSHistoryData.Visible = false;
            this.btnMPSHistoryData.Click += new System.EventHandler(this.btnMPSHistoryData_Click);
            // 
            // btnMPS1Hour
            // 
            this.btnMPS1Hour.BackColor = System.Drawing.Color.Transparent;
            this.btnMPS1Hour.BackgroundImage = global::DataLogger.Properties.Resources._11;
            this.btnMPS1Hour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMPS1Hour.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMPS1Hour.FlatAppearance.BorderSize = 0;
            this.btnMPS1Hour.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMPS1Hour.Location = new System.Drawing.Point(305, 507);
            this.btnMPS1Hour.Name = "btnMPS1Hour";
            this.btnMPS1Hour.Size = new System.Drawing.Size(58, 58);
            this.btnMPS1Hour.TabIndex = 50;
            this.btnMPS1Hour.UseVisualStyleBackColor = false;
            this.btnMPS1Hour.Visible = false;
            this.btnMPS1Hour.Click += new System.EventHandler(this.btnMPS1Hour_Click);
            // 
            // btnMPS5Minute
            // 
            this.btnMPS5Minute.BackColor = System.Drawing.Color.Transparent;
            this.btnMPS5Minute.BackgroundImage = global::DataLogger.Properties.Resources._10;
            this.btnMPS5Minute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMPS5Minute.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMPS5Minute.FlatAppearance.BorderSize = 0;
            this.btnMPS5Minute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMPS5Minute.Location = new System.Drawing.Point(168, 507);
            this.btnMPS5Minute.Name = "btnMPS5Minute";
            this.btnMPS5Minute.Size = new System.Drawing.Size(58, 58);
            this.btnMPS5Minute.TabIndex = 50;
            this.btnMPS5Minute.UseVisualStyleBackColor = true;
            this.btnMPS5Minute.Click += new System.EventHandler(this.btnMPS5Minute_Click);
            // 
            // picMPSStatus
            // 
            this.picMPSStatus.BackColor = System.Drawing.Color.Transparent;
            this.picMPSStatus.BackgroundImage = global::DataLogger.Properties.Resources.Normal;
            this.picMPSStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picMPSStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picMPSStatus.Location = new System.Drawing.Point(32, 507);
            this.picMPSStatus.Name = "picMPSStatus";
            this.picMPSStatus.Size = new System.Drawing.Size(58, 58);
            this.picMPSStatus.TabIndex = 59;
            this.picMPSStatus.TabStop = false;
            // 
            // pnLeftSide
            // 
            this.pnLeftSide.AutoSize = true;
            this.pnLeftSide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.pnLeftSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnLeftSide.Controls.Add(this.vprgMonthlyReport);
            this.pnLeftSide.Controls.Add(this.btnMaintenance);
            this.pnLeftSide.Controls.Add(this.btnMonthlyReport);
            this.pnLeftSide.Controls.Add(this.btnSetting);
            this.pnLeftSide.Controls.Add(this.btnUsers);
            this.pnLeftSide.Controls.Add(this.btnAllHistory);
            this.pnLeftSide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLeftSide.Location = new System.Drawing.Point(0, 53);
            this.pnLeftSide.Margin = new System.Windows.Forms.Padding(0);
            this.pnLeftSide.Name = "pnLeftSide";
            this.pnLeftSide.Size = new System.Drawing.Size(78, 608);
            this.pnLeftSide.TabIndex = 1;
            // 
            // vprgMonthlyReport
            // 
            this.vprgMonthlyReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.vprgMonthlyReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vprgMonthlyReport.BorderStyle = VerticalProgressBar.BorderStyles.None;
            this.vprgMonthlyReport.Color = System.Drawing.Color.Maroon;
            this.vprgMonthlyReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.vprgMonthlyReport.Location = new System.Drawing.Point(0, 70);
            this.vprgMonthlyReport.Maximum = 100;
            this.vprgMonthlyReport.Minimum = 0;
            this.vprgMonthlyReport.Name = "vprgMonthlyReport";
            this.vprgMonthlyReport.Size = new System.Drawing.Size(78, 167);
            this.vprgMonthlyReport.Step = 1;
            this.vprgMonthlyReport.Style = VerticalProgressBar.Styles.Solid;
            this.vprgMonthlyReport.TabIndex = 68;
            this.vprgMonthlyReport.Value = 90;
            this.vprgMonthlyReport.Visible = false;
            // 
            // btnMaintenance
            // 
            this.btnMaintenance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnMaintenance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMaintenance.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnMaintenance.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnMaintenance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaintenance.Image = global::DataLogger.Properties.Resources.world_clock;
            this.btnMaintenance.Location = new System.Drawing.Point(0, 352);
            this.btnMaintenance.Name = "btnMaintenance";
            this.btnMaintenance.Size = new System.Drawing.Size(78, 64);
            this.btnMaintenance.TabIndex = 50;
            this.btnMaintenance.UseVisualStyleBackColor = false;
            this.btnMaintenance.Visible = false;
            this.btnMaintenance.Click += new System.EventHandler(this.btnMaintenance_Click);
            // 
            // btnMonthlyReport
            // 
            this.btnMonthlyReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(67)))), ((int)(((byte)(96)))));
            this.btnMonthlyReport.BackgroundImage = global::DataLogger.Properties.Resources.MonthlyReportButton;
            this.btnMonthlyReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMonthlyReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMonthlyReport.FlatAppearance.BorderSize = 0;
            this.btnMonthlyReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMonthlyReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMonthlyReport.Location = new System.Drawing.Point(0, 0);
            this.btnMonthlyReport.Name = "btnMonthlyReport";
            this.btnMonthlyReport.Size = new System.Drawing.Size(78, 70);
            this.btnMonthlyReport.TabIndex = 49;
            this.btnMonthlyReport.UseVisualStyleBackColor = false;
            this.btnMonthlyReport.Click += new System.EventHandler(this.btnMonthlyReport_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSetting.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSetting.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btnSetting.Image = global::DataLogger.Properties.Resources.applications_system_60x60;
            this.btnSetting.Location = new System.Drawing.Point(0, 416);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(78, 64);
            this.btnSetting.TabIndex = 5;
            this.btnSetting.UseVisualStyleBackColor = false;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnUsers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUsers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnUsers.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsers.Image = global::DataLogger.Properties.Resources.user;
            this.btnUsers.Location = new System.Drawing.Point(0, 480);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(78, 64);
            this.btnUsers.TabIndex = 4;
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnAllHistory
            // 
            this.btnAllHistory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnAllHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAllHistory.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAllHistory.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnAllHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllHistory.Image = global::DataLogger.Properties.Resources.maintenance;
            this.btnAllHistory.Location = new System.Drawing.Point(0, 544);
            this.btnAllHistory.Name = "btnAllHistory";
            this.btnAllHistory.Size = new System.Drawing.Size(78, 64);
            this.btnAllHistory.TabIndex = 3;
            this.btnAllHistory.UseVisualStyleBackColor = false;
            this.btnAllHistory.Click += new System.EventHandler(this.btnAllHistory_Click);
            // 
            // pnHeader
            // 
            this.pnHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(56)))), ((int)(((byte)(60)))));
            this.tableLayoutPanel1.SetColumnSpan(this.pnHeader, 2);
            this.pnHeader.Controls.Add(this.label9);
            this.pnHeader.Controls.Add(this.label4);
            this.pnHeader.Controls.Add(this.label3);
            this.pnHeader.Controls.Add(this.panel18);
            this.pnHeader.Controls.Add(this.lblHeadingTime);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Margin = new System.Windows.Forms.Padding(0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(1284, 53);
            this.pnHeader.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Location = new System.Drawing.Point(45, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(273, 31);
            this.label9.TabIndex = 70;
            this.label9.Text = "STATION";
            // 
            // label4
            // 
            this.label4.Image = global::DataLogger.Properties.Resources.clock;
            this.label4.Location = new System.Drawing.Point(915, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 37);
            this.label4.TabIndex = 69;
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.Image = global::DataLogger.Properties.Resources.logo;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Location = new System.Drawing.Point(4, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 37);
            this.label3.TabIndex = 68;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(39)))));
            this.panel18.Controls.Add(this.btnLoginLogout);
            this.panel18.Controls.Add(this.lblLoginDisplayName);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(1106, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(178, 53);
            this.panel18.TabIndex = 65;
            // 
            // btnLoginLogout
            // 
            this.btnLoginLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.login;
            this.btnLoginLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLoginLogout.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLoginLogout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(141)))), ((int)(((byte)(196)))));
            this.btnLoginLogout.FlatAppearance.BorderSize = 0;
            this.btnLoginLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoginLogout.Location = new System.Drawing.Point(129, 0);
            this.btnLoginLogout.Name = "btnLoginLogout";
            this.btnLoginLogout.Size = new System.Drawing.Size(49, 53);
            this.btnLoginLogout.TabIndex = 64;
            this.btnLoginLogout.UseVisualStyleBackColor = false;
            this.btnLoginLogout.Click += new System.EventHandler(this.btnLoginLogout_Click);
            // 
            // lblLoginDisplayName
            // 
            this.lblLoginDisplayName.AutoSize = true;
            this.lblLoginDisplayName.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginDisplayName.ForeColor = System.Drawing.Color.White;
            this.lblLoginDisplayName.Location = new System.Drawing.Point(8, 18);
            this.lblLoginDisplayName.Name = "lblLoginDisplayName";
            this.lblLoginDisplayName.Size = new System.Drawing.Size(120, 17);
            this.lblLoginDisplayName.TabIndex = 51;
            this.lblLoginDisplayName.Text = "Welcome, Guest!";
            // 
            // lblHeadingTime
            // 
            this.lblHeadingTime.AutoSize = true;
            this.lblHeadingTime.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingTime.ForeColor = System.Drawing.Color.White;
            this.lblHeadingTime.Location = new System.Drawing.Point(958, 18);
            this.lblHeadingTime.Name = "lblHeadingTime";
            this.lblHeadingTime.Size = new System.Drawing.Size(143, 17);
            this.lblHeadingTime.TabIndex = 2;
            this.lblHeadingTime.Text = "25-09-2015 12:11:19";
            this.lblHeadingTime.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.152648F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.84735F));
            this.tableLayoutPanel1.Controls.Add(this.pnHeader, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnLeftSide, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel30, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1284, 661);
            this.tableLayoutPanel1.TabIndex = 69;
            // 
            // frmNewMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1284, 661);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmNewMain";
            this.RightToLeftLayout = true;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data Logger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmNewMain_FormClosing);
            this.Load += new System.EventHandler(this.frmNewMain_Load);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.pnSoftwareInfo.ResumeLayout(false);
            this.pnSoftwareInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSamplerTank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel30.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel45.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel43.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel51.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel50.ResumeLayout(false);
            this.panel49.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel37.ResumeLayout(false);
            this.panel37.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel39.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMPSStatus)).EndInit();
            this.pnLeftSide.ResumeLayout(false);
            this.pnHeader.ResumeLayout(false);
            this.pnHeader.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.ComponentModel.BackgroundWorker bgwMonthlyReport;
        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private System.Windows.Forms.Panel panel20;
        public System.Windows.Forms.Label lblMainMenuTitle;
        public System.Windows.Forms.Panel pnSoftwareInfo;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lblSurfaceWaterQuality;
        public System.Windows.Forms.Label lblAutomaticMonitoring;
        public System.Windows.Forms.Label lblThaiNguyenStation;
        public System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.PictureBox pictureBox52;
        public System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.Label lblWaterLevel;
        public System.Windows.Forms.Label lblHeaderNationName;
        public System.Windows.Forms.RichTextBox txtData;
        public System.Windows.Forms.PictureBox picSamplerTank;
        public System.Windows.Forms.Button btnLanguage;
        public System.Windows.Forms.PictureBox pictureBox5;
        public FlatButton listen;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel15;
        public System.Windows.Forms.Panel pnLeftSide;
        public System.Windows.Forms.Button btnMaintenance;
        public System.Windows.Forms.Button btnMonthlyReport;
        public System.Windows.Forms.Button btnSetting;
        public System.Windows.Forms.Button btnUsers;
        public System.Windows.Forms.Button btnAllHistory;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Panel pnHeader;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel18;
        public System.Windows.Forms.Button btnLoginLogout;
        public System.Windows.Forms.Label lblLoginDisplayName;
        public System.Windows.Forms.Label lblHeadingTime;
        public System.Windows.Forms.Label txtvar13Unit;
        public System.Windows.Forms.Label txtvar15Unit;
        public System.Windows.Forms.Label txtvar14Unit;
        public System.Windows.Forms.Label txtvar18Unit;
        public System.Windows.Forms.Label txtvar17Unit;
        public System.Windows.Forms.Label txtvar16Unit;
        public System.Windows.Forms.Label txtvar17;
        public System.Windows.Forms.Label txtvar13;
        public System.Windows.Forms.Label txtvar14;
        public System.Windows.Forms.Label txtvar15;
        public System.Windows.Forms.Label txtvar16;
        public System.Windows.Forms.TextBox txtvar18Value;
        public System.Windows.Forms.Label txtvar18;
        public System.Windows.Forms.TextBox txtvar17Value;
        public System.Windows.Forms.TextBox txtvar13Value;
        public System.Windows.Forms.TextBox txtvar16Value;
        public System.Windows.Forms.TextBox txtvar14Value;
        public System.Windows.Forms.TextBox txtvar15Value;
        public System.Windows.Forms.Label txtvar7Unit;
        public System.Windows.Forms.Label txtvar9Unit;
        public System.Windows.Forms.Label txtvar8Unit;
        public System.Windows.Forms.Label txtvar12Unit;
        public System.Windows.Forms.Label txtvar11Unit;
        public System.Windows.Forms.Label txtvar10Unit;
        public System.Windows.Forms.Label txtvar11;
        public System.Windows.Forms.Label txtvar7;
        public System.Windows.Forms.Label txtvar8;
        public System.Windows.Forms.Label txtvar9;
        public System.Windows.Forms.Label txtvar10;
        public System.Windows.Forms.TextBox txtvar12Value;
        public System.Windows.Forms.Label txtvar12;
        public System.Windows.Forms.TextBox txtvar11Value;
        public System.Windows.Forms.TextBox txtvar7Value;
        public System.Windows.Forms.TextBox txtvar10Value;
        public System.Windows.Forms.TextBox txtvar8Value;
        public System.Windows.Forms.TextBox txtvar9Value;
        public System.Windows.Forms.Label txtvar1Unit;
        public System.Windows.Forms.Label txtvar3Unit;
        public System.Windows.Forms.Label txtvar2Unit;
        public System.Windows.Forms.Label txtvar6Unit;
        public System.Windows.Forms.Label txtvar5Unit;
        public System.Windows.Forms.Label txtvar4Unit;
        public System.Windows.Forms.Label txtvar5;
        public System.Windows.Forms.Button btnMPSHistoryData;
        public System.Windows.Forms.Label txtvar1;
        public System.Windows.Forms.Button btnMPS1Hour;
        public System.Windows.Forms.Label txtvar2;
        public System.Windows.Forms.Button btnMPS5Minute;
        public System.Windows.Forms.Label txtvar3;
        public System.Windows.Forms.PictureBox picMPSStatus;
        public System.Windows.Forms.Label txtvar4;
        public System.Windows.Forms.TextBox txtvar6Value;
        public System.Windows.Forms.Label txtvar6;
        public System.Windows.Forms.TextBox txtvar5Value;
        public System.Windows.Forms.TextBox txtvar1Value;
        public System.Windows.Forms.TextBox txtvar4Value;
        public System.Windows.Forms.TextBox txtvar2Value;
        public System.Windows.Forms.TextBox txtvar3Value;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.Label txtvar19;
        public System.Windows.Forms.Label txtvar31Unit;
        public System.Windows.Forms.TextBox txtvar21Value;
        public System.Windows.Forms.Label txtvar33Unit;
        public System.Windows.Forms.TextBox txtvar20Value;
        public System.Windows.Forms.TextBox txtvar22Value;
        public System.Windows.Forms.Label txtvar32Unit;
        public System.Windows.Forms.TextBox txtvar19Value;
        public System.Windows.Forms.TextBox txtvar23Value;
        public System.Windows.Forms.Label txtvar35Unit;
        public System.Windows.Forms.Label txtvar24;
        public System.Windows.Forms.Label txtvar34Unit;
        public System.Windows.Forms.TextBox txtvar24Value;
        public System.Windows.Forms.Label txtvar35;
        public System.Windows.Forms.Label txtvar22;
        public System.Windows.Forms.Label txtvar31;
        public System.Windows.Forms.Label txtvar21;
        public System.Windows.Forms.Label txtvar32;
        public System.Windows.Forms.Label txtvar20;
        public System.Windows.Forms.Label txtvar33;
        public System.Windows.Forms.Label txtvar23;
        public System.Windows.Forms.Label txtvar34;
        public System.Windows.Forms.Label txtvar22Unit;
        public System.Windows.Forms.Label txtvar23Unit;
        public System.Windows.Forms.Label txtvar24Unit;
        public System.Windows.Forms.TextBox txtvar35Value;
        public System.Windows.Forms.Label txtvar20Unit;
        public System.Windows.Forms.TextBox txtvar31Value;
        public System.Windows.Forms.Label txtvar21Unit;
        public System.Windows.Forms.TextBox txtvar34Value;
        public System.Windows.Forms.Label txtvar19Unit;
        public System.Windows.Forms.TextBox txtvar32Value;
        public System.Windows.Forms.TextBox txtvar27Value;
        public System.Windows.Forms.TextBox txtvar33Value;
        public System.Windows.Forms.TextBox txtvar26Value;
        public System.Windows.Forms.Label txtvar25Unit;
        public System.Windows.Forms.TextBox txtvar28Value;
        public System.Windows.Forms.Label txtvar27Unit;
        public System.Windows.Forms.TextBox txtvar25Value;
        public System.Windows.Forms.Label txtvar26Unit;
        public System.Windows.Forms.TextBox txtvar29Value;
        public System.Windows.Forms.Label txtvar30Unit;
        public System.Windows.Forms.Label txtvar30;
        public System.Windows.Forms.Label txtvar29Unit;
        public System.Windows.Forms.TextBox txtvar30Value;
        public System.Windows.Forms.Label txtvar28Unit;
        public System.Windows.Forms.Label txtvar28;
        public System.Windows.Forms.Label txtvar29;
        public System.Windows.Forms.Label txtvar27;
        public System.Windows.Forms.Label txtvar25;
        public System.Windows.Forms.Label txtvar26;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel50;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label7;
        public VerticalProgressBar.VerticalProgressBar vprgMonthlyReport;
    }
}