﻿namespace DataLogger
{
    partial class frmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.MPS2 = new System.Windows.Forms.TabPage();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.var35ErrorMax = new System.Windows.Forms.TextBox();
            this.var20ErrorMin = new System.Windows.Forms.TextBox();
            this.var34ErrorMax = new System.Windows.Forms.TextBox();
            this.var35ErrorMin = new System.Windows.Forms.TextBox();
            this.var33ErrorMax = new System.Windows.Forms.TextBox();
            this.var34ErrorMin = new System.Windows.Forms.TextBox();
            this.var32ErrorMax = new System.Windows.Forms.TextBox();
            this.var33ErrorMin = new System.Windows.Forms.TextBox();
            this.var31ErrorMax = new System.Windows.Forms.TextBox();
            this.var32ErrorMin = new System.Windows.Forms.TextBox();
            this.var30ErrorMax = new System.Windows.Forms.TextBox();
            this.var31ErrorMin = new System.Windows.Forms.TextBox();
            this.var29ErrorMax = new System.Windows.Forms.TextBox();
            this.var30ErrorMin = new System.Windows.Forms.TextBox();
            this.var28ErrorMax = new System.Windows.Forms.TextBox();
            this.var29ErrorMin = new System.Windows.Forms.TextBox();
            this.var27ErrorMax = new System.Windows.Forms.TextBox();
            this.var28ErrorMin = new System.Windows.Forms.TextBox();
            this.var26ErrorMax = new System.Windows.Forms.TextBox();
            this.var27ErrorMin = new System.Windows.Forms.TextBox();
            this.var25ErrorMax = new System.Windows.Forms.TextBox();
            this.var26ErrorMin = new System.Windows.Forms.TextBox();
            this.var24ErrorMax = new System.Windows.Forms.TextBox();
            this.var25ErrorMin = new System.Windows.Forms.TextBox();
            this.var23ErrorMax = new System.Windows.Forms.TextBox();
            this.var24ErrorMin = new System.Windows.Forms.TextBox();
            this.var22ErrorMax = new System.Windows.Forms.TextBox();
            this.var23ErrorMin = new System.Windows.Forms.TextBox();
            this.var21ErrorMax = new System.Windows.Forms.TextBox();
            this.var22ErrorMin = new System.Windows.Forms.TextBox();
            this.var20ErrorMax = new System.Windows.Forms.TextBox();
            this.var21ErrorMin = new System.Windows.Forms.TextBox();
            this.var19ErrorMax = new System.Windows.Forms.TextBox();
            this.var19ErrorMin = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.var35DisplayName = new System.Windows.Forms.TextBox();
            this.var34DisplayName = new System.Windows.Forms.TextBox();
            this.var33DisplayName = new System.Windows.Forms.TextBox();
            this.var32DisplayName = new System.Windows.Forms.TextBox();
            this.var31DisplayName = new System.Windows.Forms.TextBox();
            this.var35Type = new System.Windows.Forms.TextBox();
            this.var34Type = new System.Windows.Forms.TextBox();
            this.var33Type = new System.Windows.Forms.TextBox();
            this.var32Type = new System.Windows.Forms.TextBox();
            this.var31Type = new System.Windows.Forms.TextBox();
            this.var35StatusColumn = new System.Windows.Forms.TextBox();
            this.var34StatusColumn = new System.Windows.Forms.TextBox();
            this.var33StatusColumn = new System.Windows.Forms.TextBox();
            this.var32StatusColumn = new System.Windows.Forms.TextBox();
            this.var31StatusColumn = new System.Windows.Forms.TextBox();
            this.var35ValueColumn = new System.Windows.Forms.TextBox();
            this.var35Unit = new System.Windows.Forms.TextBox();
            this.var34ValueColumn = new System.Windows.Forms.TextBox();
            this.var34Unit = new System.Windows.Forms.TextBox();
            this.var33ValueColumn = new System.Windows.Forms.TextBox();
            this.var33Unit = new System.Windows.Forms.TextBox();
            this.var32ValueColumn = new System.Windows.Forms.TextBox();
            this.var32Unit = new System.Windows.Forms.TextBox();
            this.var31ValueColumn = new System.Windows.Forms.TextBox();
            this.var31Unit = new System.Windows.Forms.TextBox();
            this.var35Offset = new System.Windows.Forms.TextBox();
            this.var34Offset = new System.Windows.Forms.TextBox();
            this.var33Offset = new System.Windows.Forms.TextBox();
            this.var32Offset = new System.Windows.Forms.TextBox();
            this.var31Offset = new System.Windows.Forms.TextBox();
            this.var35OutputMax = new System.Windows.Forms.TextBox();
            this.var35OutputMin = new System.Windows.Forms.TextBox();
            this.var35InputMax = new System.Windows.Forms.TextBox();
            this.var35InputMin = new System.Windows.Forms.TextBox();
            this.var35Channel = new System.Windows.Forms.TextBox();
            this.var34OutputMax = new System.Windows.Forms.TextBox();
            this.var34OutputMin = new System.Windows.Forms.TextBox();
            this.var34InputMax = new System.Windows.Forms.TextBox();
            this.var34InputMin = new System.Windows.Forms.TextBox();
            this.var34Channel = new System.Windows.Forms.TextBox();
            this.var33OutputMax = new System.Windows.Forms.TextBox();
            this.var33OutputMin = new System.Windows.Forms.TextBox();
            this.var33InputMax = new System.Windows.Forms.TextBox();
            this.var33InputMin = new System.Windows.Forms.TextBox();
            this.var33Channel = new System.Windows.Forms.TextBox();
            this.var32OutputMax = new System.Windows.Forms.TextBox();
            this.var32OutputMin = new System.Windows.Forms.TextBox();
            this.var32InputMax = new System.Windows.Forms.TextBox();
            this.var32InputMin = new System.Windows.Forms.TextBox();
            this.var32Channel = new System.Windows.Forms.TextBox();
            this.var31OutputMax = new System.Windows.Forms.TextBox();
            this.var31OutputMin = new System.Windows.Forms.TextBox();
            this.var31InputMax = new System.Windows.Forms.TextBox();
            this.var31InputMin = new System.Windows.Forms.TextBox();
            this.var31Channel = new System.Windows.Forms.TextBox();
            this.var30DisplayName = new System.Windows.Forms.TextBox();
            this.var29DisplayName = new System.Windows.Forms.TextBox();
            this.var28DisplayName = new System.Windows.Forms.TextBox();
            this.var27DisplayName = new System.Windows.Forms.TextBox();
            this.var26DisplayName = new System.Windows.Forms.TextBox();
            this.var25DisplayName = new System.Windows.Forms.TextBox();
            this.var30Type = new System.Windows.Forms.TextBox();
            this.var29Type = new System.Windows.Forms.TextBox();
            this.var28Type = new System.Windows.Forms.TextBox();
            this.var27Type = new System.Windows.Forms.TextBox();
            this.var26Type = new System.Windows.Forms.TextBox();
            this.var25Type = new System.Windows.Forms.TextBox();
            this.var30StatusColumn = new System.Windows.Forms.TextBox();
            this.var29StatusColumn = new System.Windows.Forms.TextBox();
            this.var28StatusColumn = new System.Windows.Forms.TextBox();
            this.var27StatusColumn = new System.Windows.Forms.TextBox();
            this.var26StatusColumn = new System.Windows.Forms.TextBox();
            this.var25StatusColumn = new System.Windows.Forms.TextBox();
            this.var30ValueColumn = new System.Windows.Forms.TextBox();
            this.var30Unit = new System.Windows.Forms.TextBox();
            this.var29ValueColumn = new System.Windows.Forms.TextBox();
            this.var29Unit = new System.Windows.Forms.TextBox();
            this.var28ValueColumn = new System.Windows.Forms.TextBox();
            this.var28Unit = new System.Windows.Forms.TextBox();
            this.var27ValueColumn = new System.Windows.Forms.TextBox();
            this.var27Unit = new System.Windows.Forms.TextBox();
            this.var26ValueColumn = new System.Windows.Forms.TextBox();
            this.var26Unit = new System.Windows.Forms.TextBox();
            this.var25ValueColumn = new System.Windows.Forms.TextBox();
            this.var25Unit = new System.Windows.Forms.TextBox();
            this.var30Offset = new System.Windows.Forms.TextBox();
            this.var29Offset = new System.Windows.Forms.TextBox();
            this.var28Offset = new System.Windows.Forms.TextBox();
            this.var27Offset = new System.Windows.Forms.TextBox();
            this.var26Offset = new System.Windows.Forms.TextBox();
            this.var25Offset = new System.Windows.Forms.TextBox();
            this.var30OutputMax = new System.Windows.Forms.TextBox();
            this.var30OutputMin = new System.Windows.Forms.TextBox();
            this.var30InputMax = new System.Windows.Forms.TextBox();
            this.var30InputMin = new System.Windows.Forms.TextBox();
            this.var30Channel = new System.Windows.Forms.TextBox();
            this.var29OutputMax = new System.Windows.Forms.TextBox();
            this.var29OutputMin = new System.Windows.Forms.TextBox();
            this.var29InputMax = new System.Windows.Forms.TextBox();
            this.var29InputMin = new System.Windows.Forms.TextBox();
            this.var29Channel = new System.Windows.Forms.TextBox();
            this.var28OutputMax = new System.Windows.Forms.TextBox();
            this.var28OutputMin = new System.Windows.Forms.TextBox();
            this.var28InputMax = new System.Windows.Forms.TextBox();
            this.var28InputMin = new System.Windows.Forms.TextBox();
            this.var28Channel = new System.Windows.Forms.TextBox();
            this.var27OutputMax = new System.Windows.Forms.TextBox();
            this.var27OutputMin = new System.Windows.Forms.TextBox();
            this.var27InputMax = new System.Windows.Forms.TextBox();
            this.var27InputMin = new System.Windows.Forms.TextBox();
            this.var27Channel = new System.Windows.Forms.TextBox();
            this.var26OutputMax = new System.Windows.Forms.TextBox();
            this.var26OutputMin = new System.Windows.Forms.TextBox();
            this.var26InputMax = new System.Windows.Forms.TextBox();
            this.var26InputMin = new System.Windows.Forms.TextBox();
            this.var26Channel = new System.Windows.Forms.TextBox();
            this.var25OutputMax = new System.Windows.Forms.TextBox();
            this.var25OutputMin = new System.Windows.Forms.TextBox();
            this.var25InputMax = new System.Windows.Forms.TextBox();
            this.var25InputMin = new System.Windows.Forms.TextBox();
            this.var25Channel = new System.Windows.Forms.TextBox();
            this.var24DisplayName = new System.Windows.Forms.TextBox();
            this.var23DisplayName = new System.Windows.Forms.TextBox();
            this.var22DisplayName = new System.Windows.Forms.TextBox();
            this.var21DisplayName = new System.Windows.Forms.TextBox();
            this.var20DisplayName = new System.Windows.Forms.TextBox();
            this.var19DisplayName = new System.Windows.Forms.TextBox();
            this.var24Type = new System.Windows.Forms.TextBox();
            this.var23Type = new System.Windows.Forms.TextBox();
            this.var22Type = new System.Windows.Forms.TextBox();
            this.var21Type = new System.Windows.Forms.TextBox();
            this.var20Type = new System.Windows.Forms.TextBox();
            this.var19Type = new System.Windows.Forms.TextBox();
            this.var24StatusColumn = new System.Windows.Forms.TextBox();
            this.var23StatusColumn = new System.Windows.Forms.TextBox();
            this.var22StatusColumn = new System.Windows.Forms.TextBox();
            this.var21StatusColumn = new System.Windows.Forms.TextBox();
            this.var20StatusColumn = new System.Windows.Forms.TextBox();
            this.var19StatusColumn = new System.Windows.Forms.TextBox();
            this.var24ValueColumn = new System.Windows.Forms.TextBox();
            this.var24Unit = new System.Windows.Forms.TextBox();
            this.var23ValueColumn = new System.Windows.Forms.TextBox();
            this.var23Unit = new System.Windows.Forms.TextBox();
            this.var22ValueColumn = new System.Windows.Forms.TextBox();
            this.var22Unit = new System.Windows.Forms.TextBox();
            this.var21ValueColumn = new System.Windows.Forms.TextBox();
            this.var21Unit = new System.Windows.Forms.TextBox();
            this.var20ValueColumn = new System.Windows.Forms.TextBox();
            this.var20Unit = new System.Windows.Forms.TextBox();
            this.var19ValueColumn = new System.Windows.Forms.TextBox();
            this.var19Unit = new System.Windows.Forms.TextBox();
            this.var24Offset = new System.Windows.Forms.TextBox();
            this.var23Offset = new System.Windows.Forms.TextBox();
            this.var22Offset = new System.Windows.Forms.TextBox();
            this.var21Offset = new System.Windows.Forms.TextBox();
            this.var20Offset = new System.Windows.Forms.TextBox();
            this.var19Offset = new System.Windows.Forms.TextBox();
            this.var24OutputMax = new System.Windows.Forms.TextBox();
            this.var24OutputMin = new System.Windows.Forms.TextBox();
            this.var24InputMax = new System.Windows.Forms.TextBox();
            this.var24InputMin = new System.Windows.Forms.TextBox();
            this.var24Channel = new System.Windows.Forms.TextBox();
            this.var23OutputMax = new System.Windows.Forms.TextBox();
            this.var23OutputMin = new System.Windows.Forms.TextBox();
            this.var23InputMax = new System.Windows.Forms.TextBox();
            this.var23InputMin = new System.Windows.Forms.TextBox();
            this.var23Channel = new System.Windows.Forms.TextBox();
            this.var22OutputMax = new System.Windows.Forms.TextBox();
            this.var22OutputMin = new System.Windows.Forms.TextBox();
            this.var22InputMax = new System.Windows.Forms.TextBox();
            this.var22InputMin = new System.Windows.Forms.TextBox();
            this.var22Channel = new System.Windows.Forms.TextBox();
            this.var21OutputMax = new System.Windows.Forms.TextBox();
            this.var21OutputMin = new System.Windows.Forms.TextBox();
            this.var21InputMax = new System.Windows.Forms.TextBox();
            this.var21InputMin = new System.Windows.Forms.TextBox();
            this.var21Channel = new System.Windows.Forms.TextBox();
            this.var20OutputMax = new System.Windows.Forms.TextBox();
            this.var20OutputMin = new System.Windows.Forms.TextBox();
            this.var20InputMax = new System.Windows.Forms.TextBox();
            this.var20InputMin = new System.Windows.Forms.TextBox();
            this.var20Channel = new System.Windows.Forms.TextBox();
            this.var19OutputMax = new System.Windows.Forms.TextBox();
            this.var19OutputMin = new System.Windows.Forms.TextBox();
            this.var19InputMax = new System.Windows.Forms.TextBox();
            this.var19InputMin = new System.Windows.Forms.TextBox();
            this.var19Channel = new System.Windows.Forms.TextBox();
            this.var35Module = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.var34Module = new System.Windows.Forms.ComboBox();
            this.var33Module = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.var32Module = new System.Windows.Forms.ComboBox();
            this.var31Module = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.var30Module = new System.Windows.Forms.ComboBox();
            this.var29Module = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.var28Module = new System.Windows.Forms.ComboBox();
            this.var27Module = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.var26Module = new System.Windows.Forms.ComboBox();
            this.var25Module = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.var24Module = new System.Windows.Forms.ComboBox();
            this.var23Module = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.var22Module = new System.Windows.Forms.ComboBox();
            this.var21Module = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.var20Module = new System.Windows.Forms.ComboBox();
            this.var19Module = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cbModule = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbFlag = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textPwd = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.dtpLastedValue = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.var18ErrorMax = new System.Windows.Forms.TextBox();
            this.var18ErrorMin = new System.Windows.Forms.TextBox();
            this.var17ErrorMax = new System.Windows.Forms.TextBox();
            this.var17ErrorMin = new System.Windows.Forms.TextBox();
            this.var16ErrorMax = new System.Windows.Forms.TextBox();
            this.var16ErrorMin = new System.Windows.Forms.TextBox();
            this.var15ErrorMax = new System.Windows.Forms.TextBox();
            this.var15ErrorMin = new System.Windows.Forms.TextBox();
            this.var14ErrorMax = new System.Windows.Forms.TextBox();
            this.var14ErrorMin = new System.Windows.Forms.TextBox();
            this.var13ErrorMax = new System.Windows.Forms.TextBox();
            this.var13ErrorMin = new System.Windows.Forms.TextBox();
            this.var12ErrorMax = new System.Windows.Forms.TextBox();
            this.var12ErrorMin = new System.Windows.Forms.TextBox();
            this.var11ErrorMax = new System.Windows.Forms.TextBox();
            this.var11ErrorMin = new System.Windows.Forms.TextBox();
            this.var10ErrorMax = new System.Windows.Forms.TextBox();
            this.var10ErrorMin = new System.Windows.Forms.TextBox();
            this.var9ErrorMax = new System.Windows.Forms.TextBox();
            this.var9ErrorMin = new System.Windows.Forms.TextBox();
            this.var8ErrorMax = new System.Windows.Forms.TextBox();
            this.var8ErrorMin = new System.Windows.Forms.TextBox();
            this.var7ErrorMax = new System.Windows.Forms.TextBox();
            this.var7ErrorMin = new System.Windows.Forms.TextBox();
            this.var6ErrorMax = new System.Windows.Forms.TextBox();
            this.var6ErrorMin = new System.Windows.Forms.TextBox();
            this.var5ErrorMax = new System.Windows.Forms.TextBox();
            this.var5ErrorMin = new System.Windows.Forms.TextBox();
            this.var4ErrorMax = new System.Windows.Forms.TextBox();
            this.var4ErrorMin = new System.Windows.Forms.TextBox();
            this.var3ErrorMax = new System.Windows.Forms.TextBox();
            this.var3ErrorMin = new System.Windows.Forms.TextBox();
            this.var2ErrorMax = new System.Windows.Forms.TextBox();
            this.var2ErrorMin = new System.Windows.Forms.TextBox();
            this.var1ErrorMax = new System.Windows.Forms.TextBox();
            this.var1ErrorMin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.var18DisplayName = new System.Windows.Forms.TextBox();
            this.var17DisplayName = new System.Windows.Forms.TextBox();
            this.var16DisplayName = new System.Windows.Forms.TextBox();
            this.var15DisplayName = new System.Windows.Forms.TextBox();
            this.var14DisplayName = new System.Windows.Forms.TextBox();
            this.var13DisplayName = new System.Windows.Forms.TextBox();
            this.var18Type = new System.Windows.Forms.TextBox();
            this.var17Type = new System.Windows.Forms.TextBox();
            this.var16Type = new System.Windows.Forms.TextBox();
            this.var15Type = new System.Windows.Forms.TextBox();
            this.var14Type = new System.Windows.Forms.TextBox();
            this.var13Type = new System.Windows.Forms.TextBox();
            this.var18StatusColumn = new System.Windows.Forms.TextBox();
            this.var17StatusColumn = new System.Windows.Forms.TextBox();
            this.var16StatusColumn = new System.Windows.Forms.TextBox();
            this.var15StatusColumn = new System.Windows.Forms.TextBox();
            this.var14StatusColumn = new System.Windows.Forms.TextBox();
            this.var13StatusColumn = new System.Windows.Forms.TextBox();
            this.var18ValueColumn = new System.Windows.Forms.TextBox();
            this.var18Unit = new System.Windows.Forms.TextBox();
            this.var17ValueColumn = new System.Windows.Forms.TextBox();
            this.var17Unit = new System.Windows.Forms.TextBox();
            this.var16ValueColumn = new System.Windows.Forms.TextBox();
            this.var16Unit = new System.Windows.Forms.TextBox();
            this.var15ValueColumn = new System.Windows.Forms.TextBox();
            this.var15Unit = new System.Windows.Forms.TextBox();
            this.var14ValueColumn = new System.Windows.Forms.TextBox();
            this.var14Unit = new System.Windows.Forms.TextBox();
            this.var13ValueColumn = new System.Windows.Forms.TextBox();
            this.var13Unit = new System.Windows.Forms.TextBox();
            this.var18Offset = new System.Windows.Forms.TextBox();
            this.var17Offset = new System.Windows.Forms.TextBox();
            this.var16Offset = new System.Windows.Forms.TextBox();
            this.var15Offset = new System.Windows.Forms.TextBox();
            this.var14Offset = new System.Windows.Forms.TextBox();
            this.var13Offset = new System.Windows.Forms.TextBox();
            this.var18OutputMax = new System.Windows.Forms.TextBox();
            this.var18OutputMin = new System.Windows.Forms.TextBox();
            this.var18InputMax = new System.Windows.Forms.TextBox();
            this.var18InputMin = new System.Windows.Forms.TextBox();
            this.var18Channel = new System.Windows.Forms.TextBox();
            this.var17OutputMax = new System.Windows.Forms.TextBox();
            this.var17OutputMin = new System.Windows.Forms.TextBox();
            this.var17InputMax = new System.Windows.Forms.TextBox();
            this.var17InputMin = new System.Windows.Forms.TextBox();
            this.var17Channel = new System.Windows.Forms.TextBox();
            this.var16OutputMax = new System.Windows.Forms.TextBox();
            this.var16OutputMin = new System.Windows.Forms.TextBox();
            this.var16InputMax = new System.Windows.Forms.TextBox();
            this.var16InputMin = new System.Windows.Forms.TextBox();
            this.var16Channel = new System.Windows.Forms.TextBox();
            this.var15OutputMax = new System.Windows.Forms.TextBox();
            this.var15OutputMin = new System.Windows.Forms.TextBox();
            this.var15InputMax = new System.Windows.Forms.TextBox();
            this.var15InputMin = new System.Windows.Forms.TextBox();
            this.var15Channel = new System.Windows.Forms.TextBox();
            this.var14OutputMax = new System.Windows.Forms.TextBox();
            this.var14OutputMin = new System.Windows.Forms.TextBox();
            this.var14InputMax = new System.Windows.Forms.TextBox();
            this.var14InputMin = new System.Windows.Forms.TextBox();
            this.var14Channel = new System.Windows.Forms.TextBox();
            this.var13OutputMax = new System.Windows.Forms.TextBox();
            this.var13OutputMin = new System.Windows.Forms.TextBox();
            this.var13InputMax = new System.Windows.Forms.TextBox();
            this.var13InputMin = new System.Windows.Forms.TextBox();
            this.var13Channel = new System.Windows.Forms.TextBox();
            this.var12DisplayName = new System.Windows.Forms.TextBox();
            this.var11DisplayName = new System.Windows.Forms.TextBox();
            this.var10DisplayName = new System.Windows.Forms.TextBox();
            this.var9DisplayName = new System.Windows.Forms.TextBox();
            this.var8DisplayName = new System.Windows.Forms.TextBox();
            this.var7DisplayName = new System.Windows.Forms.TextBox();
            this.var12Type = new System.Windows.Forms.TextBox();
            this.var11Type = new System.Windows.Forms.TextBox();
            this.var10Type = new System.Windows.Forms.TextBox();
            this.var9Type = new System.Windows.Forms.TextBox();
            this.var8Type = new System.Windows.Forms.TextBox();
            this.var7Type = new System.Windows.Forms.TextBox();
            this.var12StatusColumn = new System.Windows.Forms.TextBox();
            this.var11StatusColumn = new System.Windows.Forms.TextBox();
            this.var10StatusColumn = new System.Windows.Forms.TextBox();
            this.var9StatusColumn = new System.Windows.Forms.TextBox();
            this.var8StatusColumn = new System.Windows.Forms.TextBox();
            this.var7StatusColumn = new System.Windows.Forms.TextBox();
            this.var12ValueColumn = new System.Windows.Forms.TextBox();
            this.var12Unit = new System.Windows.Forms.TextBox();
            this.var11ValueColumn = new System.Windows.Forms.TextBox();
            this.var11Unit = new System.Windows.Forms.TextBox();
            this.var10ValueColumn = new System.Windows.Forms.TextBox();
            this.var10Unit = new System.Windows.Forms.TextBox();
            this.var9ValueColumn = new System.Windows.Forms.TextBox();
            this.var9Unit = new System.Windows.Forms.TextBox();
            this.var8ValueColumn = new System.Windows.Forms.TextBox();
            this.var8Unit = new System.Windows.Forms.TextBox();
            this.var7ValueColumn = new System.Windows.Forms.TextBox();
            this.var7Unit = new System.Windows.Forms.TextBox();
            this.var12Offset = new System.Windows.Forms.TextBox();
            this.var11Offset = new System.Windows.Forms.TextBox();
            this.var10Offset = new System.Windows.Forms.TextBox();
            this.var9Offset = new System.Windows.Forms.TextBox();
            this.var8Offset = new System.Windows.Forms.TextBox();
            this.var7Offset = new System.Windows.Forms.TextBox();
            this.var12OutputMax = new System.Windows.Forms.TextBox();
            this.var12OutputMin = new System.Windows.Forms.TextBox();
            this.var12InputMax = new System.Windows.Forms.TextBox();
            this.var12InputMin = new System.Windows.Forms.TextBox();
            this.var12Channel = new System.Windows.Forms.TextBox();
            this.var11OutputMax = new System.Windows.Forms.TextBox();
            this.var11OutputMin = new System.Windows.Forms.TextBox();
            this.var11InputMax = new System.Windows.Forms.TextBox();
            this.var11InputMin = new System.Windows.Forms.TextBox();
            this.var11Channel = new System.Windows.Forms.TextBox();
            this.var10OutputMax = new System.Windows.Forms.TextBox();
            this.var10OutputMin = new System.Windows.Forms.TextBox();
            this.var10InputMax = new System.Windows.Forms.TextBox();
            this.var10InputMin = new System.Windows.Forms.TextBox();
            this.var10Channel = new System.Windows.Forms.TextBox();
            this.var9OutputMax = new System.Windows.Forms.TextBox();
            this.var9OutputMin = new System.Windows.Forms.TextBox();
            this.var9InputMax = new System.Windows.Forms.TextBox();
            this.var9InputMin = new System.Windows.Forms.TextBox();
            this.var9Channel = new System.Windows.Forms.TextBox();
            this.var8OutputMax = new System.Windows.Forms.TextBox();
            this.var8OutputMin = new System.Windows.Forms.TextBox();
            this.var8InputMax = new System.Windows.Forms.TextBox();
            this.var8InputMin = new System.Windows.Forms.TextBox();
            this.var8Channel = new System.Windows.Forms.TextBox();
            this.var7OutputMax = new System.Windows.Forms.TextBox();
            this.var7OutputMin = new System.Windows.Forms.TextBox();
            this.var7InputMax = new System.Windows.Forms.TextBox();
            this.var7InputMin = new System.Windows.Forms.TextBox();
            this.var7Channel = new System.Windows.Forms.TextBox();
            this.var6DisplayName = new System.Windows.Forms.TextBox();
            this.var5DisplayName = new System.Windows.Forms.TextBox();
            this.var4DisplayName = new System.Windows.Forms.TextBox();
            this.var3DisplayName = new System.Windows.Forms.TextBox();
            this.var2DisplayName = new System.Windows.Forms.TextBox();
            this.var1DisplayName = new System.Windows.Forms.TextBox();
            this.var6Type = new System.Windows.Forms.TextBox();
            this.var5Type = new System.Windows.Forms.TextBox();
            this.var4Type = new System.Windows.Forms.TextBox();
            this.var3Type = new System.Windows.Forms.TextBox();
            this.var2Type = new System.Windows.Forms.TextBox();
            this.var1Type = new System.Windows.Forms.TextBox();
            this.var6StatusColumn = new System.Windows.Forms.TextBox();
            this.var5StatusColumn = new System.Windows.Forms.TextBox();
            this.var4StatusColumn = new System.Windows.Forms.TextBox();
            this.var3StatusColumn = new System.Windows.Forms.TextBox();
            this.var2StatusColumn = new System.Windows.Forms.TextBox();
            this.var1StatusColumn = new System.Windows.Forms.TextBox();
            this.var6ValueColumn = new System.Windows.Forms.TextBox();
            this.var6Unit = new System.Windows.Forms.TextBox();
            this.var5ValueColumn = new System.Windows.Forms.TextBox();
            this.var5Unit = new System.Windows.Forms.TextBox();
            this.var4ValueColumn = new System.Windows.Forms.TextBox();
            this.var4Unit = new System.Windows.Forms.TextBox();
            this.var3ValueColumn = new System.Windows.Forms.TextBox();
            this.var3Unit = new System.Windows.Forms.TextBox();
            this.var2ValueColumn = new System.Windows.Forms.TextBox();
            this.var2Unit = new System.Windows.Forms.TextBox();
            this.var1ValueColumn = new System.Windows.Forms.TextBox();
            this.var1Unit = new System.Windows.Forms.TextBox();
            this.var6Offset = new System.Windows.Forms.TextBox();
            this.var5Offset = new System.Windows.Forms.TextBox();
            this.var4Offset = new System.Windows.Forms.TextBox();
            this.var3Offset = new System.Windows.Forms.TextBox();
            this.var2Offset = new System.Windows.Forms.TextBox();
            this.var1Offset = new System.Windows.Forms.TextBox();
            this.var6OutputMax = new System.Windows.Forms.TextBox();
            this.var6OutputMin = new System.Windows.Forms.TextBox();
            this.var6InputMax = new System.Windows.Forms.TextBox();
            this.var6InputMin = new System.Windows.Forms.TextBox();
            this.var6Channel = new System.Windows.Forms.TextBox();
            this.var5OutputMax = new System.Windows.Forms.TextBox();
            this.var5OutputMin = new System.Windows.Forms.TextBox();
            this.var5InputMax = new System.Windows.Forms.TextBox();
            this.var5InputMin = new System.Windows.Forms.TextBox();
            this.var5Channel = new System.Windows.Forms.TextBox();
            this.var4OutputMax = new System.Windows.Forms.TextBox();
            this.var4OutputMin = new System.Windows.Forms.TextBox();
            this.var4InputMax = new System.Windows.Forms.TextBox();
            this.var4InputMin = new System.Windows.Forms.TextBox();
            this.var4Channel = new System.Windows.Forms.TextBox();
            this.var3OutputMax = new System.Windows.Forms.TextBox();
            this.var3OutputMin = new System.Windows.Forms.TextBox();
            this.var3InputMax = new System.Windows.Forms.TextBox();
            this.var3InputMin = new System.Windows.Forms.TextBox();
            this.var3Channel = new System.Windows.Forms.TextBox();
            this.var2OutputMax = new System.Windows.Forms.TextBox();
            this.var2OutputMin = new System.Windows.Forms.TextBox();
            this.var2InputMax = new System.Windows.Forms.TextBox();
            this.var2InputMin = new System.Windows.Forms.TextBox();
            this.var2Channel = new System.Windows.Forms.TextBox();
            this.var1OutputMax = new System.Windows.Forms.TextBox();
            this.var1OutputMin = new System.Windows.Forms.TextBox();
            this.var1InputMax = new System.Windows.Forms.TextBox();
            this.var1InputMin = new System.Windows.Forms.TextBox();
            this.var1Channel = new System.Windows.Forms.TextBox();
            this.var18Module = new System.Windows.Forms.ComboBox();
            this.var17Module = new System.Windows.Forms.ComboBox();
            this.var18 = new System.Windows.Forms.Label();
            this.var17 = new System.Windows.Forms.Label();
            this.var16Module = new System.Windows.Forms.ComboBox();
            this.var15Module = new System.Windows.Forms.ComboBox();
            this.var16 = new System.Windows.Forms.Label();
            this.var15 = new System.Windows.Forms.Label();
            this.var14Module = new System.Windows.Forms.ComboBox();
            this.var13Module = new System.Windows.Forms.ComboBox();
            this.var14 = new System.Windows.Forms.Label();
            this.var13 = new System.Windows.Forms.Label();
            this.var12Module = new System.Windows.Forms.ComboBox();
            this.var11Module = new System.Windows.Forms.ComboBox();
            this.var12 = new System.Windows.Forms.Label();
            this.var11 = new System.Windows.Forms.Label();
            this.var10Module = new System.Windows.Forms.ComboBox();
            this.var9Module = new System.Windows.Forms.ComboBox();
            this.var10 = new System.Windows.Forms.Label();
            this.var9 = new System.Windows.Forms.Label();
            this.var8Module = new System.Windows.Forms.ComboBox();
            this.var7Module = new System.Windows.Forms.ComboBox();
            this.var8 = new System.Windows.Forms.Label();
            this.var7 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.Unit = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.var6Module = new System.Windows.Forms.ComboBox();
            this.var5Module = new System.Windows.Forms.ComboBox();
            this.var6 = new System.Windows.Forms.Label();
            this.var5 = new System.Windows.Forms.Label();
            this.var4Module = new System.Windows.Forms.ComboBox();
            this.var3Module = new System.Windows.Forms.ComboBox();
            this.var4 = new System.Windows.Forms.Label();
            this.var3 = new System.Windows.Forms.Label();
            this.var2Module = new System.Windows.Forms.ComboBox();
            this.var1Module = new System.Windows.Forms.ComboBox();
            this.var2 = new System.Windows.Forms.Label();
            this.var1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnSOCKET = new System.Windows.Forms.Button();
            this.txtSocketPort = new System.Windows.Forms.TextBox();
            this.txtStationID = new System.Windows.Forms.TextBox();
            this.txtStationName = new System.Windows.Forms.TextBox();
            this.lblSocketPort = new System.Windows.Forms.Label();
            this.lblStationID = new System.Windows.Forms.Label();
            this.lblStationName = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.MPS2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = global::DataLogger.Properties.Resources.Save_Button_1;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(1046, 656);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 49);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.btnRefresh.BackgroundImage = global::DataLogger.Properties.Resources.Refesh_button;
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRefresh.FlatAppearance.BorderSize = 0;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.Window;
            this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefresh.Location = new System.Drawing.Point(923, 656);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(117, 49);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // MPS2
            // 
            this.MPS2.AutoScroll = true;
            this.MPS2.Controls.Add(this.label46);
            this.MPS2.Controls.Add(this.label45);
            this.MPS2.Controls.Add(this.var35ErrorMax);
            this.MPS2.Controls.Add(this.var20ErrorMin);
            this.MPS2.Controls.Add(this.var34ErrorMax);
            this.MPS2.Controls.Add(this.var35ErrorMin);
            this.MPS2.Controls.Add(this.var33ErrorMax);
            this.MPS2.Controls.Add(this.var34ErrorMin);
            this.MPS2.Controls.Add(this.var32ErrorMax);
            this.MPS2.Controls.Add(this.var33ErrorMin);
            this.MPS2.Controls.Add(this.var31ErrorMax);
            this.MPS2.Controls.Add(this.var32ErrorMin);
            this.MPS2.Controls.Add(this.var30ErrorMax);
            this.MPS2.Controls.Add(this.var31ErrorMin);
            this.MPS2.Controls.Add(this.var29ErrorMax);
            this.MPS2.Controls.Add(this.var30ErrorMin);
            this.MPS2.Controls.Add(this.var28ErrorMax);
            this.MPS2.Controls.Add(this.var29ErrorMin);
            this.MPS2.Controls.Add(this.var27ErrorMax);
            this.MPS2.Controls.Add(this.var28ErrorMin);
            this.MPS2.Controls.Add(this.var26ErrorMax);
            this.MPS2.Controls.Add(this.var27ErrorMin);
            this.MPS2.Controls.Add(this.var25ErrorMax);
            this.MPS2.Controls.Add(this.var26ErrorMin);
            this.MPS2.Controls.Add(this.var24ErrorMax);
            this.MPS2.Controls.Add(this.var25ErrorMin);
            this.MPS2.Controls.Add(this.var23ErrorMax);
            this.MPS2.Controls.Add(this.var24ErrorMin);
            this.MPS2.Controls.Add(this.var22ErrorMax);
            this.MPS2.Controls.Add(this.var23ErrorMin);
            this.MPS2.Controls.Add(this.var21ErrorMax);
            this.MPS2.Controls.Add(this.var22ErrorMin);
            this.MPS2.Controls.Add(this.var20ErrorMax);
            this.MPS2.Controls.Add(this.var21ErrorMin);
            this.MPS2.Controls.Add(this.var19ErrorMax);
            this.MPS2.Controls.Add(this.var19ErrorMin);
            this.MPS2.Controls.Add(this.label38);
            this.MPS2.Controls.Add(this.var35DisplayName);
            this.MPS2.Controls.Add(this.var34DisplayName);
            this.MPS2.Controls.Add(this.var33DisplayName);
            this.MPS2.Controls.Add(this.var32DisplayName);
            this.MPS2.Controls.Add(this.var31DisplayName);
            this.MPS2.Controls.Add(this.var35Type);
            this.MPS2.Controls.Add(this.var34Type);
            this.MPS2.Controls.Add(this.var33Type);
            this.MPS2.Controls.Add(this.var32Type);
            this.MPS2.Controls.Add(this.var31Type);
            this.MPS2.Controls.Add(this.var35StatusColumn);
            this.MPS2.Controls.Add(this.var34StatusColumn);
            this.MPS2.Controls.Add(this.var33StatusColumn);
            this.MPS2.Controls.Add(this.var32StatusColumn);
            this.MPS2.Controls.Add(this.var31StatusColumn);
            this.MPS2.Controls.Add(this.var35ValueColumn);
            this.MPS2.Controls.Add(this.var35Unit);
            this.MPS2.Controls.Add(this.var34ValueColumn);
            this.MPS2.Controls.Add(this.var34Unit);
            this.MPS2.Controls.Add(this.var33ValueColumn);
            this.MPS2.Controls.Add(this.var33Unit);
            this.MPS2.Controls.Add(this.var32ValueColumn);
            this.MPS2.Controls.Add(this.var32Unit);
            this.MPS2.Controls.Add(this.var31ValueColumn);
            this.MPS2.Controls.Add(this.var31Unit);
            this.MPS2.Controls.Add(this.var35Offset);
            this.MPS2.Controls.Add(this.var34Offset);
            this.MPS2.Controls.Add(this.var33Offset);
            this.MPS2.Controls.Add(this.var32Offset);
            this.MPS2.Controls.Add(this.var31Offset);
            this.MPS2.Controls.Add(this.var35OutputMax);
            this.MPS2.Controls.Add(this.var35OutputMin);
            this.MPS2.Controls.Add(this.var35InputMax);
            this.MPS2.Controls.Add(this.var35InputMin);
            this.MPS2.Controls.Add(this.var35Channel);
            this.MPS2.Controls.Add(this.var34OutputMax);
            this.MPS2.Controls.Add(this.var34OutputMin);
            this.MPS2.Controls.Add(this.var34InputMax);
            this.MPS2.Controls.Add(this.var34InputMin);
            this.MPS2.Controls.Add(this.var34Channel);
            this.MPS2.Controls.Add(this.var33OutputMax);
            this.MPS2.Controls.Add(this.var33OutputMin);
            this.MPS2.Controls.Add(this.var33InputMax);
            this.MPS2.Controls.Add(this.var33InputMin);
            this.MPS2.Controls.Add(this.var33Channel);
            this.MPS2.Controls.Add(this.var32OutputMax);
            this.MPS2.Controls.Add(this.var32OutputMin);
            this.MPS2.Controls.Add(this.var32InputMax);
            this.MPS2.Controls.Add(this.var32InputMin);
            this.MPS2.Controls.Add(this.var32Channel);
            this.MPS2.Controls.Add(this.var31OutputMax);
            this.MPS2.Controls.Add(this.var31OutputMin);
            this.MPS2.Controls.Add(this.var31InputMax);
            this.MPS2.Controls.Add(this.var31InputMin);
            this.MPS2.Controls.Add(this.var31Channel);
            this.MPS2.Controls.Add(this.var30DisplayName);
            this.MPS2.Controls.Add(this.var29DisplayName);
            this.MPS2.Controls.Add(this.var28DisplayName);
            this.MPS2.Controls.Add(this.var27DisplayName);
            this.MPS2.Controls.Add(this.var26DisplayName);
            this.MPS2.Controls.Add(this.var25DisplayName);
            this.MPS2.Controls.Add(this.var30Type);
            this.MPS2.Controls.Add(this.var29Type);
            this.MPS2.Controls.Add(this.var28Type);
            this.MPS2.Controls.Add(this.var27Type);
            this.MPS2.Controls.Add(this.var26Type);
            this.MPS2.Controls.Add(this.var25Type);
            this.MPS2.Controls.Add(this.var30StatusColumn);
            this.MPS2.Controls.Add(this.var29StatusColumn);
            this.MPS2.Controls.Add(this.var28StatusColumn);
            this.MPS2.Controls.Add(this.var27StatusColumn);
            this.MPS2.Controls.Add(this.var26StatusColumn);
            this.MPS2.Controls.Add(this.var25StatusColumn);
            this.MPS2.Controls.Add(this.var30ValueColumn);
            this.MPS2.Controls.Add(this.var30Unit);
            this.MPS2.Controls.Add(this.var29ValueColumn);
            this.MPS2.Controls.Add(this.var29Unit);
            this.MPS2.Controls.Add(this.var28ValueColumn);
            this.MPS2.Controls.Add(this.var28Unit);
            this.MPS2.Controls.Add(this.var27ValueColumn);
            this.MPS2.Controls.Add(this.var27Unit);
            this.MPS2.Controls.Add(this.var26ValueColumn);
            this.MPS2.Controls.Add(this.var26Unit);
            this.MPS2.Controls.Add(this.var25ValueColumn);
            this.MPS2.Controls.Add(this.var25Unit);
            this.MPS2.Controls.Add(this.var30Offset);
            this.MPS2.Controls.Add(this.var29Offset);
            this.MPS2.Controls.Add(this.var28Offset);
            this.MPS2.Controls.Add(this.var27Offset);
            this.MPS2.Controls.Add(this.var26Offset);
            this.MPS2.Controls.Add(this.var25Offset);
            this.MPS2.Controls.Add(this.var30OutputMax);
            this.MPS2.Controls.Add(this.var30OutputMin);
            this.MPS2.Controls.Add(this.var30InputMax);
            this.MPS2.Controls.Add(this.var30InputMin);
            this.MPS2.Controls.Add(this.var30Channel);
            this.MPS2.Controls.Add(this.var29OutputMax);
            this.MPS2.Controls.Add(this.var29OutputMin);
            this.MPS2.Controls.Add(this.var29InputMax);
            this.MPS2.Controls.Add(this.var29InputMin);
            this.MPS2.Controls.Add(this.var29Channel);
            this.MPS2.Controls.Add(this.var28OutputMax);
            this.MPS2.Controls.Add(this.var28OutputMin);
            this.MPS2.Controls.Add(this.var28InputMax);
            this.MPS2.Controls.Add(this.var28InputMin);
            this.MPS2.Controls.Add(this.var28Channel);
            this.MPS2.Controls.Add(this.var27OutputMax);
            this.MPS2.Controls.Add(this.var27OutputMin);
            this.MPS2.Controls.Add(this.var27InputMax);
            this.MPS2.Controls.Add(this.var27InputMin);
            this.MPS2.Controls.Add(this.var27Channel);
            this.MPS2.Controls.Add(this.var26OutputMax);
            this.MPS2.Controls.Add(this.var26OutputMin);
            this.MPS2.Controls.Add(this.var26InputMax);
            this.MPS2.Controls.Add(this.var26InputMin);
            this.MPS2.Controls.Add(this.var26Channel);
            this.MPS2.Controls.Add(this.var25OutputMax);
            this.MPS2.Controls.Add(this.var25OutputMin);
            this.MPS2.Controls.Add(this.var25InputMax);
            this.MPS2.Controls.Add(this.var25InputMin);
            this.MPS2.Controls.Add(this.var25Channel);
            this.MPS2.Controls.Add(this.var24DisplayName);
            this.MPS2.Controls.Add(this.var23DisplayName);
            this.MPS2.Controls.Add(this.var22DisplayName);
            this.MPS2.Controls.Add(this.var21DisplayName);
            this.MPS2.Controls.Add(this.var20DisplayName);
            this.MPS2.Controls.Add(this.var19DisplayName);
            this.MPS2.Controls.Add(this.var24Type);
            this.MPS2.Controls.Add(this.var23Type);
            this.MPS2.Controls.Add(this.var22Type);
            this.MPS2.Controls.Add(this.var21Type);
            this.MPS2.Controls.Add(this.var20Type);
            this.MPS2.Controls.Add(this.var19Type);
            this.MPS2.Controls.Add(this.var24StatusColumn);
            this.MPS2.Controls.Add(this.var23StatusColumn);
            this.MPS2.Controls.Add(this.var22StatusColumn);
            this.MPS2.Controls.Add(this.var21StatusColumn);
            this.MPS2.Controls.Add(this.var20StatusColumn);
            this.MPS2.Controls.Add(this.var19StatusColumn);
            this.MPS2.Controls.Add(this.var24ValueColumn);
            this.MPS2.Controls.Add(this.var24Unit);
            this.MPS2.Controls.Add(this.var23ValueColumn);
            this.MPS2.Controls.Add(this.var23Unit);
            this.MPS2.Controls.Add(this.var22ValueColumn);
            this.MPS2.Controls.Add(this.var22Unit);
            this.MPS2.Controls.Add(this.var21ValueColumn);
            this.MPS2.Controls.Add(this.var21Unit);
            this.MPS2.Controls.Add(this.var20ValueColumn);
            this.MPS2.Controls.Add(this.var20Unit);
            this.MPS2.Controls.Add(this.var19ValueColumn);
            this.MPS2.Controls.Add(this.var19Unit);
            this.MPS2.Controls.Add(this.var24Offset);
            this.MPS2.Controls.Add(this.var23Offset);
            this.MPS2.Controls.Add(this.var22Offset);
            this.MPS2.Controls.Add(this.var21Offset);
            this.MPS2.Controls.Add(this.var20Offset);
            this.MPS2.Controls.Add(this.var19Offset);
            this.MPS2.Controls.Add(this.var24OutputMax);
            this.MPS2.Controls.Add(this.var24OutputMin);
            this.MPS2.Controls.Add(this.var24InputMax);
            this.MPS2.Controls.Add(this.var24InputMin);
            this.MPS2.Controls.Add(this.var24Channel);
            this.MPS2.Controls.Add(this.var23OutputMax);
            this.MPS2.Controls.Add(this.var23OutputMin);
            this.MPS2.Controls.Add(this.var23InputMax);
            this.MPS2.Controls.Add(this.var23InputMin);
            this.MPS2.Controls.Add(this.var23Channel);
            this.MPS2.Controls.Add(this.var22OutputMax);
            this.MPS2.Controls.Add(this.var22OutputMin);
            this.MPS2.Controls.Add(this.var22InputMax);
            this.MPS2.Controls.Add(this.var22InputMin);
            this.MPS2.Controls.Add(this.var22Channel);
            this.MPS2.Controls.Add(this.var21OutputMax);
            this.MPS2.Controls.Add(this.var21OutputMin);
            this.MPS2.Controls.Add(this.var21InputMax);
            this.MPS2.Controls.Add(this.var21InputMin);
            this.MPS2.Controls.Add(this.var21Channel);
            this.MPS2.Controls.Add(this.var20OutputMax);
            this.MPS2.Controls.Add(this.var20OutputMin);
            this.MPS2.Controls.Add(this.var20InputMax);
            this.MPS2.Controls.Add(this.var20InputMin);
            this.MPS2.Controls.Add(this.var20Channel);
            this.MPS2.Controls.Add(this.var19OutputMax);
            this.MPS2.Controls.Add(this.var19OutputMin);
            this.MPS2.Controls.Add(this.var19InputMax);
            this.MPS2.Controls.Add(this.var19InputMin);
            this.MPS2.Controls.Add(this.var19Channel);
            this.MPS2.Controls.Add(this.var35Module);
            this.MPS2.Controls.Add(this.label2);
            this.MPS2.Controls.Add(this.var34Module);
            this.MPS2.Controls.Add(this.var33Module);
            this.MPS2.Controls.Add(this.label3);
            this.MPS2.Controls.Add(this.label10);
            this.MPS2.Controls.Add(this.var32Module);
            this.MPS2.Controls.Add(this.var31Module);
            this.MPS2.Controls.Add(this.label11);
            this.MPS2.Controls.Add(this.label12);
            this.MPS2.Controls.Add(this.var30Module);
            this.MPS2.Controls.Add(this.var29Module);
            this.MPS2.Controls.Add(this.label13);
            this.MPS2.Controls.Add(this.label14);
            this.MPS2.Controls.Add(this.var28Module);
            this.MPS2.Controls.Add(this.var27Module);
            this.MPS2.Controls.Add(this.label15);
            this.MPS2.Controls.Add(this.label16);
            this.MPS2.Controls.Add(this.var26Module);
            this.MPS2.Controls.Add(this.var25Module);
            this.MPS2.Controls.Add(this.label17);
            this.MPS2.Controls.Add(this.label18);
            this.MPS2.Controls.Add(this.label19);
            this.MPS2.Controls.Add(this.label20);
            this.MPS2.Controls.Add(this.label21);
            this.MPS2.Controls.Add(this.label22);
            this.MPS2.Controls.Add(this.label23);
            this.MPS2.Controls.Add(this.label24);
            this.MPS2.Controls.Add(this.var24Module);
            this.MPS2.Controls.Add(this.var23Module);
            this.MPS2.Controls.Add(this.label25);
            this.MPS2.Controls.Add(this.label26);
            this.MPS2.Controls.Add(this.var22Module);
            this.MPS2.Controls.Add(this.var21Module);
            this.MPS2.Controls.Add(this.label27);
            this.MPS2.Controls.Add(this.label29);
            this.MPS2.Controls.Add(this.var20Module);
            this.MPS2.Controls.Add(this.var19Module);
            this.MPS2.Controls.Add(this.label30);
            this.MPS2.Controls.Add(this.label31);
            this.MPS2.Controls.Add(this.label32);
            this.MPS2.Controls.Add(this.label33);
            this.MPS2.Controls.Add(this.label34);
            this.MPS2.Controls.Add(this.label35);
            this.MPS2.Controls.Add(this.label36);
            this.MPS2.Controls.Add(this.label37);
            this.MPS2.Location = new System.Drawing.Point(4, 22);
            this.MPS2.Name = "MPS2";
            this.MPS2.Padding = new System.Windows.Forms.Padding(3);
            this.MPS2.Size = new System.Drawing.Size(1147, 604);
            this.MPS2.TabIndex = 7;
            this.MPS2.Text = "MPS2";
            this.MPS2.UseVisualStyleBackColor = true;
            this.MPS2.Click += new System.EventHandler(this.MPS2_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(674, 58);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(51, 13);
            this.label46.TabIndex = 848;
            this.label46.Text = "Error max";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(593, 58);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(48, 13);
            this.label45.TabIndex = 847;
            this.label45.Text = "Error min";
            // 
            // var35ErrorMax
            // 
            this.var35ErrorMax.Location = new System.Drawing.Point(676, 506);
            this.var35ErrorMax.Name = "var35ErrorMax";
            this.var35ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var35ErrorMax.TabIndex = 846;
            this.var35ErrorMax.Visible = false;
            this.var35ErrorMax.TextChanged += new System.EventHandler(this.textBox69_TextChanged);
            // 
            // var20ErrorMin
            // 
            this.var20ErrorMin.Location = new System.Drawing.Point(599, 115);
            this.var20ErrorMin.Name = "var20ErrorMin";
            this.var20ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var20ErrorMin.TabIndex = 845;
            // 
            // var34ErrorMax
            // 
            this.var34ErrorMax.Location = new System.Drawing.Point(677, 480);
            this.var34ErrorMax.Name = "var34ErrorMax";
            this.var34ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var34ErrorMax.TabIndex = 844;
            this.var34ErrorMax.Visible = false;
            this.var34ErrorMax.TextChanged += new System.EventHandler(this.textBox67_TextChanged);
            // 
            // var35ErrorMin
            // 
            this.var35ErrorMin.Location = new System.Drawing.Point(599, 504);
            this.var35ErrorMin.Name = "var35ErrorMin";
            this.var35ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var35ErrorMin.TabIndex = 843;
            this.var35ErrorMin.Visible = false;
            this.var35ErrorMin.TextChanged += new System.EventHandler(this.var35ErrorMin_TextChanged);
            // 
            // var33ErrorMax
            // 
            this.var33ErrorMax.Location = new System.Drawing.Point(677, 453);
            this.var33ErrorMax.Name = "var33ErrorMax";
            this.var33ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var33ErrorMax.TabIndex = 842;
            this.var33ErrorMax.Visible = false;
            this.var33ErrorMax.TextChanged += new System.EventHandler(this.textBox65_TextChanged);
            // 
            // var34ErrorMin
            // 
            this.var34ErrorMin.Location = new System.Drawing.Point(599, 477);
            this.var34ErrorMin.Name = "var34ErrorMin";
            this.var34ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var34ErrorMin.TabIndex = 841;
            this.var34ErrorMin.Visible = false;
            this.var34ErrorMin.TextChanged += new System.EventHandler(this.var34ErrorMin_TextChanged);
            // 
            // var32ErrorMax
            // 
            this.var32ErrorMax.Location = new System.Drawing.Point(677, 428);
            this.var32ErrorMax.Name = "var32ErrorMax";
            this.var32ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var32ErrorMax.TabIndex = 840;
            this.var32ErrorMax.Visible = false;
            this.var32ErrorMax.TextChanged += new System.EventHandler(this.textBox63_TextChanged);
            // 
            // var33ErrorMin
            // 
            this.var33ErrorMin.Location = new System.Drawing.Point(599, 452);
            this.var33ErrorMin.Name = "var33ErrorMin";
            this.var33ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var33ErrorMin.TabIndex = 839;
            this.var33ErrorMin.Visible = false;
            this.var33ErrorMin.TextChanged += new System.EventHandler(this.var33ErrorMin_TextChanged);
            // 
            // var31ErrorMax
            // 
            this.var31ErrorMax.Location = new System.Drawing.Point(677, 403);
            this.var31ErrorMax.Name = "var31ErrorMax";
            this.var31ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var31ErrorMax.TabIndex = 838;
            this.var31ErrorMax.Visible = false;
            this.var31ErrorMax.TextChanged += new System.EventHandler(this.textBox61_TextChanged);
            // 
            // var32ErrorMin
            // 
            this.var32ErrorMin.Location = new System.Drawing.Point(599, 427);
            this.var32ErrorMin.Name = "var32ErrorMin";
            this.var32ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var32ErrorMin.TabIndex = 837;
            this.var32ErrorMin.Visible = false;
            this.var32ErrorMin.TextChanged += new System.EventHandler(this.var32ErrorMin_TextChanged);
            // 
            // var30ErrorMax
            // 
            this.var30ErrorMax.Location = new System.Drawing.Point(677, 376);
            this.var30ErrorMax.Name = "var30ErrorMax";
            this.var30ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var30ErrorMax.TabIndex = 836;
            this.var30ErrorMax.Visible = false;
            this.var30ErrorMax.TextChanged += new System.EventHandler(this.textBox59_TextChanged);
            // 
            // var31ErrorMin
            // 
            this.var31ErrorMin.Location = new System.Drawing.Point(599, 400);
            this.var31ErrorMin.Name = "var31ErrorMin";
            this.var31ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var31ErrorMin.TabIndex = 835;
            this.var31ErrorMin.Visible = false;
            this.var31ErrorMin.TextChanged += new System.EventHandler(this.var31ErrorMin_TextChanged);
            // 
            // var29ErrorMax
            // 
            this.var29ErrorMax.Location = new System.Drawing.Point(677, 349);
            this.var29ErrorMax.Name = "var29ErrorMax";
            this.var29ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var29ErrorMax.TabIndex = 834;
            this.var29ErrorMax.Visible = false;
            this.var29ErrorMax.TextChanged += new System.EventHandler(this.textBox57_TextChanged);
            // 
            // var30ErrorMin
            // 
            this.var30ErrorMin.Location = new System.Drawing.Point(599, 373);
            this.var30ErrorMin.Name = "var30ErrorMin";
            this.var30ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var30ErrorMin.TabIndex = 833;
            this.var30ErrorMin.Visible = false;
            this.var30ErrorMin.TextChanged += new System.EventHandler(this.var30ErrorMin_TextChanged);
            // 
            // var28ErrorMax
            // 
            this.var28ErrorMax.Location = new System.Drawing.Point(677, 324);
            this.var28ErrorMax.Name = "var28ErrorMax";
            this.var28ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var28ErrorMax.TabIndex = 832;
            this.var28ErrorMax.Visible = false;
            this.var28ErrorMax.TextChanged += new System.EventHandler(this.textBox55_TextChanged);
            // 
            // var29ErrorMin
            // 
            this.var29ErrorMin.Location = new System.Drawing.Point(599, 348);
            this.var29ErrorMin.Name = "var29ErrorMin";
            this.var29ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var29ErrorMin.TabIndex = 831;
            this.var29ErrorMin.Visible = false;
            this.var29ErrorMin.TextChanged += new System.EventHandler(this.var29ErrorMin_TextChanged);
            // 
            // var27ErrorMax
            // 
            this.var27ErrorMax.Location = new System.Drawing.Point(677, 300);
            this.var27ErrorMax.Name = "var27ErrorMax";
            this.var27ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var27ErrorMax.TabIndex = 830;
            this.var27ErrorMax.Visible = false;
            this.var27ErrorMax.TextChanged += new System.EventHandler(this.textBox53_TextChanged);
            // 
            // var28ErrorMin
            // 
            this.var28ErrorMin.Location = new System.Drawing.Point(599, 324);
            this.var28ErrorMin.Name = "var28ErrorMin";
            this.var28ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var28ErrorMin.TabIndex = 829;
            this.var28ErrorMin.Visible = false;
            this.var28ErrorMin.TextChanged += new System.EventHandler(this.var28ErrorMin_TextChanged);
            // 
            // var26ErrorMax
            // 
            this.var26ErrorMax.Location = new System.Drawing.Point(677, 274);
            this.var26ErrorMax.Name = "var26ErrorMax";
            this.var26ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var26ErrorMax.TabIndex = 828;
            this.var26ErrorMax.Visible = false;
            this.var26ErrorMax.TextChanged += new System.EventHandler(this.textBox51_TextChanged);
            // 
            // var27ErrorMin
            // 
            this.var27ErrorMin.Location = new System.Drawing.Point(599, 298);
            this.var27ErrorMin.Name = "var27ErrorMin";
            this.var27ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var27ErrorMin.TabIndex = 827;
            this.var27ErrorMin.Visible = false;
            this.var27ErrorMin.TextChanged += new System.EventHandler(this.var27ErrorMin_TextChanged);
            // 
            // var25ErrorMax
            // 
            this.var25ErrorMax.Location = new System.Drawing.Point(677, 247);
            this.var25ErrorMax.Name = "var25ErrorMax";
            this.var25ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var25ErrorMax.TabIndex = 826;
            this.var25ErrorMax.Visible = false;
            this.var25ErrorMax.TextChanged += new System.EventHandler(this.textBox49_TextChanged);
            // 
            // var26ErrorMin
            // 
            this.var26ErrorMin.Location = new System.Drawing.Point(599, 271);
            this.var26ErrorMin.Name = "var26ErrorMin";
            this.var26ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var26ErrorMin.TabIndex = 825;
            this.var26ErrorMin.Visible = false;
            this.var26ErrorMin.TextChanged += new System.EventHandler(this.var26ErrorMin_TextChanged);
            // 
            // var24ErrorMax
            // 
            this.var24ErrorMax.Location = new System.Drawing.Point(677, 219);
            this.var24ErrorMax.Name = "var24ErrorMax";
            this.var24ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var24ErrorMax.TabIndex = 824;
            this.var24ErrorMax.Visible = false;
            this.var24ErrorMax.TextChanged += new System.EventHandler(this.textBox47_TextChanged);
            // 
            // var25ErrorMin
            // 
            this.var25ErrorMin.Location = new System.Drawing.Point(599, 243);
            this.var25ErrorMin.Name = "var25ErrorMin";
            this.var25ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var25ErrorMin.TabIndex = 823;
            this.var25ErrorMin.Visible = false;
            this.var25ErrorMin.TextChanged += new System.EventHandler(this.var25ErrorMin_TextChanged);
            // 
            // var23ErrorMax
            // 
            this.var23ErrorMax.Location = new System.Drawing.Point(677, 194);
            this.var23ErrorMax.Name = "var23ErrorMax";
            this.var23ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var23ErrorMax.TabIndex = 822;
            this.var23ErrorMax.Visible = false;
            this.var23ErrorMax.TextChanged += new System.EventHandler(this.textBox45_TextChanged);
            // 
            // var24ErrorMin
            // 
            this.var24ErrorMin.Location = new System.Drawing.Point(599, 218);
            this.var24ErrorMin.Name = "var24ErrorMin";
            this.var24ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var24ErrorMin.TabIndex = 821;
            this.var24ErrorMin.Visible = false;
            this.var24ErrorMin.TextChanged += new System.EventHandler(this.var24ErrorMin_TextChanged);
            // 
            // var22ErrorMax
            // 
            this.var22ErrorMax.Location = new System.Drawing.Point(677, 168);
            this.var22ErrorMax.Name = "var22ErrorMax";
            this.var22ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var22ErrorMax.TabIndex = 820;
            // 
            // var23ErrorMin
            // 
            this.var23ErrorMin.Location = new System.Drawing.Point(599, 192);
            this.var23ErrorMin.Name = "var23ErrorMin";
            this.var23ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var23ErrorMin.TabIndex = 819;
            this.var23ErrorMin.Visible = false;
            this.var23ErrorMin.TextChanged += new System.EventHandler(this.var23ErrorMin_TextChanged);
            // 
            // var21ErrorMax
            // 
            this.var21ErrorMax.Location = new System.Drawing.Point(677, 141);
            this.var21ErrorMax.Name = "var21ErrorMax";
            this.var21ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var21ErrorMax.TabIndex = 818;
            // 
            // var22ErrorMin
            // 
            this.var22ErrorMin.Location = new System.Drawing.Point(599, 165);
            this.var22ErrorMin.Name = "var22ErrorMin";
            this.var22ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var22ErrorMin.TabIndex = 817;
            // 
            // var20ErrorMax
            // 
            this.var20ErrorMax.Location = new System.Drawing.Point(677, 116);
            this.var20ErrorMax.Name = "var20ErrorMax";
            this.var20ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var20ErrorMax.TabIndex = 816;
            // 
            // var21ErrorMin
            // 
            this.var21ErrorMin.Location = new System.Drawing.Point(599, 140);
            this.var21ErrorMin.Name = "var21ErrorMin";
            this.var21ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var21ErrorMin.TabIndex = 815;
            // 
            // var19ErrorMax
            // 
            this.var19ErrorMax.Location = new System.Drawing.Point(677, 90);
            this.var19ErrorMax.Name = "var19ErrorMax";
            this.var19ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var19ErrorMax.TabIndex = 814;
            // 
            // var19ErrorMin
            // 
            this.var19ErrorMin.Location = new System.Drawing.Point(599, 89);
            this.var19ErrorMin.Name = "var19ErrorMin";
            this.var19ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var19ErrorMin.TabIndex = 813;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(1156, 58);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 13);
            this.label38.TabIndex = 795;
            this.label38.Text = "Alarm";
            this.label38.Visible = false;
            // 
            // var35DisplayName
            // 
            this.var35DisplayName.Enabled = false;
            this.var35DisplayName.Location = new System.Drawing.Point(1073, 505);
            this.var35DisplayName.Name = "var35DisplayName";
            this.var35DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var35DisplayName.TabIndex = 794;
            this.var35DisplayName.Visible = false;
            this.var35DisplayName.TextChanged += new System.EventHandler(this.var35DisplayName_TextChanged);
            // 
            // var34DisplayName
            // 
            this.var34DisplayName.Enabled = false;
            this.var34DisplayName.Location = new System.Drawing.Point(1073, 480);
            this.var34DisplayName.Name = "var34DisplayName";
            this.var34DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var34DisplayName.TabIndex = 793;
            this.var34DisplayName.Visible = false;
            this.var34DisplayName.TextChanged += new System.EventHandler(this.var34DisplayName_TextChanged);
            // 
            // var33DisplayName
            // 
            this.var33DisplayName.Enabled = false;
            this.var33DisplayName.Location = new System.Drawing.Point(1073, 453);
            this.var33DisplayName.Name = "var33DisplayName";
            this.var33DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var33DisplayName.TabIndex = 792;
            this.var33DisplayName.Visible = false;
            this.var33DisplayName.TextChanged += new System.EventHandler(this.var33DisplayName_TextChanged);
            // 
            // var32DisplayName
            // 
            this.var32DisplayName.Enabled = false;
            this.var32DisplayName.Location = new System.Drawing.Point(1073, 429);
            this.var32DisplayName.Name = "var32DisplayName";
            this.var32DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var32DisplayName.TabIndex = 791;
            this.var32DisplayName.Visible = false;
            this.var32DisplayName.TextChanged += new System.EventHandler(this.var32DisplayName_TextChanged);
            // 
            // var31DisplayName
            // 
            this.var31DisplayName.Enabled = false;
            this.var31DisplayName.Location = new System.Drawing.Point(1073, 402);
            this.var31DisplayName.Name = "var31DisplayName";
            this.var31DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var31DisplayName.TabIndex = 790;
            this.var31DisplayName.Visible = false;
            this.var31DisplayName.TextChanged += new System.EventHandler(this.var31DisplayName_TextChanged);
            // 
            // var35Type
            // 
            this.var35Type.Enabled = false;
            this.var35Type.Location = new System.Drawing.Point(995, 505);
            this.var35Type.Name = "var35Type";
            this.var35Type.Size = new System.Drawing.Size(63, 20);
            this.var35Type.TabIndex = 788;
            this.var35Type.Visible = false;
            this.var35Type.TextChanged += new System.EventHandler(this.var35Type_TextChanged);
            // 
            // var34Type
            // 
            this.var34Type.Enabled = false;
            this.var34Type.Location = new System.Drawing.Point(995, 480);
            this.var34Type.Name = "var34Type";
            this.var34Type.Size = new System.Drawing.Size(63, 20);
            this.var34Type.TabIndex = 787;
            this.var34Type.Visible = false;
            this.var34Type.TextChanged += new System.EventHandler(this.var34Type_TextChanged);
            // 
            // var33Type
            // 
            this.var33Type.Enabled = false;
            this.var33Type.Location = new System.Drawing.Point(995, 453);
            this.var33Type.Name = "var33Type";
            this.var33Type.Size = new System.Drawing.Size(63, 20);
            this.var33Type.TabIndex = 786;
            this.var33Type.Visible = false;
            this.var33Type.TextChanged += new System.EventHandler(this.var33Type_TextChanged);
            // 
            // var32Type
            // 
            this.var32Type.Enabled = false;
            this.var32Type.Location = new System.Drawing.Point(995, 429);
            this.var32Type.Name = "var32Type";
            this.var32Type.Size = new System.Drawing.Size(63, 20);
            this.var32Type.TabIndex = 785;
            this.var32Type.Visible = false;
            this.var32Type.TextChanged += new System.EventHandler(this.var32Type_TextChanged);
            // 
            // var31Type
            // 
            this.var31Type.Enabled = false;
            this.var31Type.Location = new System.Drawing.Point(995, 402);
            this.var31Type.Name = "var31Type";
            this.var31Type.Size = new System.Drawing.Size(63, 20);
            this.var31Type.TabIndex = 784;
            this.var31Type.Visible = false;
            this.var31Type.TextChanged += new System.EventHandler(this.var31Type_TextChanged);
            // 
            // var35StatusColumn
            // 
            this.var35StatusColumn.Enabled = false;
            this.var35StatusColumn.Location = new System.Drawing.Point(911, 505);
            this.var35StatusColumn.Name = "var35StatusColumn";
            this.var35StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var35StatusColumn.TabIndex = 782;
            this.var35StatusColumn.Visible = false;
            this.var35StatusColumn.TextChanged += new System.EventHandler(this.var35StatusColumn_TextChanged);
            // 
            // var34StatusColumn
            // 
            this.var34StatusColumn.Enabled = false;
            this.var34StatusColumn.Location = new System.Drawing.Point(911, 480);
            this.var34StatusColumn.Name = "var34StatusColumn";
            this.var34StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var34StatusColumn.TabIndex = 781;
            this.var34StatusColumn.Visible = false;
            this.var34StatusColumn.TextChanged += new System.EventHandler(this.var34StatusColumn_TextChanged);
            // 
            // var33StatusColumn
            // 
            this.var33StatusColumn.Enabled = false;
            this.var33StatusColumn.Location = new System.Drawing.Point(911, 453);
            this.var33StatusColumn.Name = "var33StatusColumn";
            this.var33StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var33StatusColumn.TabIndex = 780;
            this.var33StatusColumn.Visible = false;
            this.var33StatusColumn.TextChanged += new System.EventHandler(this.var33StatusColumn_TextChanged);
            // 
            // var32StatusColumn
            // 
            this.var32StatusColumn.Enabled = false;
            this.var32StatusColumn.Location = new System.Drawing.Point(911, 429);
            this.var32StatusColumn.Name = "var32StatusColumn";
            this.var32StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var32StatusColumn.TabIndex = 779;
            this.var32StatusColumn.Visible = false;
            this.var32StatusColumn.TextChanged += new System.EventHandler(this.var32StatusColumn_TextChanged);
            // 
            // var31StatusColumn
            // 
            this.var31StatusColumn.Enabled = false;
            this.var31StatusColumn.Location = new System.Drawing.Point(911, 402);
            this.var31StatusColumn.Name = "var31StatusColumn";
            this.var31StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var31StatusColumn.TabIndex = 778;
            this.var31StatusColumn.Visible = false;
            this.var31StatusColumn.TextChanged += new System.EventHandler(this.var31StatusColumn_TextChanged);
            // 
            // var35ValueColumn
            // 
            this.var35ValueColumn.Enabled = false;
            this.var35ValueColumn.Location = new System.Drawing.Point(830, 504);
            this.var35ValueColumn.Name = "var35ValueColumn";
            this.var35ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var35ValueColumn.TabIndex = 775;
            this.var35ValueColumn.Visible = false;
            this.var35ValueColumn.TextChanged += new System.EventHandler(this.var35ValueColumn_TextChanged);
            // 
            // var35Unit
            // 
            this.var35Unit.Enabled = false;
            this.var35Unit.Location = new System.Drawing.Point(753, 504);
            this.var35Unit.Name = "var35Unit";
            this.var35Unit.Size = new System.Drawing.Size(63, 20);
            this.var35Unit.TabIndex = 774;
            this.var35Unit.Visible = false;
            this.var35Unit.TextChanged += new System.EventHandler(this.var35Unit_TextChanged);
            // 
            // var34ValueColumn
            // 
            this.var34ValueColumn.Enabled = false;
            this.var34ValueColumn.Location = new System.Drawing.Point(830, 479);
            this.var34ValueColumn.Name = "var34ValueColumn";
            this.var34ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var34ValueColumn.TabIndex = 773;
            this.var34ValueColumn.Visible = false;
            this.var34ValueColumn.TextChanged += new System.EventHandler(this.var34ValueColumn_TextChanged);
            // 
            // var34Unit
            // 
            this.var34Unit.Enabled = false;
            this.var34Unit.Location = new System.Drawing.Point(753, 479);
            this.var34Unit.Name = "var34Unit";
            this.var34Unit.Size = new System.Drawing.Size(63, 20);
            this.var34Unit.TabIndex = 772;
            this.var34Unit.Visible = false;
            this.var34Unit.TextChanged += new System.EventHandler(this.var34Unit_TextChanged);
            // 
            // var33ValueColumn
            // 
            this.var33ValueColumn.Enabled = false;
            this.var33ValueColumn.Location = new System.Drawing.Point(830, 453);
            this.var33ValueColumn.Name = "var33ValueColumn";
            this.var33ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var33ValueColumn.TabIndex = 771;
            this.var33ValueColumn.Visible = false;
            this.var33ValueColumn.TextChanged += new System.EventHandler(this.var33ValueColumn_TextChanged);
            // 
            // var33Unit
            // 
            this.var33Unit.Enabled = false;
            this.var33Unit.Location = new System.Drawing.Point(753, 453);
            this.var33Unit.Name = "var33Unit";
            this.var33Unit.Size = new System.Drawing.Size(63, 20);
            this.var33Unit.TabIndex = 770;
            this.var33Unit.Visible = false;
            this.var33Unit.TextChanged += new System.EventHandler(this.var33Unit_TextChanged);
            // 
            // var32ValueColumn
            // 
            this.var32ValueColumn.Enabled = false;
            this.var32ValueColumn.Location = new System.Drawing.Point(830, 428);
            this.var32ValueColumn.Name = "var32ValueColumn";
            this.var32ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var32ValueColumn.TabIndex = 769;
            this.var32ValueColumn.Visible = false;
            this.var32ValueColumn.TextChanged += new System.EventHandler(this.var32ValueColumn_TextChanged);
            // 
            // var32Unit
            // 
            this.var32Unit.Enabled = false;
            this.var32Unit.Location = new System.Drawing.Point(753, 428);
            this.var32Unit.Name = "var32Unit";
            this.var32Unit.Size = new System.Drawing.Size(63, 20);
            this.var32Unit.TabIndex = 768;
            this.var32Unit.Visible = false;
            this.var32Unit.TextChanged += new System.EventHandler(this.var32Unit_TextChanged);
            // 
            // var31ValueColumn
            // 
            this.var31ValueColumn.Enabled = false;
            this.var31ValueColumn.Location = new System.Drawing.Point(830, 401);
            this.var31ValueColumn.Name = "var31ValueColumn";
            this.var31ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var31ValueColumn.TabIndex = 767;
            this.var31ValueColumn.Visible = false;
            this.var31ValueColumn.TextChanged += new System.EventHandler(this.var31ValueColumn_TextChanged);
            // 
            // var31Unit
            // 
            this.var31Unit.Enabled = false;
            this.var31Unit.Location = new System.Drawing.Point(753, 401);
            this.var31Unit.Name = "var31Unit";
            this.var31Unit.Size = new System.Drawing.Size(63, 20);
            this.var31Unit.TabIndex = 766;
            this.var31Unit.Visible = false;
            this.var31Unit.TextChanged += new System.EventHandler(this.var31Unit_TextChanged);
            // 
            // var35Offset
            // 
            this.var35Offset.Enabled = false;
            this.var35Offset.Location = new System.Drawing.Point(523, 505);
            this.var35Offset.Name = "var35Offset";
            this.var35Offset.Size = new System.Drawing.Size(63, 20);
            this.var35Offset.TabIndex = 764;
            this.var35Offset.Visible = false;
            this.var35Offset.TextChanged += new System.EventHandler(this.var35Offset_TextChanged);
            // 
            // var34Offset
            // 
            this.var34Offset.Enabled = false;
            this.var34Offset.Location = new System.Drawing.Point(523, 480);
            this.var34Offset.Name = "var34Offset";
            this.var34Offset.Size = new System.Drawing.Size(63, 20);
            this.var34Offset.TabIndex = 763;
            this.var34Offset.Visible = false;
            this.var34Offset.TextChanged += new System.EventHandler(this.var34Offset_TextChanged);
            // 
            // var33Offset
            // 
            this.var33Offset.Enabled = false;
            this.var33Offset.Location = new System.Drawing.Point(523, 453);
            this.var33Offset.Name = "var33Offset";
            this.var33Offset.Size = new System.Drawing.Size(63, 20);
            this.var33Offset.TabIndex = 762;
            this.var33Offset.Visible = false;
            this.var33Offset.TextChanged += new System.EventHandler(this.var33Offset_TextChanged);
            // 
            // var32Offset
            // 
            this.var32Offset.Enabled = false;
            this.var32Offset.Location = new System.Drawing.Point(523, 429);
            this.var32Offset.Name = "var32Offset";
            this.var32Offset.Size = new System.Drawing.Size(63, 20);
            this.var32Offset.TabIndex = 761;
            this.var32Offset.Visible = false;
            this.var32Offset.TextChanged += new System.EventHandler(this.var32Offset_TextChanged);
            // 
            // var31Offset
            // 
            this.var31Offset.Enabled = false;
            this.var31Offset.Location = new System.Drawing.Point(523, 402);
            this.var31Offset.Name = "var31Offset";
            this.var31Offset.Size = new System.Drawing.Size(63, 20);
            this.var31Offset.TabIndex = 760;
            this.var31Offset.Visible = false;
            this.var31Offset.TextChanged += new System.EventHandler(this.var31Offset_TextChanged);
            // 
            // var35OutputMax
            // 
            this.var35OutputMax.Enabled = false;
            this.var35OutputMax.Location = new System.Drawing.Point(442, 504);
            this.var35OutputMax.Name = "var35OutputMax";
            this.var35OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var35OutputMax.TabIndex = 753;
            this.var35OutputMax.Visible = false;
            this.var35OutputMax.TextChanged += new System.EventHandler(this.var35OutputMax_TextChanged);
            // 
            // var35OutputMin
            // 
            this.var35OutputMin.Enabled = false;
            this.var35OutputMin.Location = new System.Drawing.Point(365, 504);
            this.var35OutputMin.Name = "var35OutputMin";
            this.var35OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var35OutputMin.TabIndex = 752;
            this.var35OutputMin.Visible = false;
            this.var35OutputMin.TextChanged += new System.EventHandler(this.var35OutputMin_TextChanged);
            // 
            // var35InputMax
            // 
            this.var35InputMax.Enabled = false;
            this.var35InputMax.Location = new System.Drawing.Point(287, 504);
            this.var35InputMax.Name = "var35InputMax";
            this.var35InputMax.Size = new System.Drawing.Size(63, 20);
            this.var35InputMax.TabIndex = 751;
            this.var35InputMax.Visible = false;
            this.var35InputMax.TextChanged += new System.EventHandler(this.var35InputMax_TextChanged);
            // 
            // var35InputMin
            // 
            this.var35InputMin.Enabled = false;
            this.var35InputMin.Location = new System.Drawing.Point(211, 503);
            this.var35InputMin.Name = "var35InputMin";
            this.var35InputMin.Size = new System.Drawing.Size(63, 20);
            this.var35InputMin.TabIndex = 750;
            this.var35InputMin.Visible = false;
            this.var35InputMin.TextChanged += new System.EventHandler(this.var35InputMin_TextChanged);
            // 
            // var35Channel
            // 
            this.var35Channel.Enabled = false;
            this.var35Channel.Location = new System.Drawing.Point(130, 504);
            this.var35Channel.Name = "var35Channel";
            this.var35Channel.Size = new System.Drawing.Size(63, 20);
            this.var35Channel.TabIndex = 748;
            this.var35Channel.Visible = false;
            this.var35Channel.TextChanged += new System.EventHandler(this.var35Channel_TextChanged);
            // 
            // var34OutputMax
            // 
            this.var34OutputMax.Enabled = false;
            this.var34OutputMax.Location = new System.Drawing.Point(442, 479);
            this.var34OutputMax.Name = "var34OutputMax";
            this.var34OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var34OutputMax.TabIndex = 745;
            this.var34OutputMax.Visible = false;
            this.var34OutputMax.TextChanged += new System.EventHandler(this.var34OutputMax_TextChanged);
            // 
            // var34OutputMin
            // 
            this.var34OutputMin.Enabled = false;
            this.var34OutputMin.Location = new System.Drawing.Point(365, 479);
            this.var34OutputMin.Name = "var34OutputMin";
            this.var34OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var34OutputMin.TabIndex = 744;
            this.var34OutputMin.Visible = false;
            this.var34OutputMin.TextChanged += new System.EventHandler(this.var34OutputMin_TextChanged);
            // 
            // var34InputMax
            // 
            this.var34InputMax.Enabled = false;
            this.var34InputMax.Location = new System.Drawing.Point(287, 479);
            this.var34InputMax.Name = "var34InputMax";
            this.var34InputMax.Size = new System.Drawing.Size(63, 20);
            this.var34InputMax.TabIndex = 743;
            this.var34InputMax.Visible = false;
            this.var34InputMax.TextChanged += new System.EventHandler(this.var34InputMax_TextChanged);
            // 
            // var34InputMin
            // 
            this.var34InputMin.Enabled = false;
            this.var34InputMin.Location = new System.Drawing.Point(211, 479);
            this.var34InputMin.Name = "var34InputMin";
            this.var34InputMin.Size = new System.Drawing.Size(63, 20);
            this.var34InputMin.TabIndex = 742;
            this.var34InputMin.Visible = false;
            this.var34InputMin.TextChanged += new System.EventHandler(this.var34InputMin_TextChanged);
            // 
            // var34Channel
            // 
            this.var34Channel.Enabled = false;
            this.var34Channel.Location = new System.Drawing.Point(130, 479);
            this.var34Channel.Name = "var34Channel";
            this.var34Channel.Size = new System.Drawing.Size(63, 20);
            this.var34Channel.TabIndex = 740;
            this.var34Channel.Visible = false;
            this.var34Channel.TextChanged += new System.EventHandler(this.var34Channel_TextChanged);
            // 
            // var33OutputMax
            // 
            this.var33OutputMax.Enabled = false;
            this.var33OutputMax.Location = new System.Drawing.Point(442, 453);
            this.var33OutputMax.Name = "var33OutputMax";
            this.var33OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var33OutputMax.TabIndex = 739;
            this.var33OutputMax.Visible = false;
            this.var33OutputMax.TextChanged += new System.EventHandler(this.var33OutputMax_TextChanged);
            // 
            // var33OutputMin
            // 
            this.var33OutputMin.Enabled = false;
            this.var33OutputMin.Location = new System.Drawing.Point(365, 453);
            this.var33OutputMin.Name = "var33OutputMin";
            this.var33OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var33OutputMin.TabIndex = 738;
            this.var33OutputMin.Visible = false;
            this.var33OutputMin.TextChanged += new System.EventHandler(this.var33OutputMin_TextChanged);
            // 
            // var33InputMax
            // 
            this.var33InputMax.Enabled = false;
            this.var33InputMax.Location = new System.Drawing.Point(287, 453);
            this.var33InputMax.Name = "var33InputMax";
            this.var33InputMax.Size = new System.Drawing.Size(63, 20);
            this.var33InputMax.TabIndex = 737;
            this.var33InputMax.Visible = false;
            this.var33InputMax.TextChanged += new System.EventHandler(this.var33InputMax_TextChanged);
            // 
            // var33InputMin
            // 
            this.var33InputMin.Enabled = false;
            this.var33InputMin.Location = new System.Drawing.Point(211, 452);
            this.var33InputMin.Name = "var33InputMin";
            this.var33InputMin.Size = new System.Drawing.Size(63, 20);
            this.var33InputMin.TabIndex = 736;
            this.var33InputMin.Visible = false;
            this.var33InputMin.TextChanged += new System.EventHandler(this.var33InputMin_TextChanged);
            // 
            // var33Channel
            // 
            this.var33Channel.Enabled = false;
            this.var33Channel.Location = new System.Drawing.Point(130, 453);
            this.var33Channel.Name = "var33Channel";
            this.var33Channel.Size = new System.Drawing.Size(63, 20);
            this.var33Channel.TabIndex = 734;
            this.var33Channel.Visible = false;
            this.var33Channel.TextChanged += new System.EventHandler(this.var33Channel_TextChanged);
            // 
            // var32OutputMax
            // 
            this.var32OutputMax.Enabled = false;
            this.var32OutputMax.Location = new System.Drawing.Point(442, 428);
            this.var32OutputMax.Name = "var32OutputMax";
            this.var32OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var32OutputMax.TabIndex = 731;
            this.var32OutputMax.Visible = false;
            this.var32OutputMax.TextChanged += new System.EventHandler(this.var32OutputMax_TextChanged);
            // 
            // var32OutputMin
            // 
            this.var32OutputMin.Enabled = false;
            this.var32OutputMin.Location = new System.Drawing.Point(365, 428);
            this.var32OutputMin.Name = "var32OutputMin";
            this.var32OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var32OutputMin.TabIndex = 730;
            this.var32OutputMin.Visible = false;
            this.var32OutputMin.TextChanged += new System.EventHandler(this.var32OutputMin_TextChanged);
            // 
            // var32InputMax
            // 
            this.var32InputMax.Enabled = false;
            this.var32InputMax.Location = new System.Drawing.Point(287, 428);
            this.var32InputMax.Name = "var32InputMax";
            this.var32InputMax.Size = new System.Drawing.Size(63, 20);
            this.var32InputMax.TabIndex = 729;
            this.var32InputMax.Visible = false;
            this.var32InputMax.TextChanged += new System.EventHandler(this.var32InputMax_TextChanged);
            // 
            // var32InputMin
            // 
            this.var32InputMin.Enabled = false;
            this.var32InputMin.Location = new System.Drawing.Point(211, 427);
            this.var32InputMin.Name = "var32InputMin";
            this.var32InputMin.Size = new System.Drawing.Size(63, 20);
            this.var32InputMin.TabIndex = 728;
            this.var32InputMin.Visible = false;
            this.var32InputMin.TextChanged += new System.EventHandler(this.var32InputMin_TextChanged);
            // 
            // var32Channel
            // 
            this.var32Channel.Enabled = false;
            this.var32Channel.Location = new System.Drawing.Point(130, 428);
            this.var32Channel.Name = "var32Channel";
            this.var32Channel.Size = new System.Drawing.Size(63, 20);
            this.var32Channel.TabIndex = 726;
            this.var32Channel.Visible = false;
            this.var32Channel.TextChanged += new System.EventHandler(this.var32Channel_TextChanged);
            // 
            // var31OutputMax
            // 
            this.var31OutputMax.Enabled = false;
            this.var31OutputMax.Location = new System.Drawing.Point(442, 401);
            this.var31OutputMax.Name = "var31OutputMax";
            this.var31OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var31OutputMax.TabIndex = 725;
            this.var31OutputMax.Visible = false;
            this.var31OutputMax.TextChanged += new System.EventHandler(this.var31OutputMax_TextChanged);
            // 
            // var31OutputMin
            // 
            this.var31OutputMin.Enabled = false;
            this.var31OutputMin.Location = new System.Drawing.Point(365, 401);
            this.var31OutputMin.Name = "var31OutputMin";
            this.var31OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var31OutputMin.TabIndex = 724;
            this.var31OutputMin.Visible = false;
            this.var31OutputMin.TextChanged += new System.EventHandler(this.var31OutputMin_TextChanged);
            // 
            // var31InputMax
            // 
            this.var31InputMax.Enabled = false;
            this.var31InputMax.Location = new System.Drawing.Point(287, 401);
            this.var31InputMax.Name = "var31InputMax";
            this.var31InputMax.Size = new System.Drawing.Size(63, 20);
            this.var31InputMax.TabIndex = 723;
            this.var31InputMax.Visible = false;
            this.var31InputMax.TextChanged += new System.EventHandler(this.var31InputMax_TextChanged);
            // 
            // var31InputMin
            // 
            this.var31InputMin.Enabled = false;
            this.var31InputMin.Location = new System.Drawing.Point(211, 401);
            this.var31InputMin.Name = "var31InputMin";
            this.var31InputMin.Size = new System.Drawing.Size(63, 20);
            this.var31InputMin.TabIndex = 722;
            this.var31InputMin.Visible = false;
            this.var31InputMin.TextChanged += new System.EventHandler(this.var31InputMin_TextChanged);
            // 
            // var31Channel
            // 
            this.var31Channel.Enabled = false;
            this.var31Channel.Location = new System.Drawing.Point(130, 401);
            this.var31Channel.Name = "var31Channel";
            this.var31Channel.Size = new System.Drawing.Size(63, 20);
            this.var31Channel.TabIndex = 720;
            this.var31Channel.Visible = false;
            this.var31Channel.TextChanged += new System.EventHandler(this.var31Channel_TextChanged);
            // 
            // var30DisplayName
            // 
            this.var30DisplayName.Enabled = false;
            this.var30DisplayName.Location = new System.Drawing.Point(1073, 375);
            this.var30DisplayName.Name = "var30DisplayName";
            this.var30DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var30DisplayName.TabIndex = 717;
            this.var30DisplayName.Visible = false;
            this.var30DisplayName.TextChanged += new System.EventHandler(this.var30DisplayName_TextChanged);
            // 
            // var29DisplayName
            // 
            this.var29DisplayName.Enabled = false;
            this.var29DisplayName.Location = new System.Drawing.Point(1073, 349);
            this.var29DisplayName.Name = "var29DisplayName";
            this.var29DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var29DisplayName.TabIndex = 716;
            this.var29DisplayName.Visible = false;
            this.var29DisplayName.TextChanged += new System.EventHandler(this.var29DisplayName_TextChanged);
            // 
            // var28DisplayName
            // 
            this.var28DisplayName.Enabled = false;
            this.var28DisplayName.Location = new System.Drawing.Point(1073, 324);
            this.var28DisplayName.Name = "var28DisplayName";
            this.var28DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var28DisplayName.TabIndex = 715;
            this.var28DisplayName.Visible = false;
            this.var28DisplayName.TextChanged += new System.EventHandler(this.var28DisplayName_TextChanged);
            // 
            // var27DisplayName
            // 
            this.var27DisplayName.Enabled = false;
            this.var27DisplayName.Location = new System.Drawing.Point(1073, 297);
            this.var27DisplayName.Name = "var27DisplayName";
            this.var27DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var27DisplayName.TabIndex = 714;
            this.var27DisplayName.Visible = false;
            this.var27DisplayName.TextChanged += new System.EventHandler(this.var27DisplayName_TextChanged);
            // 
            // var26DisplayName
            // 
            this.var26DisplayName.Enabled = false;
            this.var26DisplayName.Location = new System.Drawing.Point(1073, 273);
            this.var26DisplayName.Name = "var26DisplayName";
            this.var26DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var26DisplayName.TabIndex = 713;
            this.var26DisplayName.Visible = false;
            this.var26DisplayName.TextChanged += new System.EventHandler(this.var26DisplayName_TextChanged);
            // 
            // var25DisplayName
            // 
            this.var25DisplayName.Enabled = false;
            this.var25DisplayName.Location = new System.Drawing.Point(1073, 246);
            this.var25DisplayName.Name = "var25DisplayName";
            this.var25DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var25DisplayName.TabIndex = 712;
            this.var25DisplayName.Visible = false;
            this.var25DisplayName.TextChanged += new System.EventHandler(this.var25DisplayName_TextChanged);
            // 
            // var30Type
            // 
            this.var30Type.Enabled = false;
            this.var30Type.Location = new System.Drawing.Point(995, 375);
            this.var30Type.Name = "var30Type";
            this.var30Type.Size = new System.Drawing.Size(63, 20);
            this.var30Type.TabIndex = 711;
            this.var30Type.Visible = false;
            this.var30Type.TextChanged += new System.EventHandler(this.var30Type_TextChanged);
            // 
            // var29Type
            // 
            this.var29Type.Enabled = false;
            this.var29Type.Location = new System.Drawing.Point(995, 349);
            this.var29Type.Name = "var29Type";
            this.var29Type.Size = new System.Drawing.Size(63, 20);
            this.var29Type.TabIndex = 710;
            this.var29Type.Visible = false;
            this.var29Type.TextChanged += new System.EventHandler(this.var29Type_TextChanged);
            // 
            // var28Type
            // 
            this.var28Type.Enabled = false;
            this.var28Type.Location = new System.Drawing.Point(995, 324);
            this.var28Type.Name = "var28Type";
            this.var28Type.Size = new System.Drawing.Size(63, 20);
            this.var28Type.TabIndex = 709;
            this.var28Type.Visible = false;
            this.var28Type.TextChanged += new System.EventHandler(this.var28Type_TextChanged);
            // 
            // var27Type
            // 
            this.var27Type.Enabled = false;
            this.var27Type.Location = new System.Drawing.Point(995, 297);
            this.var27Type.Name = "var27Type";
            this.var27Type.Size = new System.Drawing.Size(63, 20);
            this.var27Type.TabIndex = 708;
            this.var27Type.Visible = false;
            this.var27Type.TextChanged += new System.EventHandler(this.var27Type_TextChanged);
            // 
            // var26Type
            // 
            this.var26Type.Enabled = false;
            this.var26Type.Location = new System.Drawing.Point(995, 273);
            this.var26Type.Name = "var26Type";
            this.var26Type.Size = new System.Drawing.Size(63, 20);
            this.var26Type.TabIndex = 707;
            this.var26Type.Visible = false;
            this.var26Type.TextChanged += new System.EventHandler(this.var26Type_TextChanged);
            // 
            // var25Type
            // 
            this.var25Type.Enabled = false;
            this.var25Type.Location = new System.Drawing.Point(995, 246);
            this.var25Type.Name = "var25Type";
            this.var25Type.Size = new System.Drawing.Size(63, 20);
            this.var25Type.TabIndex = 706;
            this.var25Type.Visible = false;
            this.var25Type.TextChanged += new System.EventHandler(this.var25Type_TextChanged);
            // 
            // var30StatusColumn
            // 
            this.var30StatusColumn.Enabled = false;
            this.var30StatusColumn.Location = new System.Drawing.Point(911, 375);
            this.var30StatusColumn.Name = "var30StatusColumn";
            this.var30StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var30StatusColumn.TabIndex = 705;
            this.var30StatusColumn.Visible = false;
            this.var30StatusColumn.TextChanged += new System.EventHandler(this.var30StatusColumn_TextChanged);
            // 
            // var29StatusColumn
            // 
            this.var29StatusColumn.Enabled = false;
            this.var29StatusColumn.Location = new System.Drawing.Point(911, 349);
            this.var29StatusColumn.Name = "var29StatusColumn";
            this.var29StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var29StatusColumn.TabIndex = 704;
            this.var29StatusColumn.Visible = false;
            this.var29StatusColumn.TextChanged += new System.EventHandler(this.var29StatusColumn_TextChanged);
            // 
            // var28StatusColumn
            // 
            this.var28StatusColumn.Enabled = false;
            this.var28StatusColumn.Location = new System.Drawing.Point(911, 324);
            this.var28StatusColumn.Name = "var28StatusColumn";
            this.var28StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var28StatusColumn.TabIndex = 703;
            this.var28StatusColumn.Visible = false;
            this.var28StatusColumn.TextChanged += new System.EventHandler(this.var28StatusColumn_TextChanged);
            // 
            // var27StatusColumn
            // 
            this.var27StatusColumn.Enabled = false;
            this.var27StatusColumn.Location = new System.Drawing.Point(911, 297);
            this.var27StatusColumn.Name = "var27StatusColumn";
            this.var27StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var27StatusColumn.TabIndex = 702;
            this.var27StatusColumn.Visible = false;
            this.var27StatusColumn.TextChanged += new System.EventHandler(this.var27StatusColumn_TextChanged);
            // 
            // var26StatusColumn
            // 
            this.var26StatusColumn.Enabled = false;
            this.var26StatusColumn.Location = new System.Drawing.Point(911, 273);
            this.var26StatusColumn.Name = "var26StatusColumn";
            this.var26StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var26StatusColumn.TabIndex = 701;
            this.var26StatusColumn.Visible = false;
            this.var26StatusColumn.TextChanged += new System.EventHandler(this.var26StatusColumn_TextChanged);
            // 
            // var25StatusColumn
            // 
            this.var25StatusColumn.Enabled = false;
            this.var25StatusColumn.Location = new System.Drawing.Point(911, 246);
            this.var25StatusColumn.Name = "var25StatusColumn";
            this.var25StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var25StatusColumn.TabIndex = 700;
            this.var25StatusColumn.Visible = false;
            this.var25StatusColumn.TextChanged += new System.EventHandler(this.var25StatusColumn_TextChanged);
            // 
            // var30ValueColumn
            // 
            this.var30ValueColumn.Enabled = false;
            this.var30ValueColumn.Location = new System.Drawing.Point(830, 375);
            this.var30ValueColumn.Name = "var30ValueColumn";
            this.var30ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var30ValueColumn.TabIndex = 699;
            this.var30ValueColumn.Visible = false;
            this.var30ValueColumn.TextChanged += new System.EventHandler(this.var30ValueColumn_TextChanged);
            // 
            // var30Unit
            // 
            this.var30Unit.Enabled = false;
            this.var30Unit.Location = new System.Drawing.Point(753, 375);
            this.var30Unit.Name = "var30Unit";
            this.var30Unit.Size = new System.Drawing.Size(63, 20);
            this.var30Unit.TabIndex = 698;
            this.var30Unit.Visible = false;
            this.var30Unit.TextChanged += new System.EventHandler(this.var30Unit_TextChanged);
            // 
            // var29ValueColumn
            // 
            this.var29ValueColumn.Enabled = false;
            this.var29ValueColumn.Location = new System.Drawing.Point(830, 348);
            this.var29ValueColumn.Name = "var29ValueColumn";
            this.var29ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var29ValueColumn.TabIndex = 697;
            this.var29ValueColumn.Visible = false;
            this.var29ValueColumn.TextChanged += new System.EventHandler(this.var29ValueColumn_TextChanged);
            // 
            // var29Unit
            // 
            this.var29Unit.Enabled = false;
            this.var29Unit.Location = new System.Drawing.Point(753, 348);
            this.var29Unit.Name = "var29Unit";
            this.var29Unit.Size = new System.Drawing.Size(63, 20);
            this.var29Unit.TabIndex = 696;
            this.var29Unit.Visible = false;
            this.var29Unit.TextChanged += new System.EventHandler(this.var29Unit_TextChanged);
            // 
            // var28ValueColumn
            // 
            this.var28ValueColumn.Enabled = false;
            this.var28ValueColumn.Location = new System.Drawing.Point(830, 323);
            this.var28ValueColumn.Name = "var28ValueColumn";
            this.var28ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var28ValueColumn.TabIndex = 695;
            this.var28ValueColumn.Visible = false;
            this.var28ValueColumn.TextChanged += new System.EventHandler(this.var28ValueColumn_TextChanged);
            // 
            // var28Unit
            // 
            this.var28Unit.Enabled = false;
            this.var28Unit.Location = new System.Drawing.Point(753, 323);
            this.var28Unit.Name = "var28Unit";
            this.var28Unit.Size = new System.Drawing.Size(63, 20);
            this.var28Unit.TabIndex = 694;
            this.var28Unit.Visible = false;
            this.var28Unit.TextChanged += new System.EventHandler(this.var28Unit_TextChanged);
            // 
            // var27ValueColumn
            // 
            this.var27ValueColumn.Enabled = false;
            this.var27ValueColumn.Location = new System.Drawing.Point(830, 297);
            this.var27ValueColumn.Name = "var27ValueColumn";
            this.var27ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var27ValueColumn.TabIndex = 693;
            this.var27ValueColumn.Visible = false;
            this.var27ValueColumn.TextChanged += new System.EventHandler(this.var27ValueColumn_TextChanged);
            // 
            // var27Unit
            // 
            this.var27Unit.Enabled = false;
            this.var27Unit.Location = new System.Drawing.Point(753, 297);
            this.var27Unit.Name = "var27Unit";
            this.var27Unit.Size = new System.Drawing.Size(63, 20);
            this.var27Unit.TabIndex = 692;
            this.var27Unit.Visible = false;
            this.var27Unit.TextChanged += new System.EventHandler(this.var27Unit_TextChanged);
            // 
            // var26ValueColumn
            // 
            this.var26ValueColumn.Enabled = false;
            this.var26ValueColumn.Location = new System.Drawing.Point(830, 272);
            this.var26ValueColumn.Name = "var26ValueColumn";
            this.var26ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var26ValueColumn.TabIndex = 691;
            this.var26ValueColumn.Visible = false;
            this.var26ValueColumn.TextChanged += new System.EventHandler(this.var26ValueColumn_TextChanged);
            // 
            // var26Unit
            // 
            this.var26Unit.Enabled = false;
            this.var26Unit.Location = new System.Drawing.Point(753, 272);
            this.var26Unit.Name = "var26Unit";
            this.var26Unit.Size = new System.Drawing.Size(63, 20);
            this.var26Unit.TabIndex = 690;
            this.var26Unit.Visible = false;
            this.var26Unit.TextChanged += new System.EventHandler(this.var26Unit_TextChanged);
            // 
            // var25ValueColumn
            // 
            this.var25ValueColumn.Enabled = false;
            this.var25ValueColumn.Location = new System.Drawing.Point(830, 245);
            this.var25ValueColumn.Name = "var25ValueColumn";
            this.var25ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var25ValueColumn.TabIndex = 689;
            this.var25ValueColumn.Visible = false;
            this.var25ValueColumn.TextChanged += new System.EventHandler(this.var25ValueColumn_TextChanged);
            // 
            // var25Unit
            // 
            this.var25Unit.Enabled = false;
            this.var25Unit.Location = new System.Drawing.Point(753, 245);
            this.var25Unit.Name = "var25Unit";
            this.var25Unit.Size = new System.Drawing.Size(63, 20);
            this.var25Unit.TabIndex = 688;
            this.var25Unit.Visible = false;
            this.var25Unit.TextChanged += new System.EventHandler(this.var25Unit_TextChanged);
            // 
            // var30Offset
            // 
            this.var30Offset.Enabled = false;
            this.var30Offset.Location = new System.Drawing.Point(523, 375);
            this.var30Offset.Name = "var30Offset";
            this.var30Offset.Size = new System.Drawing.Size(63, 20);
            this.var30Offset.TabIndex = 687;
            this.var30Offset.Visible = false;
            this.var30Offset.TextChanged += new System.EventHandler(this.var30Offset_TextChanged);
            // 
            // var29Offset
            // 
            this.var29Offset.Enabled = false;
            this.var29Offset.Location = new System.Drawing.Point(523, 349);
            this.var29Offset.Name = "var29Offset";
            this.var29Offset.Size = new System.Drawing.Size(63, 20);
            this.var29Offset.TabIndex = 686;
            this.var29Offset.Visible = false;
            this.var29Offset.TextChanged += new System.EventHandler(this.var29Offset_TextChanged);
            // 
            // var28Offset
            // 
            this.var28Offset.Enabled = false;
            this.var28Offset.Location = new System.Drawing.Point(523, 324);
            this.var28Offset.Name = "var28Offset";
            this.var28Offset.Size = new System.Drawing.Size(63, 20);
            this.var28Offset.TabIndex = 685;
            this.var28Offset.Visible = false;
            this.var28Offset.TextChanged += new System.EventHandler(this.var28Offset_TextChanged);
            // 
            // var27Offset
            // 
            this.var27Offset.Enabled = false;
            this.var27Offset.Location = new System.Drawing.Point(523, 297);
            this.var27Offset.Name = "var27Offset";
            this.var27Offset.Size = new System.Drawing.Size(63, 20);
            this.var27Offset.TabIndex = 684;
            this.var27Offset.Visible = false;
            this.var27Offset.TextChanged += new System.EventHandler(this.var27Offset_TextChanged);
            // 
            // var26Offset
            // 
            this.var26Offset.Enabled = false;
            this.var26Offset.Location = new System.Drawing.Point(523, 273);
            this.var26Offset.Name = "var26Offset";
            this.var26Offset.Size = new System.Drawing.Size(63, 20);
            this.var26Offset.TabIndex = 683;
            this.var26Offset.Visible = false;
            this.var26Offset.TextChanged += new System.EventHandler(this.var26Offset_TextChanged);
            // 
            // var25Offset
            // 
            this.var25Offset.Enabled = false;
            this.var25Offset.Location = new System.Drawing.Point(523, 246);
            this.var25Offset.Name = "var25Offset";
            this.var25Offset.Size = new System.Drawing.Size(63, 20);
            this.var25Offset.TabIndex = 682;
            this.var25Offset.Visible = false;
            this.var25Offset.TextChanged += new System.EventHandler(this.var25Offset_TextChanged);
            // 
            // var30OutputMax
            // 
            this.var30OutputMax.Enabled = false;
            this.var30OutputMax.Location = new System.Drawing.Point(442, 375);
            this.var30OutputMax.Name = "var30OutputMax";
            this.var30OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var30OutputMax.TabIndex = 681;
            this.var30OutputMax.Visible = false;
            this.var30OutputMax.TextChanged += new System.EventHandler(this.var30OutputMax_TextChanged);
            // 
            // var30OutputMin
            // 
            this.var30OutputMin.Enabled = false;
            this.var30OutputMin.Location = new System.Drawing.Point(365, 375);
            this.var30OutputMin.Name = "var30OutputMin";
            this.var30OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var30OutputMin.TabIndex = 680;
            this.var30OutputMin.Visible = false;
            this.var30OutputMin.TextChanged += new System.EventHandler(this.var30OutputMin_TextChanged);
            // 
            // var30InputMax
            // 
            this.var30InputMax.Enabled = false;
            this.var30InputMax.Location = new System.Drawing.Point(287, 375);
            this.var30InputMax.Name = "var30InputMax";
            this.var30InputMax.Size = new System.Drawing.Size(63, 20);
            this.var30InputMax.TabIndex = 679;
            this.var30InputMax.Visible = false;
            this.var30InputMax.TextChanged += new System.EventHandler(this.var30InputMax_TextChanged);
            // 
            // var30InputMin
            // 
            this.var30InputMin.Enabled = false;
            this.var30InputMin.Location = new System.Drawing.Point(211, 374);
            this.var30InputMin.Name = "var30InputMin";
            this.var30InputMin.Size = new System.Drawing.Size(63, 20);
            this.var30InputMin.TabIndex = 678;
            this.var30InputMin.Visible = false;
            this.var30InputMin.TextChanged += new System.EventHandler(this.var30InputMin_TextChanged);
            // 
            // var30Channel
            // 
            this.var30Channel.Enabled = false;
            this.var30Channel.Location = new System.Drawing.Point(130, 375);
            this.var30Channel.Name = "var30Channel";
            this.var30Channel.Size = new System.Drawing.Size(63, 20);
            this.var30Channel.TabIndex = 676;
            this.var30Channel.Visible = false;
            this.var30Channel.TextChanged += new System.EventHandler(this.var30Channel_TextChanged);
            // 
            // var29OutputMax
            // 
            this.var29OutputMax.Enabled = false;
            this.var29OutputMax.Location = new System.Drawing.Point(442, 348);
            this.var29OutputMax.Name = "var29OutputMax";
            this.var29OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var29OutputMax.TabIndex = 675;
            this.var29OutputMax.Visible = false;
            this.var29OutputMax.TextChanged += new System.EventHandler(this.var29OutputMax_TextChanged);
            // 
            // var29OutputMin
            // 
            this.var29OutputMin.Enabled = false;
            this.var29OutputMin.Location = new System.Drawing.Point(365, 348);
            this.var29OutputMin.Name = "var29OutputMin";
            this.var29OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var29OutputMin.TabIndex = 674;
            this.var29OutputMin.Visible = false;
            this.var29OutputMin.TextChanged += new System.EventHandler(this.var29OutputMin_TextChanged);
            // 
            // var29InputMax
            // 
            this.var29InputMax.Enabled = false;
            this.var29InputMax.Location = new System.Drawing.Point(287, 348);
            this.var29InputMax.Name = "var29InputMax";
            this.var29InputMax.Size = new System.Drawing.Size(63, 20);
            this.var29InputMax.TabIndex = 673;
            this.var29InputMax.Visible = false;
            this.var29InputMax.TextChanged += new System.EventHandler(this.var29InputMax_TextChanged);
            // 
            // var29InputMin
            // 
            this.var29InputMin.Enabled = false;
            this.var29InputMin.Location = new System.Drawing.Point(211, 347);
            this.var29InputMin.Name = "var29InputMin";
            this.var29InputMin.Size = new System.Drawing.Size(63, 20);
            this.var29InputMin.TabIndex = 672;
            this.var29InputMin.Visible = false;
            this.var29InputMin.TextChanged += new System.EventHandler(this.var29InputMin_TextChanged);
            // 
            // var29Channel
            // 
            this.var29Channel.Enabled = false;
            this.var29Channel.Location = new System.Drawing.Point(130, 348);
            this.var29Channel.Name = "var29Channel";
            this.var29Channel.Size = new System.Drawing.Size(63, 20);
            this.var29Channel.TabIndex = 670;
            this.var29Channel.Visible = false;
            this.var29Channel.TextChanged += new System.EventHandler(this.var29Channel_TextChanged);
            // 
            // var28OutputMax
            // 
            this.var28OutputMax.Enabled = false;
            this.var28OutputMax.Location = new System.Drawing.Point(442, 323);
            this.var28OutputMax.Name = "var28OutputMax";
            this.var28OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var28OutputMax.TabIndex = 667;
            this.var28OutputMax.Visible = false;
            this.var28OutputMax.TextChanged += new System.EventHandler(this.var28OutputMax_TextChanged);
            // 
            // var28OutputMin
            // 
            this.var28OutputMin.Enabled = false;
            this.var28OutputMin.Location = new System.Drawing.Point(365, 323);
            this.var28OutputMin.Name = "var28OutputMin";
            this.var28OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var28OutputMin.TabIndex = 666;
            this.var28OutputMin.Visible = false;
            this.var28OutputMin.TextChanged += new System.EventHandler(this.var28OutputMin_TextChanged);
            // 
            // var28InputMax
            // 
            this.var28InputMax.Enabled = false;
            this.var28InputMax.Location = new System.Drawing.Point(287, 323);
            this.var28InputMax.Name = "var28InputMax";
            this.var28InputMax.Size = new System.Drawing.Size(63, 20);
            this.var28InputMax.TabIndex = 665;
            this.var28InputMax.Visible = false;
            this.var28InputMax.TextChanged += new System.EventHandler(this.var28InputMax_TextChanged);
            // 
            // var28InputMin
            // 
            this.var28InputMin.Enabled = false;
            this.var28InputMin.Location = new System.Drawing.Point(211, 323);
            this.var28InputMin.Name = "var28InputMin";
            this.var28InputMin.Size = new System.Drawing.Size(63, 20);
            this.var28InputMin.TabIndex = 664;
            this.var28InputMin.Visible = false;
            this.var28InputMin.TextChanged += new System.EventHandler(this.var28InputMin_TextChanged);
            // 
            // var28Channel
            // 
            this.var28Channel.Enabled = false;
            this.var28Channel.Location = new System.Drawing.Point(130, 323);
            this.var28Channel.Name = "var28Channel";
            this.var28Channel.Size = new System.Drawing.Size(63, 20);
            this.var28Channel.TabIndex = 662;
            this.var28Channel.Visible = false;
            this.var28Channel.TextChanged += new System.EventHandler(this.var28Channel_TextChanged);
            // 
            // var27OutputMax
            // 
            this.var27OutputMax.Enabled = false;
            this.var27OutputMax.Location = new System.Drawing.Point(442, 297);
            this.var27OutputMax.Name = "var27OutputMax";
            this.var27OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var27OutputMax.TabIndex = 661;
            this.var27OutputMax.Visible = false;
            this.var27OutputMax.TextChanged += new System.EventHandler(this.var27OutputMax_TextChanged);
            // 
            // var27OutputMin
            // 
            this.var27OutputMin.Enabled = false;
            this.var27OutputMin.Location = new System.Drawing.Point(365, 297);
            this.var27OutputMin.Name = "var27OutputMin";
            this.var27OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var27OutputMin.TabIndex = 660;
            this.var27OutputMin.Visible = false;
            this.var27OutputMin.TextChanged += new System.EventHandler(this.var27OutputMin_TextChanged);
            // 
            // var27InputMax
            // 
            this.var27InputMax.Enabled = false;
            this.var27InputMax.Location = new System.Drawing.Point(287, 297);
            this.var27InputMax.Name = "var27InputMax";
            this.var27InputMax.Size = new System.Drawing.Size(63, 20);
            this.var27InputMax.TabIndex = 659;
            this.var27InputMax.Visible = false;
            this.var27InputMax.TextChanged += new System.EventHandler(this.var27InputMax_TextChanged);
            // 
            // var27InputMin
            // 
            this.var27InputMin.Enabled = false;
            this.var27InputMin.Location = new System.Drawing.Point(211, 296);
            this.var27InputMin.Name = "var27InputMin";
            this.var27InputMin.Size = new System.Drawing.Size(63, 20);
            this.var27InputMin.TabIndex = 658;
            this.var27InputMin.Visible = false;
            this.var27InputMin.TextChanged += new System.EventHandler(this.var27InputMin_TextChanged);
            // 
            // var27Channel
            // 
            this.var27Channel.Enabled = false;
            this.var27Channel.Location = new System.Drawing.Point(130, 297);
            this.var27Channel.Name = "var27Channel";
            this.var27Channel.Size = new System.Drawing.Size(63, 20);
            this.var27Channel.TabIndex = 656;
            this.var27Channel.Visible = false;
            this.var27Channel.TextChanged += new System.EventHandler(this.var27Channel_TextChanged);
            // 
            // var26OutputMax
            // 
            this.var26OutputMax.Enabled = false;
            this.var26OutputMax.Location = new System.Drawing.Point(442, 272);
            this.var26OutputMax.Name = "var26OutputMax";
            this.var26OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var26OutputMax.TabIndex = 653;
            this.var26OutputMax.Visible = false;
            this.var26OutputMax.TextChanged += new System.EventHandler(this.var26OutputMax_TextChanged);
            // 
            // var26OutputMin
            // 
            this.var26OutputMin.Enabled = false;
            this.var26OutputMin.Location = new System.Drawing.Point(365, 272);
            this.var26OutputMin.Name = "var26OutputMin";
            this.var26OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var26OutputMin.TabIndex = 652;
            this.var26OutputMin.Visible = false;
            this.var26OutputMin.TextChanged += new System.EventHandler(this.var26OutputMin_TextChanged);
            // 
            // var26InputMax
            // 
            this.var26InputMax.Enabled = false;
            this.var26InputMax.Location = new System.Drawing.Point(287, 272);
            this.var26InputMax.Name = "var26InputMax";
            this.var26InputMax.Size = new System.Drawing.Size(63, 20);
            this.var26InputMax.TabIndex = 651;
            this.var26InputMax.Visible = false;
            this.var26InputMax.TextChanged += new System.EventHandler(this.var26InputMax_TextChanged);
            // 
            // var26InputMin
            // 
            this.var26InputMin.Enabled = false;
            this.var26InputMin.Location = new System.Drawing.Point(211, 271);
            this.var26InputMin.Name = "var26InputMin";
            this.var26InputMin.Size = new System.Drawing.Size(63, 20);
            this.var26InputMin.TabIndex = 650;
            this.var26InputMin.Visible = false;
            this.var26InputMin.TextChanged += new System.EventHandler(this.var26InputMin_TextChanged);
            // 
            // var26Channel
            // 
            this.var26Channel.Enabled = false;
            this.var26Channel.Location = new System.Drawing.Point(130, 272);
            this.var26Channel.Name = "var26Channel";
            this.var26Channel.Size = new System.Drawing.Size(63, 20);
            this.var26Channel.TabIndex = 648;
            this.var26Channel.Visible = false;
            this.var26Channel.TextChanged += new System.EventHandler(this.var26Channel_TextChanged);
            // 
            // var25OutputMax
            // 
            this.var25OutputMax.Enabled = false;
            this.var25OutputMax.Location = new System.Drawing.Point(442, 245);
            this.var25OutputMax.Name = "var25OutputMax";
            this.var25OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var25OutputMax.TabIndex = 647;
            this.var25OutputMax.Visible = false;
            this.var25OutputMax.TextChanged += new System.EventHandler(this.var25OutputMax_TextChanged);
            // 
            // var25OutputMin
            // 
            this.var25OutputMin.Enabled = false;
            this.var25OutputMin.Location = new System.Drawing.Point(365, 245);
            this.var25OutputMin.Name = "var25OutputMin";
            this.var25OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var25OutputMin.TabIndex = 646;
            this.var25OutputMin.Visible = false;
            this.var25OutputMin.TextChanged += new System.EventHandler(this.var25OutputMin_TextChanged);
            // 
            // var25InputMax
            // 
            this.var25InputMax.Enabled = false;
            this.var25InputMax.Location = new System.Drawing.Point(287, 245);
            this.var25InputMax.Name = "var25InputMax";
            this.var25InputMax.Size = new System.Drawing.Size(63, 20);
            this.var25InputMax.TabIndex = 645;
            this.var25InputMax.Visible = false;
            this.var25InputMax.TextChanged += new System.EventHandler(this.var25InputMax_TextChanged);
            // 
            // var25InputMin
            // 
            this.var25InputMin.Enabled = false;
            this.var25InputMin.Location = new System.Drawing.Point(211, 245);
            this.var25InputMin.Name = "var25InputMin";
            this.var25InputMin.Size = new System.Drawing.Size(63, 20);
            this.var25InputMin.TabIndex = 644;
            this.var25InputMin.Visible = false;
            this.var25InputMin.TextChanged += new System.EventHandler(this.var25InputMin_TextChanged);
            // 
            // var25Channel
            // 
            this.var25Channel.Enabled = false;
            this.var25Channel.Location = new System.Drawing.Point(130, 245);
            this.var25Channel.Name = "var25Channel";
            this.var25Channel.Size = new System.Drawing.Size(63, 20);
            this.var25Channel.TabIndex = 642;
            this.var25Channel.Visible = false;
            this.var25Channel.TextChanged += new System.EventHandler(this.var25Channel_TextChanged);
            // 
            // var24DisplayName
            // 
            this.var24DisplayName.Enabled = false;
            this.var24DisplayName.Location = new System.Drawing.Point(1073, 219);
            this.var24DisplayName.Name = "var24DisplayName";
            this.var24DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var24DisplayName.TabIndex = 639;
            this.var24DisplayName.Visible = false;
            this.var24DisplayName.TextChanged += new System.EventHandler(this.var24DisplayName_TextChanged);
            // 
            // var23DisplayName
            // 
            this.var23DisplayName.Enabled = false;
            this.var23DisplayName.Location = new System.Drawing.Point(1073, 193);
            this.var23DisplayName.Name = "var23DisplayName";
            this.var23DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var23DisplayName.TabIndex = 638;
            this.var23DisplayName.Visible = false;
            this.var23DisplayName.TextChanged += new System.EventHandler(this.var23DisplayName_TextChanged);
            // 
            // var22DisplayName
            // 
            this.var22DisplayName.Location = new System.Drawing.Point(1073, 168);
            this.var22DisplayName.Name = "var22DisplayName";
            this.var22DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var22DisplayName.TabIndex = 637;
            // 
            // var21DisplayName
            // 
            this.var21DisplayName.Location = new System.Drawing.Point(1073, 141);
            this.var21DisplayName.Name = "var21DisplayName";
            this.var21DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var21DisplayName.TabIndex = 636;
            // 
            // var20DisplayName
            // 
            this.var20DisplayName.Location = new System.Drawing.Point(1073, 117);
            this.var20DisplayName.Name = "var20DisplayName";
            this.var20DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var20DisplayName.TabIndex = 635;
            // 
            // var19DisplayName
            // 
            this.var19DisplayName.Location = new System.Drawing.Point(1073, 90);
            this.var19DisplayName.Name = "var19DisplayName";
            this.var19DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var19DisplayName.TabIndex = 634;
            // 
            // var24Type
            // 
            this.var24Type.Enabled = false;
            this.var24Type.Location = new System.Drawing.Point(995, 219);
            this.var24Type.Name = "var24Type";
            this.var24Type.Size = new System.Drawing.Size(63, 20);
            this.var24Type.TabIndex = 632;
            this.var24Type.Visible = false;
            this.var24Type.TextChanged += new System.EventHandler(this.var24Type_TextChanged);
            // 
            // var23Type
            // 
            this.var23Type.Enabled = false;
            this.var23Type.Location = new System.Drawing.Point(995, 193);
            this.var23Type.Name = "var23Type";
            this.var23Type.Size = new System.Drawing.Size(63, 20);
            this.var23Type.TabIndex = 631;
            this.var23Type.Visible = false;
            this.var23Type.TextChanged += new System.EventHandler(this.var23Type_TextChanged);
            // 
            // var22Type
            // 
            this.var22Type.Enabled = false;
            this.var22Type.Location = new System.Drawing.Point(995, 168);
            this.var22Type.Name = "var22Type";
            this.var22Type.Size = new System.Drawing.Size(63, 20);
            this.var22Type.TabIndex = 630;
            // 
            // var21Type
            // 
            this.var21Type.Enabled = false;
            this.var21Type.Location = new System.Drawing.Point(995, 141);
            this.var21Type.Name = "var21Type";
            this.var21Type.Size = new System.Drawing.Size(63, 20);
            this.var21Type.TabIndex = 629;
            // 
            // var20Type
            // 
            this.var20Type.Enabled = false;
            this.var20Type.Location = new System.Drawing.Point(995, 117);
            this.var20Type.Name = "var20Type";
            this.var20Type.Size = new System.Drawing.Size(63, 20);
            this.var20Type.TabIndex = 628;
            // 
            // var19Type
            // 
            this.var19Type.Enabled = false;
            this.var19Type.Location = new System.Drawing.Point(995, 90);
            this.var19Type.Name = "var19Type";
            this.var19Type.Size = new System.Drawing.Size(63, 20);
            this.var19Type.TabIndex = 627;
            // 
            // var24StatusColumn
            // 
            this.var24StatusColumn.Enabled = false;
            this.var24StatusColumn.Location = new System.Drawing.Point(911, 219);
            this.var24StatusColumn.Name = "var24StatusColumn";
            this.var24StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var24StatusColumn.TabIndex = 625;
            this.var24StatusColumn.Visible = false;
            this.var24StatusColumn.TextChanged += new System.EventHandler(this.var24StatusColumn_TextChanged);
            // 
            // var23StatusColumn
            // 
            this.var23StatusColumn.Enabled = false;
            this.var23StatusColumn.Location = new System.Drawing.Point(911, 193);
            this.var23StatusColumn.Name = "var23StatusColumn";
            this.var23StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var23StatusColumn.TabIndex = 624;
            this.var23StatusColumn.Visible = false;
            this.var23StatusColumn.TextChanged += new System.EventHandler(this.var23StatusColumn_TextChanged);
            // 
            // var22StatusColumn
            // 
            this.var22StatusColumn.Enabled = false;
            this.var22StatusColumn.Location = new System.Drawing.Point(911, 168);
            this.var22StatusColumn.Name = "var22StatusColumn";
            this.var22StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var22StatusColumn.TabIndex = 623;
            // 
            // var21StatusColumn
            // 
            this.var21StatusColumn.Enabled = false;
            this.var21StatusColumn.Location = new System.Drawing.Point(911, 141);
            this.var21StatusColumn.Name = "var21StatusColumn";
            this.var21StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var21StatusColumn.TabIndex = 622;
            // 
            // var20StatusColumn
            // 
            this.var20StatusColumn.Enabled = false;
            this.var20StatusColumn.Location = new System.Drawing.Point(911, 117);
            this.var20StatusColumn.Name = "var20StatusColumn";
            this.var20StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var20StatusColumn.TabIndex = 621;
            // 
            // var19StatusColumn
            // 
            this.var19StatusColumn.Enabled = false;
            this.var19StatusColumn.Location = new System.Drawing.Point(911, 90);
            this.var19StatusColumn.Name = "var19StatusColumn";
            this.var19StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var19StatusColumn.TabIndex = 620;
            // 
            // var24ValueColumn
            // 
            this.var24ValueColumn.Enabled = false;
            this.var24ValueColumn.Location = new System.Drawing.Point(830, 219);
            this.var24ValueColumn.Name = "var24ValueColumn";
            this.var24ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var24ValueColumn.TabIndex = 618;
            this.var24ValueColumn.Visible = false;
            this.var24ValueColumn.TextChanged += new System.EventHandler(this.var24ValueColumn_TextChanged);
            // 
            // var24Unit
            // 
            this.var24Unit.Enabled = false;
            this.var24Unit.Location = new System.Drawing.Point(753, 219);
            this.var24Unit.Name = "var24Unit";
            this.var24Unit.Size = new System.Drawing.Size(63, 20);
            this.var24Unit.TabIndex = 617;
            this.var24Unit.Visible = false;
            this.var24Unit.TextChanged += new System.EventHandler(this.var24Unit_TextChanged);
            // 
            // var23ValueColumn
            // 
            this.var23ValueColumn.Enabled = false;
            this.var23ValueColumn.Location = new System.Drawing.Point(830, 192);
            this.var23ValueColumn.Name = "var23ValueColumn";
            this.var23ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var23ValueColumn.TabIndex = 616;
            this.var23ValueColumn.Visible = false;
            this.var23ValueColumn.TextChanged += new System.EventHandler(this.var23ValueColumn_TextChanged);
            // 
            // var23Unit
            // 
            this.var23Unit.Enabled = false;
            this.var23Unit.Location = new System.Drawing.Point(753, 192);
            this.var23Unit.Name = "var23Unit";
            this.var23Unit.Size = new System.Drawing.Size(63, 20);
            this.var23Unit.TabIndex = 615;
            this.var23Unit.Visible = false;
            this.var23Unit.TextChanged += new System.EventHandler(this.var23Unit_TextChanged);
            // 
            // var22ValueColumn
            // 
            this.var22ValueColumn.Enabled = false;
            this.var22ValueColumn.Location = new System.Drawing.Point(830, 167);
            this.var22ValueColumn.Name = "var22ValueColumn";
            this.var22ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var22ValueColumn.TabIndex = 614;
            // 
            // var22Unit
            // 
            this.var22Unit.Location = new System.Drawing.Point(753, 167);
            this.var22Unit.Name = "var22Unit";
            this.var22Unit.Size = new System.Drawing.Size(63, 20);
            this.var22Unit.TabIndex = 613;
            // 
            // var21ValueColumn
            // 
            this.var21ValueColumn.Enabled = false;
            this.var21ValueColumn.Location = new System.Drawing.Point(830, 141);
            this.var21ValueColumn.Name = "var21ValueColumn";
            this.var21ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var21ValueColumn.TabIndex = 612;
            // 
            // var21Unit
            // 
            this.var21Unit.Location = new System.Drawing.Point(753, 141);
            this.var21Unit.Name = "var21Unit";
            this.var21Unit.Size = new System.Drawing.Size(63, 20);
            this.var21Unit.TabIndex = 611;
            // 
            // var20ValueColumn
            // 
            this.var20ValueColumn.Enabled = false;
            this.var20ValueColumn.Location = new System.Drawing.Point(830, 116);
            this.var20ValueColumn.Name = "var20ValueColumn";
            this.var20ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var20ValueColumn.TabIndex = 610;
            // 
            // var20Unit
            // 
            this.var20Unit.Location = new System.Drawing.Point(753, 116);
            this.var20Unit.Name = "var20Unit";
            this.var20Unit.Size = new System.Drawing.Size(63, 20);
            this.var20Unit.TabIndex = 609;
            // 
            // var19ValueColumn
            // 
            this.var19ValueColumn.Enabled = false;
            this.var19ValueColumn.Location = new System.Drawing.Point(830, 89);
            this.var19ValueColumn.Name = "var19ValueColumn";
            this.var19ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var19ValueColumn.TabIndex = 608;
            // 
            // var19Unit
            // 
            this.var19Unit.Location = new System.Drawing.Point(753, 89);
            this.var19Unit.Name = "var19Unit";
            this.var19Unit.Size = new System.Drawing.Size(63, 20);
            this.var19Unit.TabIndex = 607;
            // 
            // var24Offset
            // 
            this.var24Offset.Enabled = false;
            this.var24Offset.Location = new System.Drawing.Point(523, 219);
            this.var24Offset.Name = "var24Offset";
            this.var24Offset.Size = new System.Drawing.Size(63, 20);
            this.var24Offset.TabIndex = 604;
            this.var24Offset.Visible = false;
            this.var24Offset.TextChanged += new System.EventHandler(this.var24Offset_TextChanged);
            // 
            // var23Offset
            // 
            this.var23Offset.Enabled = false;
            this.var23Offset.Location = new System.Drawing.Point(523, 193);
            this.var23Offset.Name = "var23Offset";
            this.var23Offset.Size = new System.Drawing.Size(63, 20);
            this.var23Offset.TabIndex = 603;
            this.var23Offset.Visible = false;
            this.var23Offset.TextChanged += new System.EventHandler(this.var23Offset_TextChanged);
            // 
            // var22Offset
            // 
            this.var22Offset.Enabled = false;
            this.var22Offset.Location = new System.Drawing.Point(523, 168);
            this.var22Offset.Name = "var22Offset";
            this.var22Offset.Size = new System.Drawing.Size(63, 20);
            this.var22Offset.TabIndex = 602;
            // 
            // var21Offset
            // 
            this.var21Offset.Enabled = false;
            this.var21Offset.Location = new System.Drawing.Point(523, 141);
            this.var21Offset.Name = "var21Offset";
            this.var21Offset.Size = new System.Drawing.Size(63, 20);
            this.var21Offset.TabIndex = 601;
            // 
            // var20Offset
            // 
            this.var20Offset.Enabled = false;
            this.var20Offset.Location = new System.Drawing.Point(523, 117);
            this.var20Offset.Name = "var20Offset";
            this.var20Offset.Size = new System.Drawing.Size(63, 20);
            this.var20Offset.TabIndex = 600;
            // 
            // var19Offset
            // 
            this.var19Offset.Enabled = false;
            this.var19Offset.Location = new System.Drawing.Point(523, 90);
            this.var19Offset.Name = "var19Offset";
            this.var19Offset.Size = new System.Drawing.Size(63, 20);
            this.var19Offset.TabIndex = 599;
            // 
            // var24OutputMax
            // 
            this.var24OutputMax.Enabled = false;
            this.var24OutputMax.Location = new System.Drawing.Point(442, 219);
            this.var24OutputMax.Name = "var24OutputMax";
            this.var24OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var24OutputMax.TabIndex = 597;
            this.var24OutputMax.Visible = false;
            this.var24OutputMax.TextChanged += new System.EventHandler(this.var24OutputMax_TextChanged);
            // 
            // var24OutputMin
            // 
            this.var24OutputMin.Enabled = false;
            this.var24OutputMin.Location = new System.Drawing.Point(365, 219);
            this.var24OutputMin.Name = "var24OutputMin";
            this.var24OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var24OutputMin.TabIndex = 596;
            this.var24OutputMin.Visible = false;
            this.var24OutputMin.TextChanged += new System.EventHandler(this.var24OutputMin_TextChanged);
            // 
            // var24InputMax
            // 
            this.var24InputMax.Enabled = false;
            this.var24InputMax.Location = new System.Drawing.Point(287, 219);
            this.var24InputMax.Name = "var24InputMax";
            this.var24InputMax.Size = new System.Drawing.Size(63, 20);
            this.var24InputMax.TabIndex = 595;
            this.var24InputMax.Visible = false;
            this.var24InputMax.TextChanged += new System.EventHandler(this.var24InputMax_TextChanged);
            // 
            // var24InputMin
            // 
            this.var24InputMin.Enabled = false;
            this.var24InputMin.Location = new System.Drawing.Point(211, 218);
            this.var24InputMin.Name = "var24InputMin";
            this.var24InputMin.Size = new System.Drawing.Size(63, 20);
            this.var24InputMin.TabIndex = 594;
            this.var24InputMin.Visible = false;
            this.var24InputMin.TextChanged += new System.EventHandler(this.var24InputMin_TextChanged);
            // 
            // var24Channel
            // 
            this.var24Channel.Enabled = false;
            this.var24Channel.Location = new System.Drawing.Point(130, 219);
            this.var24Channel.Name = "var24Channel";
            this.var24Channel.Size = new System.Drawing.Size(63, 20);
            this.var24Channel.TabIndex = 592;
            this.var24Channel.Visible = false;
            this.var24Channel.TextChanged += new System.EventHandler(this.var24Channel_TextChanged);
            // 
            // var23OutputMax
            // 
            this.var23OutputMax.Enabled = false;
            this.var23OutputMax.Location = new System.Drawing.Point(442, 192);
            this.var23OutputMax.Name = "var23OutputMax";
            this.var23OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var23OutputMax.TabIndex = 591;
            this.var23OutputMax.Visible = false;
            this.var23OutputMax.TextChanged += new System.EventHandler(this.var23OutputMax_TextChanged);
            // 
            // var23OutputMin
            // 
            this.var23OutputMin.Enabled = false;
            this.var23OutputMin.Location = new System.Drawing.Point(365, 192);
            this.var23OutputMin.Name = "var23OutputMin";
            this.var23OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var23OutputMin.TabIndex = 590;
            this.var23OutputMin.Visible = false;
            this.var23OutputMin.TextChanged += new System.EventHandler(this.var23OutputMin_TextChanged);
            // 
            // var23InputMax
            // 
            this.var23InputMax.Enabled = false;
            this.var23InputMax.Location = new System.Drawing.Point(287, 192);
            this.var23InputMax.Name = "var23InputMax";
            this.var23InputMax.Size = new System.Drawing.Size(63, 20);
            this.var23InputMax.TabIndex = 589;
            this.var23InputMax.Visible = false;
            this.var23InputMax.TextChanged += new System.EventHandler(this.var23InputMax_TextChanged);
            // 
            // var23InputMin
            // 
            this.var23InputMin.Enabled = false;
            this.var23InputMin.Location = new System.Drawing.Point(211, 191);
            this.var23InputMin.Name = "var23InputMin";
            this.var23InputMin.Size = new System.Drawing.Size(63, 20);
            this.var23InputMin.TabIndex = 588;
            this.var23InputMin.Visible = false;
            this.var23InputMin.TextChanged += new System.EventHandler(this.var23InputMin_TextChanged);
            // 
            // var23Channel
            // 
            this.var23Channel.Enabled = false;
            this.var23Channel.Location = new System.Drawing.Point(130, 192);
            this.var23Channel.Name = "var23Channel";
            this.var23Channel.Size = new System.Drawing.Size(63, 20);
            this.var23Channel.TabIndex = 586;
            this.var23Channel.Visible = false;
            this.var23Channel.TextChanged += new System.EventHandler(this.var23Channel_TextChanged);
            // 
            // var22OutputMax
            // 
            this.var22OutputMax.Enabled = false;
            this.var22OutputMax.Location = new System.Drawing.Point(442, 167);
            this.var22OutputMax.Name = "var22OutputMax";
            this.var22OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var22OutputMax.TabIndex = 583;
            // 
            // var22OutputMin
            // 
            this.var22OutputMin.Enabled = false;
            this.var22OutputMin.Location = new System.Drawing.Point(365, 167);
            this.var22OutputMin.Name = "var22OutputMin";
            this.var22OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var22OutputMin.TabIndex = 582;
            // 
            // var22InputMax
            // 
            this.var22InputMax.Enabled = false;
            this.var22InputMax.Location = new System.Drawing.Point(287, 167);
            this.var22InputMax.Name = "var22InputMax";
            this.var22InputMax.Size = new System.Drawing.Size(63, 20);
            this.var22InputMax.TabIndex = 581;
            // 
            // var22InputMin
            // 
            this.var22InputMin.Enabled = false;
            this.var22InputMin.Location = new System.Drawing.Point(211, 167);
            this.var22InputMin.Name = "var22InputMin";
            this.var22InputMin.Size = new System.Drawing.Size(63, 20);
            this.var22InputMin.TabIndex = 580;
            // 
            // var22Channel
            // 
            this.var22Channel.Enabled = false;
            this.var22Channel.Location = new System.Drawing.Point(130, 167);
            this.var22Channel.Name = "var22Channel";
            this.var22Channel.Size = new System.Drawing.Size(63, 20);
            this.var22Channel.TabIndex = 578;
            // 
            // var21OutputMax
            // 
            this.var21OutputMax.Enabled = false;
            this.var21OutputMax.Location = new System.Drawing.Point(442, 141);
            this.var21OutputMax.Name = "var21OutputMax";
            this.var21OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var21OutputMax.TabIndex = 577;
            // 
            // var21OutputMin
            // 
            this.var21OutputMin.Enabled = false;
            this.var21OutputMin.Location = new System.Drawing.Point(365, 141);
            this.var21OutputMin.Name = "var21OutputMin";
            this.var21OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var21OutputMin.TabIndex = 576;
            // 
            // var21InputMax
            // 
            this.var21InputMax.Enabled = false;
            this.var21InputMax.Location = new System.Drawing.Point(287, 141);
            this.var21InputMax.Name = "var21InputMax";
            this.var21InputMax.Size = new System.Drawing.Size(63, 20);
            this.var21InputMax.TabIndex = 575;
            // 
            // var21InputMin
            // 
            this.var21InputMin.Enabled = false;
            this.var21InputMin.Location = new System.Drawing.Point(211, 140);
            this.var21InputMin.Name = "var21InputMin";
            this.var21InputMin.Size = new System.Drawing.Size(63, 20);
            this.var21InputMin.TabIndex = 574;
            // 
            // var21Channel
            // 
            this.var21Channel.Enabled = false;
            this.var21Channel.Location = new System.Drawing.Point(130, 141);
            this.var21Channel.Name = "var21Channel";
            this.var21Channel.Size = new System.Drawing.Size(63, 20);
            this.var21Channel.TabIndex = 572;
            // 
            // var20OutputMax
            // 
            this.var20OutputMax.Enabled = false;
            this.var20OutputMax.Location = new System.Drawing.Point(442, 116);
            this.var20OutputMax.Name = "var20OutputMax";
            this.var20OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var20OutputMax.TabIndex = 569;
            // 
            // var20OutputMin
            // 
            this.var20OutputMin.Enabled = false;
            this.var20OutputMin.Location = new System.Drawing.Point(365, 116);
            this.var20OutputMin.Name = "var20OutputMin";
            this.var20OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var20OutputMin.TabIndex = 568;
            // 
            // var20InputMax
            // 
            this.var20InputMax.Enabled = false;
            this.var20InputMax.Location = new System.Drawing.Point(287, 116);
            this.var20InputMax.Name = "var20InputMax";
            this.var20InputMax.Size = new System.Drawing.Size(63, 20);
            this.var20InputMax.TabIndex = 567;
            // 
            // var20InputMin
            // 
            this.var20InputMin.Enabled = false;
            this.var20InputMin.Location = new System.Drawing.Point(211, 115);
            this.var20InputMin.Name = "var20InputMin";
            this.var20InputMin.Size = new System.Drawing.Size(63, 20);
            this.var20InputMin.TabIndex = 566;
            // 
            // var20Channel
            // 
            this.var20Channel.Enabled = false;
            this.var20Channel.Location = new System.Drawing.Point(130, 116);
            this.var20Channel.Name = "var20Channel";
            this.var20Channel.Size = new System.Drawing.Size(63, 20);
            this.var20Channel.TabIndex = 564;
            // 
            // var19OutputMax
            // 
            this.var19OutputMax.Enabled = false;
            this.var19OutputMax.Location = new System.Drawing.Point(442, 89);
            this.var19OutputMax.Name = "var19OutputMax";
            this.var19OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var19OutputMax.TabIndex = 563;
            // 
            // var19OutputMin
            // 
            this.var19OutputMin.Enabled = false;
            this.var19OutputMin.Location = new System.Drawing.Point(365, 89);
            this.var19OutputMin.Name = "var19OutputMin";
            this.var19OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var19OutputMin.TabIndex = 562;
            // 
            // var19InputMax
            // 
            this.var19InputMax.Enabled = false;
            this.var19InputMax.Location = new System.Drawing.Point(287, 89);
            this.var19InputMax.Name = "var19InputMax";
            this.var19InputMax.Size = new System.Drawing.Size(63, 20);
            this.var19InputMax.TabIndex = 561;
            // 
            // var19InputMin
            // 
            this.var19InputMin.Enabled = false;
            this.var19InputMin.Location = new System.Drawing.Point(211, 89);
            this.var19InputMin.Name = "var19InputMin";
            this.var19InputMin.Size = new System.Drawing.Size(63, 20);
            this.var19InputMin.TabIndex = 560;
            // 
            // var19Channel
            // 
            this.var19Channel.Enabled = false;
            this.var19Channel.Location = new System.Drawing.Point(130, 89);
            this.var19Channel.Name = "var19Channel";
            this.var19Channel.Size = new System.Drawing.Size(63, 20);
            this.var19Channel.TabIndex = 558;
            // 
            // var35Module
            // 
            this.var35Module.Enabled = false;
            this.var35Module.FormattingEnabled = true;
            this.var35Module.Location = new System.Drawing.Point(58, 503);
            this.var35Module.Name = "var35Module";
            this.var35Module.Size = new System.Drawing.Size(53, 21);
            this.var35Module.TabIndex = 749;
            this.var35Module.Visible = false;
            this.var35Module.SelectedIndexChanged += new System.EventHandler(this.var35Module_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(16, 508);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 746;
            this.label2.Text = "var35";
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // var34Module
            // 
            this.var34Module.Enabled = false;
            this.var34Module.FormattingEnabled = true;
            this.var34Module.Location = new System.Drawing.Point(58, 479);
            this.var34Module.Name = "var34Module";
            this.var34Module.Size = new System.Drawing.Size(53, 21);
            this.var34Module.TabIndex = 741;
            this.var34Module.Visible = false;
            this.var34Module.SelectedIndexChanged += new System.EventHandler(this.var34Module_SelectedIndexChanged);
            // 
            // var33Module
            // 
            this.var33Module.Enabled = false;
            this.var33Module.FormattingEnabled = true;
            this.var33Module.Location = new System.Drawing.Point(58, 452);
            this.var33Module.Name = "var33Module";
            this.var33Module.Size = new System.Drawing.Size(53, 21);
            this.var33Module.TabIndex = 735;
            this.var33Module.Visible = false;
            this.var33Module.SelectedIndexChanged += new System.EventHandler(this.var33Module_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(16, 483);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 733;
            this.label3.Text = "var34";
            this.label3.Visible = false;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Location = new System.Drawing.Point(16, 455);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 732;
            this.label10.Text = "var33";
            this.label10.Visible = false;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // var32Module
            // 
            this.var32Module.Enabled = false;
            this.var32Module.FormattingEnabled = true;
            this.var32Module.Location = new System.Drawing.Point(58, 427);
            this.var32Module.Name = "var32Module";
            this.var32Module.Size = new System.Drawing.Size(53, 21);
            this.var32Module.TabIndex = 727;
            this.var32Module.Visible = false;
            this.var32Module.SelectedIndexChanged += new System.EventHandler(this.var32Module_SelectedIndexChanged);
            // 
            // var31Module
            // 
            this.var31Module.Enabled = false;
            this.var31Module.FormattingEnabled = true;
            this.var31Module.Location = new System.Drawing.Point(58, 401);
            this.var31Module.Name = "var31Module";
            this.var31Module.Size = new System.Drawing.Size(53, 21);
            this.var31Module.TabIndex = 721;
            this.var31Module.Visible = false;
            this.var31Module.SelectedIndexChanged += new System.EventHandler(this.var31Module_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Enabled = false;
            this.label11.Location = new System.Drawing.Point(16, 430);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 719;
            this.label11.Text = "var32";
            this.label11.Visible = false;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Location = new System.Drawing.Point(16, 405);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 718;
            this.label12.Text = "var31";
            this.label12.Visible = false;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // var30Module
            // 
            this.var30Module.Enabled = false;
            this.var30Module.FormattingEnabled = true;
            this.var30Module.Location = new System.Drawing.Point(58, 374);
            this.var30Module.Name = "var30Module";
            this.var30Module.Size = new System.Drawing.Size(53, 21);
            this.var30Module.TabIndex = 677;
            this.var30Module.Visible = false;
            this.var30Module.SelectedIndexChanged += new System.EventHandler(this.var30Module_SelectedIndexChanged);
            // 
            // var29Module
            // 
            this.var29Module.Enabled = false;
            this.var29Module.FormattingEnabled = true;
            this.var29Module.Location = new System.Drawing.Point(58, 347);
            this.var29Module.Name = "var29Module";
            this.var29Module.Size = new System.Drawing.Size(53, 21);
            this.var29Module.TabIndex = 671;
            this.var29Module.Visible = false;
            this.var29Module.SelectedIndexChanged += new System.EventHandler(this.var29Module_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Enabled = false;
            this.label13.Location = new System.Drawing.Point(16, 377);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 13);
            this.label13.TabIndex = 669;
            this.label13.Text = "var30";
            this.label13.Visible = false;
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Location = new System.Drawing.Point(16, 352);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 668;
            this.label14.Text = "var29";
            this.label14.Visible = false;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // var28Module
            // 
            this.var28Module.Enabled = false;
            this.var28Module.FormattingEnabled = true;
            this.var28Module.Location = new System.Drawing.Point(58, 323);
            this.var28Module.Name = "var28Module";
            this.var28Module.Size = new System.Drawing.Size(53, 21);
            this.var28Module.TabIndex = 663;
            this.var28Module.Visible = false;
            this.var28Module.SelectedIndexChanged += new System.EventHandler(this.var28Module_SelectedIndexChanged);
            // 
            // var27Module
            // 
            this.var27Module.Enabled = false;
            this.var27Module.FormattingEnabled = true;
            this.var27Module.Location = new System.Drawing.Point(58, 296);
            this.var27Module.Name = "var27Module";
            this.var27Module.Size = new System.Drawing.Size(53, 21);
            this.var27Module.TabIndex = 657;
            this.var27Module.Visible = false;
            this.var27Module.SelectedIndexChanged += new System.EventHandler(this.var27Module_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Enabled = false;
            this.label15.Location = new System.Drawing.Point(16, 327);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 655;
            this.label15.Text = "var28";
            this.label15.Visible = false;
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Enabled = false;
            this.label16.Location = new System.Drawing.Point(16, 299);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 13);
            this.label16.TabIndex = 654;
            this.label16.Text = "var27";
            this.label16.Visible = false;
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // var26Module
            // 
            this.var26Module.Enabled = false;
            this.var26Module.FormattingEnabled = true;
            this.var26Module.Location = new System.Drawing.Point(58, 271);
            this.var26Module.Name = "var26Module";
            this.var26Module.Size = new System.Drawing.Size(53, 21);
            this.var26Module.TabIndex = 649;
            this.var26Module.Visible = false;
            this.var26Module.SelectedIndexChanged += new System.EventHandler(this.var26Module_SelectedIndexChanged);
            // 
            // var25Module
            // 
            this.var25Module.Enabled = false;
            this.var25Module.FormattingEnabled = true;
            this.var25Module.Location = new System.Drawing.Point(58, 245);
            this.var25Module.Name = "var25Module";
            this.var25Module.Size = new System.Drawing.Size(53, 21);
            this.var25Module.TabIndex = 643;
            this.var25Module.Visible = false;
            this.var25Module.SelectedIndexChanged += new System.EventHandler(this.var25Module_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Location = new System.Drawing.Point(16, 274);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 641;
            this.label17.Text = "var26";
            this.label17.Visible = false;
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Location = new System.Drawing.Point(16, 249);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 13);
            this.label18.TabIndex = 640;
            this.label18.Text = "var25";
            this.label18.Visible = false;
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1069, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 633;
            this.label19.Text = "Display Name";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(991, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 626;
            this.label20.Text = "Type";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(907, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 13);
            this.label21.TabIndex = 619;
            this.label21.Text = "Status Column";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(827, 56);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 13);
            this.label22.TabIndex = 606;
            this.label22.Text = "Value Column";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(755, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(26, 13);
            this.label23.TabIndex = 605;
            this.label23.Text = "Unit";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(519, 57);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 598;
            this.label24.Text = "Offset";
            // 
            // var24Module
            // 
            this.var24Module.Enabled = false;
            this.var24Module.FormattingEnabled = true;
            this.var24Module.Location = new System.Drawing.Point(58, 218);
            this.var24Module.Name = "var24Module";
            this.var24Module.Size = new System.Drawing.Size(53, 21);
            this.var24Module.TabIndex = 593;
            this.var24Module.Visible = false;
            this.var24Module.SelectedIndexChanged += new System.EventHandler(this.var24Module_SelectedIndexChanged);
            // 
            // var23Module
            // 
            this.var23Module.Enabled = false;
            this.var23Module.FormattingEnabled = true;
            this.var23Module.Location = new System.Drawing.Point(58, 191);
            this.var23Module.Name = "var23Module";
            this.var23Module.Size = new System.Drawing.Size(53, 21);
            this.var23Module.TabIndex = 587;
            this.var23Module.Visible = false;
            this.var23Module.SelectedIndexChanged += new System.EventHandler(this.var23Module_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Enabled = false;
            this.label25.Location = new System.Drawing.Point(16, 221);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 585;
            this.label25.Text = "var24";
            this.label25.Visible = false;
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Enabled = false;
            this.label26.Location = new System.Drawing.Point(16, 196);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 584;
            this.label26.Text = "var23";
            this.label26.Visible = false;
            this.label26.Click += new System.EventHandler(this.label26_Click);
            // 
            // var22Module
            // 
            this.var22Module.Enabled = false;
            this.var22Module.FormattingEnabled = true;
            this.var22Module.Location = new System.Drawing.Point(58, 167);
            this.var22Module.Name = "var22Module";
            this.var22Module.Size = new System.Drawing.Size(53, 21);
            this.var22Module.TabIndex = 579;
            // 
            // var21Module
            // 
            this.var21Module.Enabled = false;
            this.var21Module.FormattingEnabled = true;
            this.var21Module.Location = new System.Drawing.Point(58, 140);
            this.var21Module.Name = "var21Module";
            this.var21Module.Size = new System.Drawing.Size(53, 21);
            this.var21Module.TabIndex = 573;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(16, 171);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(34, 13);
            this.label27.TabIndex = 571;
            this.label27.Text = "var22";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(16, 143);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 13);
            this.label29.TabIndex = 570;
            this.label29.Text = "var21";
            // 
            // var20Module
            // 
            this.var20Module.Enabled = false;
            this.var20Module.FormattingEnabled = true;
            this.var20Module.Location = new System.Drawing.Point(58, 115);
            this.var20Module.Name = "var20Module";
            this.var20Module.Size = new System.Drawing.Size(53, 21);
            this.var20Module.TabIndex = 565;
            // 
            // var19Module
            // 
            this.var19Module.Enabled = false;
            this.var19Module.FormattingEnabled = true;
            this.var19Module.Location = new System.Drawing.Point(58, 89);
            this.var19Module.Name = "var19Module";
            this.var19Module.Size = new System.Drawing.Size(53, 21);
            this.var19Module.TabIndex = 559;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(16, 118);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 13);
            this.label30.TabIndex = 557;
            this.label30.Text = "var20";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 93);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 13);
            this.label31.TabIndex = 556;
            this.label31.Text = "var19";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(439, 56);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(61, 13);
            this.label32.TabIndex = 555;
            this.label32.Text = "Output max";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(367, 56);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(58, 13);
            this.label33.TabIndex = 554;
            this.label33.Text = "Output min";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(285, 56);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 553;
            this.label34.Text = "Input max";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(213, 54);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 13);
            this.label35.TabIndex = 552;
            this.label35.Text = "Input min";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(126, 54);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(66, 13);
            this.label36.TabIndex = 551;
            this.label36.Text = "Channel No.";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(55, 54);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(56, 13);
            this.label37.TabIndex = 550;
            this.label37.Text = "Module ID";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cbModule);
            this.tabPage4.Controls.Add(this.panel1);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage4.Size = new System.Drawing.Size(1147, 604);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "FTP";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // cbModule
            // 
            this.cbModule.Enabled = false;
            this.cbModule.FormattingEnabled = true;
            this.cbModule.Location = new System.Drawing.Point(489, 120);
            this.cbModule.Name = "cbModule";
            this.cbModule.Size = new System.Drawing.Size(53, 21);
            this.cbModule.TabIndex = 561;
            this.cbModule.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::DataLogger.Properties.Resources.Control;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.dgvData);
            this.panel1.Location = new System.Drawing.Point(19, 147);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(964, 440);
            this.panel1.TabIndex = 9;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToOrderColumns = true;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(12, 14);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(934, 411);
            this.dgvData.TabIndex = 2;
            this.dgvData.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvData_CellClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbFlag);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textPwd);
            this.groupBox1.Controls.Add(this.label57);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.txtFolder);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.dtpLastedValue);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.txtUsername);
            this.groupBox1.Controls.Add(this.txtIP);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(53)))), ((int)(((byte)(56)))));
            this.groupBox1.Location = new System.Drawing.Point(21, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(962, 108);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FTP Info";
            // 
            // cbFlag
            // 
            this.cbFlag.Location = new System.Drawing.Point(144, 73);
            this.cbFlag.Name = "cbFlag";
            this.cbFlag.Size = new System.Drawing.Size(121, 21);
            this.cbFlag.TabIndex = 62;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(83)))), ((int)(((byte)(98)))));
            this.button2.Location = new System.Drawing.Point(842, 76);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 23);
            this.button2.TabIndex = 61;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnDelete.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(83)))), ((int)(((byte)(98)))));
            this.btnDelete.Location = new System.Drawing.Point(842, 47);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(101, 23);
            this.btnDelete.TabIndex = 60;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(83)))), ((int)(((byte)(98)))));
            this.button1.Location = new System.Drawing.Point(842, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 23);
            this.button1.TabIndex = 59;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textPwd
            // 
            this.textPwd.Location = new System.Drawing.Point(530, 43);
            this.textPwd.Name = "textPwd";
            this.textPwd.Size = new System.Drawing.Size(236, 21);
            this.textPwd.TabIndex = 58;
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(453, 43);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(57, 20);
            this.label57.TabIndex = 57;
            this.label57.Text = "Password";
            this.label57.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(453, 19);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 20);
            this.label39.TabIndex = 1;
            this.label39.Text = "Folder";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(530, 19);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(236, 21);
            this.txtFolder.TabIndex = 5;
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(453, 71);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(68, 20);
            this.label56.TabIndex = 56;
            this.label56.Text = "Lasted Value";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpLastedValue
            // 
            this.dtpLastedValue.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpLastedValue.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLastedValue.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLastedValue.Location = new System.Drawing.Point(530, 69);
            this.dtpLastedValue.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.dtpLastedValue.Name = "dtpLastedValue";
            this.dtpLastedValue.Size = new System.Drawing.Size(121, 21);
            this.dtpLastedValue.TabIndex = 55;
            this.dtpLastedValue.Value = new System.DateTime(2015, 10, 22, 0, 0, 0, 0);
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(68, 74);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(65, 20);
            this.label40.TabIndex = 8;
            this.label40.Text = "Flag";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(144, 45);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(226, 21);
            this.txtUsername.TabIndex = 6;
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(144, 20);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(226, 21);
            this.txtIP.TabIndex = 2;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(68, 45);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(65, 20);
            this.label41.TabIndex = 2;
            this.label41.Text = "Username";
            this.label41.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(68, 20);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 20);
            this.label42.TabIndex = 0;
            this.label42.Text = "IP";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabPage5
            // 
            this.tabPage5.AutoScroll = true;
            this.tabPage5.Controls.Add(this.label44);
            this.tabPage5.Controls.Add(this.label43);
            this.tabPage5.Controls.Add(this.var18ErrorMax);
            this.tabPage5.Controls.Add(this.var18ErrorMin);
            this.tabPage5.Controls.Add(this.var17ErrorMax);
            this.tabPage5.Controls.Add(this.var17ErrorMin);
            this.tabPage5.Controls.Add(this.var16ErrorMax);
            this.tabPage5.Controls.Add(this.var16ErrorMin);
            this.tabPage5.Controls.Add(this.var15ErrorMax);
            this.tabPage5.Controls.Add(this.var15ErrorMin);
            this.tabPage5.Controls.Add(this.var14ErrorMax);
            this.tabPage5.Controls.Add(this.var14ErrorMin);
            this.tabPage5.Controls.Add(this.var13ErrorMax);
            this.tabPage5.Controls.Add(this.var13ErrorMin);
            this.tabPage5.Controls.Add(this.var12ErrorMax);
            this.tabPage5.Controls.Add(this.var12ErrorMin);
            this.tabPage5.Controls.Add(this.var11ErrorMax);
            this.tabPage5.Controls.Add(this.var11ErrorMin);
            this.tabPage5.Controls.Add(this.var10ErrorMax);
            this.tabPage5.Controls.Add(this.var10ErrorMin);
            this.tabPage5.Controls.Add(this.var9ErrorMax);
            this.tabPage5.Controls.Add(this.var9ErrorMin);
            this.tabPage5.Controls.Add(this.var8ErrorMax);
            this.tabPage5.Controls.Add(this.var8ErrorMin);
            this.tabPage5.Controls.Add(this.var7ErrorMax);
            this.tabPage5.Controls.Add(this.var7ErrorMin);
            this.tabPage5.Controls.Add(this.var6ErrorMax);
            this.tabPage5.Controls.Add(this.var6ErrorMin);
            this.tabPage5.Controls.Add(this.var5ErrorMax);
            this.tabPage5.Controls.Add(this.var5ErrorMin);
            this.tabPage5.Controls.Add(this.var4ErrorMax);
            this.tabPage5.Controls.Add(this.var4ErrorMin);
            this.tabPage5.Controls.Add(this.var3ErrorMax);
            this.tabPage5.Controls.Add(this.var3ErrorMin);
            this.tabPage5.Controls.Add(this.var2ErrorMax);
            this.tabPage5.Controls.Add(this.var2ErrorMin);
            this.tabPage5.Controls.Add(this.var1ErrorMax);
            this.tabPage5.Controls.Add(this.var1ErrorMin);
            this.tabPage5.Controls.Add(this.label1);
            this.tabPage5.Controls.Add(this.var18DisplayName);
            this.tabPage5.Controls.Add(this.var17DisplayName);
            this.tabPage5.Controls.Add(this.var16DisplayName);
            this.tabPage5.Controls.Add(this.var15DisplayName);
            this.tabPage5.Controls.Add(this.var14DisplayName);
            this.tabPage5.Controls.Add(this.var13DisplayName);
            this.tabPage5.Controls.Add(this.var18Type);
            this.tabPage5.Controls.Add(this.var17Type);
            this.tabPage5.Controls.Add(this.var16Type);
            this.tabPage5.Controls.Add(this.var15Type);
            this.tabPage5.Controls.Add(this.var14Type);
            this.tabPage5.Controls.Add(this.var13Type);
            this.tabPage5.Controls.Add(this.var18StatusColumn);
            this.tabPage5.Controls.Add(this.var17StatusColumn);
            this.tabPage5.Controls.Add(this.var16StatusColumn);
            this.tabPage5.Controls.Add(this.var15StatusColumn);
            this.tabPage5.Controls.Add(this.var14StatusColumn);
            this.tabPage5.Controls.Add(this.var13StatusColumn);
            this.tabPage5.Controls.Add(this.var18ValueColumn);
            this.tabPage5.Controls.Add(this.var18Unit);
            this.tabPage5.Controls.Add(this.var17ValueColumn);
            this.tabPage5.Controls.Add(this.var17Unit);
            this.tabPage5.Controls.Add(this.var16ValueColumn);
            this.tabPage5.Controls.Add(this.var16Unit);
            this.tabPage5.Controls.Add(this.var15ValueColumn);
            this.tabPage5.Controls.Add(this.var15Unit);
            this.tabPage5.Controls.Add(this.var14ValueColumn);
            this.tabPage5.Controls.Add(this.var14Unit);
            this.tabPage5.Controls.Add(this.var13ValueColumn);
            this.tabPage5.Controls.Add(this.var13Unit);
            this.tabPage5.Controls.Add(this.var18Offset);
            this.tabPage5.Controls.Add(this.var17Offset);
            this.tabPage5.Controls.Add(this.var16Offset);
            this.tabPage5.Controls.Add(this.var15Offset);
            this.tabPage5.Controls.Add(this.var14Offset);
            this.tabPage5.Controls.Add(this.var13Offset);
            this.tabPage5.Controls.Add(this.var18OutputMax);
            this.tabPage5.Controls.Add(this.var18OutputMin);
            this.tabPage5.Controls.Add(this.var18InputMax);
            this.tabPage5.Controls.Add(this.var18InputMin);
            this.tabPage5.Controls.Add(this.var18Channel);
            this.tabPage5.Controls.Add(this.var17OutputMax);
            this.tabPage5.Controls.Add(this.var17OutputMin);
            this.tabPage5.Controls.Add(this.var17InputMax);
            this.tabPage5.Controls.Add(this.var17InputMin);
            this.tabPage5.Controls.Add(this.var17Channel);
            this.tabPage5.Controls.Add(this.var16OutputMax);
            this.tabPage5.Controls.Add(this.var16OutputMin);
            this.tabPage5.Controls.Add(this.var16InputMax);
            this.tabPage5.Controls.Add(this.var16InputMin);
            this.tabPage5.Controls.Add(this.var16Channel);
            this.tabPage5.Controls.Add(this.var15OutputMax);
            this.tabPage5.Controls.Add(this.var15OutputMin);
            this.tabPage5.Controls.Add(this.var15InputMax);
            this.tabPage5.Controls.Add(this.var15InputMin);
            this.tabPage5.Controls.Add(this.var15Channel);
            this.tabPage5.Controls.Add(this.var14OutputMax);
            this.tabPage5.Controls.Add(this.var14OutputMin);
            this.tabPage5.Controls.Add(this.var14InputMax);
            this.tabPage5.Controls.Add(this.var14InputMin);
            this.tabPage5.Controls.Add(this.var14Channel);
            this.tabPage5.Controls.Add(this.var13OutputMax);
            this.tabPage5.Controls.Add(this.var13OutputMin);
            this.tabPage5.Controls.Add(this.var13InputMax);
            this.tabPage5.Controls.Add(this.var13InputMin);
            this.tabPage5.Controls.Add(this.var13Channel);
            this.tabPage5.Controls.Add(this.var12DisplayName);
            this.tabPage5.Controls.Add(this.var11DisplayName);
            this.tabPage5.Controls.Add(this.var10DisplayName);
            this.tabPage5.Controls.Add(this.var9DisplayName);
            this.tabPage5.Controls.Add(this.var8DisplayName);
            this.tabPage5.Controls.Add(this.var7DisplayName);
            this.tabPage5.Controls.Add(this.var12Type);
            this.tabPage5.Controls.Add(this.var11Type);
            this.tabPage5.Controls.Add(this.var10Type);
            this.tabPage5.Controls.Add(this.var9Type);
            this.tabPage5.Controls.Add(this.var8Type);
            this.tabPage5.Controls.Add(this.var7Type);
            this.tabPage5.Controls.Add(this.var12StatusColumn);
            this.tabPage5.Controls.Add(this.var11StatusColumn);
            this.tabPage5.Controls.Add(this.var10StatusColumn);
            this.tabPage5.Controls.Add(this.var9StatusColumn);
            this.tabPage5.Controls.Add(this.var8StatusColumn);
            this.tabPage5.Controls.Add(this.var7StatusColumn);
            this.tabPage5.Controls.Add(this.var12ValueColumn);
            this.tabPage5.Controls.Add(this.var12Unit);
            this.tabPage5.Controls.Add(this.var11ValueColumn);
            this.tabPage5.Controls.Add(this.var11Unit);
            this.tabPage5.Controls.Add(this.var10ValueColumn);
            this.tabPage5.Controls.Add(this.var10Unit);
            this.tabPage5.Controls.Add(this.var9ValueColumn);
            this.tabPage5.Controls.Add(this.var9Unit);
            this.tabPage5.Controls.Add(this.var8ValueColumn);
            this.tabPage5.Controls.Add(this.var8Unit);
            this.tabPage5.Controls.Add(this.var7ValueColumn);
            this.tabPage5.Controls.Add(this.var7Unit);
            this.tabPage5.Controls.Add(this.var12Offset);
            this.tabPage5.Controls.Add(this.var11Offset);
            this.tabPage5.Controls.Add(this.var10Offset);
            this.tabPage5.Controls.Add(this.var9Offset);
            this.tabPage5.Controls.Add(this.var8Offset);
            this.tabPage5.Controls.Add(this.var7Offset);
            this.tabPage5.Controls.Add(this.var12OutputMax);
            this.tabPage5.Controls.Add(this.var12OutputMin);
            this.tabPage5.Controls.Add(this.var12InputMax);
            this.tabPage5.Controls.Add(this.var12InputMin);
            this.tabPage5.Controls.Add(this.var12Channel);
            this.tabPage5.Controls.Add(this.var11OutputMax);
            this.tabPage5.Controls.Add(this.var11OutputMin);
            this.tabPage5.Controls.Add(this.var11InputMax);
            this.tabPage5.Controls.Add(this.var11InputMin);
            this.tabPage5.Controls.Add(this.var11Channel);
            this.tabPage5.Controls.Add(this.var10OutputMax);
            this.tabPage5.Controls.Add(this.var10OutputMin);
            this.tabPage5.Controls.Add(this.var10InputMax);
            this.tabPage5.Controls.Add(this.var10InputMin);
            this.tabPage5.Controls.Add(this.var10Channel);
            this.tabPage5.Controls.Add(this.var9OutputMax);
            this.tabPage5.Controls.Add(this.var9OutputMin);
            this.tabPage5.Controls.Add(this.var9InputMax);
            this.tabPage5.Controls.Add(this.var9InputMin);
            this.tabPage5.Controls.Add(this.var9Channel);
            this.tabPage5.Controls.Add(this.var8OutputMax);
            this.tabPage5.Controls.Add(this.var8OutputMin);
            this.tabPage5.Controls.Add(this.var8InputMax);
            this.tabPage5.Controls.Add(this.var8InputMin);
            this.tabPage5.Controls.Add(this.var8Channel);
            this.tabPage5.Controls.Add(this.var7OutputMax);
            this.tabPage5.Controls.Add(this.var7OutputMin);
            this.tabPage5.Controls.Add(this.var7InputMax);
            this.tabPage5.Controls.Add(this.var7InputMin);
            this.tabPage5.Controls.Add(this.var7Channel);
            this.tabPage5.Controls.Add(this.var6DisplayName);
            this.tabPage5.Controls.Add(this.var5DisplayName);
            this.tabPage5.Controls.Add(this.var4DisplayName);
            this.tabPage5.Controls.Add(this.var3DisplayName);
            this.tabPage5.Controls.Add(this.var2DisplayName);
            this.tabPage5.Controls.Add(this.var1DisplayName);
            this.tabPage5.Controls.Add(this.var6Type);
            this.tabPage5.Controls.Add(this.var5Type);
            this.tabPage5.Controls.Add(this.var4Type);
            this.tabPage5.Controls.Add(this.var3Type);
            this.tabPage5.Controls.Add(this.var2Type);
            this.tabPage5.Controls.Add(this.var1Type);
            this.tabPage5.Controls.Add(this.var6StatusColumn);
            this.tabPage5.Controls.Add(this.var5StatusColumn);
            this.tabPage5.Controls.Add(this.var4StatusColumn);
            this.tabPage5.Controls.Add(this.var3StatusColumn);
            this.tabPage5.Controls.Add(this.var2StatusColumn);
            this.tabPage5.Controls.Add(this.var1StatusColumn);
            this.tabPage5.Controls.Add(this.var6ValueColumn);
            this.tabPage5.Controls.Add(this.var6Unit);
            this.tabPage5.Controls.Add(this.var5ValueColumn);
            this.tabPage5.Controls.Add(this.var5Unit);
            this.tabPage5.Controls.Add(this.var4ValueColumn);
            this.tabPage5.Controls.Add(this.var4Unit);
            this.tabPage5.Controls.Add(this.var3ValueColumn);
            this.tabPage5.Controls.Add(this.var3Unit);
            this.tabPage5.Controls.Add(this.var2ValueColumn);
            this.tabPage5.Controls.Add(this.var2Unit);
            this.tabPage5.Controls.Add(this.var1ValueColumn);
            this.tabPage5.Controls.Add(this.var1Unit);
            this.tabPage5.Controls.Add(this.var6Offset);
            this.tabPage5.Controls.Add(this.var5Offset);
            this.tabPage5.Controls.Add(this.var4Offset);
            this.tabPage5.Controls.Add(this.var3Offset);
            this.tabPage5.Controls.Add(this.var2Offset);
            this.tabPage5.Controls.Add(this.var1Offset);
            this.tabPage5.Controls.Add(this.var6OutputMax);
            this.tabPage5.Controls.Add(this.var6OutputMin);
            this.tabPage5.Controls.Add(this.var6InputMax);
            this.tabPage5.Controls.Add(this.var6InputMin);
            this.tabPage5.Controls.Add(this.var6Channel);
            this.tabPage5.Controls.Add(this.var5OutputMax);
            this.tabPage5.Controls.Add(this.var5OutputMin);
            this.tabPage5.Controls.Add(this.var5InputMax);
            this.tabPage5.Controls.Add(this.var5InputMin);
            this.tabPage5.Controls.Add(this.var5Channel);
            this.tabPage5.Controls.Add(this.var4OutputMax);
            this.tabPage5.Controls.Add(this.var4OutputMin);
            this.tabPage5.Controls.Add(this.var4InputMax);
            this.tabPage5.Controls.Add(this.var4InputMin);
            this.tabPage5.Controls.Add(this.var4Channel);
            this.tabPage5.Controls.Add(this.var3OutputMax);
            this.tabPage5.Controls.Add(this.var3OutputMin);
            this.tabPage5.Controls.Add(this.var3InputMax);
            this.tabPage5.Controls.Add(this.var3InputMin);
            this.tabPage5.Controls.Add(this.var3Channel);
            this.tabPage5.Controls.Add(this.var2OutputMax);
            this.tabPage5.Controls.Add(this.var2OutputMin);
            this.tabPage5.Controls.Add(this.var2InputMax);
            this.tabPage5.Controls.Add(this.var2InputMin);
            this.tabPage5.Controls.Add(this.var2Channel);
            this.tabPage5.Controls.Add(this.var1OutputMax);
            this.tabPage5.Controls.Add(this.var1OutputMin);
            this.tabPage5.Controls.Add(this.var1InputMax);
            this.tabPage5.Controls.Add(this.var1InputMin);
            this.tabPage5.Controls.Add(this.var1Channel);
            this.tabPage5.Controls.Add(this.var18Module);
            this.tabPage5.Controls.Add(this.var17Module);
            this.tabPage5.Controls.Add(this.var18);
            this.tabPage5.Controls.Add(this.var17);
            this.tabPage5.Controls.Add(this.var16Module);
            this.tabPage5.Controls.Add(this.var15Module);
            this.tabPage5.Controls.Add(this.var16);
            this.tabPage5.Controls.Add(this.var15);
            this.tabPage5.Controls.Add(this.var14Module);
            this.tabPage5.Controls.Add(this.var13Module);
            this.tabPage5.Controls.Add(this.var14);
            this.tabPage5.Controls.Add(this.var13);
            this.tabPage5.Controls.Add(this.var12Module);
            this.tabPage5.Controls.Add(this.var11Module);
            this.tabPage5.Controls.Add(this.var12);
            this.tabPage5.Controls.Add(this.var11);
            this.tabPage5.Controls.Add(this.var10Module);
            this.tabPage5.Controls.Add(this.var9Module);
            this.tabPage5.Controls.Add(this.var10);
            this.tabPage5.Controls.Add(this.var9);
            this.tabPage5.Controls.Add(this.var8Module);
            this.tabPage5.Controls.Add(this.var7Module);
            this.tabPage5.Controls.Add(this.var8);
            this.tabPage5.Controls.Add(this.var7);
            this.tabPage5.Controls.Add(this.label55);
            this.tabPage5.Controls.Add(this.label54);
            this.tabPage5.Controls.Add(this.label52);
            this.tabPage5.Controls.Add(this.label53);
            this.tabPage5.Controls.Add(this.Unit);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.var6Module);
            this.tabPage5.Controls.Add(this.var5Module);
            this.tabPage5.Controls.Add(this.var6);
            this.tabPage5.Controls.Add(this.var5);
            this.tabPage5.Controls.Add(this.var4Module);
            this.tabPage5.Controls.Add(this.var3Module);
            this.tabPage5.Controls.Add(this.var4);
            this.tabPage5.Controls.Add(this.var3);
            this.tabPage5.Controls.Add(this.var2Module);
            this.tabPage5.Controls.Add(this.var1Module);
            this.tabPage5.Controls.Add(this.var2);
            this.tabPage5.Controls.Add(this.var1);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.label7);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage5.Size = new System.Drawing.Size(1147, 604);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "MPS";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(670, 18);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(51, 13);
            this.label44.TabIndex = 625;
            this.label44.Text = "Error max";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(595, 18);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 13);
            this.label43.TabIndex = 624;
            this.label43.Text = "Error min";
            // 
            // var18ErrorMax
            // 
            this.var18ErrorMax.Enabled = false;
            this.var18ErrorMax.Location = new System.Drawing.Point(673, 490);
            this.var18ErrorMax.Name = "var18ErrorMax";
            this.var18ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var18ErrorMax.TabIndex = 623;
            this.var18ErrorMax.Visible = false;
            // 
            // var18ErrorMin
            // 
            this.var18ErrorMin.Enabled = false;
            this.var18ErrorMin.Location = new System.Drawing.Point(598, 490);
            this.var18ErrorMin.Name = "var18ErrorMin";
            this.var18ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var18ErrorMin.TabIndex = 622;
            this.var18ErrorMin.Visible = false;
            // 
            // var17ErrorMax
            // 
            this.var17ErrorMax.Enabled = false;
            this.var17ErrorMax.Location = new System.Drawing.Point(673, 465);
            this.var17ErrorMax.Name = "var17ErrorMax";
            this.var17ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var17ErrorMax.TabIndex = 621;
            // 
            // var17ErrorMin
            // 
            this.var17ErrorMin.Enabled = false;
            this.var17ErrorMin.Location = new System.Drawing.Point(598, 465);
            this.var17ErrorMin.Name = "var17ErrorMin";
            this.var17ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var17ErrorMin.TabIndex = 620;
            // 
            // var16ErrorMax
            // 
            this.var16ErrorMax.Enabled = false;
            this.var16ErrorMax.Location = new System.Drawing.Point(673, 440);
            this.var16ErrorMax.Name = "var16ErrorMax";
            this.var16ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var16ErrorMax.TabIndex = 619;
            // 
            // var16ErrorMin
            // 
            this.var16ErrorMin.Enabled = false;
            this.var16ErrorMin.Location = new System.Drawing.Point(598, 440);
            this.var16ErrorMin.Name = "var16ErrorMin";
            this.var16ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var16ErrorMin.TabIndex = 618;
            // 
            // var15ErrorMax
            // 
            this.var15ErrorMax.Enabled = false;
            this.var15ErrorMax.Location = new System.Drawing.Point(673, 415);
            this.var15ErrorMax.Name = "var15ErrorMax";
            this.var15ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var15ErrorMax.TabIndex = 617;
            // 
            // var15ErrorMin
            // 
            this.var15ErrorMin.Enabled = false;
            this.var15ErrorMin.Location = new System.Drawing.Point(598, 415);
            this.var15ErrorMin.Name = "var15ErrorMin";
            this.var15ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var15ErrorMin.TabIndex = 616;
            // 
            // var14ErrorMax
            // 
            this.var14ErrorMax.Location = new System.Drawing.Point(673, 389);
            this.var14ErrorMax.Name = "var14ErrorMax";
            this.var14ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var14ErrorMax.TabIndex = 615;
            // 
            // var14ErrorMin
            // 
            this.var14ErrorMin.Location = new System.Drawing.Point(598, 389);
            this.var14ErrorMin.Name = "var14ErrorMin";
            this.var14ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var14ErrorMin.TabIndex = 614;
            // 
            // var13ErrorMax
            // 
            this.var13ErrorMax.Location = new System.Drawing.Point(673, 362);
            this.var13ErrorMax.Name = "var13ErrorMax";
            this.var13ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var13ErrorMax.TabIndex = 613;
            // 
            // var13ErrorMin
            // 
            this.var13ErrorMin.Location = new System.Drawing.Point(598, 362);
            this.var13ErrorMin.Name = "var13ErrorMin";
            this.var13ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var13ErrorMin.TabIndex = 612;
            // 
            // var12ErrorMax
            // 
            this.var12ErrorMax.Enabled = false;
            this.var12ErrorMax.Location = new System.Drawing.Point(673, 334);
            this.var12ErrorMax.Name = "var12ErrorMax";
            this.var12ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var12ErrorMax.TabIndex = 611;
            // 
            // var12ErrorMin
            // 
            this.var12ErrorMin.Enabled = false;
            this.var12ErrorMin.Location = new System.Drawing.Point(598, 334);
            this.var12ErrorMin.Name = "var12ErrorMin";
            this.var12ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var12ErrorMin.TabIndex = 610;
            // 
            // var11ErrorMax
            // 
            this.var11ErrorMax.Location = new System.Drawing.Point(673, 310);
            this.var11ErrorMax.Name = "var11ErrorMax";
            this.var11ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var11ErrorMax.TabIndex = 609;
            // 
            // var11ErrorMin
            // 
            this.var11ErrorMin.Location = new System.Drawing.Point(598, 310);
            this.var11ErrorMin.Name = "var11ErrorMin";
            this.var11ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var11ErrorMin.TabIndex = 608;
            // 
            // var10ErrorMax
            // 
            this.var10ErrorMax.Location = new System.Drawing.Point(673, 284);
            this.var10ErrorMax.Name = "var10ErrorMax";
            this.var10ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var10ErrorMax.TabIndex = 607;
            // 
            // var10ErrorMin
            // 
            this.var10ErrorMin.Location = new System.Drawing.Point(598, 284);
            this.var10ErrorMin.Name = "var10ErrorMin";
            this.var10ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var10ErrorMin.TabIndex = 606;
            // 
            // var9ErrorMax
            // 
            this.var9ErrorMax.Enabled = false;
            this.var9ErrorMax.Location = new System.Drawing.Point(673, 256);
            this.var9ErrorMax.Name = "var9ErrorMax";
            this.var9ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var9ErrorMax.TabIndex = 605;
            // 
            // var9ErrorMin
            // 
            this.var9ErrorMin.Enabled = false;
            this.var9ErrorMin.Location = new System.Drawing.Point(598, 256);
            this.var9ErrorMin.Name = "var9ErrorMin";
            this.var9ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var9ErrorMin.TabIndex = 604;
            // 
            // var8ErrorMax
            // 
            this.var8ErrorMax.Location = new System.Drawing.Point(673, 232);
            this.var8ErrorMax.Name = "var8ErrorMax";
            this.var8ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var8ErrorMax.TabIndex = 603;
            // 
            // var8ErrorMin
            // 
            this.var8ErrorMin.Location = new System.Drawing.Point(598, 232);
            this.var8ErrorMin.Name = "var8ErrorMin";
            this.var8ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var8ErrorMin.TabIndex = 602;
            // 
            // var7ErrorMax
            // 
            this.var7ErrorMax.Location = new System.Drawing.Point(673, 206);
            this.var7ErrorMax.Name = "var7ErrorMax";
            this.var7ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var7ErrorMax.TabIndex = 601;
            // 
            // var7ErrorMin
            // 
            this.var7ErrorMin.Location = new System.Drawing.Point(598, 206);
            this.var7ErrorMin.Name = "var7ErrorMin";
            this.var7ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var7ErrorMin.TabIndex = 600;
            // 
            // var6ErrorMax
            // 
            this.var6ErrorMax.Location = new System.Drawing.Point(673, 179);
            this.var6ErrorMax.Name = "var6ErrorMax";
            this.var6ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var6ErrorMax.TabIndex = 599;
            // 
            // var6ErrorMin
            // 
            this.var6ErrorMin.Location = new System.Drawing.Point(598, 179);
            this.var6ErrorMin.Name = "var6ErrorMin";
            this.var6ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var6ErrorMin.TabIndex = 598;
            // 
            // var5ErrorMax
            // 
            this.var5ErrorMax.Location = new System.Drawing.Point(673, 154);
            this.var5ErrorMax.Name = "var5ErrorMax";
            this.var5ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var5ErrorMax.TabIndex = 597;
            // 
            // var5ErrorMin
            // 
            this.var5ErrorMin.Location = new System.Drawing.Point(598, 154);
            this.var5ErrorMin.Name = "var5ErrorMin";
            this.var5ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var5ErrorMin.TabIndex = 596;
            // 
            // var4ErrorMax
            // 
            this.var4ErrorMax.Location = new System.Drawing.Point(673, 128);
            this.var4ErrorMax.Name = "var4ErrorMax";
            this.var4ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var4ErrorMax.TabIndex = 595;
            // 
            // var4ErrorMin
            // 
            this.var4ErrorMin.Location = new System.Drawing.Point(598, 128);
            this.var4ErrorMin.Name = "var4ErrorMin";
            this.var4ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var4ErrorMin.TabIndex = 594;
            // 
            // var3ErrorMax
            // 
            this.var3ErrorMax.Location = new System.Drawing.Point(673, 101);
            this.var3ErrorMax.Name = "var3ErrorMax";
            this.var3ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var3ErrorMax.TabIndex = 593;
            // 
            // var3ErrorMin
            // 
            this.var3ErrorMin.Location = new System.Drawing.Point(598, 101);
            this.var3ErrorMin.Name = "var3ErrorMin";
            this.var3ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var3ErrorMin.TabIndex = 592;
            // 
            // var2ErrorMax
            // 
            this.var2ErrorMax.Location = new System.Drawing.Point(673, 77);
            this.var2ErrorMax.Name = "var2ErrorMax";
            this.var2ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var2ErrorMax.TabIndex = 591;
            // 
            // var2ErrorMin
            // 
            this.var2ErrorMin.Location = new System.Drawing.Point(598, 77);
            this.var2ErrorMin.Name = "var2ErrorMin";
            this.var2ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var2ErrorMin.TabIndex = 590;
            // 
            // var1ErrorMax
            // 
            this.var1ErrorMax.Location = new System.Drawing.Point(673, 50);
            this.var1ErrorMax.Name = "var1ErrorMax";
            this.var1ErrorMax.Size = new System.Drawing.Size(63, 20);
            this.var1ErrorMax.TabIndex = 589;
            // 
            // var1ErrorMin
            // 
            this.var1ErrorMin.Location = new System.Drawing.Point(598, 50);
            this.var1ErrorMin.Name = "var1ErrorMin";
            this.var1ErrorMin.Size = new System.Drawing.Size(63, 20);
            this.var1ErrorMin.TabIndex = 588;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(1149, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 569;
            this.label1.Text = "Alarm";
            this.label1.Visible = false;
            // 
            // var18DisplayName
            // 
            this.var18DisplayName.Enabled = false;
            this.var18DisplayName.Location = new System.Drawing.Point(1069, 491);
            this.var18DisplayName.Name = "var18DisplayName";
            this.var18DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var18DisplayName.TabIndex = 549;
            this.var18DisplayName.Visible = false;
            // 
            // var17DisplayName
            // 
            this.var17DisplayName.Enabled = false;
            this.var17DisplayName.Location = new System.Drawing.Point(1069, 465);
            this.var17DisplayName.Name = "var17DisplayName";
            this.var17DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var17DisplayName.TabIndex = 548;
            // 
            // var16DisplayName
            // 
            this.var16DisplayName.Enabled = false;
            this.var16DisplayName.Location = new System.Drawing.Point(1069, 440);
            this.var16DisplayName.Name = "var16DisplayName";
            this.var16DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var16DisplayName.TabIndex = 547;
            // 
            // var15DisplayName
            // 
            this.var15DisplayName.Enabled = false;
            this.var15DisplayName.Location = new System.Drawing.Point(1069, 413);
            this.var15DisplayName.Name = "var15DisplayName";
            this.var15DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var15DisplayName.TabIndex = 546;
            // 
            // var14DisplayName
            // 
            this.var14DisplayName.Location = new System.Drawing.Point(1069, 389);
            this.var14DisplayName.Name = "var14DisplayName";
            this.var14DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var14DisplayName.TabIndex = 545;
            // 
            // var13DisplayName
            // 
            this.var13DisplayName.Location = new System.Drawing.Point(1069, 362);
            this.var13DisplayName.Name = "var13DisplayName";
            this.var13DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var13DisplayName.TabIndex = 544;
            // 
            // var18Type
            // 
            this.var18Type.Enabled = false;
            this.var18Type.Location = new System.Drawing.Point(991, 491);
            this.var18Type.Name = "var18Type";
            this.var18Type.Size = new System.Drawing.Size(63, 20);
            this.var18Type.TabIndex = 543;
            this.var18Type.Visible = false;
            // 
            // var17Type
            // 
            this.var17Type.Enabled = false;
            this.var17Type.Location = new System.Drawing.Point(991, 465);
            this.var17Type.Name = "var17Type";
            this.var17Type.Size = new System.Drawing.Size(63, 20);
            this.var17Type.TabIndex = 542;
            // 
            // var16Type
            // 
            this.var16Type.Enabled = false;
            this.var16Type.Location = new System.Drawing.Point(991, 440);
            this.var16Type.Name = "var16Type";
            this.var16Type.Size = new System.Drawing.Size(63, 20);
            this.var16Type.TabIndex = 541;
            // 
            // var15Type
            // 
            this.var15Type.Enabled = false;
            this.var15Type.Location = new System.Drawing.Point(991, 413);
            this.var15Type.Name = "var15Type";
            this.var15Type.Size = new System.Drawing.Size(63, 20);
            this.var15Type.TabIndex = 540;
            // 
            // var14Type
            // 
            this.var14Type.Enabled = false;
            this.var14Type.Location = new System.Drawing.Point(991, 389);
            this.var14Type.Name = "var14Type";
            this.var14Type.Size = new System.Drawing.Size(63, 20);
            this.var14Type.TabIndex = 539;
            // 
            // var13Type
            // 
            this.var13Type.Enabled = false;
            this.var13Type.Location = new System.Drawing.Point(991, 362);
            this.var13Type.Name = "var13Type";
            this.var13Type.Size = new System.Drawing.Size(63, 20);
            this.var13Type.TabIndex = 538;
            // 
            // var18StatusColumn
            // 
            this.var18StatusColumn.Enabled = false;
            this.var18StatusColumn.Location = new System.Drawing.Point(907, 491);
            this.var18StatusColumn.Name = "var18StatusColumn";
            this.var18StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var18StatusColumn.TabIndex = 537;
            this.var18StatusColumn.Visible = false;
            // 
            // var17StatusColumn
            // 
            this.var17StatusColumn.Enabled = false;
            this.var17StatusColumn.Location = new System.Drawing.Point(907, 465);
            this.var17StatusColumn.Name = "var17StatusColumn";
            this.var17StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var17StatusColumn.TabIndex = 536;
            // 
            // var16StatusColumn
            // 
            this.var16StatusColumn.Enabled = false;
            this.var16StatusColumn.Location = new System.Drawing.Point(907, 440);
            this.var16StatusColumn.Name = "var16StatusColumn";
            this.var16StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var16StatusColumn.TabIndex = 535;
            // 
            // var15StatusColumn
            // 
            this.var15StatusColumn.Enabled = false;
            this.var15StatusColumn.Location = new System.Drawing.Point(907, 413);
            this.var15StatusColumn.Name = "var15StatusColumn";
            this.var15StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var15StatusColumn.TabIndex = 534;
            // 
            // var14StatusColumn
            // 
            this.var14StatusColumn.Enabled = false;
            this.var14StatusColumn.Location = new System.Drawing.Point(907, 389);
            this.var14StatusColumn.Name = "var14StatusColumn";
            this.var14StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var14StatusColumn.TabIndex = 533;
            // 
            // var13StatusColumn
            // 
            this.var13StatusColumn.Enabled = false;
            this.var13StatusColumn.Location = new System.Drawing.Point(907, 362);
            this.var13StatusColumn.Name = "var13StatusColumn";
            this.var13StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var13StatusColumn.TabIndex = 532;
            // 
            // var18ValueColumn
            // 
            this.var18ValueColumn.Enabled = false;
            this.var18ValueColumn.Location = new System.Drawing.Point(826, 491);
            this.var18ValueColumn.Name = "var18ValueColumn";
            this.var18ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var18ValueColumn.TabIndex = 531;
            this.var18ValueColumn.Visible = false;
            // 
            // var18Unit
            // 
            this.var18Unit.Enabled = false;
            this.var18Unit.Location = new System.Drawing.Point(749, 491);
            this.var18Unit.Name = "var18Unit";
            this.var18Unit.Size = new System.Drawing.Size(63, 20);
            this.var18Unit.TabIndex = 530;
            this.var18Unit.Visible = false;
            // 
            // var17ValueColumn
            // 
            this.var17ValueColumn.Enabled = false;
            this.var17ValueColumn.Location = new System.Drawing.Point(826, 464);
            this.var17ValueColumn.Name = "var17ValueColumn";
            this.var17ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var17ValueColumn.TabIndex = 529;
            // 
            // var17Unit
            // 
            this.var17Unit.Enabled = false;
            this.var17Unit.Location = new System.Drawing.Point(749, 464);
            this.var17Unit.Name = "var17Unit";
            this.var17Unit.Size = new System.Drawing.Size(63, 20);
            this.var17Unit.TabIndex = 528;
            // 
            // var16ValueColumn
            // 
            this.var16ValueColumn.Enabled = false;
            this.var16ValueColumn.Location = new System.Drawing.Point(826, 439);
            this.var16ValueColumn.Name = "var16ValueColumn";
            this.var16ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var16ValueColumn.TabIndex = 527;
            // 
            // var16Unit
            // 
            this.var16Unit.Enabled = false;
            this.var16Unit.Location = new System.Drawing.Point(749, 439);
            this.var16Unit.Name = "var16Unit";
            this.var16Unit.Size = new System.Drawing.Size(63, 20);
            this.var16Unit.TabIndex = 526;
            // 
            // var15ValueColumn
            // 
            this.var15ValueColumn.Enabled = false;
            this.var15ValueColumn.Location = new System.Drawing.Point(826, 413);
            this.var15ValueColumn.Name = "var15ValueColumn";
            this.var15ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var15ValueColumn.TabIndex = 525;
            // 
            // var15Unit
            // 
            this.var15Unit.Enabled = false;
            this.var15Unit.Location = new System.Drawing.Point(749, 413);
            this.var15Unit.Name = "var15Unit";
            this.var15Unit.Size = new System.Drawing.Size(63, 20);
            this.var15Unit.TabIndex = 524;
            // 
            // var14ValueColumn
            // 
            this.var14ValueColumn.Enabled = false;
            this.var14ValueColumn.Location = new System.Drawing.Point(826, 388);
            this.var14ValueColumn.Name = "var14ValueColumn";
            this.var14ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var14ValueColumn.TabIndex = 523;
            // 
            // var14Unit
            // 
            this.var14Unit.Location = new System.Drawing.Point(749, 388);
            this.var14Unit.Name = "var14Unit";
            this.var14Unit.Size = new System.Drawing.Size(63, 20);
            this.var14Unit.TabIndex = 522;
            // 
            // var13ValueColumn
            // 
            this.var13ValueColumn.Enabled = false;
            this.var13ValueColumn.Location = new System.Drawing.Point(826, 361);
            this.var13ValueColumn.Name = "var13ValueColumn";
            this.var13ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var13ValueColumn.TabIndex = 521;
            // 
            // var13Unit
            // 
            this.var13Unit.Location = new System.Drawing.Point(749, 361);
            this.var13Unit.Name = "var13Unit";
            this.var13Unit.Size = new System.Drawing.Size(63, 20);
            this.var13Unit.TabIndex = 520;
            // 
            // var18Offset
            // 
            this.var18Offset.Enabled = false;
            this.var18Offset.Location = new System.Drawing.Point(522, 491);
            this.var18Offset.Name = "var18Offset";
            this.var18Offset.Size = new System.Drawing.Size(63, 20);
            this.var18Offset.TabIndex = 519;
            this.var18Offset.Visible = false;
            this.var18Offset.TextChanged += new System.EventHandler(this.var18Offset_TextChanged);
            // 
            // var17Offset
            // 
            this.var17Offset.Enabled = false;
            this.var17Offset.Location = new System.Drawing.Point(522, 465);
            this.var17Offset.Name = "var17Offset";
            this.var17Offset.Size = new System.Drawing.Size(63, 20);
            this.var17Offset.TabIndex = 518;
            this.var17Offset.TextChanged += new System.EventHandler(this.var17Offset_TextChanged);
            // 
            // var16Offset
            // 
            this.var16Offset.Enabled = false;
            this.var16Offset.Location = new System.Drawing.Point(522, 440);
            this.var16Offset.Name = "var16Offset";
            this.var16Offset.Size = new System.Drawing.Size(63, 20);
            this.var16Offset.TabIndex = 517;
            this.var16Offset.TextChanged += new System.EventHandler(this.var16Offset_TextChanged);
            // 
            // var15Offset
            // 
            this.var15Offset.Enabled = false;
            this.var15Offset.Location = new System.Drawing.Point(522, 413);
            this.var15Offset.Name = "var15Offset";
            this.var15Offset.Size = new System.Drawing.Size(63, 20);
            this.var15Offset.TabIndex = 516;
            this.var15Offset.TextChanged += new System.EventHandler(this.var15Offset_TextChanged);
            // 
            // var14Offset
            // 
            this.var14Offset.Enabled = false;
            this.var14Offset.Location = new System.Drawing.Point(522, 389);
            this.var14Offset.Name = "var14Offset";
            this.var14Offset.Size = new System.Drawing.Size(63, 20);
            this.var14Offset.TabIndex = 515;
            this.var14Offset.TextChanged += new System.EventHandler(this.var14Offset_TextChanged);
            // 
            // var13Offset
            // 
            this.var13Offset.Enabled = false;
            this.var13Offset.Location = new System.Drawing.Point(522, 362);
            this.var13Offset.Name = "var13Offset";
            this.var13Offset.Size = new System.Drawing.Size(63, 20);
            this.var13Offset.TabIndex = 514;
            this.var13Offset.TextChanged += new System.EventHandler(this.var13Offset_TextChanged);
            // 
            // var18OutputMax
            // 
            this.var18OutputMax.Enabled = false;
            this.var18OutputMax.Location = new System.Drawing.Point(441, 491);
            this.var18OutputMax.Name = "var18OutputMax";
            this.var18OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var18OutputMax.TabIndex = 513;
            this.var18OutputMax.Visible = false;
            // 
            // var18OutputMin
            // 
            this.var18OutputMin.Enabled = false;
            this.var18OutputMin.Location = new System.Drawing.Point(364, 491);
            this.var18OutputMin.Name = "var18OutputMin";
            this.var18OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var18OutputMin.TabIndex = 512;
            this.var18OutputMin.Visible = false;
            // 
            // var18InputMax
            // 
            this.var18InputMax.Enabled = false;
            this.var18InputMax.Location = new System.Drawing.Point(286, 491);
            this.var18InputMax.Name = "var18InputMax";
            this.var18InputMax.Size = new System.Drawing.Size(63, 20);
            this.var18InputMax.TabIndex = 511;
            this.var18InputMax.Visible = false;
            // 
            // var18InputMin
            // 
            this.var18InputMin.Enabled = false;
            this.var18InputMin.Location = new System.Drawing.Point(210, 490);
            this.var18InputMin.Name = "var18InputMin";
            this.var18InputMin.Size = new System.Drawing.Size(63, 20);
            this.var18InputMin.TabIndex = 510;
            this.var18InputMin.Visible = false;
            // 
            // var18Channel
            // 
            this.var18Channel.Enabled = false;
            this.var18Channel.Location = new System.Drawing.Point(129, 491);
            this.var18Channel.Name = "var18Channel";
            this.var18Channel.Size = new System.Drawing.Size(63, 20);
            this.var18Channel.TabIndex = 508;
            this.var18Channel.Visible = false;
            // 
            // var17OutputMax
            // 
            this.var17OutputMax.Enabled = false;
            this.var17OutputMax.Location = new System.Drawing.Point(441, 464);
            this.var17OutputMax.Name = "var17OutputMax";
            this.var17OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var17OutputMax.TabIndex = 507;
            // 
            // var17OutputMin
            // 
            this.var17OutputMin.Enabled = false;
            this.var17OutputMin.Location = new System.Drawing.Point(364, 464);
            this.var17OutputMin.Name = "var17OutputMin";
            this.var17OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var17OutputMin.TabIndex = 506;
            // 
            // var17InputMax
            // 
            this.var17InputMax.Enabled = false;
            this.var17InputMax.Location = new System.Drawing.Point(286, 464);
            this.var17InputMax.Name = "var17InputMax";
            this.var17InputMax.Size = new System.Drawing.Size(63, 20);
            this.var17InputMax.TabIndex = 505;
            // 
            // var17InputMin
            // 
            this.var17InputMin.Enabled = false;
            this.var17InputMin.Location = new System.Drawing.Point(210, 463);
            this.var17InputMin.Name = "var17InputMin";
            this.var17InputMin.Size = new System.Drawing.Size(63, 20);
            this.var17InputMin.TabIndex = 504;
            // 
            // var17Channel
            // 
            this.var17Channel.Enabled = false;
            this.var17Channel.Location = new System.Drawing.Point(129, 464);
            this.var17Channel.Name = "var17Channel";
            this.var17Channel.Size = new System.Drawing.Size(63, 20);
            this.var17Channel.TabIndex = 502;
            // 
            // var16OutputMax
            // 
            this.var16OutputMax.Enabled = false;
            this.var16OutputMax.Location = new System.Drawing.Point(441, 439);
            this.var16OutputMax.Name = "var16OutputMax";
            this.var16OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var16OutputMax.TabIndex = 499;
            // 
            // var16OutputMin
            // 
            this.var16OutputMin.Enabled = false;
            this.var16OutputMin.Location = new System.Drawing.Point(364, 439);
            this.var16OutputMin.Name = "var16OutputMin";
            this.var16OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var16OutputMin.TabIndex = 498;
            // 
            // var16InputMax
            // 
            this.var16InputMax.Enabled = false;
            this.var16InputMax.Location = new System.Drawing.Point(286, 439);
            this.var16InputMax.Name = "var16InputMax";
            this.var16InputMax.Size = new System.Drawing.Size(63, 20);
            this.var16InputMax.TabIndex = 497;
            // 
            // var16InputMin
            // 
            this.var16InputMin.Enabled = false;
            this.var16InputMin.Location = new System.Drawing.Point(210, 439);
            this.var16InputMin.Name = "var16InputMin";
            this.var16InputMin.Size = new System.Drawing.Size(63, 20);
            this.var16InputMin.TabIndex = 496;
            // 
            // var16Channel
            // 
            this.var16Channel.Enabled = false;
            this.var16Channel.Location = new System.Drawing.Point(129, 439);
            this.var16Channel.Name = "var16Channel";
            this.var16Channel.Size = new System.Drawing.Size(63, 20);
            this.var16Channel.TabIndex = 494;
            // 
            // var15OutputMax
            // 
            this.var15OutputMax.Enabled = false;
            this.var15OutputMax.Location = new System.Drawing.Point(441, 413);
            this.var15OutputMax.Name = "var15OutputMax";
            this.var15OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var15OutputMax.TabIndex = 493;
            // 
            // var15OutputMin
            // 
            this.var15OutputMin.Enabled = false;
            this.var15OutputMin.Location = new System.Drawing.Point(364, 413);
            this.var15OutputMin.Name = "var15OutputMin";
            this.var15OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var15OutputMin.TabIndex = 492;
            // 
            // var15InputMax
            // 
            this.var15InputMax.Enabled = false;
            this.var15InputMax.Location = new System.Drawing.Point(286, 413);
            this.var15InputMax.Name = "var15InputMax";
            this.var15InputMax.Size = new System.Drawing.Size(63, 20);
            this.var15InputMax.TabIndex = 491;
            // 
            // var15InputMin
            // 
            this.var15InputMin.Enabled = false;
            this.var15InputMin.Location = new System.Drawing.Point(210, 412);
            this.var15InputMin.Name = "var15InputMin";
            this.var15InputMin.Size = new System.Drawing.Size(63, 20);
            this.var15InputMin.TabIndex = 490;
            // 
            // var15Channel
            // 
            this.var15Channel.Enabled = false;
            this.var15Channel.Location = new System.Drawing.Point(129, 413);
            this.var15Channel.Name = "var15Channel";
            this.var15Channel.Size = new System.Drawing.Size(63, 20);
            this.var15Channel.TabIndex = 488;
            // 
            // var14OutputMax
            // 
            this.var14OutputMax.Enabled = false;
            this.var14OutputMax.Location = new System.Drawing.Point(441, 388);
            this.var14OutputMax.Name = "var14OutputMax";
            this.var14OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var14OutputMax.TabIndex = 485;
            // 
            // var14OutputMin
            // 
            this.var14OutputMin.Enabled = false;
            this.var14OutputMin.Location = new System.Drawing.Point(364, 388);
            this.var14OutputMin.Name = "var14OutputMin";
            this.var14OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var14OutputMin.TabIndex = 484;
            // 
            // var14InputMax
            // 
            this.var14InputMax.Enabled = false;
            this.var14InputMax.Location = new System.Drawing.Point(286, 388);
            this.var14InputMax.Name = "var14InputMax";
            this.var14InputMax.Size = new System.Drawing.Size(63, 20);
            this.var14InputMax.TabIndex = 483;
            // 
            // var14InputMin
            // 
            this.var14InputMin.Enabled = false;
            this.var14InputMin.Location = new System.Drawing.Point(210, 387);
            this.var14InputMin.Name = "var14InputMin";
            this.var14InputMin.Size = new System.Drawing.Size(63, 20);
            this.var14InputMin.TabIndex = 482;
            // 
            // var14Channel
            // 
            this.var14Channel.Enabled = false;
            this.var14Channel.Location = new System.Drawing.Point(129, 388);
            this.var14Channel.Name = "var14Channel";
            this.var14Channel.Size = new System.Drawing.Size(63, 20);
            this.var14Channel.TabIndex = 480;
            // 
            // var13OutputMax
            // 
            this.var13OutputMax.Enabled = false;
            this.var13OutputMax.Location = new System.Drawing.Point(441, 361);
            this.var13OutputMax.Name = "var13OutputMax";
            this.var13OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var13OutputMax.TabIndex = 479;
            // 
            // var13OutputMin
            // 
            this.var13OutputMin.Enabled = false;
            this.var13OutputMin.Location = new System.Drawing.Point(364, 361);
            this.var13OutputMin.Name = "var13OutputMin";
            this.var13OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var13OutputMin.TabIndex = 478;
            // 
            // var13InputMax
            // 
            this.var13InputMax.Enabled = false;
            this.var13InputMax.Location = new System.Drawing.Point(286, 361);
            this.var13InputMax.Name = "var13InputMax";
            this.var13InputMax.Size = new System.Drawing.Size(63, 20);
            this.var13InputMax.TabIndex = 477;
            // 
            // var13InputMin
            // 
            this.var13InputMin.Enabled = false;
            this.var13InputMin.Location = new System.Drawing.Point(210, 361);
            this.var13InputMin.Name = "var13InputMin";
            this.var13InputMin.Size = new System.Drawing.Size(63, 20);
            this.var13InputMin.TabIndex = 476;
            // 
            // var13Channel
            // 
            this.var13Channel.Enabled = false;
            this.var13Channel.Location = new System.Drawing.Point(129, 361);
            this.var13Channel.Name = "var13Channel";
            this.var13Channel.Size = new System.Drawing.Size(63, 20);
            this.var13Channel.TabIndex = 474;
            // 
            // var12DisplayName
            // 
            this.var12DisplayName.Enabled = false;
            this.var12DisplayName.Location = new System.Drawing.Point(1069, 335);
            this.var12DisplayName.Name = "var12DisplayName";
            this.var12DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var12DisplayName.TabIndex = 471;
            // 
            // var11DisplayName
            // 
            this.var11DisplayName.Location = new System.Drawing.Point(1069, 309);
            this.var11DisplayName.Name = "var11DisplayName";
            this.var11DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var11DisplayName.TabIndex = 470;
            // 
            // var10DisplayName
            // 
            this.var10DisplayName.Location = new System.Drawing.Point(1069, 284);
            this.var10DisplayName.Name = "var10DisplayName";
            this.var10DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var10DisplayName.TabIndex = 469;
            // 
            // var9DisplayName
            // 
            this.var9DisplayName.Enabled = false;
            this.var9DisplayName.Location = new System.Drawing.Point(1069, 257);
            this.var9DisplayName.Name = "var9DisplayName";
            this.var9DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var9DisplayName.TabIndex = 468;
            // 
            // var8DisplayName
            // 
            this.var8DisplayName.Location = new System.Drawing.Point(1069, 233);
            this.var8DisplayName.Name = "var8DisplayName";
            this.var8DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var8DisplayName.TabIndex = 467;
            // 
            // var7DisplayName
            // 
            this.var7DisplayName.Location = new System.Drawing.Point(1069, 206);
            this.var7DisplayName.Name = "var7DisplayName";
            this.var7DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var7DisplayName.TabIndex = 466;
            // 
            // var12Type
            // 
            this.var12Type.Enabled = false;
            this.var12Type.Location = new System.Drawing.Point(991, 335);
            this.var12Type.Name = "var12Type";
            this.var12Type.Size = new System.Drawing.Size(63, 20);
            this.var12Type.TabIndex = 465;
            // 
            // var11Type
            // 
            this.var11Type.Enabled = false;
            this.var11Type.Location = new System.Drawing.Point(991, 309);
            this.var11Type.Name = "var11Type";
            this.var11Type.Size = new System.Drawing.Size(63, 20);
            this.var11Type.TabIndex = 464;
            // 
            // var10Type
            // 
            this.var10Type.Enabled = false;
            this.var10Type.Location = new System.Drawing.Point(991, 284);
            this.var10Type.Name = "var10Type";
            this.var10Type.Size = new System.Drawing.Size(63, 20);
            this.var10Type.TabIndex = 463;
            // 
            // var9Type
            // 
            this.var9Type.Enabled = false;
            this.var9Type.Location = new System.Drawing.Point(991, 257);
            this.var9Type.Name = "var9Type";
            this.var9Type.Size = new System.Drawing.Size(63, 20);
            this.var9Type.TabIndex = 462;
            // 
            // var8Type
            // 
            this.var8Type.Enabled = false;
            this.var8Type.Location = new System.Drawing.Point(991, 233);
            this.var8Type.Name = "var8Type";
            this.var8Type.Size = new System.Drawing.Size(63, 20);
            this.var8Type.TabIndex = 461;
            // 
            // var7Type
            // 
            this.var7Type.Enabled = false;
            this.var7Type.Location = new System.Drawing.Point(991, 206);
            this.var7Type.Name = "var7Type";
            this.var7Type.Size = new System.Drawing.Size(63, 20);
            this.var7Type.TabIndex = 460;
            // 
            // var12StatusColumn
            // 
            this.var12StatusColumn.Enabled = false;
            this.var12StatusColumn.Location = new System.Drawing.Point(907, 335);
            this.var12StatusColumn.Name = "var12StatusColumn";
            this.var12StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var12StatusColumn.TabIndex = 459;
            // 
            // var11StatusColumn
            // 
            this.var11StatusColumn.Enabled = false;
            this.var11StatusColumn.Location = new System.Drawing.Point(907, 309);
            this.var11StatusColumn.Name = "var11StatusColumn";
            this.var11StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var11StatusColumn.TabIndex = 458;
            // 
            // var10StatusColumn
            // 
            this.var10StatusColumn.Enabled = false;
            this.var10StatusColumn.Location = new System.Drawing.Point(907, 284);
            this.var10StatusColumn.Name = "var10StatusColumn";
            this.var10StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var10StatusColumn.TabIndex = 457;
            // 
            // var9StatusColumn
            // 
            this.var9StatusColumn.Enabled = false;
            this.var9StatusColumn.Location = new System.Drawing.Point(907, 257);
            this.var9StatusColumn.Name = "var9StatusColumn";
            this.var9StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var9StatusColumn.TabIndex = 456;
            // 
            // var8StatusColumn
            // 
            this.var8StatusColumn.Enabled = false;
            this.var8StatusColumn.Location = new System.Drawing.Point(907, 233);
            this.var8StatusColumn.Name = "var8StatusColumn";
            this.var8StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var8StatusColumn.TabIndex = 455;
            // 
            // var7StatusColumn
            // 
            this.var7StatusColumn.Enabled = false;
            this.var7StatusColumn.Location = new System.Drawing.Point(907, 206);
            this.var7StatusColumn.Name = "var7StatusColumn";
            this.var7StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var7StatusColumn.TabIndex = 454;
            // 
            // var12ValueColumn
            // 
            this.var12ValueColumn.Enabled = false;
            this.var12ValueColumn.Location = new System.Drawing.Point(826, 335);
            this.var12ValueColumn.Name = "var12ValueColumn";
            this.var12ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var12ValueColumn.TabIndex = 453;
            // 
            // var12Unit
            // 
            this.var12Unit.Enabled = false;
            this.var12Unit.Location = new System.Drawing.Point(749, 335);
            this.var12Unit.Name = "var12Unit";
            this.var12Unit.Size = new System.Drawing.Size(63, 20);
            this.var12Unit.TabIndex = 452;
            // 
            // var11ValueColumn
            // 
            this.var11ValueColumn.Enabled = false;
            this.var11ValueColumn.Location = new System.Drawing.Point(826, 308);
            this.var11ValueColumn.Name = "var11ValueColumn";
            this.var11ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var11ValueColumn.TabIndex = 451;
            // 
            // var11Unit
            // 
            this.var11Unit.Location = new System.Drawing.Point(749, 308);
            this.var11Unit.Name = "var11Unit";
            this.var11Unit.Size = new System.Drawing.Size(63, 20);
            this.var11Unit.TabIndex = 450;
            // 
            // var10ValueColumn
            // 
            this.var10ValueColumn.Enabled = false;
            this.var10ValueColumn.Location = new System.Drawing.Point(826, 283);
            this.var10ValueColumn.Name = "var10ValueColumn";
            this.var10ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var10ValueColumn.TabIndex = 449;
            // 
            // var10Unit
            // 
            this.var10Unit.Location = new System.Drawing.Point(749, 283);
            this.var10Unit.Name = "var10Unit";
            this.var10Unit.Size = new System.Drawing.Size(63, 20);
            this.var10Unit.TabIndex = 448;
            // 
            // var9ValueColumn
            // 
            this.var9ValueColumn.Enabled = false;
            this.var9ValueColumn.Location = new System.Drawing.Point(826, 257);
            this.var9ValueColumn.Name = "var9ValueColumn";
            this.var9ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var9ValueColumn.TabIndex = 447;
            // 
            // var9Unit
            // 
            this.var9Unit.Enabled = false;
            this.var9Unit.Location = new System.Drawing.Point(749, 257);
            this.var9Unit.Name = "var9Unit";
            this.var9Unit.Size = new System.Drawing.Size(63, 20);
            this.var9Unit.TabIndex = 446;
            // 
            // var8ValueColumn
            // 
            this.var8ValueColumn.Enabled = false;
            this.var8ValueColumn.Location = new System.Drawing.Point(826, 232);
            this.var8ValueColumn.Name = "var8ValueColumn";
            this.var8ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var8ValueColumn.TabIndex = 445;
            // 
            // var8Unit
            // 
            this.var8Unit.Location = new System.Drawing.Point(749, 232);
            this.var8Unit.Name = "var8Unit";
            this.var8Unit.Size = new System.Drawing.Size(63, 20);
            this.var8Unit.TabIndex = 444;
            // 
            // var7ValueColumn
            // 
            this.var7ValueColumn.Enabled = false;
            this.var7ValueColumn.Location = new System.Drawing.Point(826, 205);
            this.var7ValueColumn.Name = "var7ValueColumn";
            this.var7ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var7ValueColumn.TabIndex = 443;
            // 
            // var7Unit
            // 
            this.var7Unit.Location = new System.Drawing.Point(749, 205);
            this.var7Unit.Name = "var7Unit";
            this.var7Unit.Size = new System.Drawing.Size(63, 20);
            this.var7Unit.TabIndex = 442;
            // 
            // var12Offset
            // 
            this.var12Offset.Enabled = false;
            this.var12Offset.Location = new System.Drawing.Point(522, 335);
            this.var12Offset.Name = "var12Offset";
            this.var12Offset.Size = new System.Drawing.Size(63, 20);
            this.var12Offset.TabIndex = 441;
            this.var12Offset.TextChanged += new System.EventHandler(this.var12Offset_TextChanged);
            // 
            // var11Offset
            // 
            this.var11Offset.Enabled = false;
            this.var11Offset.Location = new System.Drawing.Point(522, 309);
            this.var11Offset.Name = "var11Offset";
            this.var11Offset.Size = new System.Drawing.Size(63, 20);
            this.var11Offset.TabIndex = 440;
            this.var11Offset.TextChanged += new System.EventHandler(this.var11Offset_TextChanged);
            // 
            // var10Offset
            // 
            this.var10Offset.Enabled = false;
            this.var10Offset.Location = new System.Drawing.Point(522, 284);
            this.var10Offset.Name = "var10Offset";
            this.var10Offset.Size = new System.Drawing.Size(63, 20);
            this.var10Offset.TabIndex = 439;
            this.var10Offset.TextChanged += new System.EventHandler(this.var10Offset_TextChanged);
            // 
            // var9Offset
            // 
            this.var9Offset.Enabled = false;
            this.var9Offset.Location = new System.Drawing.Point(522, 257);
            this.var9Offset.Name = "var9Offset";
            this.var9Offset.Size = new System.Drawing.Size(63, 20);
            this.var9Offset.TabIndex = 438;
            this.var9Offset.TextChanged += new System.EventHandler(this.var9Offset_TextChanged);
            // 
            // var8Offset
            // 
            this.var8Offset.Enabled = false;
            this.var8Offset.Location = new System.Drawing.Point(522, 233);
            this.var8Offset.Name = "var8Offset";
            this.var8Offset.Size = new System.Drawing.Size(63, 20);
            this.var8Offset.TabIndex = 437;
            this.var8Offset.TextChanged += new System.EventHandler(this.var8Offset_TextChanged);
            // 
            // var7Offset
            // 
            this.var7Offset.Enabled = false;
            this.var7Offset.Location = new System.Drawing.Point(522, 206);
            this.var7Offset.Name = "var7Offset";
            this.var7Offset.Size = new System.Drawing.Size(63, 20);
            this.var7Offset.TabIndex = 436;
            this.var7Offset.TextChanged += new System.EventHandler(this.var7Offset_TextChanged);
            // 
            // var12OutputMax
            // 
            this.var12OutputMax.Enabled = false;
            this.var12OutputMax.Location = new System.Drawing.Point(441, 335);
            this.var12OutputMax.Name = "var12OutputMax";
            this.var12OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var12OutputMax.TabIndex = 435;
            // 
            // var12OutputMin
            // 
            this.var12OutputMin.Enabled = false;
            this.var12OutputMin.Location = new System.Drawing.Point(364, 335);
            this.var12OutputMin.Name = "var12OutputMin";
            this.var12OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var12OutputMin.TabIndex = 434;
            // 
            // var12InputMax
            // 
            this.var12InputMax.Enabled = false;
            this.var12InputMax.Location = new System.Drawing.Point(286, 335);
            this.var12InputMax.Name = "var12InputMax";
            this.var12InputMax.Size = new System.Drawing.Size(63, 20);
            this.var12InputMax.TabIndex = 433;
            // 
            // var12InputMin
            // 
            this.var12InputMin.Enabled = false;
            this.var12InputMin.Location = new System.Drawing.Point(210, 334);
            this.var12InputMin.Name = "var12InputMin";
            this.var12InputMin.Size = new System.Drawing.Size(63, 20);
            this.var12InputMin.TabIndex = 432;
            // 
            // var12Channel
            // 
            this.var12Channel.Enabled = false;
            this.var12Channel.Location = new System.Drawing.Point(129, 335);
            this.var12Channel.Name = "var12Channel";
            this.var12Channel.Size = new System.Drawing.Size(63, 20);
            this.var12Channel.TabIndex = 430;
            // 
            // var11OutputMax
            // 
            this.var11OutputMax.Enabled = false;
            this.var11OutputMax.Location = new System.Drawing.Point(441, 308);
            this.var11OutputMax.Name = "var11OutputMax";
            this.var11OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var11OutputMax.TabIndex = 429;
            // 
            // var11OutputMin
            // 
            this.var11OutputMin.Enabled = false;
            this.var11OutputMin.Location = new System.Drawing.Point(364, 308);
            this.var11OutputMin.Name = "var11OutputMin";
            this.var11OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var11OutputMin.TabIndex = 428;
            // 
            // var11InputMax
            // 
            this.var11InputMax.Enabled = false;
            this.var11InputMax.Location = new System.Drawing.Point(286, 308);
            this.var11InputMax.Name = "var11InputMax";
            this.var11InputMax.Size = new System.Drawing.Size(63, 20);
            this.var11InputMax.TabIndex = 427;
            // 
            // var11InputMin
            // 
            this.var11InputMin.Enabled = false;
            this.var11InputMin.Location = new System.Drawing.Point(210, 307);
            this.var11InputMin.Name = "var11InputMin";
            this.var11InputMin.Size = new System.Drawing.Size(63, 20);
            this.var11InputMin.TabIndex = 426;
            // 
            // var11Channel
            // 
            this.var11Channel.Enabled = false;
            this.var11Channel.Location = new System.Drawing.Point(129, 308);
            this.var11Channel.Name = "var11Channel";
            this.var11Channel.Size = new System.Drawing.Size(63, 20);
            this.var11Channel.TabIndex = 424;
            // 
            // var10OutputMax
            // 
            this.var10OutputMax.Enabled = false;
            this.var10OutputMax.Location = new System.Drawing.Point(441, 283);
            this.var10OutputMax.Name = "var10OutputMax";
            this.var10OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var10OutputMax.TabIndex = 421;
            // 
            // var10OutputMin
            // 
            this.var10OutputMin.Enabled = false;
            this.var10OutputMin.Location = new System.Drawing.Point(364, 283);
            this.var10OutputMin.Name = "var10OutputMin";
            this.var10OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var10OutputMin.TabIndex = 420;
            // 
            // var10InputMax
            // 
            this.var10InputMax.Enabled = false;
            this.var10InputMax.Location = new System.Drawing.Point(286, 283);
            this.var10InputMax.Name = "var10InputMax";
            this.var10InputMax.Size = new System.Drawing.Size(63, 20);
            this.var10InputMax.TabIndex = 419;
            // 
            // var10InputMin
            // 
            this.var10InputMin.Enabled = false;
            this.var10InputMin.Location = new System.Drawing.Point(210, 283);
            this.var10InputMin.Name = "var10InputMin";
            this.var10InputMin.Size = new System.Drawing.Size(63, 20);
            this.var10InputMin.TabIndex = 418;
            // 
            // var10Channel
            // 
            this.var10Channel.Enabled = false;
            this.var10Channel.Location = new System.Drawing.Point(129, 283);
            this.var10Channel.Name = "var10Channel";
            this.var10Channel.Size = new System.Drawing.Size(63, 20);
            this.var10Channel.TabIndex = 416;
            // 
            // var9OutputMax
            // 
            this.var9OutputMax.Enabled = false;
            this.var9OutputMax.Location = new System.Drawing.Point(441, 257);
            this.var9OutputMax.Name = "var9OutputMax";
            this.var9OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var9OutputMax.TabIndex = 415;
            // 
            // var9OutputMin
            // 
            this.var9OutputMin.Enabled = false;
            this.var9OutputMin.Location = new System.Drawing.Point(364, 257);
            this.var9OutputMin.Name = "var9OutputMin";
            this.var9OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var9OutputMin.TabIndex = 414;
            // 
            // var9InputMax
            // 
            this.var9InputMax.Enabled = false;
            this.var9InputMax.Location = new System.Drawing.Point(286, 257);
            this.var9InputMax.Name = "var9InputMax";
            this.var9InputMax.Size = new System.Drawing.Size(63, 20);
            this.var9InputMax.TabIndex = 413;
            // 
            // var9InputMin
            // 
            this.var9InputMin.Enabled = false;
            this.var9InputMin.Location = new System.Drawing.Point(210, 256);
            this.var9InputMin.Name = "var9InputMin";
            this.var9InputMin.Size = new System.Drawing.Size(63, 20);
            this.var9InputMin.TabIndex = 412;
            // 
            // var9Channel
            // 
            this.var9Channel.Enabled = false;
            this.var9Channel.Location = new System.Drawing.Point(129, 257);
            this.var9Channel.Name = "var9Channel";
            this.var9Channel.Size = new System.Drawing.Size(63, 20);
            this.var9Channel.TabIndex = 410;
            // 
            // var8OutputMax
            // 
            this.var8OutputMax.Enabled = false;
            this.var8OutputMax.Location = new System.Drawing.Point(441, 232);
            this.var8OutputMax.Name = "var8OutputMax";
            this.var8OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var8OutputMax.TabIndex = 407;
            // 
            // var8OutputMin
            // 
            this.var8OutputMin.Enabled = false;
            this.var8OutputMin.Location = new System.Drawing.Point(364, 232);
            this.var8OutputMin.Name = "var8OutputMin";
            this.var8OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var8OutputMin.TabIndex = 406;
            // 
            // var8InputMax
            // 
            this.var8InputMax.Enabled = false;
            this.var8InputMax.Location = new System.Drawing.Point(286, 232);
            this.var8InputMax.Name = "var8InputMax";
            this.var8InputMax.Size = new System.Drawing.Size(63, 20);
            this.var8InputMax.TabIndex = 405;
            // 
            // var8InputMin
            // 
            this.var8InputMin.Enabled = false;
            this.var8InputMin.Location = new System.Drawing.Point(210, 231);
            this.var8InputMin.Name = "var8InputMin";
            this.var8InputMin.Size = new System.Drawing.Size(63, 20);
            this.var8InputMin.TabIndex = 404;
            // 
            // var8Channel
            // 
            this.var8Channel.Enabled = false;
            this.var8Channel.Location = new System.Drawing.Point(129, 232);
            this.var8Channel.Name = "var8Channel";
            this.var8Channel.Size = new System.Drawing.Size(63, 20);
            this.var8Channel.TabIndex = 402;
            // 
            // var7OutputMax
            // 
            this.var7OutputMax.Enabled = false;
            this.var7OutputMax.Location = new System.Drawing.Point(441, 205);
            this.var7OutputMax.Name = "var7OutputMax";
            this.var7OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var7OutputMax.TabIndex = 401;
            // 
            // var7OutputMin
            // 
            this.var7OutputMin.Enabled = false;
            this.var7OutputMin.Location = new System.Drawing.Point(364, 205);
            this.var7OutputMin.Name = "var7OutputMin";
            this.var7OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var7OutputMin.TabIndex = 400;
            // 
            // var7InputMax
            // 
            this.var7InputMax.Enabled = false;
            this.var7InputMax.Location = new System.Drawing.Point(286, 205);
            this.var7InputMax.Name = "var7InputMax";
            this.var7InputMax.Size = new System.Drawing.Size(63, 20);
            this.var7InputMax.TabIndex = 399;
            // 
            // var7InputMin
            // 
            this.var7InputMin.Enabled = false;
            this.var7InputMin.Location = new System.Drawing.Point(210, 205);
            this.var7InputMin.Name = "var7InputMin";
            this.var7InputMin.Size = new System.Drawing.Size(63, 20);
            this.var7InputMin.TabIndex = 398;
            // 
            // var7Channel
            // 
            this.var7Channel.Enabled = false;
            this.var7Channel.Location = new System.Drawing.Point(129, 205);
            this.var7Channel.Name = "var7Channel";
            this.var7Channel.Size = new System.Drawing.Size(63, 20);
            this.var7Channel.TabIndex = 396;
            // 
            // var6DisplayName
            // 
            this.var6DisplayName.Location = new System.Drawing.Point(1069, 179);
            this.var6DisplayName.Name = "var6DisplayName";
            this.var6DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var6DisplayName.TabIndex = 393;
            // 
            // var5DisplayName
            // 
            this.var5DisplayName.Location = new System.Drawing.Point(1069, 153);
            this.var5DisplayName.Name = "var5DisplayName";
            this.var5DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var5DisplayName.TabIndex = 392;
            // 
            // var4DisplayName
            // 
            this.var4DisplayName.Location = new System.Drawing.Point(1069, 128);
            this.var4DisplayName.Name = "var4DisplayName";
            this.var4DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var4DisplayName.TabIndex = 391;
            // 
            // var3DisplayName
            // 
            this.var3DisplayName.Location = new System.Drawing.Point(1069, 101);
            this.var3DisplayName.Name = "var3DisplayName";
            this.var3DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var3DisplayName.TabIndex = 390;
            // 
            // var2DisplayName
            // 
            this.var2DisplayName.Location = new System.Drawing.Point(1069, 77);
            this.var2DisplayName.Name = "var2DisplayName";
            this.var2DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var2DisplayName.TabIndex = 389;
            // 
            // var1DisplayName
            // 
            this.var1DisplayName.Location = new System.Drawing.Point(1069, 50);
            this.var1DisplayName.Name = "var1DisplayName";
            this.var1DisplayName.Size = new System.Drawing.Size(63, 20);
            this.var1DisplayName.TabIndex = 388;
            // 
            // var6Type
            // 
            this.var6Type.Enabled = false;
            this.var6Type.Location = new System.Drawing.Point(991, 179);
            this.var6Type.Name = "var6Type";
            this.var6Type.Size = new System.Drawing.Size(63, 20);
            this.var6Type.TabIndex = 386;
            // 
            // var5Type
            // 
            this.var5Type.Enabled = false;
            this.var5Type.Location = new System.Drawing.Point(991, 153);
            this.var5Type.Name = "var5Type";
            this.var5Type.Size = new System.Drawing.Size(63, 20);
            this.var5Type.TabIndex = 385;
            // 
            // var4Type
            // 
            this.var4Type.Enabled = false;
            this.var4Type.Location = new System.Drawing.Point(991, 128);
            this.var4Type.Name = "var4Type";
            this.var4Type.Size = new System.Drawing.Size(63, 20);
            this.var4Type.TabIndex = 384;
            // 
            // var3Type
            // 
            this.var3Type.Enabled = false;
            this.var3Type.Location = new System.Drawing.Point(991, 101);
            this.var3Type.Name = "var3Type";
            this.var3Type.Size = new System.Drawing.Size(63, 20);
            this.var3Type.TabIndex = 383;
            // 
            // var2Type
            // 
            this.var2Type.Enabled = false;
            this.var2Type.Location = new System.Drawing.Point(991, 77);
            this.var2Type.Name = "var2Type";
            this.var2Type.Size = new System.Drawing.Size(63, 20);
            this.var2Type.TabIndex = 382;
            // 
            // var1Type
            // 
            this.var1Type.Enabled = false;
            this.var1Type.Location = new System.Drawing.Point(991, 50);
            this.var1Type.Name = "var1Type";
            this.var1Type.Size = new System.Drawing.Size(63, 20);
            this.var1Type.TabIndex = 381;
            // 
            // var6StatusColumn
            // 
            this.var6StatusColumn.Enabled = false;
            this.var6StatusColumn.Location = new System.Drawing.Point(907, 179);
            this.var6StatusColumn.Name = "var6StatusColumn";
            this.var6StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var6StatusColumn.TabIndex = 379;
            // 
            // var5StatusColumn
            // 
            this.var5StatusColumn.Enabled = false;
            this.var5StatusColumn.Location = new System.Drawing.Point(907, 153);
            this.var5StatusColumn.Name = "var5StatusColumn";
            this.var5StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var5StatusColumn.TabIndex = 378;
            // 
            // var4StatusColumn
            // 
            this.var4StatusColumn.Enabled = false;
            this.var4StatusColumn.Location = new System.Drawing.Point(907, 128);
            this.var4StatusColumn.Name = "var4StatusColumn";
            this.var4StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var4StatusColumn.TabIndex = 377;
            // 
            // var3StatusColumn
            // 
            this.var3StatusColumn.Enabled = false;
            this.var3StatusColumn.Location = new System.Drawing.Point(907, 101);
            this.var3StatusColumn.Name = "var3StatusColumn";
            this.var3StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var3StatusColumn.TabIndex = 376;
            // 
            // var2StatusColumn
            // 
            this.var2StatusColumn.Enabled = false;
            this.var2StatusColumn.Location = new System.Drawing.Point(907, 77);
            this.var2StatusColumn.Name = "var2StatusColumn";
            this.var2StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var2StatusColumn.TabIndex = 375;
            // 
            // var1StatusColumn
            // 
            this.var1StatusColumn.Enabled = false;
            this.var1StatusColumn.Location = new System.Drawing.Point(907, 50);
            this.var1StatusColumn.Name = "var1StatusColumn";
            this.var1StatusColumn.Size = new System.Drawing.Size(63, 20);
            this.var1StatusColumn.TabIndex = 374;
            // 
            // var6ValueColumn
            // 
            this.var6ValueColumn.Enabled = false;
            this.var6ValueColumn.Location = new System.Drawing.Point(826, 179);
            this.var6ValueColumn.Name = "var6ValueColumn";
            this.var6ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var6ValueColumn.TabIndex = 372;
            // 
            // var6Unit
            // 
            this.var6Unit.Location = new System.Drawing.Point(749, 179);
            this.var6Unit.Name = "var6Unit";
            this.var6Unit.Size = new System.Drawing.Size(63, 20);
            this.var6Unit.TabIndex = 371;
            // 
            // var5ValueColumn
            // 
            this.var5ValueColumn.Enabled = false;
            this.var5ValueColumn.Location = new System.Drawing.Point(826, 152);
            this.var5ValueColumn.Name = "var5ValueColumn";
            this.var5ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var5ValueColumn.TabIndex = 370;
            // 
            // var5Unit
            // 
            this.var5Unit.Location = new System.Drawing.Point(749, 152);
            this.var5Unit.Name = "var5Unit";
            this.var5Unit.Size = new System.Drawing.Size(63, 20);
            this.var5Unit.TabIndex = 369;
            // 
            // var4ValueColumn
            // 
            this.var4ValueColumn.Enabled = false;
            this.var4ValueColumn.Location = new System.Drawing.Point(826, 127);
            this.var4ValueColumn.Name = "var4ValueColumn";
            this.var4ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var4ValueColumn.TabIndex = 368;
            // 
            // var4Unit
            // 
            this.var4Unit.Location = new System.Drawing.Point(749, 127);
            this.var4Unit.Name = "var4Unit";
            this.var4Unit.Size = new System.Drawing.Size(63, 20);
            this.var4Unit.TabIndex = 367;
            // 
            // var3ValueColumn
            // 
            this.var3ValueColumn.Enabled = false;
            this.var3ValueColumn.Location = new System.Drawing.Point(826, 101);
            this.var3ValueColumn.Name = "var3ValueColumn";
            this.var3ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var3ValueColumn.TabIndex = 366;
            // 
            // var3Unit
            // 
            this.var3Unit.Location = new System.Drawing.Point(749, 101);
            this.var3Unit.Name = "var3Unit";
            this.var3Unit.Size = new System.Drawing.Size(63, 20);
            this.var3Unit.TabIndex = 365;
            // 
            // var2ValueColumn
            // 
            this.var2ValueColumn.Enabled = false;
            this.var2ValueColumn.Location = new System.Drawing.Point(826, 76);
            this.var2ValueColumn.Name = "var2ValueColumn";
            this.var2ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var2ValueColumn.TabIndex = 364;
            // 
            // var2Unit
            // 
            this.var2Unit.Location = new System.Drawing.Point(749, 76);
            this.var2Unit.Name = "var2Unit";
            this.var2Unit.Size = new System.Drawing.Size(63, 20);
            this.var2Unit.TabIndex = 363;
            // 
            // var1ValueColumn
            // 
            this.var1ValueColumn.Enabled = false;
            this.var1ValueColumn.Location = new System.Drawing.Point(826, 49);
            this.var1ValueColumn.Name = "var1ValueColumn";
            this.var1ValueColumn.Size = new System.Drawing.Size(63, 20);
            this.var1ValueColumn.TabIndex = 362;
            // 
            // var1Unit
            // 
            this.var1Unit.Location = new System.Drawing.Point(749, 49);
            this.var1Unit.Name = "var1Unit";
            this.var1Unit.Size = new System.Drawing.Size(63, 20);
            this.var1Unit.TabIndex = 361;
            // 
            // var6Offset
            // 
            this.var6Offset.Enabled = false;
            this.var6Offset.Location = new System.Drawing.Point(522, 179);
            this.var6Offset.Name = "var6Offset";
            this.var6Offset.Size = new System.Drawing.Size(63, 20);
            this.var6Offset.TabIndex = 358;
            this.var6Offset.TextChanged += new System.EventHandler(this.var6Offset_TextChanged);
            // 
            // var5Offset
            // 
            this.var5Offset.Enabled = false;
            this.var5Offset.Location = new System.Drawing.Point(522, 153);
            this.var5Offset.Name = "var5Offset";
            this.var5Offset.Size = new System.Drawing.Size(63, 20);
            this.var5Offset.TabIndex = 357;
            this.var5Offset.TextChanged += new System.EventHandler(this.var5Offset_TextChanged);
            // 
            // var4Offset
            // 
            this.var4Offset.Enabled = false;
            this.var4Offset.Location = new System.Drawing.Point(522, 128);
            this.var4Offset.Name = "var4Offset";
            this.var4Offset.Size = new System.Drawing.Size(63, 20);
            this.var4Offset.TabIndex = 356;
            this.var4Offset.TextChanged += new System.EventHandler(this.var4Offset_TextChanged);
            // 
            // var3Offset
            // 
            this.var3Offset.Enabled = false;
            this.var3Offset.Location = new System.Drawing.Point(522, 101);
            this.var3Offset.Name = "var3Offset";
            this.var3Offset.Size = new System.Drawing.Size(63, 20);
            this.var3Offset.TabIndex = 355;
            this.var3Offset.TextChanged += new System.EventHandler(this.var3Offset_TextChanged);
            // 
            // var2Offset
            // 
            this.var2Offset.Enabled = false;
            this.var2Offset.Location = new System.Drawing.Point(522, 77);
            this.var2Offset.Name = "var2Offset";
            this.var2Offset.Size = new System.Drawing.Size(63, 20);
            this.var2Offset.TabIndex = 354;
            this.var2Offset.TextChanged += new System.EventHandler(this.var2Offset_TextChanged);
            // 
            // var1Offset
            // 
            this.var1Offset.Enabled = false;
            this.var1Offset.Location = new System.Drawing.Point(522, 50);
            this.var1Offset.Name = "var1Offset";
            this.var1Offset.Size = new System.Drawing.Size(63, 20);
            this.var1Offset.TabIndex = 353;
            this.var1Offset.TextChanged += new System.EventHandler(this.var1Offset_TextChanged);
            // 
            // var6OutputMax
            // 
            this.var6OutputMax.Enabled = false;
            this.var6OutputMax.Location = new System.Drawing.Point(441, 179);
            this.var6OutputMax.Name = "var6OutputMax";
            this.var6OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var6OutputMax.TabIndex = 349;
            // 
            // var6OutputMin
            // 
            this.var6OutputMin.Enabled = false;
            this.var6OutputMin.Location = new System.Drawing.Point(364, 179);
            this.var6OutputMin.Name = "var6OutputMin";
            this.var6OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var6OutputMin.TabIndex = 348;
            // 
            // var6InputMax
            // 
            this.var6InputMax.Enabled = false;
            this.var6InputMax.Location = new System.Drawing.Point(286, 179);
            this.var6InputMax.Name = "var6InputMax";
            this.var6InputMax.Size = new System.Drawing.Size(63, 20);
            this.var6InputMax.TabIndex = 347;
            // 
            // var6InputMin
            // 
            this.var6InputMin.Enabled = false;
            this.var6InputMin.Location = new System.Drawing.Point(210, 178);
            this.var6InputMin.Name = "var6InputMin";
            this.var6InputMin.Size = new System.Drawing.Size(63, 20);
            this.var6InputMin.TabIndex = 346;
            // 
            // var6Channel
            // 
            this.var6Channel.Enabled = false;
            this.var6Channel.Location = new System.Drawing.Point(129, 179);
            this.var6Channel.Name = "var6Channel";
            this.var6Channel.Size = new System.Drawing.Size(63, 20);
            this.var6Channel.TabIndex = 344;
            // 
            // var5OutputMax
            // 
            this.var5OutputMax.Enabled = false;
            this.var5OutputMax.Location = new System.Drawing.Point(441, 152);
            this.var5OutputMax.Name = "var5OutputMax";
            this.var5OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var5OutputMax.TabIndex = 343;
            // 
            // var5OutputMin
            // 
            this.var5OutputMin.Enabled = false;
            this.var5OutputMin.Location = new System.Drawing.Point(364, 152);
            this.var5OutputMin.Name = "var5OutputMin";
            this.var5OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var5OutputMin.TabIndex = 342;
            // 
            // var5InputMax
            // 
            this.var5InputMax.Enabled = false;
            this.var5InputMax.Location = new System.Drawing.Point(286, 152);
            this.var5InputMax.Name = "var5InputMax";
            this.var5InputMax.Size = new System.Drawing.Size(63, 20);
            this.var5InputMax.TabIndex = 341;
            // 
            // var5InputMin
            // 
            this.var5InputMin.Enabled = false;
            this.var5InputMin.Location = new System.Drawing.Point(210, 151);
            this.var5InputMin.Name = "var5InputMin";
            this.var5InputMin.Size = new System.Drawing.Size(63, 20);
            this.var5InputMin.TabIndex = 340;
            // 
            // var5Channel
            // 
            this.var5Channel.Enabled = false;
            this.var5Channel.Location = new System.Drawing.Point(129, 152);
            this.var5Channel.Name = "var5Channel";
            this.var5Channel.Size = new System.Drawing.Size(63, 20);
            this.var5Channel.TabIndex = 338;
            // 
            // var4OutputMax
            // 
            this.var4OutputMax.Enabled = false;
            this.var4OutputMax.Location = new System.Drawing.Point(441, 127);
            this.var4OutputMax.Name = "var4OutputMax";
            this.var4OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var4OutputMax.TabIndex = 335;
            // 
            // var4OutputMin
            // 
            this.var4OutputMin.Enabled = false;
            this.var4OutputMin.Location = new System.Drawing.Point(364, 127);
            this.var4OutputMin.Name = "var4OutputMin";
            this.var4OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var4OutputMin.TabIndex = 334;
            // 
            // var4InputMax
            // 
            this.var4InputMax.Enabled = false;
            this.var4InputMax.Location = new System.Drawing.Point(286, 127);
            this.var4InputMax.Name = "var4InputMax";
            this.var4InputMax.Size = new System.Drawing.Size(63, 20);
            this.var4InputMax.TabIndex = 333;
            // 
            // var4InputMin
            // 
            this.var4InputMin.Enabled = false;
            this.var4InputMin.Location = new System.Drawing.Point(210, 127);
            this.var4InputMin.Name = "var4InputMin";
            this.var4InputMin.Size = new System.Drawing.Size(63, 20);
            this.var4InputMin.TabIndex = 332;
            // 
            // var4Channel
            // 
            this.var4Channel.Enabled = false;
            this.var4Channel.Location = new System.Drawing.Point(129, 127);
            this.var4Channel.Name = "var4Channel";
            this.var4Channel.Size = new System.Drawing.Size(63, 20);
            this.var4Channel.TabIndex = 330;
            // 
            // var3OutputMax
            // 
            this.var3OutputMax.Enabled = false;
            this.var3OutputMax.Location = new System.Drawing.Point(441, 101);
            this.var3OutputMax.Name = "var3OutputMax";
            this.var3OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var3OutputMax.TabIndex = 329;
            // 
            // var3OutputMin
            // 
            this.var3OutputMin.Enabled = false;
            this.var3OutputMin.Location = new System.Drawing.Point(364, 101);
            this.var3OutputMin.Name = "var3OutputMin";
            this.var3OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var3OutputMin.TabIndex = 328;
            // 
            // var3InputMax
            // 
            this.var3InputMax.Enabled = false;
            this.var3InputMax.Location = new System.Drawing.Point(286, 101);
            this.var3InputMax.Name = "var3InputMax";
            this.var3InputMax.Size = new System.Drawing.Size(63, 20);
            this.var3InputMax.TabIndex = 327;
            // 
            // var3InputMin
            // 
            this.var3InputMin.Enabled = false;
            this.var3InputMin.Location = new System.Drawing.Point(210, 100);
            this.var3InputMin.Name = "var3InputMin";
            this.var3InputMin.Size = new System.Drawing.Size(63, 20);
            this.var3InputMin.TabIndex = 326;
            // 
            // var3Channel
            // 
            this.var3Channel.Enabled = false;
            this.var3Channel.Location = new System.Drawing.Point(129, 101);
            this.var3Channel.Name = "var3Channel";
            this.var3Channel.Size = new System.Drawing.Size(63, 20);
            this.var3Channel.TabIndex = 324;
            // 
            // var2OutputMax
            // 
            this.var2OutputMax.Enabled = false;
            this.var2OutputMax.Location = new System.Drawing.Point(441, 76);
            this.var2OutputMax.Name = "var2OutputMax";
            this.var2OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var2OutputMax.TabIndex = 321;
            // 
            // var2OutputMin
            // 
            this.var2OutputMin.Enabled = false;
            this.var2OutputMin.Location = new System.Drawing.Point(364, 76);
            this.var2OutputMin.Name = "var2OutputMin";
            this.var2OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var2OutputMin.TabIndex = 320;
            // 
            // var2InputMax
            // 
            this.var2InputMax.Enabled = false;
            this.var2InputMax.Location = new System.Drawing.Point(286, 76);
            this.var2InputMax.Name = "var2InputMax";
            this.var2InputMax.Size = new System.Drawing.Size(63, 20);
            this.var2InputMax.TabIndex = 319;
            // 
            // var2InputMin
            // 
            this.var2InputMin.Enabled = false;
            this.var2InputMin.Location = new System.Drawing.Point(210, 75);
            this.var2InputMin.Name = "var2InputMin";
            this.var2InputMin.Size = new System.Drawing.Size(63, 20);
            this.var2InputMin.TabIndex = 318;
            // 
            // var2Channel
            // 
            this.var2Channel.Enabled = false;
            this.var2Channel.Location = new System.Drawing.Point(129, 76);
            this.var2Channel.Name = "var2Channel";
            this.var2Channel.Size = new System.Drawing.Size(63, 20);
            this.var2Channel.TabIndex = 316;
            // 
            // var1OutputMax
            // 
            this.var1OutputMax.Enabled = false;
            this.var1OutputMax.Location = new System.Drawing.Point(441, 49);
            this.var1OutputMax.Name = "var1OutputMax";
            this.var1OutputMax.Size = new System.Drawing.Size(63, 20);
            this.var1OutputMax.TabIndex = 315;
            // 
            // var1OutputMin
            // 
            this.var1OutputMin.Enabled = false;
            this.var1OutputMin.Location = new System.Drawing.Point(364, 49);
            this.var1OutputMin.Name = "var1OutputMin";
            this.var1OutputMin.Size = new System.Drawing.Size(63, 20);
            this.var1OutputMin.TabIndex = 314;
            // 
            // var1InputMax
            // 
            this.var1InputMax.Enabled = false;
            this.var1InputMax.Location = new System.Drawing.Point(286, 49);
            this.var1InputMax.Name = "var1InputMax";
            this.var1InputMax.Size = new System.Drawing.Size(63, 20);
            this.var1InputMax.TabIndex = 313;
            // 
            // var1InputMin
            // 
            this.var1InputMin.Enabled = false;
            this.var1InputMin.Location = new System.Drawing.Point(210, 49);
            this.var1InputMin.Name = "var1InputMin";
            this.var1InputMin.Size = new System.Drawing.Size(63, 20);
            this.var1InputMin.TabIndex = 312;
            // 
            // var1Channel
            // 
            this.var1Channel.Enabled = false;
            this.var1Channel.Location = new System.Drawing.Point(129, 49);
            this.var1Channel.Name = "var1Channel";
            this.var1Channel.Size = new System.Drawing.Size(63, 20);
            this.var1Channel.TabIndex = 310;
            // 
            // var18Module
            // 
            this.var18Module.Enabled = false;
            this.var18Module.FormattingEnabled = true;
            this.var18Module.Location = new System.Drawing.Point(57, 490);
            this.var18Module.Name = "var18Module";
            this.var18Module.Size = new System.Drawing.Size(53, 21);
            this.var18Module.TabIndex = 509;
            this.var18Module.Visible = false;
            // 
            // var17Module
            // 
            this.var17Module.Enabled = false;
            this.var17Module.FormattingEnabled = true;
            this.var17Module.Location = new System.Drawing.Point(57, 463);
            this.var17Module.Name = "var17Module";
            this.var17Module.Size = new System.Drawing.Size(53, 21);
            this.var17Module.TabIndex = 503;
            // 
            // var18
            // 
            this.var18.AutoSize = true;
            this.var18.Location = new System.Drawing.Point(15, 493);
            this.var18.Name = "var18";
            this.var18.Size = new System.Drawing.Size(34, 13);
            this.var18.TabIndex = 501;
            this.var18.Text = "var18";
            this.var18.Visible = false;
            // 
            // var17
            // 
            this.var17.AutoSize = true;
            this.var17.Location = new System.Drawing.Point(15, 468);
            this.var17.Name = "var17";
            this.var17.Size = new System.Drawing.Size(34, 13);
            this.var17.TabIndex = 500;
            this.var17.Text = "var17";
            // 
            // var16Module
            // 
            this.var16Module.Enabled = false;
            this.var16Module.FormattingEnabled = true;
            this.var16Module.Location = new System.Drawing.Point(57, 439);
            this.var16Module.Name = "var16Module";
            this.var16Module.Size = new System.Drawing.Size(53, 21);
            this.var16Module.TabIndex = 495;
            // 
            // var15Module
            // 
            this.var15Module.Enabled = false;
            this.var15Module.FormattingEnabled = true;
            this.var15Module.Location = new System.Drawing.Point(57, 412);
            this.var15Module.Name = "var15Module";
            this.var15Module.Size = new System.Drawing.Size(53, 21);
            this.var15Module.TabIndex = 489;
            // 
            // var16
            // 
            this.var16.AutoSize = true;
            this.var16.Location = new System.Drawing.Point(15, 443);
            this.var16.Name = "var16";
            this.var16.Size = new System.Drawing.Size(34, 13);
            this.var16.TabIndex = 487;
            this.var16.Text = "var16";
            // 
            // var15
            // 
            this.var15.AutoSize = true;
            this.var15.Location = new System.Drawing.Point(15, 415);
            this.var15.Name = "var15";
            this.var15.Size = new System.Drawing.Size(34, 13);
            this.var15.TabIndex = 486;
            this.var15.Text = "var15";
            // 
            // var14Module
            // 
            this.var14Module.Enabled = false;
            this.var14Module.FormattingEnabled = true;
            this.var14Module.Location = new System.Drawing.Point(57, 387);
            this.var14Module.Name = "var14Module";
            this.var14Module.Size = new System.Drawing.Size(53, 21);
            this.var14Module.TabIndex = 481;
            // 
            // var13Module
            // 
            this.var13Module.Enabled = false;
            this.var13Module.FormattingEnabled = true;
            this.var13Module.Location = new System.Drawing.Point(57, 361);
            this.var13Module.Name = "var13Module";
            this.var13Module.Size = new System.Drawing.Size(53, 21);
            this.var13Module.TabIndex = 475;
            // 
            // var14
            // 
            this.var14.AutoSize = true;
            this.var14.Location = new System.Drawing.Point(15, 390);
            this.var14.Name = "var14";
            this.var14.Size = new System.Drawing.Size(34, 13);
            this.var14.TabIndex = 473;
            this.var14.Text = "var14";
            // 
            // var13
            // 
            this.var13.AutoSize = true;
            this.var13.Location = new System.Drawing.Point(15, 365);
            this.var13.Name = "var13";
            this.var13.Size = new System.Drawing.Size(34, 13);
            this.var13.TabIndex = 472;
            this.var13.Text = "var13";
            // 
            // var12Module
            // 
            this.var12Module.Enabled = false;
            this.var12Module.FormattingEnabled = true;
            this.var12Module.Location = new System.Drawing.Point(57, 334);
            this.var12Module.Name = "var12Module";
            this.var12Module.Size = new System.Drawing.Size(53, 21);
            this.var12Module.TabIndex = 431;
            // 
            // var11Module
            // 
            this.var11Module.Enabled = false;
            this.var11Module.FormattingEnabled = true;
            this.var11Module.Location = new System.Drawing.Point(57, 307);
            this.var11Module.Name = "var11Module";
            this.var11Module.Size = new System.Drawing.Size(53, 21);
            this.var11Module.TabIndex = 425;
            // 
            // var12
            // 
            this.var12.AutoSize = true;
            this.var12.Location = new System.Drawing.Point(15, 337);
            this.var12.Name = "var12";
            this.var12.Size = new System.Drawing.Size(34, 13);
            this.var12.TabIndex = 423;
            this.var12.Text = "var12";
            // 
            // var11
            // 
            this.var11.AutoSize = true;
            this.var11.Location = new System.Drawing.Point(15, 312);
            this.var11.Name = "var11";
            this.var11.Size = new System.Drawing.Size(34, 13);
            this.var11.TabIndex = 422;
            this.var11.Text = "var11";
            // 
            // var10Module
            // 
            this.var10Module.Enabled = false;
            this.var10Module.FormattingEnabled = true;
            this.var10Module.Location = new System.Drawing.Point(57, 283);
            this.var10Module.Name = "var10Module";
            this.var10Module.Size = new System.Drawing.Size(53, 21);
            this.var10Module.TabIndex = 417;
            // 
            // var9Module
            // 
            this.var9Module.Enabled = false;
            this.var9Module.FormattingEnabled = true;
            this.var9Module.Location = new System.Drawing.Point(57, 256);
            this.var9Module.Name = "var9Module";
            this.var9Module.Size = new System.Drawing.Size(53, 21);
            this.var9Module.TabIndex = 411;
            // 
            // var10
            // 
            this.var10.AutoSize = true;
            this.var10.Location = new System.Drawing.Point(15, 287);
            this.var10.Name = "var10";
            this.var10.Size = new System.Drawing.Size(34, 13);
            this.var10.TabIndex = 409;
            this.var10.Text = "var10";
            // 
            // var9
            // 
            this.var9.AutoSize = true;
            this.var9.Location = new System.Drawing.Point(15, 259);
            this.var9.Name = "var9";
            this.var9.Size = new System.Drawing.Size(28, 13);
            this.var9.TabIndex = 408;
            this.var9.Text = "var9";
            // 
            // var8Module
            // 
            this.var8Module.Enabled = false;
            this.var8Module.FormattingEnabled = true;
            this.var8Module.Location = new System.Drawing.Point(57, 231);
            this.var8Module.Name = "var8Module";
            this.var8Module.Size = new System.Drawing.Size(53, 21);
            this.var8Module.TabIndex = 403;
            // 
            // var7Module
            // 
            this.var7Module.Enabled = false;
            this.var7Module.FormattingEnabled = true;
            this.var7Module.Location = new System.Drawing.Point(57, 205);
            this.var7Module.Name = "var7Module";
            this.var7Module.Size = new System.Drawing.Size(53, 21);
            this.var7Module.TabIndex = 397;
            // 
            // var8
            // 
            this.var8.AutoSize = true;
            this.var8.Location = new System.Drawing.Point(15, 234);
            this.var8.Name = "var8";
            this.var8.Size = new System.Drawing.Size(28, 13);
            this.var8.TabIndex = 395;
            this.var8.Text = "var8";
            // 
            // var7
            // 
            this.var7.AutoSize = true;
            this.var7.Location = new System.Drawing.Point(15, 209);
            this.var7.Name = "var7";
            this.var7.Size = new System.Drawing.Size(28, 13);
            this.var7.TabIndex = 394;
            this.var7.Text = "var7";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(1065, 17);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(72, 13);
            this.label55.TabIndex = 387;
            this.label55.Text = "Display Name";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(987, 17);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 380;
            this.label54.Text = "Type";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(903, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(75, 13);
            this.label52.TabIndex = 373;
            this.label52.Text = "Status Column";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(823, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(72, 13);
            this.label53.TabIndex = 360;
            this.label53.Text = "Value Column";
            // 
            // Unit
            // 
            this.Unit.AutoSize = true;
            this.Unit.Location = new System.Drawing.Point(751, 16);
            this.Unit.Name = "Unit";
            this.Unit.Size = new System.Drawing.Size(26, 13);
            this.Unit.TabIndex = 359;
            this.Unit.Text = "Unit";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(518, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 13);
            this.label28.TabIndex = 351;
            this.label28.Text = "Offset";
            // 
            // var6Module
            // 
            this.var6Module.Enabled = false;
            this.var6Module.FormattingEnabled = true;
            this.var6Module.Location = new System.Drawing.Point(57, 178);
            this.var6Module.Name = "var6Module";
            this.var6Module.Size = new System.Drawing.Size(53, 21);
            this.var6Module.TabIndex = 345;
            // 
            // var5Module
            // 
            this.var5Module.Enabled = false;
            this.var5Module.FormattingEnabled = true;
            this.var5Module.Location = new System.Drawing.Point(57, 151);
            this.var5Module.Name = "var5Module";
            this.var5Module.Size = new System.Drawing.Size(53, 21);
            this.var5Module.TabIndex = 339;
            // 
            // var6
            // 
            this.var6.AutoSize = true;
            this.var6.Location = new System.Drawing.Point(15, 181);
            this.var6.Name = "var6";
            this.var6.Size = new System.Drawing.Size(28, 13);
            this.var6.TabIndex = 337;
            this.var6.Text = "var6";
            // 
            // var5
            // 
            this.var5.AutoSize = true;
            this.var5.Location = new System.Drawing.Point(15, 156);
            this.var5.Name = "var5";
            this.var5.Size = new System.Drawing.Size(28, 13);
            this.var5.TabIndex = 336;
            this.var5.Text = "var5";
            // 
            // var4Module
            // 
            this.var4Module.Enabled = false;
            this.var4Module.FormattingEnabled = true;
            this.var4Module.Location = new System.Drawing.Point(57, 127);
            this.var4Module.Name = "var4Module";
            this.var4Module.Size = new System.Drawing.Size(53, 21);
            this.var4Module.TabIndex = 331;
            // 
            // var3Module
            // 
            this.var3Module.Enabled = false;
            this.var3Module.FormattingEnabled = true;
            this.var3Module.Location = new System.Drawing.Point(57, 100);
            this.var3Module.Name = "var3Module";
            this.var3Module.Size = new System.Drawing.Size(53, 21);
            this.var3Module.TabIndex = 325;
            // 
            // var4
            // 
            this.var4.AutoSize = true;
            this.var4.Location = new System.Drawing.Point(15, 131);
            this.var4.Name = "var4";
            this.var4.Size = new System.Drawing.Size(28, 13);
            this.var4.TabIndex = 323;
            this.var4.Text = "var4";
            // 
            // var3
            // 
            this.var3.AutoSize = true;
            this.var3.Location = new System.Drawing.Point(15, 103);
            this.var3.Name = "var3";
            this.var3.Size = new System.Drawing.Size(28, 13);
            this.var3.TabIndex = 322;
            this.var3.Text = "var3";
            // 
            // var2Module
            // 
            this.var2Module.Enabled = false;
            this.var2Module.FormattingEnabled = true;
            this.var2Module.Location = new System.Drawing.Point(57, 75);
            this.var2Module.Name = "var2Module";
            this.var2Module.Size = new System.Drawing.Size(53, 21);
            this.var2Module.TabIndex = 317;
            // 
            // var1Module
            // 
            this.var1Module.Enabled = false;
            this.var1Module.FormattingEnabled = true;
            this.var1Module.Location = new System.Drawing.Point(57, 49);
            this.var1Module.Name = "var1Module";
            this.var1Module.Size = new System.Drawing.Size(53, 21);
            this.var1Module.TabIndex = 311;
            // 
            // var2
            // 
            this.var2.AutoSize = true;
            this.var2.Location = new System.Drawing.Point(15, 78);
            this.var2.Name = "var2";
            this.var2.Size = new System.Drawing.Size(28, 13);
            this.var2.TabIndex = 309;
            this.var2.Text = "var2";
            // 
            // var1
            // 
            this.var1.AutoSize = true;
            this.var1.Location = new System.Drawing.Point(15, 53);
            this.var1.Name = "var1";
            this.var1.Size = new System.Drawing.Size(28, 13);
            this.var1.TabIndex = 308;
            this.var1.Text = "var1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(438, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 301;
            this.label8.Text = "Output max";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(366, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 300;
            this.label9.Text = "Output min";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(284, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 297;
            this.label7.Text = "Input max";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(212, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 296;
            this.label6.Text = "Input min";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(125, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 295;
            this.label5.Text = "Channel No.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 294;
            this.label4.Text = "Module ID";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnShow);
            this.tabPage1.Controls.Add(this.btnSOCKET);
            this.tabPage1.Controls.Add(this.txtSocketPort);
            this.tabPage1.Controls.Add(this.txtStationID);
            this.tabPage1.Controls.Add(this.txtStationName);
            this.tabPage1.Controls.Add(this.lblSocketPort);
            this.tabPage1.Controls.Add(this.lblStationID);
            this.tabPage1.Controls.Add(this.lblStationName);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(1147, 604);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Station";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(363, 113);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 20;
            this.btnShow.Text = "Hide";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Visible = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnSOCKET
            // 
            this.btnSOCKET.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSOCKET.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSOCKET.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSOCKET.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSOCKET.Image = global::DataLogger.Properties.Resources.ON_switch_96x25;
            this.btnSOCKET.Location = new System.Drawing.Point(245, 102);
            this.btnSOCKET.Name = "btnSOCKET";
            this.btnSOCKET.Size = new System.Drawing.Size(81, 40);
            this.btnSOCKET.TabIndex = 19;
            this.btnSOCKET.UseVisualStyleBackColor = true;
            this.btnSOCKET.Click += new System.EventHandler(this.btnSOCKET_Click);
            // 
            // txtSocketPort
            // 
            this.txtSocketPort.Location = new System.Drawing.Point(88, 113);
            this.txtSocketPort.Name = "txtSocketPort";
            this.txtSocketPort.Size = new System.Drawing.Size(118, 20);
            this.txtSocketPort.TabIndex = 18;
            // 
            // txtStationID
            // 
            this.txtStationID.Location = new System.Drawing.Point(88, 63);
            this.txtStationID.Name = "txtStationID";
            this.txtStationID.Size = new System.Drawing.Size(238, 20);
            this.txtStationID.TabIndex = 17;
            // 
            // txtStationName
            // 
            this.txtStationName.Location = new System.Drawing.Point(88, 21);
            this.txtStationName.Name = "txtStationName";
            this.txtStationName.Size = new System.Drawing.Size(118, 20);
            this.txtStationName.TabIndex = 16;
            // 
            // lblSocketPort
            // 
            this.lblSocketPort.AutoSize = true;
            this.lblSocketPort.Location = new System.Drawing.Point(13, 116);
            this.lblSocketPort.Name = "lblSocketPort";
            this.lblSocketPort.Size = new System.Drawing.Size(62, 13);
            this.lblSocketPort.TabIndex = 15;
            this.lblSocketPort.Text = "Socket port";
            // 
            // lblStationID
            // 
            this.lblStationID.AutoSize = true;
            this.lblStationID.Location = new System.Drawing.Point(13, 66);
            this.lblStationID.Name = "lblStationID";
            this.lblStationID.Size = new System.Drawing.Size(54, 13);
            this.lblStationID.TabIndex = 14;
            this.lblStationID.Text = "Station ID";
            // 
            // lblStationName
            // 
            this.lblStationName.AutoSize = true;
            this.lblStationName.Location = new System.Drawing.Point(13, 24);
            this.lblStationName.Name = "lblStationName";
            this.lblStationName.Size = new System.Drawing.Size(69, 13);
            this.lblStationName.TabIndex = 13;
            this.lblStationName.Text = "Station name";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.MPS2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(9, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1155, 630);
            this.tabControl1.TabIndex = 88;
            // 
            // frmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1179, 717);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.frmConfiguration_Load);
            this.MPS2.ResumeLayout(false);
            this.MPS2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.Button btnRefresh;
        public System.Windows.Forms.TabPage MPS2;
        public System.Windows.Forms.TabPage tabPage4;
        public System.Windows.Forms.TabPage tabPage5;
        public System.Windows.Forms.TextBox var18DisplayName;
        public System.Windows.Forms.TextBox var17DisplayName;
        public System.Windows.Forms.TextBox var16DisplayName;
        public System.Windows.Forms.TextBox var15DisplayName;
        public System.Windows.Forms.TextBox var14DisplayName;
        public System.Windows.Forms.TextBox var13DisplayName;
        public System.Windows.Forms.TextBox var18Type;
        public System.Windows.Forms.TextBox var17Type;
        public System.Windows.Forms.TextBox var16Type;
        public System.Windows.Forms.TextBox var15Type;
        public System.Windows.Forms.TextBox var14Type;
        public System.Windows.Forms.TextBox var13Type;
        public System.Windows.Forms.TextBox var18StatusColumn;
        public System.Windows.Forms.TextBox var17StatusColumn;
        public System.Windows.Forms.TextBox var16StatusColumn;
        public System.Windows.Forms.TextBox var15StatusColumn;
        public System.Windows.Forms.TextBox var14StatusColumn;
        public System.Windows.Forms.TextBox var13StatusColumn;
        public System.Windows.Forms.TextBox var18ValueColumn;
        public System.Windows.Forms.TextBox var18Unit;
        public System.Windows.Forms.TextBox var17ValueColumn;
        public System.Windows.Forms.TextBox var17Unit;
        public System.Windows.Forms.TextBox var16ValueColumn;
        public System.Windows.Forms.TextBox var16Unit;
        public System.Windows.Forms.TextBox var15ValueColumn;
        public System.Windows.Forms.TextBox var15Unit;
        public System.Windows.Forms.TextBox var14ValueColumn;
        public System.Windows.Forms.TextBox var14Unit;
        public System.Windows.Forms.TextBox var13ValueColumn;
        public System.Windows.Forms.TextBox var13Unit;
        public System.Windows.Forms.TextBox var18Offset;
        public System.Windows.Forms.TextBox var17Offset;
        public System.Windows.Forms.TextBox var16Offset;
        public System.Windows.Forms.TextBox var15Offset;
        public System.Windows.Forms.TextBox var14Offset;
        public System.Windows.Forms.TextBox var13Offset;
        public System.Windows.Forms.TextBox var18OutputMax;
        public System.Windows.Forms.TextBox var18OutputMin;
        public System.Windows.Forms.TextBox var18InputMax;
        public System.Windows.Forms.TextBox var18InputMin;
        public System.Windows.Forms.TextBox var18Channel;
        public System.Windows.Forms.TextBox var17OutputMax;
        public System.Windows.Forms.TextBox var17OutputMin;
        public System.Windows.Forms.TextBox var17InputMax;
        public System.Windows.Forms.TextBox var17InputMin;
        public System.Windows.Forms.TextBox var17Channel;
        public System.Windows.Forms.TextBox var16OutputMax;
        public System.Windows.Forms.TextBox var16OutputMin;
        public System.Windows.Forms.TextBox var16InputMax;
        public System.Windows.Forms.TextBox var16InputMin;
        public System.Windows.Forms.TextBox var16Channel;
        public System.Windows.Forms.TextBox var15OutputMax;
        public System.Windows.Forms.TextBox var15OutputMin;
        public System.Windows.Forms.TextBox var15InputMax;
        public System.Windows.Forms.TextBox var15InputMin;
        public System.Windows.Forms.TextBox var15Channel;
        public System.Windows.Forms.TextBox var14OutputMax;
        public System.Windows.Forms.TextBox var14OutputMin;
        public System.Windows.Forms.TextBox var14InputMax;
        public System.Windows.Forms.TextBox var14InputMin;
        public System.Windows.Forms.TextBox var14Channel;
        public System.Windows.Forms.TextBox var13OutputMax;
        public System.Windows.Forms.TextBox var13OutputMin;
        public System.Windows.Forms.TextBox var13InputMax;
        public System.Windows.Forms.TextBox var13InputMin;
        public System.Windows.Forms.TextBox var13Channel;
        public System.Windows.Forms.TextBox var12DisplayName;
        public System.Windows.Forms.TextBox var11DisplayName;
        public System.Windows.Forms.TextBox var10DisplayName;
        public System.Windows.Forms.TextBox var9DisplayName;
        public System.Windows.Forms.TextBox var8DisplayName;
        public System.Windows.Forms.TextBox var7DisplayName;
        public System.Windows.Forms.TextBox var12Type;
        public System.Windows.Forms.TextBox var11Type;
        public System.Windows.Forms.TextBox var10Type;
        public System.Windows.Forms.TextBox var9Type;
        public System.Windows.Forms.TextBox var8Type;
        public System.Windows.Forms.TextBox var7Type;
        public System.Windows.Forms.TextBox var12StatusColumn;
        public System.Windows.Forms.TextBox var11StatusColumn;
        public System.Windows.Forms.TextBox var10StatusColumn;
        public System.Windows.Forms.TextBox var9StatusColumn;
        public System.Windows.Forms.TextBox var8StatusColumn;
        public System.Windows.Forms.TextBox var7StatusColumn;
        public System.Windows.Forms.TextBox var12ValueColumn;
        public System.Windows.Forms.TextBox var12Unit;
        public System.Windows.Forms.TextBox var11ValueColumn;
        public System.Windows.Forms.TextBox var11Unit;
        public System.Windows.Forms.TextBox var10ValueColumn;
        public System.Windows.Forms.TextBox var10Unit;
        public System.Windows.Forms.TextBox var9ValueColumn;
        public System.Windows.Forms.TextBox var9Unit;
        public System.Windows.Forms.TextBox var8ValueColumn;
        public System.Windows.Forms.TextBox var8Unit;
        public System.Windows.Forms.TextBox var7ValueColumn;
        public System.Windows.Forms.TextBox var7Unit;
        public System.Windows.Forms.TextBox var12Offset;
        public System.Windows.Forms.TextBox var11Offset;
        public System.Windows.Forms.TextBox var10Offset;
        public System.Windows.Forms.TextBox var9Offset;
        public System.Windows.Forms.TextBox var8Offset;
        public System.Windows.Forms.TextBox var7Offset;
        public System.Windows.Forms.TextBox var12OutputMax;
        public System.Windows.Forms.TextBox var12OutputMin;
        public System.Windows.Forms.TextBox var12InputMax;
        public System.Windows.Forms.TextBox var12InputMin;
        public System.Windows.Forms.TextBox var12Channel;
        public System.Windows.Forms.TextBox var11OutputMax;
        public System.Windows.Forms.TextBox var11OutputMin;
        public System.Windows.Forms.TextBox var11InputMax;
        public System.Windows.Forms.TextBox var11InputMin;
        public System.Windows.Forms.TextBox var11Channel;
        public System.Windows.Forms.TextBox var10OutputMax;
        public System.Windows.Forms.TextBox var10OutputMin;
        public System.Windows.Forms.TextBox var10InputMax;
        public System.Windows.Forms.TextBox var10InputMin;
        public System.Windows.Forms.TextBox var10Channel;
        public System.Windows.Forms.TextBox var9OutputMax;
        public System.Windows.Forms.TextBox var9OutputMin;
        public System.Windows.Forms.TextBox var9InputMax;
        public System.Windows.Forms.TextBox var9InputMin;
        public System.Windows.Forms.TextBox var9Channel;
        public System.Windows.Forms.TextBox var8OutputMax;
        public System.Windows.Forms.TextBox var8OutputMin;
        public System.Windows.Forms.TextBox var8InputMax;
        public System.Windows.Forms.TextBox var8InputMin;
        public System.Windows.Forms.TextBox var8Channel;
        public System.Windows.Forms.TextBox var7OutputMax;
        public System.Windows.Forms.TextBox var7OutputMin;
        public System.Windows.Forms.TextBox var7InputMax;
        public System.Windows.Forms.TextBox var7InputMin;
        public System.Windows.Forms.TextBox var7Channel;
        public System.Windows.Forms.TextBox var6DisplayName;
        public System.Windows.Forms.TextBox var5DisplayName;
        public System.Windows.Forms.TextBox var4DisplayName;
        public System.Windows.Forms.TextBox var3DisplayName;
        public System.Windows.Forms.TextBox var2DisplayName;
        public System.Windows.Forms.TextBox var1DisplayName;
        public System.Windows.Forms.TextBox var6Type;
        public System.Windows.Forms.TextBox var5Type;
        public System.Windows.Forms.TextBox var4Type;
        public System.Windows.Forms.TextBox var3Type;
        public System.Windows.Forms.TextBox var2Type;
        public System.Windows.Forms.TextBox var1Type;
        public System.Windows.Forms.TextBox var6StatusColumn;
        public System.Windows.Forms.TextBox var5StatusColumn;
        public System.Windows.Forms.TextBox var4StatusColumn;
        public System.Windows.Forms.TextBox var3StatusColumn;
        public System.Windows.Forms.TextBox var2StatusColumn;
        public System.Windows.Forms.TextBox var1StatusColumn;
        public System.Windows.Forms.TextBox var6ValueColumn;
        public System.Windows.Forms.TextBox var6Unit;
        public System.Windows.Forms.TextBox var5ValueColumn;
        public System.Windows.Forms.TextBox var5Unit;
        public System.Windows.Forms.TextBox var4ValueColumn;
        public System.Windows.Forms.TextBox var4Unit;
        public System.Windows.Forms.TextBox var3ValueColumn;
        public System.Windows.Forms.TextBox var3Unit;
        public System.Windows.Forms.TextBox var2ValueColumn;
        public System.Windows.Forms.TextBox var2Unit;
        public System.Windows.Forms.TextBox var1ValueColumn;
        public System.Windows.Forms.TextBox var1Unit;
        public System.Windows.Forms.TextBox var6Offset;
        public System.Windows.Forms.TextBox var5Offset;
        public System.Windows.Forms.TextBox var4Offset;
        public System.Windows.Forms.TextBox var3Offset;
        public System.Windows.Forms.TextBox var2Offset;
        public System.Windows.Forms.TextBox var1Offset;
        public System.Windows.Forms.TextBox var6OutputMax;
        public System.Windows.Forms.TextBox var6OutputMin;
        public System.Windows.Forms.TextBox var6InputMax;
        public System.Windows.Forms.TextBox var6InputMin;
        public System.Windows.Forms.TextBox var6Channel;
        public System.Windows.Forms.TextBox var5OutputMax;
        public System.Windows.Forms.TextBox var5OutputMin;
        public System.Windows.Forms.TextBox var5InputMax;
        public System.Windows.Forms.TextBox var5InputMin;
        public System.Windows.Forms.TextBox var5Channel;
        public System.Windows.Forms.TextBox var4OutputMax;
        public System.Windows.Forms.TextBox var4OutputMin;
        public System.Windows.Forms.TextBox var4InputMax;
        public System.Windows.Forms.TextBox var4InputMin;
        public System.Windows.Forms.TextBox var4Channel;
        public System.Windows.Forms.TextBox var3OutputMax;
        public System.Windows.Forms.TextBox var3OutputMin;
        public System.Windows.Forms.TextBox var3InputMax;
        public System.Windows.Forms.TextBox var3InputMin;
        public System.Windows.Forms.TextBox var3Channel;
        public System.Windows.Forms.TextBox var2OutputMax;
        public System.Windows.Forms.TextBox var2OutputMin;
        public System.Windows.Forms.TextBox var2InputMax;
        public System.Windows.Forms.TextBox var2InputMin;
        public System.Windows.Forms.TextBox var2Channel;
        public System.Windows.Forms.TextBox var1OutputMax;
        public System.Windows.Forms.TextBox var1OutputMin;
        public System.Windows.Forms.TextBox var1InputMax;
        public System.Windows.Forms.TextBox var1InputMin;
        public System.Windows.Forms.TextBox var1Channel;
        public System.Windows.Forms.ComboBox var18Module;
        public System.Windows.Forms.ComboBox var17Module;
        public System.Windows.Forms.Label var18;
        public System.Windows.Forms.Label var17;
        public System.Windows.Forms.ComboBox var16Module;
        public System.Windows.Forms.ComboBox var15Module;
        public System.Windows.Forms.Label var16;
        public System.Windows.Forms.Label var15;
        public System.Windows.Forms.ComboBox var14Module;
        public System.Windows.Forms.ComboBox var13Module;
        public System.Windows.Forms.Label var14;
        public System.Windows.Forms.Label var13;
        public System.Windows.Forms.ComboBox var12Module;
        public System.Windows.Forms.ComboBox var11Module;
        public System.Windows.Forms.Label var12;
        public System.Windows.Forms.Label var11;
        public System.Windows.Forms.ComboBox var10Module;
        public System.Windows.Forms.ComboBox var9Module;
        public System.Windows.Forms.Label var10;
        public System.Windows.Forms.Label var9;
        public System.Windows.Forms.ComboBox var8Module;
        public System.Windows.Forms.ComboBox var7Module;
        public System.Windows.Forms.Label var8;
        public System.Windows.Forms.Label var7;
        public System.Windows.Forms.Label label55;
        public System.Windows.Forms.Label label54;
        public System.Windows.Forms.Label label52;
        public System.Windows.Forms.Label label53;
        public System.Windows.Forms.Label Unit;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.ComboBox var6Module;
        public System.Windows.Forms.ComboBox var5Module;
        public System.Windows.Forms.Label var6;
        public System.Windows.Forms.Label var5;
        public System.Windows.Forms.ComboBox var4Module;
        public System.Windows.Forms.ComboBox var3Module;
        public System.Windows.Forms.Label var4;
        public System.Windows.Forms.Label var3;
        public System.Windows.Forms.ComboBox var2Module;
        public System.Windows.Forms.ComboBox var1Module;
        public System.Windows.Forms.Label var2;
        public System.Windows.Forms.Label var1;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.Button btnShow;
        public System.Windows.Forms.Button btnSOCKET;
        public System.Windows.Forms.TextBox txtSocketPort;
        public System.Windows.Forms.TextBox txtStationID;
        public System.Windows.Forms.TextBox txtStationName;
        public System.Windows.Forms.Label lblSocketPort;
        public System.Windows.Forms.Label lblStationID;
        public System.Windows.Forms.Label lblStationName;
        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TextBox var35DisplayName;
        public System.Windows.Forms.TextBox var34DisplayName;
        public System.Windows.Forms.TextBox var33DisplayName;
        public System.Windows.Forms.TextBox var32DisplayName;
        public System.Windows.Forms.TextBox var31DisplayName;
        public System.Windows.Forms.TextBox var35Type;
        public System.Windows.Forms.TextBox var34Type;
        public System.Windows.Forms.TextBox var33Type;
        public System.Windows.Forms.TextBox var32Type;
        public System.Windows.Forms.TextBox var31Type;
        public System.Windows.Forms.TextBox var35StatusColumn;
        public System.Windows.Forms.TextBox var34StatusColumn;
        public System.Windows.Forms.TextBox var33StatusColumn;
        public System.Windows.Forms.TextBox var32StatusColumn;
        public System.Windows.Forms.TextBox var31StatusColumn;
        public System.Windows.Forms.TextBox var35ValueColumn;
        public System.Windows.Forms.TextBox var35Unit;
        public System.Windows.Forms.TextBox var34ValueColumn;
        public System.Windows.Forms.TextBox var34Unit;
        public System.Windows.Forms.TextBox var33ValueColumn;
        public System.Windows.Forms.TextBox var33Unit;
        public System.Windows.Forms.TextBox var32ValueColumn;
        public System.Windows.Forms.TextBox var32Unit;
        public System.Windows.Forms.TextBox var31ValueColumn;
        public System.Windows.Forms.TextBox var31Unit;
        public System.Windows.Forms.TextBox var35Offset;
        public System.Windows.Forms.TextBox var34Offset;
        public System.Windows.Forms.TextBox var33Offset;
        public System.Windows.Forms.TextBox var32Offset;
        public System.Windows.Forms.TextBox var31Offset;
        public System.Windows.Forms.TextBox var35OutputMax;
        public System.Windows.Forms.TextBox var35OutputMin;
        public System.Windows.Forms.TextBox var35InputMax;
        public System.Windows.Forms.TextBox var35InputMin;
        public System.Windows.Forms.TextBox var35Channel;
        public System.Windows.Forms.TextBox var34OutputMax;
        public System.Windows.Forms.TextBox var34OutputMin;
        public System.Windows.Forms.TextBox var34InputMax;
        public System.Windows.Forms.TextBox var34InputMin;
        public System.Windows.Forms.TextBox var34Channel;
        public System.Windows.Forms.TextBox var33OutputMax;
        public System.Windows.Forms.TextBox var33OutputMin;
        public System.Windows.Forms.TextBox var33InputMax;
        public System.Windows.Forms.TextBox var33InputMin;
        public System.Windows.Forms.TextBox var33Channel;
        public System.Windows.Forms.TextBox var32OutputMax;
        public System.Windows.Forms.TextBox var32OutputMin;
        public System.Windows.Forms.TextBox var32InputMax;
        public System.Windows.Forms.TextBox var32InputMin;
        public System.Windows.Forms.TextBox var32Channel;
        public System.Windows.Forms.TextBox var31OutputMax;
        public System.Windows.Forms.TextBox var31OutputMin;
        public System.Windows.Forms.TextBox var31InputMax;
        public System.Windows.Forms.TextBox var31InputMin;
        public System.Windows.Forms.TextBox var31Channel;
        public System.Windows.Forms.TextBox var30DisplayName;
        public System.Windows.Forms.TextBox var29DisplayName;
        public System.Windows.Forms.TextBox var28DisplayName;
        public System.Windows.Forms.TextBox var27DisplayName;
        public System.Windows.Forms.TextBox var26DisplayName;
        public System.Windows.Forms.TextBox var25DisplayName;
        public System.Windows.Forms.TextBox var30Type;
        public System.Windows.Forms.TextBox var29Type;
        public System.Windows.Forms.TextBox var28Type;
        public System.Windows.Forms.TextBox var27Type;
        public System.Windows.Forms.TextBox var26Type;
        public System.Windows.Forms.TextBox var25Type;
        public System.Windows.Forms.TextBox var30StatusColumn;
        public System.Windows.Forms.TextBox var29StatusColumn;
        public System.Windows.Forms.TextBox var28StatusColumn;
        public System.Windows.Forms.TextBox var27StatusColumn;
        public System.Windows.Forms.TextBox var26StatusColumn;
        public System.Windows.Forms.TextBox var25StatusColumn;
        public System.Windows.Forms.TextBox var30ValueColumn;
        public System.Windows.Forms.TextBox var30Unit;
        public System.Windows.Forms.TextBox var29ValueColumn;
        public System.Windows.Forms.TextBox var29Unit;
        public System.Windows.Forms.TextBox var28ValueColumn;
        public System.Windows.Forms.TextBox var28Unit;
        public System.Windows.Forms.TextBox var27ValueColumn;
        public System.Windows.Forms.TextBox var27Unit;
        public System.Windows.Forms.TextBox var26ValueColumn;
        public System.Windows.Forms.TextBox var26Unit;
        public System.Windows.Forms.TextBox var25ValueColumn;
        public System.Windows.Forms.TextBox var25Unit;
        public System.Windows.Forms.TextBox var30Offset;
        public System.Windows.Forms.TextBox var29Offset;
        public System.Windows.Forms.TextBox var28Offset;
        public System.Windows.Forms.TextBox var27Offset;
        public System.Windows.Forms.TextBox var26Offset;
        public System.Windows.Forms.TextBox var25Offset;
        public System.Windows.Forms.TextBox var30OutputMax;
        public System.Windows.Forms.TextBox var30OutputMin;
        public System.Windows.Forms.TextBox var30InputMax;
        public System.Windows.Forms.TextBox var30InputMin;
        public System.Windows.Forms.TextBox var30Channel;
        public System.Windows.Forms.TextBox var29OutputMax;
        public System.Windows.Forms.TextBox var29OutputMin;
        public System.Windows.Forms.TextBox var29InputMax;
        public System.Windows.Forms.TextBox var29InputMin;
        public System.Windows.Forms.TextBox var29Channel;
        public System.Windows.Forms.TextBox var28OutputMax;
        public System.Windows.Forms.TextBox var28OutputMin;
        public System.Windows.Forms.TextBox var28InputMax;
        public System.Windows.Forms.TextBox var28InputMin;
        public System.Windows.Forms.TextBox var28Channel;
        public System.Windows.Forms.TextBox var27OutputMax;
        public System.Windows.Forms.TextBox var27OutputMin;
        public System.Windows.Forms.TextBox var27InputMax;
        public System.Windows.Forms.TextBox var27InputMin;
        public System.Windows.Forms.TextBox var27Channel;
        public System.Windows.Forms.TextBox var26OutputMax;
        public System.Windows.Forms.TextBox var26OutputMin;
        public System.Windows.Forms.TextBox var26InputMax;
        public System.Windows.Forms.TextBox var26InputMin;
        public System.Windows.Forms.TextBox var26Channel;
        public System.Windows.Forms.TextBox var25OutputMax;
        public System.Windows.Forms.TextBox var25OutputMin;
        public System.Windows.Forms.TextBox var25InputMax;
        public System.Windows.Forms.TextBox var25InputMin;
        public System.Windows.Forms.TextBox var25Channel;
        public System.Windows.Forms.TextBox var24DisplayName;
        public System.Windows.Forms.TextBox var23DisplayName;
        public System.Windows.Forms.TextBox var22DisplayName;
        public System.Windows.Forms.TextBox var21DisplayName;
        public System.Windows.Forms.TextBox var20DisplayName;
        public System.Windows.Forms.TextBox var19DisplayName;
        public System.Windows.Forms.TextBox var24Type;
        public System.Windows.Forms.TextBox var23Type;
        public System.Windows.Forms.TextBox var22Type;
        public System.Windows.Forms.TextBox var21Type;
        public System.Windows.Forms.TextBox var20Type;
        public System.Windows.Forms.TextBox var19Type;
        public System.Windows.Forms.TextBox var24StatusColumn;
        public System.Windows.Forms.TextBox var23StatusColumn;
        public System.Windows.Forms.TextBox var22StatusColumn;
        public System.Windows.Forms.TextBox var21StatusColumn;
        public System.Windows.Forms.TextBox var20StatusColumn;
        public System.Windows.Forms.TextBox var19StatusColumn;
        public System.Windows.Forms.TextBox var24ValueColumn;
        public System.Windows.Forms.TextBox var24Unit;
        public System.Windows.Forms.TextBox var23ValueColumn;
        public System.Windows.Forms.TextBox var23Unit;
        public System.Windows.Forms.TextBox var22ValueColumn;
        public System.Windows.Forms.TextBox var22Unit;
        public System.Windows.Forms.TextBox var21ValueColumn;
        public System.Windows.Forms.TextBox var21Unit;
        public System.Windows.Forms.TextBox var20ValueColumn;
        public System.Windows.Forms.TextBox var20Unit;
        public System.Windows.Forms.TextBox var19ValueColumn;
        public System.Windows.Forms.TextBox var19Unit;
        public System.Windows.Forms.TextBox var24Offset;
        public System.Windows.Forms.TextBox var23Offset;
        public System.Windows.Forms.TextBox var22Offset;
        public System.Windows.Forms.TextBox var21Offset;
        public System.Windows.Forms.TextBox var20Offset;
        public System.Windows.Forms.TextBox var19Offset;
        public System.Windows.Forms.TextBox var24OutputMax;
        public System.Windows.Forms.TextBox var24OutputMin;
        public System.Windows.Forms.TextBox var24InputMax;
        public System.Windows.Forms.TextBox var24InputMin;
        public System.Windows.Forms.TextBox var24Channel;
        public System.Windows.Forms.TextBox var23OutputMax;
        public System.Windows.Forms.TextBox var23OutputMin;
        public System.Windows.Forms.TextBox var23InputMax;
        public System.Windows.Forms.TextBox var23InputMin;
        public System.Windows.Forms.TextBox var23Channel;
        public System.Windows.Forms.TextBox var22OutputMax;
        public System.Windows.Forms.TextBox var22OutputMin;
        public System.Windows.Forms.TextBox var22InputMax;
        public System.Windows.Forms.TextBox var22InputMin;
        public System.Windows.Forms.TextBox var22Channel;
        public System.Windows.Forms.TextBox var21OutputMax;
        public System.Windows.Forms.TextBox var21OutputMin;
        public System.Windows.Forms.TextBox var21InputMax;
        public System.Windows.Forms.TextBox var21InputMin;
        public System.Windows.Forms.TextBox var21Channel;
        public System.Windows.Forms.TextBox var20OutputMax;
        public System.Windows.Forms.TextBox var20OutputMin;
        public System.Windows.Forms.TextBox var20InputMax;
        public System.Windows.Forms.TextBox var20InputMin;
        public System.Windows.Forms.TextBox var20Channel;
        public System.Windows.Forms.TextBox var19OutputMax;
        public System.Windows.Forms.TextBox var19OutputMin;
        public System.Windows.Forms.TextBox var19InputMax;
        public System.Windows.Forms.TextBox var19InputMin;
        public System.Windows.Forms.TextBox var19Channel;
        public System.Windows.Forms.ComboBox var35Module;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox var34Module;
        public System.Windows.Forms.ComboBox var33Module;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox var32Module;
        public System.Windows.Forms.ComboBox var31Module;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.ComboBox var30Module;
        public System.Windows.Forms.ComboBox var29Module;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.ComboBox var28Module;
        public System.Windows.Forms.ComboBox var27Module;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.ComboBox var26Module;
        public System.Windows.Forms.ComboBox var25Module;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.ComboBox var24Module;
        public System.Windows.Forms.ComboBox var23Module;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.ComboBox var22Module;
        public System.Windows.Forms.ComboBox var21Module;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.ComboBox var20Module;
        public System.Windows.Forms.ComboBox var19Module;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbModule;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.DataGridView dgvData;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox cbFlag;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox textPwd;
        public System.Windows.Forms.Label label57;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.TextBox txtFolder;
        public System.Windows.Forms.Label label56;
        public System.Windows.Forms.DateTimePicker dtpLastedValue;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.TextBox txtUsername;
        public System.Windows.Forms.TextBox txtIP;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.TextBox var35ErrorMax;
        public System.Windows.Forms.TextBox var20ErrorMin;
        public System.Windows.Forms.TextBox var34ErrorMax;
        public System.Windows.Forms.TextBox var35ErrorMin;
        public System.Windows.Forms.TextBox var33ErrorMax;
        public System.Windows.Forms.TextBox var34ErrorMin;
        public System.Windows.Forms.TextBox var32ErrorMax;
        public System.Windows.Forms.TextBox var33ErrorMin;
        public System.Windows.Forms.TextBox var31ErrorMax;
        public System.Windows.Forms.TextBox var32ErrorMin;
        public System.Windows.Forms.TextBox var30ErrorMax;
        public System.Windows.Forms.TextBox var31ErrorMin;
        public System.Windows.Forms.TextBox var29ErrorMax;
        public System.Windows.Forms.TextBox var30ErrorMin;
        public System.Windows.Forms.TextBox var28ErrorMax;
        public System.Windows.Forms.TextBox var29ErrorMin;
        public System.Windows.Forms.TextBox var27ErrorMax;
        public System.Windows.Forms.TextBox var28ErrorMin;
        public System.Windows.Forms.TextBox var26ErrorMax;
        public System.Windows.Forms.TextBox var27ErrorMin;
        public System.Windows.Forms.TextBox var25ErrorMax;
        public System.Windows.Forms.TextBox var26ErrorMin;
        public System.Windows.Forms.TextBox var24ErrorMax;
        public System.Windows.Forms.TextBox var25ErrorMin;
        public System.Windows.Forms.TextBox var23ErrorMax;
        public System.Windows.Forms.TextBox var24ErrorMin;
        public System.Windows.Forms.TextBox var22ErrorMax;
        public System.Windows.Forms.TextBox var23ErrorMin;
        public System.Windows.Forms.TextBox var21ErrorMax;
        public System.Windows.Forms.TextBox var22ErrorMin;
        public System.Windows.Forms.TextBox var20ErrorMax;
        public System.Windows.Forms.TextBox var21ErrorMin;
        public System.Windows.Forms.TextBox var19ErrorMax;
        public System.Windows.Forms.TextBox var19ErrorMin;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.TextBox var18ErrorMax;
        public System.Windows.Forms.TextBox var18ErrorMin;
        public System.Windows.Forms.TextBox var17ErrorMax;
        public System.Windows.Forms.TextBox var17ErrorMin;
        public System.Windows.Forms.TextBox var16ErrorMax;
        public System.Windows.Forms.TextBox var16ErrorMin;
        public System.Windows.Forms.TextBox var15ErrorMax;
        public System.Windows.Forms.TextBox var15ErrorMin;
        public System.Windows.Forms.TextBox var14ErrorMax;
        public System.Windows.Forms.TextBox var14ErrorMin;
        public System.Windows.Forms.TextBox var13ErrorMax;
        public System.Windows.Forms.TextBox var13ErrorMin;
        public System.Windows.Forms.TextBox var12ErrorMax;
        public System.Windows.Forms.TextBox var12ErrorMin;
        public System.Windows.Forms.TextBox var11ErrorMax;
        public System.Windows.Forms.TextBox var11ErrorMin;
        public System.Windows.Forms.TextBox var10ErrorMax;
        public System.Windows.Forms.TextBox var10ErrorMin;
        public System.Windows.Forms.TextBox var9ErrorMax;
        public System.Windows.Forms.TextBox var9ErrorMin;
        public System.Windows.Forms.TextBox var8ErrorMax;
        public System.Windows.Forms.TextBox var8ErrorMin;
        public System.Windows.Forms.TextBox var7ErrorMax;
        public System.Windows.Forms.TextBox var7ErrorMin;
        public System.Windows.Forms.TextBox var6ErrorMax;
        public System.Windows.Forms.TextBox var6ErrorMin;
        public System.Windows.Forms.TextBox var5ErrorMax;
        public System.Windows.Forms.TextBox var5ErrorMin;
        public System.Windows.Forms.TextBox var4ErrorMax;
        public System.Windows.Forms.TextBox var4ErrorMin;
        public System.Windows.Forms.TextBox var3ErrorMax;
        public System.Windows.Forms.TextBox var3ErrorMin;
        public System.Windows.Forms.TextBox var2ErrorMax;
        public System.Windows.Forms.TextBox var2ErrorMin;
        public System.Windows.Forms.TextBox var1ErrorMax;
        public System.Windows.Forms.TextBox var1ErrorMin;
    }
}